---------------------------
|  Directory description  |
---------------------------

├── builddir                    [ Directory automatically generated. It contains build intermediate files]
├── runtime                     [ Translator runtime : tools allowing to convert PharmML -> mlxtran/mlxproject, mlxporject -> PharmML]
├── doc
├── setup                       [ setup directory contains setup command to download mandatory component from Lixoft web site]
├── lib                         [ External libraries used by the converter : Qt, non opensource mlxtran parser ]
│   ├── linux64
│   └── win64
├── scripts                     [ Contains build scripts ]
├── sources                     [ Source code that are opened ]
│   ├── mlxToSO                 [ Perl script converting Monolix Ouput to Standard Output]
│   ├── see-plugin              [ Monolix Plugin source code used by SEE (Mango) ]
│   └── translator              [ C++ code source of translator (consersion PharmML -> mlxtran/mlxproject, mlxporject -> PharmML]
└── tools                       [ External tools ]
    ├── 7-Zip-w64               [ 7 zip, used to generate plugin package ]
    ├── apache-ant-1.9.7        [ Ant build tools for plugin ]
    ├── apache-ant-1.9.7        [ Ant build tools for plugin ]
    ├── jdk1.7.0_75-l64         [ Java (JDK) for Linux ]
    ├── jdk1.7.0_75-w64         [ Java (JDK) for Windows ]
    ├── monolix-see-plugin-l64  [ SEE plugin : delivery for Mango SEE (Linux)]
    └── monolix-see-plugin-w64  [ SEE plugin : delivery for Mango SEE (Windows)]
    ├── mlxlibrary-l64          [ Mlxlibrary for Linux ]
    └── mlxlibrary-w64          [ Mlxlibrary for Windows ]


----------------------------
|  Build Under Linux (x64) |
----------------------------

*** Prerequists ***
- g++/gcc (>= 4.4)
- cmake (>= 2.8.4)
- p7z-full

*** Build ***
- go into 'scripts' directory and execute build.sh
- this command create monolix-plugin.zip and mlxlirbary.zip (located into tools directory)

----------------------------------
|  Build Under Windows OS (x64)  |
----------------------------------

*** Prerequists ***
- Visual studio 2010
- cmake (>= 2.8.4)

*** Build ****
- go into directory 'scripts' and execute build.bat
- this command create monolix-plugin.zip and mlxlibrary.zip (located into tools directory)


-------------
|  Cleanup  |
-------------

to clean directories you can execute 'clean.sh' under Linux OS and 'clean.bat' under Windows OS


-----------------------------
|  Build chain description  |
-----------------------------
The setup command downloads libraries and tools needs by build process. The build process is 
launched by the command 'build.bat' (Windows OS) or 'build.sh' (Linux OS) and execute the
following task :
- create build and runtime temporary directories
- patch qt.conf into lib to setup qt path
- execute cmake
- make lixofLanguageTransltor (the tool that converts the PharmML file into Monolix Project or Mlxlibrary
model and Lixoft scripts to PharmML)
- copy the created tool into Monolix plugin and Mlxlibrary tool directory
- generate Monolix plugin by using the ant command and jdk version 1.7
- finally, zip plugin and Mlxlibrary folder


