--------------
| MlxLibrary |
--------------

The folder tools/mlxlibrary-w64 and tools/mlxlibrary-l64 contains Mlxlibray tool box
When the converter is build the translator (consersion PharmML -> mlxtran/mlxproject, mlxproject -> PharmML)
is automatically copied into the 'lib' directory of Mlxlibrary.
