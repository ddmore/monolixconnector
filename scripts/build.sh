#!/bin/sh

# Mr Proper
./clean.sh
cd ..

# Create build environment
mkdir builddir
mkdir -p runtime/lib
root_path=`pwd`
cd builddir
echo "[Paths]" > $root_path/lib/linux64/qt-4.8.5-gcc4.6.3x64-release/bin/qt.conf

# build translator
echo "Prefix=$root_path/lib/linux64/qt-4.8.5-gcc4.6.3x64-release" >> $root_path/lib/linux64/qt-4.8.5-gcc4.6.3x64-release/bin/qt.conf
cmake -DMLX_QT_VERSION=4.8.5 \
      -DQT_DIR=$root_path/lib/linux64/qt-4.8.5-gcc4.6.3x64-release/ \
      -DCOMPILER_CXX=/usr/bin/g++-4.6 \
      -DCOMPILER_CC=/usr/bin/gcc-4.6 \
      -DRUNTIME_PATH=$root_path/runtime \
      ../sources/translator
make && make install

# copy translator to plugin & mlxlibrary
cp -r $root_path/runtime/* $root_path/sources/see-plugin/src/META-INF/lixofttools/linux64/
cp -r $root_path/runtime/lib/* $root_path/tools/mlxlibrary-l64/lib/
cd $root_path/sources/see-plugin/

# build plugin
#ant

#cp $root_path/sources/see-plugin/dist/*.jar $root_path/tools/monolix-see-plugin-l64/converter-toolbox-distribution/lib/
#cd $root_path/tools/monolix-see-plugin-l64
#cp -r $root_path/sources/mlxToSO/* $root_path/tools//monolix-see-plugin-l64/Monolix432s/bin/
#7z a ../monolix-plugin.zip *
#cd $root_path/tools/
#7z a mlxlibrary.zip mlxlibrary-l64
