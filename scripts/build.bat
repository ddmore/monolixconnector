@rem prepare build environment variables
@set script_path=%~dp0
@if %script_path:~-1%==\ SET script_path=%script_path:~0,-1%
@for %%D in (%script_path%) do SET script_path_DRIVE=%%~dD
@echo Script directory = %script_path%
@echo Script drive = %script_path_DRIVE%
%script_path_DRIVE%
@cd %script_path%\..
@set root_path=%cd%
@echo ROOT PATH %root_path%
@cd %root_path%
@md builddir
@md runtime
@md runtime\lib

@rem generate qt.conf
@set qt_path=%root_path%\lib\win64\qt-4.8.5-msvc2010x64
@set qt_path=%qt_path:\=/%
@echo [Paths]>%root_path%\lib\win64\qt-4.8.5-msvc2010x64\bin\qt.conf
@echo Prefix=%qt_path%/>>%root_path%\lib\win64\qt-4.8.5-msvc2010x64\bin\qt.conf

@rem build translator
@cd builddir
@cmake -G"Visual Studio 10 Win64" -DMLX_QT_VERSION=4.8.5 -DQT_DIR=%root_path%\lib\win64\qt-4.8.5-msvc2010x64 -DRUNTIME_PATH=%root_path%/runtime ../sources/translator
@cmake --build . --target install --config release

@rem copy translator to java plugin & mlxlibrary
@md %root_path%\sources\see-plugin\src\META-INF\lixofttools\win64\lib
@xcopy %root_path%\runtime\lib %root_path%\sources\see-plugin\src\META-INF\lixofttools\win64\lib /D /E /C /R /H /K /I /Y
@xcopy %root_path%\runtime\lib %root_path%\tools\mlxlibrary-w64\lib /D /E /C /R /H /K /I /Y

@rem build plugin
@xcopy %root_path%\sources\mlxToSO %root_path%\tools\monolix-see-plugin-w64\Monolix432s\bin\ /R  /Y
@xcopy %root_path%\sources\mlxToSO\lixoft %root_path%\tools\monolix-see-plugin-w64\Monolix432s\bin\lixoft /R  /Y

@cd %root_path%\sources\see-plugin\
set JAVA_HOME=%root_path%\tools\jdk1.7.0_75-w64\
CALL %root_path%\tools\apache-ant-1.9.7\bin\ant -Dplatforms.JDK_1.7.home=%JAVA_HOME% -Dlibs.ddmoretoolbox.classpath=%root_path%\sources\see-plugin\libs\converter-toolbox-api-1.0.1.jar
@copy %root_path%\sources\see-plugin\dist\*.jar %root_path%\tools\monolix-see-plugin-w64\converter-toolbox-distribution\lib\


@rem generate plugin zip
cd %root_path%\tools\monolix-see-plugin-w64
CALL ..\7-Zip-w64\7z.exe a ..\monolix-plugin.zip *
cd %root_path%\tools\
CALL 7-Zip-w64\7z.exe a mlxlibrary.zip mlxlibrary-w64

@rem return to initial path
@cd %script_path% 
