#!/bin/sh

current_path=`dirname $0`
cd $current_path
scripts_path=`pwd`

rm -rf $scripts_path/../output-tests
mkdir -p $scripts_path/../output-tests/mlx
cd $scripts_path/../output-tests/mlx
output_test_path=`pwd`

monolixcmd=$scripts_path/../tools/monolix-see-plugin-l64/Monolix432s/Monolix432grid/bin/Monolix.sh

cd $scripts_path
cd ../sources/see-plugin/src/META-INF/lixofttools/linux64/lib
tool_path=`pwd`

cd $scripts_path/../testsuite/pharmml/0.8.1
xml081test_path=`pwd`
mkdir $output_test_path/ptest



for p in *.xml; do
 pbasename=`echo $p | sed -e 's/.xml//'`
 projectname=$pbasename/$pbasename\_project.mlxtran
 projectdir=$pbasename/$pbasename\_project
 $tool_path/lixoftLanguageTranslator --from=pharmml --to=mlxproject --input-file=$xml081test_path/$p --output-dir=$output_test_path/ptest 2>$output_test_path/ptest/$p.err 1>$output_test_path/ptest/$p.out
 $monolixcmd -p $output_test_path/ptest/$projectname -f run 1>>$output_test_path/ptest/converter.out 2>>$output_test_path/ptest/converter.err
 cd $scripts_path/../sources/mlxToSO
 perl mlxToSO.pl --config=config.ini --result-directory=$output_test_path/ptest/$projectdir  --project=$pbasename.xml --output-directory=$output_test_path/ptest/$pbasename
 cd -
 cd $scripts_path/jtools
 java -jar SO-0.3-Validator.jar $output_test_path/ptest/$pbasename/$pbasename.SO.xml 2> $output_test_path/ptest/$pbasename/$pbasename.SO.err 1>$output_test_path/ptest/$pbasename/$pbasename.SO.out
 cd -
done 
