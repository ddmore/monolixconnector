@set script_path=%~dp0
@if %script_path:~-1%==\ SET script_path=%script_path:~0,-1%
@for %%D in (%script_path%) do SET script_path_DRIVE=%%~dD
@echo Script directory = %script_path%
@echo Script drive = %script_path_DRIVE%
%script_path_DRIVE%
@cd %script_path%\..
@set root_path=%cd%
@cd %root_path%

@rmdir /s /q %root_path%\builddir
@rmdir /s /q %root_path%\runtime
@rmdir /s /q %root_path%\sources\see-plugin\dist\*
@rmdir /s /q %root_path%\sources\see-plugin\build\*
@rmdir /s /q %root_path%\sources\see-plugin\src\META-INF\lixofttools\win64\*
@del %root_path%\tools\monolix-plugin.zip
@del %root_path%\tools\mlxlibrary.zip
@del %root_path%\tools\monolix-see-plugin-w64\converter-toolbox-distribution\lib\*.jar

@cd %script_path%