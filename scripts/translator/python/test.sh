runtime=/home/raphael/workspace/mlx432/work/lixoftlanguagetranslators-release/runtime
validatorJar=/home/raphael/workspace/mlx432/tests/translator/ddmore-libpharmml-validator/PharmML-0.8-validator.jar

rm -rf output 
cp -r ../resources  output 

python playAllTranslator.py  --output output --input  output --input-extension .mlxtran --runtime $runtime  --arguments '--from=mlxproject --to=pharmml' --thread 6 --output-extension .xml

python playAllTranslator.py  --output output --input output --input-extension _model.txt --runtime $runtime  --arguments '--from=mlxproject --to=pharmml' --thread 6 --output-extension .xml --log-file output/runOldV2.log
cat output/runOldV2.log>> output/playAllTranslator.log

./validation.sh $validatorJar

./pharmml2mlxtran.sh $runtime

if ! [ -d logDir ] ; then
 mkdir logDir
fi
cp output/playAllTranslator.log logDir/playAllMlxtran2pharmml.log

cp output/playAllPharmml2mlxtran.log logDir/

cp output/validator.log logDir/pharmmlValidator.log

gedit logDir/playAllMlxtran2pharmml.log logDir/playAllPharmml2mlxtran.log logDir/pharmmlValidator.log &

