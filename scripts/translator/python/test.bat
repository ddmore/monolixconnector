@set runtime=C:\Users\lixoft\workspace\ddmorewp5\runtime
@set validatorJar=C:\Users\lixoft\workspace\pharmmlValidator\PharmML-0.8-validator.jar

@rd /S/Q output 
@xcopy  /S /I /E ..\resources  output  /D  /C /R /H /K  /Y /Q
@CALL python playAllTranslator.py  --output output --input  output --input-extension .mlxtran --runtime %runtime%  --arguments "--from=mlxproject --to=pharmml" --thread 6 --output-extension .xml
@CALL python playAllTranslator.py  --output output --input output --input-extension _model.txt --runtime %runtime%  --arguments "--from=mlxproject --to=pharmml" --thread 6 --output-extension .xml --log-file output/runOldV2.log
@CALL cat output/runOldV2.log>> output/playAllTranslator.log
@CALL validation.bat %validatorJar%
@CALL pharmml2mlxtran.bat %runtime%

@md  logDir

@copy output\playAllTranslator.log logDir\playAllMlxtran2pharmml.log 

@copy output\playAllPharmml2mlxtran.log logDir\ 

@copy  output\validator.log logDir\pharmmlValidator.log 

:notepad logDir\playAllMlxtran2pharmml.log logDir\playAllPharmml2mlxtran.log logDir\pharmmlValidator.log 

