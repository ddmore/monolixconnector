#!/bin/sh

current_path=`dirname $0`
cd $current_path
scripts_path=`pwd`

cd ../runtime
runtime=`pwd`
cd $scripts_path/jtools
validatorJar=`pwd`/PharmML-0.8-validator.jar

cd $scripts_path/pytools
pytools=`pwd`

rm -rf $scripts_path/../output-tests
mkdir -p $scripts_path/../output-tests/pharmml
cd $scripts_path/../output-tests/pharmml
output_test_path=`pwd`

cp -r $scripts_path/../testsuite/mlxtran $output_test_path

python $pytools/playAllTranslator.py  --output $output_test_path --input  $output_test_path --input-extension .mlxtran --runtime $runtime  --arguments '--from=mlxproject --to=pharmml' --thread 6 --output-extension .xml

python $pytools/playAllTranslator.py  --output $output_test_path --input $output_test_path --input-extension _model.txt --runtime $runtime  --arguments '--from=mlxproject --to=pharmml' --thread 6 --output-extension .xml --log-file $output_test_path/runOldV2.log

#cat output/runOldV2.log>> output/playAllTranslator.log

rm -rf  $output_test_path/validator.log ; find   $output_test_path  |grep  xml$|xargs -i java -jar $validatorJar {}>> $output_test_path/validator.log 2>&1

python $pytools/playAllPharmml2mlxtran.py  --input $output_test_path --input-extension .xml --runtime $runtime --arguments '--pharmm-version=0.8 --from=pharmml --to=mlxproject' --thread 6 

exit;

if ! [ -d logDir ] ; then
 mkdir logDir
fi
cp output/playAllTranslator.log logDir/playAllMlxtran2pharmml.log

cp output/playAllPharmml2mlxtran.log logDir/

cp output/validator.log logDir/pharmmlValidator.log

#gedit logDir/playAllMlxtran2pharmml.log logDir/playAllPharmml2mlxtran.log logDir/pharmmlValidator.log &

