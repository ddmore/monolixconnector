#!/bin/bash

cd `dirname $0`/..
root_path=`pwd`

rm -rf $root_path/builddir
rm -rf $root_path/runtime
rm -rf $root_path/output-tests
rm -rf $root_path/sources/see-plugin/dist/*
rm -rf $root_path/sources/see-plugin/build/*
rm -rf $root_path/sources/see-plugin/src/META-INF/lixofttools/linux64/*
rm -rf $root_path/tools/monolix-plugin.zip
rm -rf $root_path/tools/mlxlibrary.zip
rm -rf $root_path/tools/monolix-see-plugin-l64/converter-toolbox-distribution/lib/*.jar
