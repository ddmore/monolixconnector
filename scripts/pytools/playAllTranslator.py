#! /usr/bin/python
# -*- coding: UTF-8 -*-

import os
import sys
import shutil
import multiprocessing


currentDir = os.getcwd()
sys.path.append(os.path.abspath(currentDir+"/../../"))
from RoutinesNightlyBuild import *
import argparse
## get script call arguments
inargs = argparse.ArgumentParser(description='convert  all mlxtran  founded in inputdir'
                                             ' into xmlx using language translator')

inargs.add_argument('--output', dest='output', help='output directory for xmlx result',
                    metavar='path',required=True)
inargs.add_argument('--input', dest='input', help='input directory mlxtran files',
                    metavar='path',required=True)
inargs.add_argument('--output-extension', dest='outExtension', help='extension to append on input file name, for output files',
                    metavar='char', default=".xml")
inargs.add_argument('--log-extension', dest='logExtension', help='extensionto append on input file name for output log files',
                    metavar='char', default="_2pharmml.log")
inargs.add_argument('--input-extension', dest='extension', help='extension of files where  mlxtran code must be founded',
                    metavar='path',default='.mlxtran')
inargs.add_argument('--runtime', dest='runtimePath', help='runtime path' ,metavar='path',required=True)
#inargs.add_argument('--machine-name', dest='machineName', help='machine name' ,metavar='string',required=True)
inargs.add_argument('--thread', dest='thread', help='number of threads' ,metavar='N',default="1")
inargs.add_argument('--log-file', dest='logFile', help='log file' ,metavar='string',default="")
inargs.add_argument('--arguments', dest='arguments', help='arguments of lixoftLanguageTranslator' ,
                    metavar='string',default=' --from=mlxproject --to=xmlx')
args = inargs.parse_args()

runtimePath = os.path.abspath(args.runtimePath)
outputDir = os.path.abspath(args.output)
inputDir = os.path.abspath(args.input)
#machineName = args.machineName
thread = int(args.thread)
extension = args.extension
logFile = args.logFile
logExtension = args.logExtension
outExtension = args.outExtension

if logFile =="":
    logFile = outputDir+"/playAllTranslator.log"

outLogFile = os.path.abspath(logFile)
arguments = args.arguments
import sys
outLogFile = unixpath(outLogFile)
outputDir = unixpath(outputDir)
inputDir = unixpath(inputDir)

## copy testFolder to output

if not os.path.exists(outputDir):
    os.mkdir(outputDir)

outStream = open(outLogFile,'w')
outStream.close()
translatorExe = runtimePath+"/lib/lixoftLanguageTranslator"
if sys.platform.lower() ==  "win32":
    translatorExe = runtimePath+"/lib/lixoftLanguageTranslator.exe"
translatorExe = unixpath(translatorExe)

if not  os.path.isfile(translatorExe):
    quit("Error!!!\n  file '"+ translatorExe+ "' not found")

def CallTranslator(infile,OutLogFile):
    if sys.platform.lower() ==  "win32zefek":
        print("not  yed coded")
    else:
        
        base = os.path.basename(infile)
        outdir = outputDir+"/" +os.path.dirname(infile.replace(inputDir,""))
        outdir = unixpath(outdir)
        outfileBase =  outdir +"/"+os.path.splitext(base)[0]
        outfile = outfileBase +outExtension 
        outfile = unixpath(outfile)		
        if not os.path.exists(outdir):
            os.makedirs(outdir)        
        logfile = outfileBase+ logExtension
        outW=open(logfile,'w')
        outW.write("\n**##** converting "+infile+"\n")
        outW.close()
        script = (translatorExe+ "  "+  arguments + " --input-file="+unixpath(infile)+"  --output-file="
                  +outfile+" >>" +logfile.replace("\\","/"))+" 2>&1"
           				  
        tryScript(script)        	
        catLogFile2OutFile(logfile, OutLogFile)
    return 

## find smlxtran files
mlxtranFiles =  findFiles(inputDir,"*"+extension)

from multiprocessing.pool import ThreadPool as Pool

if __name__ == '__main__':
    pool = Pool(thread)

    for fname in mlxtranFiles:
        pool.apply_async(CallTranslator, args=(fname,outLogFile))
    pool.close()
    pool.join()

#logFiles = findFiles(outputDir,"*.xml.log")
#catLogFiles2OutFile(logFiles,outLogFile)
