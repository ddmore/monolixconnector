
import os
import sys
import fnmatch
import shutil

def buildScript(configFile,outputDir,machineName):
	errFile = outputDir + "/build"+machineName+".log"
	redirection =  "  >" +errFile +" 2>&1"
	script = "perl builder.pl --config=" + configFile  + " --errorDir="+outputDir + redirection
	buildError =  tryScript(script)
	return buildError

def packagingScript(configFile,runtimePath,matlabPath,packageOutputLocation,errFile):

	redirection =  "  >" +errFile +" 2>&1"
	script = ("perl packsuite.pl --matlab-path=" + matlabPath + " --runtime-path=" + runtimePath + " --output-directory="
				+ packageOutputLocation + redirection)
	packError =  tryScript(script)
	return packError

def status(error):
    
	if error > 0:
		state ="KO"
	else:
		state ="OK"
	return state

def   tryScript(script):
	try:	
		result = os.system(script)
	except:
		strErr = "\n ***** the following script failed: \n  "+ script + "\n ***** \n"
		os.sys.stdout.write(strErr)
	return  result

def catScript(logfile,outfile):
    if  sys.platform.lower() == "win32":
        catscript = "type "+ os.path.abspath(logfile) +">>" + os.path.abspath(outfile)
    else:
        catscript = "cat "+ os.path.abspath(logfile) +">>" + os.path.abspath(outfile)

    return catscript

def catLogFiles2OutFile(logFiles, outfile):
	for logfile in logFiles:
		catscript = catScript(logfile,outfile)
		tryScript(catscript)
	return

def catLogFile2OutFile(logFiles, outfile):
	tryScript(catScript(logFiles,outfile))
	return

def findFiles(Dir,pattern):
	listFiles = []
	for root, dirs, files in os.walk(Dir):
		for filename in fnmatch.filter(files, pattern):
			listFiles.append(os.path.join(root, filename))
	return listFiles

def removeFiles(Dir,pattern):	
	for root, dirs, files in os.walk(Dir):
		for filename in fnmatch.filter(files, pattern):
			os.remove(os.path.join(root, filename))
	return 

def removeDirs(Dir,pattern):	
	for root, dirs, files in os.walk(Dir):
		for filename in fnmatch.filter(files, pattern):
			shutil.rmtree(root)
			break
	return 

#modified vesrion of shutils to preserve contents of dst not pressent in src
from  shutil import copy,copy2,Error,copystat
def copytree2(src, dst, symlinks=False):
	names = os.listdir(src)
	if not os.path.exists(dst):
		os.makedirs(dst)
	errors = []
	for name in names:
		srcname = os.path.join(src, name)
		dstname = os.path.join(dst, name)
		try:
			if symlinks and os.path.islink(srcname):
				linkto = os.readlink(srcname)
				os.symlink(linkto, dstname)
			elif os.path.isdir(srcname):
				copytree2(srcname, dstname, symlinks)
			else:
				copy2(srcname, dstname)
			# XXX What about devices, sockets etc.?
		except OSError as why:
			errors.append((srcname, dstname, str(why)))
		# catch the Error from the recursive copytree so that we can
		# continue with other files
		except Error as err:
			errors.extend(err.args[0])
	try:
		copystat(src, dst)
	except OSError as why:
		# can't copy file access times on Windows
		if why.winerror is None:
			errors.extend((src, dst, str(why)))
	if errors:
			raise Error(errors)

def unixpath(path):
	pathout = path.replace('\\','/')
	pathout = pathout.replace('//','/')
	return pathout


def catAllLogFiles(sourceFiles,extension,outfile):
    for script in sourceFiles:
        base = os.path.basename(script)
        logfile = os.path.dirname(script)+"/" +os.path.splitext(base)[0]+ extension
        print(logfile)		
        if os.path.isfile(logfile):
            catLogFile2OutFile(logfile, outfile)
