#! /usr/bin/python
# -*- coding: UTF-8 -*-

import os
import sys
import shutil
import multiprocessing


currentDir = os.getcwd()
sys.path.append(os.path.abspath(currentDir+"/../../"))
from RoutinesNightlyBuild import *
import argparse
## get script call arguments
inargs = argparse.ArgumentParser(description='convert  all mlxtran  founded in inputdir'
                                             ' into xmlx using language translator')


inargs.add_argument('--input', dest='input', help='input directory mlxtran files',
                    metavar='path',required=True)
inargs.add_argument('--input-extension', dest='extension', help='extension of files where  mlxtran code must be founded',
                    metavar='path',default='.mlxtran')
inargs.add_argument('--runtime', dest='runtimePath', help='runtime path' ,metavar='path',required=True)
inargs.add_argument('--thread', dest='thread', help='number of threads' ,metavar='N',default="1")
inargs.add_argument('--log-file', dest='logFile', help='log file' ,metavar='string',default="")
inargs.add_argument('--arguments', dest='arguments', help='arguments of lixoftLanguageTranslator' ,
                    metavar='string',default=' --from=mlxproject --to=xmlx')
args = inargs.parse_args()

runtimePath = os.path.abspath(args.runtimePath)
inputDir = os.path.abspath(args.input)
thread = int(args.thread)
extension = args.extension
logFile = args.logFile

if logFile =="":
    logFile = inputDir+"/playAllPharmml2mlxtran.log"

outLogFile = os.path.abspath(logFile)
arguments = args.arguments
import sys
outLogFile = unixpath(outLogFile)
inputDir = unixpath(inputDir)

outStream = open(outLogFile,'w')
outStream.close()
translatorExe = runtimePath+"/lib/lixoftLanguageTranslator"
if sys.platform.lower() ==  "win32":
    translatorExe = runtimePath+"/lib/lixoftLanguageTranslator.exe"
translatorExe = unixpath(translatorExe)

if not  os.path.isfile(translatorExe):
    quit("Error!!!\n  file '"+ translatorExe+ "' not found")

def CallTranslator(infile,OutLogFile):
    if sys.platform.lower() ==  "win32zefek":
        print("not  yed coded")
    else:
        
        base = os.path.basename(infile)        
        outdir = os.path.dirname(infile)
        outfileDir = outdir +"/"+os.path.splitext(base)[0]
        outfileDir = unixpath(outfileDir)     	
        logfile = outfileDir+"_2mlxtran.log"
        outW=open(logfile,'w')
        outW.write("\n**##** converting "+infile+"\n")
        outW.close()
        script = (translatorExe+ "  "+  arguments + " --input-file="+unixpath(infile)+"  --output-dir="
                  +outfileDir+" >>" +logfile.replace("\\","/"))+" 2>&1"                  				  
        tryScript(script)        	
        catLogFile2OutFile(logfile, OutLogFile)
    return 

## find smlxtran files
pharmmlFiles =  findFiles(inputDir,"*"+extension)


from multiprocessing.pool import ThreadPool as Pool

if __name__ == '__main__':
    pool = Pool(thread)

    for fname in pharmmlFiles:
        pool.apply_async(CallTranslator, args=(fname,outLogFile))
    pool.close()
    pool.join()


