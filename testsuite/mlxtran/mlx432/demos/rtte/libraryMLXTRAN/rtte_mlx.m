function ll = rtte_mlx( phi, Id, X )
%Structural  model rtte_mlx
%
% Repeated Time To Event model with constant hazard function

try
    phi;
catch
    ll=getdata;
    return
end

time = X.obs(:,1);
y=X.data;

HBASE = phi(:,1);
%##################################################
N=size(phi,1);
nbs=Id.Nbs(:,2);
j2=0;
ll=zeros(size(time));
for i=1:N
    j1=j2+1;
    j2=j2+nbs(i);
    t0=0;
    for j=j1:j2
        haz = HBASE(i)/365;
        tj=time(j);
        if y(j)>0
            ll(j)= log(haz) -haz*(tj-t0);
            t0=tj;
        else
            ll(j)= -haz*(tj-t0);
        end
    end
end

%####################################
%####################################
function model_info = getdata
%returns the information about this model

model_info=struct;

model_info.nb_param=1;% number of parameters "phi"
model_info.phi_names={'HBASE'};% name of the parameter
model_info.phi_tex  ={''};% tex name of the parameter for the plots
model_info.logstruct={'L'};% default distribution of the parameter

model_info.nb_varex=1;% number of regression variables
model_info.x_names={...
    'time'};% names of the regression variables
model_info.x_tex={...
    ''};% tex names of the regression variables for the plots

model_info.nb_outputs=1;% number of outputs
model_info.y_names={'Event'};% name of the output
model_info.y_tex={''};% tex name of the output for the plots

model_info.nb_ode=0;% number of differential equations
model_info.ode=0;% name of the output

model_info.iop_ode=0;% type of solver

model_info.dose=0;% 1 if the model needs dose information

model_info.cat_model='other';% type of model

model_info.desc='Repeated Time To Event model with constant hazard function';%description of the model

model_info.outtype={'event'};

