DESCRIPTION: RTTE with constant hazard function

INPUT:
parameter = Te  

OBSERVATION:
Event = {type=event, 
         eventType=intervalCensored, 
         maxEventNumber=1,
         intervalLength=5,     ; used for the graphics (not mandatory)
         rightCensoringTime=200,  ; used for the graphics (not mandatory)
         hazard=1/Te
}

OUTPUT:
output = Event
