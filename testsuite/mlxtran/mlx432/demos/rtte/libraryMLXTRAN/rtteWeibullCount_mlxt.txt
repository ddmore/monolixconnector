DESCRIPTION: RTTE - Weibull distribution
RTTE treated as count data 

INPUT:
parameter = {lambda, beta}  
regressor = tpe

OBSERVATION:
Event = {type=count, 
HAZ=(t/lambda)^beta - (tpe/lambda)^beta
if k>0
  haz=(beta/lambda)*(t/lambda)^(beta-1)
  lpk = log(haz) - HAZ
else
  lpk = - HAZ
end
log(P(Event=k)) = lpk
}

OUTPUT:
output = Event

