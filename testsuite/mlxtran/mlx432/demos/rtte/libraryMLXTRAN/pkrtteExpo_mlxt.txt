DESCRIPTION: PK + event 

INPUT:
parameter = {ka, V, Cl, gamma}  

EQUATION:
Cc = pkmodel(ka, V, Cl)

OBSERVATION:
Concentration = {type=continuous, prediction=Cc, errorModel=proportional}
Hemorrhaging  = {type=event, hazard=gamma*Cc}

OUTPUT:
output = {Concentration, Hemorrhaging}

