DESCRIPTION: First order oral absorption with a lag-time, and ordered categorical data

INPUT:
parameter = {Tlag, ka, V, Cl, th1, th2, th3}

PK:
Cc= pkmodel(Tlag,ka,V,Cl)

OBSERVATION:
Level = {
	type=categorical
	categories={1,2,3}
	logit(P(Level<=1)) = -th1 + th2*Cc
	logit(P(Level<=2)) = -th1 + th2*Cc + th3
}

OUTPUT:
output = {Cc, Level}
