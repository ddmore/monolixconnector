DESCRIPTION: Sequential PK/PD model.
Individual PK parameters have been estimated first, they are defined as regression variables.

INPUT:
parameter =  {Imax, C50, Rin, kout}
regressor =  {Tlag, ka, V, Cl}

EQUATION:
Cc = pkmodel(Tlag,ka,V,Cl)
E_0 = Rin/kout
ddt_E= Rin*(1-Imax*Cc/(Cc+C50)) - kout*E

OUTPUT:
output = E
