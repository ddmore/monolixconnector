DESCRIPTION: First order transit absorption with one compartment, and parameters: Mtt, Ktr, ka, V, Cl
Illustrates implicit formulas for absorption keywords Mtt, Ktr, and ka.

INPUT:
parameter = {Mtt, Ktr, ka, V, Cl}

EQUATION:
nbTrCpts = Mtt*Ktr ;Continuous "number" of transit compartments.
N = nbTrCpts - 1
elapsed = Ktr*(t-tDose)
k = Cl/V

if amtDose > 0
  transitFormula = amtDose*Ktr*exp(N*log(elapsed) - elapsed - gammaln(nbTrCpts)) - ka*Ad
else
  transitFormula = -ka*Ad
end

ddt_Ad = transitFormula
ddt_Ac = ka*Ad - k*Ac
Cc = Ac/V

OUTPUT:
output = Cc


;Function gammaln computes the logarithm of the gamma function.

;In file oral1_1cpt_MttKtrkaVCl_mlxt, absorption is defined using
;the keywords Mtt, Ktr, and ka instead of modifying the ODE.

