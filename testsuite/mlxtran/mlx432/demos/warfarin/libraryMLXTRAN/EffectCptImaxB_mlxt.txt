DESCRIPTION: PK model + effect cpt + Imax model

INPUT:
parameter =  {Tlag, ka, V, Cl, ke0, Imax, C50, S0}

EQUATION:
k = Cl/V
p1 = amtDose*ka/(V*(ka-k))
tl = max(t-Tlag-tDose,0)
Cc = p1*(exp(-k*tl) - exp(-ka*tl))

ddt_Ce = -ke0*(Ce-Cc)

PCA= S0*(1-Imax*Ce/(Ce+C50)) 

OUTPUT:
output = {Cc, PCA}
