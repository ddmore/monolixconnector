DESCRIPTION: Turn over model for single dose

INPUT:
parameter =  {Tlag, ka, V, Cl, Imax, C50, Rin, kout}

EQUATION:
k = Cl/V
p1 = amtDose*ka/(V*(ka-k))
tl = max(t-Tlag-tDose,0)
Cc = p1*(exp(-k*tl) - exp(-ka*tl))
E_0 = Rin/kout
ddt_E= Rin*(1-Imax*Cc/(Cc+C50)) - kout*E

OUTPUT:
output = {Cc, E}
