DESCRIPTION: 
PK model, first order absorption with one compartment
Version 2 using the PK macros

INPUT:
parameter = {ka, V, Cl}

PK:
compartment(amount=Ac)
absorption(ka)
elimination(k=Cl/V) 
Cc=Ac/V

OUTPUT:
output = Cc

