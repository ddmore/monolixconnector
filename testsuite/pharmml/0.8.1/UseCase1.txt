

[INDIVIDUAL]
input={PPV_CL,POP_V,POP_KA,POP_CL,BETA_V_WT,PPV_TLAG,PPV_V,POP_TLAG,logtWT,BETA_CL_WT,PPV_KA}

DEFINITION:
CL = { distribution = normal, transformation = log, reference = POP_CL, covariate={logtWT}, coefficient={BETA_CL_WT},sd={PPV_CL}}
V = { distribution = normal, transformation = log, reference = POP_V, covariate={logtWT}, coefficient={BETA_V_WT},sd={PPV_V}}
KA = { distribution = normal, transformation = log, reference = POP_KA, sd={PPV_KA}}
TLAG = { distribution = normal, transformation = log, reference = POP_TLAG, sd={PPV_TLAG}}
correlation={r(CL,V)=0.01}


[LONGITUDINAL]
input={RUV_PROP,V,KA,CL,RUV_ADD,TLAG}

EQUATION:
if (t)>=(TLAG)
RATEIN=(GUT)*(KA)
else
RATEIN=0
end
GUT_0 = 0
CENTRAL_0 = 0
CC = (CENTRAL)/(V)
ddt_GUT = -(RATEIN)
ddt_CENTRAL = (RATEIN)-(((CL)*(CENTRAL))/(V))

DEFINITION:
Y = { distribution = normal, prediction = CC,  errorModel=combined1(RUV_ADD, RUV_PROP)}

