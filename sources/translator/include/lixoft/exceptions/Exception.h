#ifndef __MLX_EXCEPTION_H__
#define __MLX_EXCEPTION_H__

#include <string>

#if (defined(_WIN32) || defined(WIN32))
#define __PRETTY_FUNCTION__ __FUNCTION__
#endif

#define THROW_FUNCTION      lixoft::exception::Exception::source(__PRETTY_FUNCTION__)
#define THROW_LINE_FILE     __LINE__,__FILE__

namespace lixoft {
namespace exception {

    /*!
        \brief Monolix Exception class
    */
    class Exception : public std::exception
    {
        public:
            Exception() throw();
            Exception(const Exception& cpy) throw();
            Exception(const std::string& source,
                      const std::string& message,
                      int line = -1,
                      const char* file = NULL) throw();
            virtual ~Exception() throw();
            // what from std::exception
            virtual const char* what() const throw();
            // return the source of the message, i.e. the class/function name of the 'thrower'
            const std::string& source() const;
            // return the message related to the error
            const std::string& message() const;
            // return the line of the message
            int line() const;
            const std::string& file() const;
            void display(std::string& displayOut) const;
            std::string displayAll() const;
            std::string displayText() const;
            static std::string source(const std::string& functionDecl);

        protected:

            std::string _source;
            std::string _message;
            int         _line;
            std::string _file;

    };
}
} // namespace lixoft

/**** Helpers ****/

#define MLX_EXCEPTION_DECL_GEN(Api, Name, Base)                                                                                             \
class Api Name : public Base                                                                                                                \
{                                                                                                                                           \
                                                                                                                                            \
public:                                                                                                                                     \
                                                                                                                                            \
  Name(const std::string& source, const std::string& message, int line = -1, const char* file = NULL) throw();                              \
                                                                                                                                            \
  virtual ~Name() throw();                                                                                                                  \
                                                                                                                                            \
  Name(const Name& model) throw();                                                                                                          \
                                                                                                                                            \
  Name& operator=(const Name& input) throw();                                                                                               \
                                                                                                                                            \
  virtual const char* what() const throw();                                                                                                 \
                                                                                                                                            \
};

#define MLX_EXCEPTION_DECL(NamePrefix, BasePrefix) MLX_EXCEPTION_DECL_GEN(lixoftCore_API, NamePrefix ## Exception, BasePrefix ## Exception)

#define MLX_EXCEPTION_IMPL_GEN(Name, Base)                                                                                                  \
                                                                                                                                            \
Name::Name(const std::string& source, const std::string& message, int line, const char* file) throw():                                      \
Base(source, message, line, file)                                                                                                           \
{}                                                                                                                                          \
                                                                                                                                            \
Name::~Name() throw()                                                                                                                       \
{}                                                                                                                                          \
                                                                                                                                            \
Name::Name(const Name& model) throw():                                                                                                      \
Base(model._source, model._message, model._line, model._file.c_str())                                                                       \
{}                                                                                                                                          \
                                                                                                                                            \
Name& Name::operator=(const Name& input) throw()                                                                                            \
{                                                                                                                                           \
  if (this != &input)                                                                                                                       \
  {                                                                                                                                         \
    Base::operator=(input);                                                                                                                 \
  }                                                                                                                                         \
                                                                                                                                            \
  return *this;                                                                                                                             \
}                                                                                                                                           \
                                                                                                                                            \
const char* Name::what() const throw()                                                                                                      \
{                                                                                                                                           \
  return #Name;                                                                                                                             \
}

#define MLX_EXCEPTION_IMPL(NamePrefix, BasePrefix) MLX_EXCEPTION_IMPL_GEN(NamePrefix ## Exception, BasePrefix ## Exception)

#endif // __MLX_EXCEPTION_H__
