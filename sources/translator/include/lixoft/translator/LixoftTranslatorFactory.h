#ifndef __LIXOFT_TRANSLATOR_FACTORY_H__
#define __LIXOFT_TRANSLATOR_FACTORY_H__

#include <QSharedPointer>
#include <QString>

#include "lixoft/translator/lixoftTranslators.h"
#include "lixoft/translator/LanguageTranslator.h"

namespace lixoft {
namespace translator {

class LixoftTranslatorFactory : public LixoftObject
{
    public:
        static QSharedPointer<LanguageTranslator> getTranslator(const QString& fromLanguage,
                                                                const QString& toLanguage,
                                                                const QString& sourceFile,
                                                                const QString& output,                                                                
                                                                const QVector<QPair<QString,QString> >& options,
                                                                int outputFormat,
                                                                const QString& pharmmlversion);

};

}
}

#endif
