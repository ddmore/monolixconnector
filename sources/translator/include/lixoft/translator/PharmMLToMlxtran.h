#ifndef __PHARMML_TO_MLXTRAN_H__
#define __PHARMML_TO_MLXTRAN_H__

#include <QTextStream>

#include "lixoft/translator/PharmMLLoader.h"

namespace lixoft {
namespace translator {

class PharmMLToMlxtran : public PharmMLLoader
{
  public:
    PharmMLToMlxtran(const QString& sourceFile,
                     const QString& output,
                     const QVector<QPair<QString,QString> >& options,
                     int outputFormat);
    virtual ~PharmMLToMlxtran();

    virtual void checkOptions();
    virtual void translate();
    virtual void serialize();
    virtual QString serializeToString();
    void serializeOLD();
  protected:
    QString contextualSerialize(QVector<VariableDefinition*>& variables);
    void extractInputs(QSet<QString>& inputs, QSet<QString>& removedInputs, QVector<VariableDefinition*>& variables);
};

}
}

#endif 
