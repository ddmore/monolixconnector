#ifndef __PHARMML_LOADER_08_H__
#define __PHARMML_LOADER_08_H__

#include <QVector>
#include <QString>
#include <QStringList>
#include <QPair>
#include <QMap>
#include <QSet>
#include <QDomNode>
#include <QDomElement>
#include <QTextStream>

#include "lixoft/translator/LanguageTranslator.h"


namespace lixoft {
namespace translator {

class PharmMLLoader08: public LanguageTranslator
{
    public:
        PharmMLLoader08(const QString& sourceFile,
                      const QString& output,
                      const QVector<QPair<QString,QString> >& options,
                      int outputFormat);

        void loadFromRoot(QDomElement& root);

        virtual ~PharmMLLoader08();

        virtual void checkOptions(){}
        virtual void translate(){}
        virtual void serialize(){}
        virtual QString serializeToString() { return ""; }

        virtual QString toString() const;

    protected:
      enum BlockNameId
      {
          COVARIATE_SECTION = 1,
          INDIVIDUAL_SECTION,
          LONGITUDINAL_SECTION
      };

      class LNodeSibling
      {
        public:
           LNodeSibling(){}
           ~LNodeSibling(){}
           QStringList blockId;
           QDomNode siblingNode;
      };

      class RandomVariableData
      {
        public:
          RandomVariableData(){ isVariance = false; }
          ~RandomVariableData(){}
          bool isVariance;
          QString mean;
          QString stddev;
          QString omegaName;
          QVector<QPair<QString,QString> > variability;
          QString asEquation;
      };

      class ModelDefinition
      {
         public:
          ModelDefinition(){}
          ~ModelDefinition(){}
          QString transformation;
          QString population;
          QVector<QString> etas;
          QMap<QString,QString> covariates;
          QSet<QString> dependentVariables;
      };

      class VariableDefinition : public LixoftObject
      {
        public:
          enum
          {
              IS_VARIABLE = 0,
              IS_CATEGORICAL_VARIABLE,
              IS_RANDOM_VARIABLE,
              IS_STATISTICAL_DEFINITION,
          };
          VariableDefinition():_type(IS_VARIABLE){}
          ~VariableDefinition(){}
          QString name;
          QString equation; // store mlxtran equation
          QSet<QString> dependentVariables;
          QString centeredBy;
          virtual QString toString() const
          {
              return equation;
          }
          int type() const
          { return _type; }

          protected:
            int _type;
      };

      class CatagoricalVariableDefinition : public VariableDefinition
      {
        public:
          CatagoricalVariableDefinition()
          { _type = VariableDefinition::IS_CATEGORICAL_VARIABLE; }
          ~CatagoricalVariableDefinition(){}
          QVector<QString> modalities;
          virtual QString toString() const
          {
              return equation;
          }
      };

      class RandomVariableDefinition : public VariableDefinition
      {
        public:
          RandomVariableDefinition()
          { _type = VariableDefinition::IS_RANDOM_VARIABLE; isVariance = false; needWrite = false; }
          ~RandomVariableDefinition(){}
          bool isVariance;
          QString stddev;
          QString omegaName;
          QString mean;
          QVector<QPair<QString,QString> > variabilityReferences;
          bool needWrite;
          virtual QString toString() const
          {
            QString eq  = (QString("__X_")+name + QString(" = normal(0,1)\n") );
            eq += (name + QString(" = ") + mean + QString("+ sqrt(") + stddev + QString(")*__X_")+name);
            return eq;
          }
      };

      class StatisticalDefinition : public VariableDefinition
      {
        public:
          StatisticalDefinition()
          { _type = VariableDefinition::IS_STATISTICAL_DEFINITION; errorFunction = false; withVariance=false;}
          ~StatisticalDefinition(){}          
          QString transformation;
          QString population;
          bool withVariance;
          QVector<QPair<QString,QString> > omegaNames;
          QVector<QString> omegas;
          QVector<QString> omegas2;
          QVector<QString> etas;
          QVector<QString> errorParameters;
          bool errorFunction;
          QMap<QString,QString> covariates;
          bool isParameterModel;
          virtual QString toString() const
          {
            if (equation.length()) return equation;
            QString modelDefinition="";
            modelDefinition+=(QString("{ distribution=")+transformation+QString("normal, "));
            QString covariatesStr;
            QString coefficientsStr;
            for (QMap<QString,QString>::const_iterator it = covariates.begin();
                 it != covariates.end();it++)
            {
                QMap<QString,QString>::const_iterator nextIt = it;
                nextIt++;
                if (nextIt != covariates.end())
                {
                    covariatesStr+=(it.key()+QString(", "));
                    coefficientsStr+=(it.value()+QString(", "));
                }
                else
                {
                    covariatesStr+=it.key();
                    coefficientsStr+=it.value();
                }
            }
            if (covariatesStr.length())
                modelDefinition+=QString(" covariate={") + covariatesStr + QString("}, ");
            if (coefficientsStr.length())
                modelDefinition+=QString(" coefficient={") + coefficientsStr + QString("}, ");
            modelDefinition+=(QString("prediction=")+population+QString(", sd={"));
            for (int oi = 0;oi<omegas.size();oi++)
            {
                if (oi == (omegas.size()-1))
                {
                    modelDefinition+=omegas[oi];
                }
                else
                {
                    modelDefinition+=(omegas[oi]+", ");
                }
            }
            modelDefinition+="} }";
            if (name.length()) {
              modelDefinition = name + QString(" = ") + modelDefinition;
              return modelDefinition;
            }
            return "";
          }

      };

      class SMEquation
      {
          public:
          SMEquation(){}
          ~SMEquation(){}
          QString initialODEParameter;
          QString equation;
          QString description;

      };
      class ParameterCorrelation
      {
          public:
          ParameterCorrelation():asInput(false),level(0){}
          ~ParameterCorrelation(){}
          QString randomVariable1;
          QString randomVariable2;
          QString coefficient;
          QString covCoefName;
          QString equation;
          bool asInput;
          QString blkRef;
          QString blkSymb;
          int level;
      };
      class Dosing
      {
          public:
          Dosing(){}
          ~Dosing(){}
          QString dosingTypes;
          QString inputTarget;
          QString dosingTime;

      };

      class DataSet
      {
          public:
              DataSet(){}
              ~DataSet(){}
              QMap<int,QPair<QString,QString> > columns;
              QString dataUrl;
              QString columnDelimiter;
              QString name;
              QString dataFormat;
      };

      class Categorical
      {
          public:
              Categorical(){}
              ~Categorical(){}
              QMap<QString,QString> modalitiesAlias;
              QSet<QString> simpleParameters;
              QString categoryVariable;
              QString equations;

      };

      class SymbolId
      {
        public:
          QString transformed;
      };



    QString parseQDomNodeAttribute(const QDomNode& node,
                                   const QString& attributeName);

    void traverseTo(QVector<LNodeSibling>& selectedNodes,
                    QStringList blockRefs,
                    const QDomNode& fromNode, const QStringList& nodePaths);

    void equationToVariableDefinition(int blockNameId,
                                      VariableDefinition& variable,
                                      const QString& variableNameAssigned,
                                      QSet<QString>& assigned,
                                      const QString& variableName,
                                      const QStringList& blkIds,
                                      const PharmMLLoader08::LNodeSibling& equationNode);

    void getRandomVariable(const QDomNode& node, PharmMLLoader08::RandomVariableDefinition& randomVariable);
    void getIndividualParameter(const QDomNode& node,
                                const QMap<QString,RandomVariableDefinition*>& randomVariables,
                                const QSet<QString>& registeredSimpleParameters,
                                PharmMLLoader08::StatisticalDefinition& statisticalDefiniftionVariable);


    QVector<QString> pharmlEquationToMlxtran(int blockNameId,
                                               const QDomNode& node,
                                               const QString& leftPart,
                                               QMap<QString,QVector<QString> >& blockRef,
                                               QSet<QString>& dependentVariable,
                                               const QString& blkIdRef /*=""*/,
                                               const QMap<QString,QString>& aliases/* = QMap<QString,QString>()*/);


      void pharmMLCovariateModelToMlxtran(const QDomNode& node);
      void pharmMLParameterModelToMlxtran(const QDomNode& node);
      void pharmMLStructuralModelToMlxtran(const QDomNode& node);
      void pharmMLObservationModelToMlxtran(const QDomNode& node);
      void pharmMLEstimationTaskToMlxtran(const QDomNode& node);
      void pharmMLDataToMlxtran(const QDomNode& node);

      void getCovariateModel(QDomElement& root, QVector<VariableDefinition*>& covariates, QSet<QString>& globalParameterRegistered);
      void getIndividualParameterModel(QDomElement& root, QVector<VariableDefinition*>& individualParameters, QSet<QString>& globalParameterRegistered);
      void getStructuralModel(QDomElement& root,QVector<VariableDefinition*>& structural,
                              QSet<QString>& globalParameterRegistered);
      void getObservationModel(QDomElement& root, QVector<VariableDefinition*>& observations, QSet<QString>& globalParameterRegistered);
      void getColumnDepotRef(QVector<QPair<QString,QString> >& dtdes,QDomNode& node);
      void getDataSet(QDomElement& root);
      void getInitialValue(QDomElement root);



      QVector<PharmMLLoader08::VariableDefinition*> covariatesSection;
      QVector<PharmMLLoader08::VariableDefinition*> individualParametersSection;
      QVector<PharmMLLoader08::VariableDefinition*> structuralModelSection;
      QVector<PharmMLLoader08::VariableDefinition*> observationModelSection;
      QMap<QString,QString> columnMapping;
      QMap<QString,QPair<QString,QString> > administrationMapping;
      QMap<QString,QString> columnNameTypeMapping;
      QMap<QString,QVector<QPair<QString,QString> > > errorModelParameterMapping;
      QMap<QString,QMap<QString,QString> > covariateCategoryMapping;
      QMap<QString,QString> pharmMLNamesMapping;
      bool isMonolixCompliant;
      QString _modelDescription;
      QString _pkBlock;
      QMap<int,QString> occColumnNames;
      QSet<QString> _macroParameters;
      QSet<QString> _macroPKAmount;
      QSet<QString> _macroInputFilter;
      QMap<QString, QString> _continuousCovariates;
      QMap<QString, QMap<QString,QString> > _categoricalCovariates;
      QMap<QString,RandomVariableData> _randomVariables;
      QMap<QString,StatisticalDefinition> _individualParameters;
      QSet<QString> _individualParametersSymbols;
      QMap<QString,QVector<QString> > _structuralModelParameters;
      QMap<QString,SMEquation> _structuralModelEquations;
      QVector<QString> _structuralModelOutputs;
      QVector<ParameterCorrelation> _parameterCorrelation;
      QVector<QPair<QString,QString> > _observationRandomEffects;
      QMap<QString,Dosing> _dosingVariables;
      QMap<QString,QString> _initialValues;
      QMap<QString,QVector<QString> > _errorModelParameters;
      QMap<QString,QString> _errorModel;
      QSet<QString> _predictions;
      DataSet _dataSet;
      QMap<QString,Categorical> _discreteModels;
      QMap<QString,SymbolId> _symbolIds;
      QMap<QString,QString> doseTimeValues;
      QMap<QString,int> variabilityLevelsIds;
};

}
}

#endif
