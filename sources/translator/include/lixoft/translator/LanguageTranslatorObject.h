#ifndef __LANGUAGE_TRANSLATOR_OBJECT_H__
#define __LANGUAGE_TRANSLATOR_OBJECT_H__

#include <QStringList>

#include "lixoft/translator/lixoftTranslators.h"
#include "lixoft/LixoftObject.h"


namespace lixoft {
namespace translator {

class LanguageTranslatorObject : public LixoftObject
{
  public:
    LanguageTranslatorObject();
    virtual ~LanguageTranslatorObject();
    QStringList& errorStack();
    const QStringList& errorStack() const;
    QStringList& warningStack();
    const QStringList& warningStack() const;
    void reset();
  private:
    QStringList _errorStack;
    QStringList _warningStack;
    

};

}
}

#endif
