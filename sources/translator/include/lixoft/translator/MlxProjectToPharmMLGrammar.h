#ifndef __MLXPROJECT_TO_PHARMML_GRAMMAR_H__
#define __MLXPROJECT_TO_PHARMML_GRAMMAR_H__

#include "lixoft/exceptions/Exception.h"
#include "lixoft/LixoftObject.h"

#include "monolix/qi.hpp"
#include "monolix/lex_lexertl.hpp"
#include "monolix/boost/mpl/vector.hpp"
#include "monolix/boost/lexical_cast.hpp"
#include <string.h>

namespace lixoft {
namespace translator {

typedef std::string::const_iterator base_iterator_type;
typedef mlxboost::mpl::vector<std::string> Attributes;
typedef mlxboost::spirit::lex::lexertl::token<base_iterator_type, Attributes> token_type;
typedef mlxboost::spirit::lex::lexertl::actor_lexer<token_type> Lexer_type;
typedef mlxboost::spirit::lex::token_def<std::string> TokenString;

class Lexer : public  mlxboost::spirit::lex::lexer<Lexer_type>
{
public:
    Lexer();
    TokenString _if, _else, _elseif, _end;
    TokenString _popen, _pclose, _bopen, _bclose, _iopen, _iclose,_comma, _arrow;
    TokenString _relop, _assign, _unop, _plus, _minus, _divide, _power, _star, _binop;
    TokenString _int, _real, _condOp;
    TokenString _stringLit, _charLit;
    mlxboost::spirit::lex::token_def<mlxboost::spirit::lex::omit> _ws, _lComment,_eol,_lContinue,_emptyLine;
    TokenString  _identifier, _delay, _transitRateFuncDef;
    std::vector<TokenString> _mathUnAryFunctions, _mathNAryFunctions,  _statFunctions, _pkFunctions;
};

enum TokenIdType {UNOP =0x10000, PLUS, MINUS, CONDOP, RELOP, BINOP,  POPEN, BOPEN,  //  warning !!!  this line is ordered
                  MATHUNARYFUNC, MATH_NARYFUNC, STATFUNC, PKFUNC, USERFUNC, //  warning !!!  this line is ordered
                  ASSIGN,  IF, ELSE, ELSEIF, END, PCLOSE,  BCLOSE,
                  COMMA, REAL, INT, DELAY, IDANY, SECTION, KEYWORD,  UNKNOWN
                 } ;

struct GetTokenValue : mlxboost::static_visitor<std::string>
{
    template <typename Tt> // the token value can be a variant over any of the exposed attribute types
    std::string operator()(mlxboost::variant<Tt> const& v) const {
        return mlxboost::apply_visitor(*this, v);
    }

    template <typename T> // the default value is a pair of iterators into the source sequence
    std::string operator()(mlxboost::iterator_range<T> const& v) const {
        return std::string(v.begin(), v.end());
    }

    template <typename T>
    std::string operator()(T const& v) const {
        // not taken unless used in Spirit Qi rules, I guess
        return std::string("attr<") + typeid(v).name() + ">(" + mlxboost::lexical_cast<std::string>(v) + ")";
    }
};

typedef std::pair<unsigned long int, std::string> TokenPair;
typedef std::vector<TokenPair> TokenPairs;
typedef std::pair<std::string, TokenPairs > TokenExpression;
typedef std::vector<TokenExpression> TokenExpressions;
typedef mlxboost::iterator_range<base_iterator_type> ITRange;

struct GrammarResults
{
    GrammarResults(TokenExpressions  & results);
    void storePairs(const unsigned long int & id, const ITRange &value);
    TokenPairs _tokenPairs;
    TokenExpressions & _results;
};

struct GrammarEquation : mlxboost::spirit::qi::grammar<Lexer::iterator_type >, GrammarResults
{
    explicit GrammarEquation(const Lexer & lexer, TokenExpressions  & results);
    mlxboost::spirit::qi::rule<Lexer::iterator_type>
    _expressions, _basicExpressions,   _equation, _functionDef, _functionName, _transfProbaFuncName, _ifCondition, _elseifCondition,
    _functionCall, _binOperator, _unOperator, _relOperator, _relCondition, _relOperations,  _assignOperations,
    _paramVal, _operations, _operationsAndRel, _parenthesisOper, _pkMacro, _funcParam,
    _pkMacroOptArg, _pkMacroArgs,  _pkEquation,  _pkFunctionName,  _observationPrefix, _assignVal, _definitionObs,
    _probaRelCondition, _probaDef, _condProbaDef,  _transfProbaDef,  _transfProba,
    _transfCondProbaDef,  _event, _continuous, _count, _categorical, _endObservation, _input;
    TokenIdType _keyword;

};


struct GrammarInput : mlxboost::spirit::qi::grammar<Lexer::iterator_type >, GrammarResults
{
    explicit GrammarInput(const Lexer & lexer, TokenExpressions  & results);
    mlxboost::spirit::qi::rule<Lexer::iterator_type> _input;
};

} // namespace translator
} // namespace lixoft


#endif
