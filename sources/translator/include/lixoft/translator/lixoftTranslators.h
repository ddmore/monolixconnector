#ifndef lixoft_core_lixoftLanguageTranslators_h_INCLUDED
#define lixoft_core_lixoftLanguageTranslators_h_INCLUDED

#if (defined(_WIN32) || defined(_WIN32_WCE))
  #if defined(lixoftLanguageTranslators_EXPORTS)
    #define lixoftLanguageTranslators_API __declspec(dllexport)
  #elif !defined(lixoftLanguageTranslators_HIDES)
    #define lixoftLanguageTranslators_API __declspec(dllimport)
  #endif
#elif __GNUC__ >= 4
  #define lixoftLanguageTranslators_API __attribute__((__visibility__("default")))
  #define lixoftLanguageTranslators_TPL_BASE_API __attribute__((__visibility__("default")))
#endif

#if !defined(lixoftLanguageTranslators_API)
  #define lixoftLanguageTranslators_API
#endif

#if !defined(lixoftLanguageTranslators_TPL_BASE_API)
  #define lixoftLanguageTranslators_TPL_BASE_API
#endif

#endif // lixoft_core_lixoftLanguageTranslators_h_INCLUDED
