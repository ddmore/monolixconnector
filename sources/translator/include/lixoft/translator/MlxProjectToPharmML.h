#ifndef __MLXPROJECT_TO_PHARMML_H__
#define __MLXPROJECT_TO_PHARMML_H__

#include "lixoft/translator/LanguageTranslator.h"
#include "lixoft/translator/MlxProjectToPharmMLAPI.h"


namespace lixoft {
namespace translator {

class MlxProjectToPharmML : public LanguageTranslator
{
public:
    MlxProjectToPharmML(const QString& sourceFile,
                        const QString& output,
                        const QVector<QPair<QString,QString> >& options,
                        const int & outputType);
    virtual ~MlxProjectToPharmML();

    virtual void checkOptions();
    virtual void translate();
    virtual void serialize();
    virtual QString serializeToString();
};

class PharmXmlConverter : public mlxPoco::Util::XMLConfiguration, public LixoftObject, public mlxPoco::XML::DOMWriter
{
public:
    PharmXmlConverter(const QString &sourceFile);
    virtual ~PharmXmlConverter();
    void translate(const Element* mlxDocument);
    void saveWithXmlDeclaration(const std::string&  path) const;
private:
    void writeHeader();

    PharmXmlConverter();

    std::pair<std::string,std::string> pharmmlColTypes(const QString & colname);

    std::string pharmmlDelimiterTypes(const std::string & delimiter);

    bool pharmmlTransformationName(const std::string & mlxTransformation, std::string& pharmmlTransformation);

    void cleanMlxDataPath(std::string & path);

    void findDataParam(const QStringList & vHeaders,const Node* mlxtProject,QVector<QString>& catNamesInData); // find unparsed data parameters: fill _categories and _dataSymb

    std::string setDataDelimiter(const std::string &delimiter);

    void checklineSize(const int& lineSize, const int &  firstLineSize);

    void tokenizeToPharmml(const std::string& input, Element*pharmml);

    void longitudinalToPharmml(const char* file, Element* modelDefinition);

    template<typename iterator>
    void writeNextToken(iterator & iter, const iterator & tokenEnd, Element* parentElt, Element* currentElt) ;

    std::string pharmmlOpName(const std::string &opName);

    void checkId(const long unsigned int&  previousId, const long unsigned int& id, const std::string tokenValue, const int &  line, const char* file);

    void writeTransformation(const Node *mlxPriorTransformation, Element*structuredModel, const Node *mlxPrior=0);

    void writeInitialization(const Node*interceptInit,Element* parametersToEstimate, const std::string& mlxParamName, const std::string& descriptionArgName="", const  Node* descriptionArg=0);

    void writeVariability(const Node* mlxVariability, Element* parametersToEstimate, Element* structuredModel, Element* parameterModel,Node* &lastPopParameter, const std::string& mlxParamName);

    void mlxErrorModel(const std::string &name, std::string &errorModel);

    void createFunctionDefNode(Element*&functionDef);

    void appendFunctionArg(Element* functionNode, const std::string & argument);

    void appendSymbRef(Element* node, const std::string & symbIdRefAttribute, const std::string & blkIdRefAttribute="");

    void writeRandomVariable(const std::string & symbIdName,  std::string  symbIdRefAttribute,  std::string  blkIdRefAttribute,
                             std::string meanName,  std::string  stdevName, Element* stdevAssign, Element* parameterModel, Node* & lastPopParameter);

    template<typename iterator>
    void createBinRelationNode(Element* & binRelation, const iterator & iter);

    void createOperationNode(Element*& operation, const std::string & opName, int & order);

    void createEstimationStepNode(Element*& estimationStep, Element* externalDataSet);

    void writeEquationSection(TokenExpressions::iterator & resultsBegin, const TokenExpressions::const_iterator & resultsEnd, Element* structuralModel);

    template<typename iterator>
    void writeToPharmml(iterator &iter, const iterator & tokenEnd, Element* pharmml, long unsigned int& prevId);

    template<typename iterator>
    void getFunctionArgument(iterator& begTok, const iterator& endTok, Element* argument,
                             Node* &argumentFirstchild, long unsigned int prevId, const std::string& functionName);

    void writeConditionalStatements(TokenExpressions::iterator & expressionsBegin, const TokenExpressions::const_iterator & expressionsEnd, Element* element);

    Node* writeConditionalStatements(TokenExpressions::iterator & expressionsBegin, const TokenExpressions::const_iterator & expressionsEnd, Element* element, std::set<std::string>& variables);

    void writeMathCondition(const TokenExpression expression, Element* conditionalStatement);

    void writePkMacros(TokenExpressions::iterator & expressionsBegin, const TokenExpressions::const_iterator & expressionsEnd, Element* element, int & cmt);

    void writeProbaDefinition(TokenPairs & expression, Element* CountData, bool isTransformed = false);

    void writeConditionalProbaDef(TokenPairs & expression, Element* CountData, bool isTransformed = false);

    void writePopParameter(const std::string& parameterName, Node* &lastPopParameter,Element* parameterModel);

    void setIndivParameterAttribute(Element* structuralModel);

    void createVariableNode(Element * &variable, const std::string & varName, const std::string & varType);

    void renameAssignToLogicBinop(Element* state);

    void writeCountProbaDefinition(TokenPairs &expression, Element* countData, bool isTransformed = false);

    void setDosingKeywdAttributes(Element* structuralModel, Element* structuralModelChild, bool & isTdose, bool & isAmtDose);

    void mapDosingKeywdInDataset(Element* externalDataSet, const bool & isTdose, const bool & isAmtDose);

    void createColumMapping(Element*& columnMapping, const std::string & columnIdRef);

    void createStructualModel(Element*modelDefinition, Element* &structuralModel);

    bool findCategoryMapping(Element* externalDataSet, Element* &columnMapping, const std::string & varName);

    bool setCategoricalName(Element* category, Element* categoryMapping, std::string & catName, const std::string& observationName,bool isCatId=true);

    void insertStateCategoryVariable(Element* categoricalData, Element* dependance, Element* & stateVariable, const std::string & stateName);

    void appendDescription(Element* element, const std::string comments);

    void createVariabilityNode(Element * & variabilityModel,Element * modelDefinition);

    void createErrorModelNode(Element *parameterModel, const  std::string &varname, Node* &lastPopParameter, Element* parametersToEstimate,
                              Element* modelDefinition, TokenPairs  &standardDevPairs, bool & isStandardDeviation, const unsigned & lengthObsModel, const std::string& errorModelName,
                              const std::string& iStructuralModel,const std::string& prediction, const std::string& observationName, bool & isParameterModel, const mlxPoco::XML::NodeList* mlxErrModParams,
                              const Node *mlxErrorModAutoCorr, std::vector<std::string>& errorModels, Element *& outStyle,Element *& outStyleAssign,const TokenPairs &errorModelPairs);

    QRegExp _dataDelimiter;
    std::string _dataPath;
    std::vector<std::vector<std::string> > _categories;
    std::vector<std::string> _dataSymb; // user names (header) in  data file
    std::vector<std::string> _userFuncIds;
    std::vector<std::string> _individualParameters;
    QString _sourceFile;
};

mlxPoco::XML::Node* getChildNode(const mlxPoco::XML::Node* node,const std::string& name);

mlxPoco::XML::Node* getChildNode(const mlxPoco::XML::Element* element,const std::string& name);

mlxPoco::XML::NodeList* getChildNodesByTagName(const mlxPoco::XML::Node* node,const std::string& name);

bool isNotEmpty(const mlxPoco::XML::Node* node);

} // namespace translator
} // namespace lixoft


#endif
