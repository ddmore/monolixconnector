#ifndef __PHARMML08_TO_MLXTRAN_H__
#define __PHARMML08_TO_MLXTRAN_H__

#include <QTextStream>

#include "lixoft/translator/PharmMLLoader08.h"

namespace lixoft {
namespace translator {

class PharmML08ToMlxtran : public PharmMLLoader08
{
  public:
    PharmML08ToMlxtran(const QString& sourceFile,
                     const QString& output,
                     const QVector<QPair<QString,QString> >& options,
                     int outputFormat);
    virtual ~PharmML08ToMlxtran();

    virtual void checkOptions();
    virtual void translate();
    virtual void serialize();
    virtual QString serializeToString();
    void serializeOLD();
  protected:
    QString contextualSerialize(QVector<VariableDefinition*>& variables);
    void extractInputs(QSet<QString>& inputs, QSet<QString>& removedInputs, QVector<VariableDefinition*>& variables);
};

}
}

#endif 
