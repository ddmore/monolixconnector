#ifndef __PHARMML08_TO_MLXPROJECT_H__
#define __PHARMML08_TO_MLXPROJECT_H__

#include <QVector>
#include <QString>
#include <QPair>
#include <QMap>
#include <QSet>
#include <QDomNode>
#include <QTextStream>

#include "lixoft/translator/lixoftTranslators.h"
#include "lixoft/translator/LanguageTranslatorObject.h"
#include "lixoft/translator/LanguageTranslator.h"
#include "lixoft/translator/PharmMLLoader08.h"

namespace lixoft {
namespace translator {

class PharmML08ToMlxProject : public PharmMLLoader08
{
  public:
    PharmML08ToMlxProject(const QString& sourceFile,
                        const QString& output,
                        const QVector<QPair<QString,QString> >& options,
                        int outputFormat);
    virtual ~PharmML08ToMlxProject();

    virtual void checkOptions();
    virtual void translate();
    virtual void serialize();
    virtual QString serializeToString();
  //  void serializeBCK();

};

}
}

#endif
