#ifndef __MLXPROJECT_TO_MLXTRAN_H__
#define __MLXPROJECT_TO_MLXTRAN_H__

#include <QVector>

#include "lixoft/translator/LanguageTranslator.h"
#include "monolix/parsers/MLXTranParserResults.h"

namespace lixoft {
namespace translator {

class MlxProjectToMlxtran : public LanguageTranslator
{
  public:
    MlxProjectToMlxtran(const QString& sourceFile,
                        const QString& output,
                        const QVector<QPair<QString,QString> >& options,
                        int outputFormat);
    virtual ~MlxProjectToMlxtran();

    virtual void checkOptions();
    virtual void translate();
    virtual void serialize();
    virtual QString serializeToString();
  protected:
    bool _withOnlyObservationModel;
    bool _withObservationModel;
    monolix::parsers::MLXTranParserResults _parseResult;
    QString _project;
    QString _initialParameters;
    QString _structuralModel;
    bool _projectIsOk;
  private:
    QString buildModelDefinition(const QString& modelId,
                                 const QString& modelName,
                                 const QString& prediction,
                                 const QString& errorModelName,
                                 QVector<QString>& modelParameters) const;
};

}
}

#endif
