#ifndef __PHARMML_TO_MLXPROJECT_H__
#define __PHARMML_TO_MLXPROJECT_H__

#include <QVector>
#include <QString>
#include <QPair>
#include <QMap>
#include <QSet>
#include <QDomNode>
#include <QTextStream>

#include "lixoft/translator/lixoftTranslators.h"
#include "lixoft/translator/LanguageTranslatorObject.h"
#include "lixoft/translator/LanguageTranslator.h"
#include "lixoft/translator/PharmMLLoader.h"

namespace lixoft {
namespace translator {

class PharmMLToMlxProject : public PharmMLLoader
{
  public:
    PharmMLToMlxProject(const QString& sourceFile,
                        const QString& output,
                        const QVector<QPair<QString,QString> >& options,
                        int outputFormat);
    virtual ~PharmMLToMlxProject();

    virtual void checkOptions();
    virtual void translate();
    virtual void serialize();
    virtual QString serializeToString();
  //  void serializeBCK();

};

}
}

#endif
