#ifndef __MLXPROJECT_TO_PHARMMLAPI_H__
#define __MLXPROJECT_TO_PHARMMLAPI_H__

#include "monolix/Poco/DOM/Node.h"
#include "monolix/Poco/DOM/Element.h"
#include "monolix/Poco/DOM/Comment.h"
#include "monolix/Poco/Util/XMLConfiguration.h"
#include "monolix/Poco/DOM/DOMWriter.h"
#include "monolix/Poco/DOM/Text.h"
#include "monolix/Poco/DOM/NodeList.h"
#include "monolix/Poco/FileStream.h"
#include "monolix/Poco/DOM/NamedNodeMap.h"
#include "monolix/Poco/DOM/AutoPtr.h"
#include "monolix/Poco/XML/XMLWriter.h"

#include "lixoft/exceptions/Exception.h"
#include "lixoft/translator/lixoftTranslators.h"

#include <QRegExp>
#include <QFileInfo>
#include <QFile>
#include <QDir>
#include <QString>
#include <QTextStream>

namespace lixoft {
namespace translator {

using mlxPoco::XML::Element;
using mlxPoco::XML::Node;
using mlxPoco::XML::NamedNodeMap;
using mlxPoco::XML::AutoPtr;
using mlxPoco::XML::XMLWriter;

typedef std::pair<unsigned long int, std::string> TokenPair;
typedef std::vector<TokenPair> TokenPairs;
typedef std::pair<std::string, TokenPairs > TokenExpression;
typedef std::vector<TokenExpression> TokenExpressions;

enum TokenIdType {UNOP =0x10000, PLUS, MINUS, CONDOP, RELOP, BINOP,  POPEN, BOPEN,  //  warning !!!  this line is ordered
                  MATHUNARYFUNC, MATH_NARYFUNC, STATFUNC, PKFUNC, USERFUNC, //  warning !!!  this line is ordered
                  ASSIGN,  IF, ELSE, ELSEIF, END, PCLOSE,  BCLOSE,
                  COMMA, REAL, INT, DELAY, IDANY, SECTION, KEYWORD,  UNKNOWN
                 } ;

lixoftCore_API bool parseMlxtranFile(const char* sourceFile, Element*& mlxDocument, Node* mlxRoot, QString& outputXML);

lixoftCore_API void splitMlxtranIntoSections(std::string & longitudinalStr, std::vector<std::pair<std::string, std::string>  > & sections);

lixoftCore_API bool parseMlxtranEquationSection(const std::string &input,TokenExpressions &  results );

lixoftCore_API bool parseMlxtranInputSection(const std::string &input,TokenExpressions &  results);

lixoftCore_API void getMlxtranFuncArgs(const std::string& strInput, std::set<std::string> &mlxFuncArgs);

} // namespace translator
} // namespace lixoft


#endif
