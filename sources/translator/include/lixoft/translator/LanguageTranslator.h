#ifndef __LANGUAGE_TRANSLATOR_H__
#define __LANGUAGE_TRANSLATOR_H__

#include <QString>
#include <QPair>
#include <QVector>

#include "lixoft/translator/lixoftTranslators.h"
#include "lixoft/LixoftObject.h"


namespace lixoft {
namespace translator {

class LanguageTranslator : public LixoftObject
{
  public:
    enum
    {
        OUTPUT_IS_UNDEF = 0x00,
        OUTPUT_IS_A_FILE = 0x01,
        OUTPUT_IS_A_DIRECTORY = 0x02
    };
    LanguageTranslator(const QString& sourceFile,
                       const QString& output,
                       const QVector<QPair<QString,QString> >& options,
                       int outputFormat);
    virtual ~LanguageTranslator();
    virtual void checkOptions() = 0;
    virtual void translate()    = 0;
    virtual void serialize()    = 0;
    virtual QString serializeToString() = 0;
    virtual QString toString() const;

  protected:
    QString _sourceFile;
    QString _output;
    int _outputFormat;
    QString _outBaseName;
    QVector<QPair<QString,QString> > _options;
    void checkFileAndDirectory(const QString& translatorClass);

};

}
}

#endif
