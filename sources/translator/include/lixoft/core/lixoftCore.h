#ifndef lixoft_core_lixoftCore_h_INCLUDED
#define lixoft_core_lixoftCore_h_INCLUDED

#if (defined(_WIN32) || defined(_WIN32_WCE))
  #if defined(lixoftCore_EXPORTS)
    #define lixoftCore_API __declspec(dllexport)
  #elif !defined(lixoftCore_HIDES)
    #define lixoftCore_API __declspec(dllimport)
  #endif
#elif __GNUC__ >= 4
  #define lixoftCore_API __attribute__((__visibility__("default")))
  #define lixoftCore_TPL_BASE_API __attribute__((__visibility__("default")))
#endif

#if !defined(lixoftCore_API)
  #define lixoftCore_API
#endif

#if !defined(lixoftCore_TPL_BASE_API)
  #define lixoftCore_TPL_BASE_API
#endif

#endif // lixoft_core_lixoftCore_h_INCLUDED
