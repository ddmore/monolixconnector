#ifndef __LIXOFT_OBJECT_H__
#define __LIXOFT_OBJECT_H__

#include <QString>
#include <QVector>
#include <QMap>
#include <QMutex>

#include "lixoft/exceptions/Exception.h"

#define DEFAULT_PRUNE_MESSAGE 100

namespace lixoft
{
namespace translator {
    class LixoftObject
    {
        public:
            enum LixoftObjectInformationLevels
            {
                UNDEF    = 0x01,
                INFO     = 0x02,
                WARNING  = 0x04,
                LERROR   = 0x08,
                CRITICAL = 0x10,
                DEBUG    = 0x20,
                TRACE    = 0x40,
                ALL      = 0x7f
            };

            enum LixoftObjectMessageOutputStream
            {
                LIXOFT_STDOUT = 0,
                LIXOFT_STDERR = 2,
                LIXOFT_LOGGER = 4
            };

            LixoftObject(bool isNull = false);
            LixoftObject(const LixoftObject& cpy);
            virtual ~LixoftObject();


            LixoftObject& operator=(const LixoftObject& cpy);
            /**
             * Throw exception with log.
             * @param ex exception to throw
             */
            void throwNow(const lixoft::exception::Exception& ex) const;

            /**
             * Throw exception stored in exception stack with log.
             */
            void throwNow() const;


            /**
             * Push exception into exception stack
             * @param ex exception to thraw later
             */
            void throwLater(const lixoft::exception::Exception& ex);

            /**
             *  Add a simple message
             *  @param level is LixoftObjectInformationLevels
             *  @param message message to add
            */
            void addMessage(int level, QString message);

            /**
             *  Display message stack
             *  @param output is LixoftObjectMessageOutputStream
             *  @param level select level to display
            */
            void displayMessage(int output = LIXOFT_STDERR, int level = INFO | WARNING | LERROR | CRITICAL);

            /**
             *  Display message (without storage in message stack)
             *  @param message to display
             *  @param output is LixoftObjectMessageOutputStream
             *  @param level select level to display
            */
            void displayMessage(QString message, int output = LIXOFT_STDERR, int level = INFO | WARNING | LERROR | CRITICAL);

            /**
             *  Count the number of messages according to the level
             *  @return the number of messages;
            */
            int countMessages(int level = INFO | WARNING | LERROR | CRITICAL) const;

            /**
             *  Count the number of exception in exception stack (register by @see throwLater)
             *  @return the number of messages;
            */
            int countExceptions() const;

            /**
             * wait until notify called
             */
            void wait() const;

            /**
             * wake up wait object
             */
            void notify() const;


            virtual QString toString() const;

            static const LixoftObject& null();            
            bool isNull() const;

        private:
            /*
             *  Null object manager
             *  when a function return an object by reference, sometimes it is necessary
             *  to return a null object (see design pattern Null Object)
             */
            static LixoftObject _nullObject;
            bool _isNull;

            // logger
            //monolix::logger::MLXLogger& _logger; // JM (seen with EB october 2014): Commented because logger crashes on WINDOWS

            // limit stack of messages and exceptions
            int _pruneLevel;

            // stack of exceptions and messages
            mutable QVector<lixoft::exception::Exception> _exceptionStack;
            mutable QMap<int, QVector<QString> > _messageStack;

            mutable QMutex* _waiter;

            void prune() const;
    };
}
}

#endif
