#include <iostream>
#include <typeinfo>
#ifndef WIN32
#include <cxxabi.h>
#endif
#include "lixoft/LixoftObject.h"

#include <QString>
#include <QMap>
#include <QVector>


namespace lixoft
{
namespace translator {
    LixoftObject LixoftObject::_nullObject(true);

    LixoftObject::LixoftObject(bool isNull):_isNull(isNull),                                            
                                            _pruneLevel(DEFAULT_PRUNE_MESSAGE)
    {
        _waiter = new QMutex();
    }

    LixoftObject::LixoftObject(const LixoftObject& cpy)
    {
        _isNull = cpy._isNull;
        _pruneLevel = cpy._pruneLevel;
        _waiter = new QMutex();
    }

    LixoftObject::~LixoftObject()
    {
        _waiter->lock(); // wait until the object is lock by another thread
        _waiter->unlock();
        delete _waiter;
    }

    LixoftObject&  LixoftObject::operator=(const LixoftObject& cpy)
    {
        _isNull = cpy._isNull;
        _pruneLevel = cpy._pruneLevel;
        _waiter = new QMutex();
        return *this;
    }


    void LixoftObject::throwNow(const lixoft::exception::Exception& ex) const
    {
        prune();
        std::cerr<<ex.displayAll().c_str(),ex.source().c_str();
        throw ex;
    }

    void LixoftObject::throwNow() const
    {
        if (_exceptionStack.size())
        {
            QString compiledMessages="[Delayed exceptions]\n";
            for (int ex_i = 0;ex_i<_exceptionStack.size();ex_i++)
            {
                compiledMessages+=QString((std::string("[Delayed exception] ") + _exceptionStack[ex_i].displayAll() + "\n").c_str());
            }
            _exceptionStack.clear();
            throw lixoft::exception::Exception("lixoft::LixoftObject",compiledMessages.toUtf8().data(),THROW_LINE_FILE);
        }
    }

    void LixoftObject::throwLater(const lixoft::exception::Exception& ex)
    {
        std::cerr<<ex.displayAll().c_str(),ex.source().c_str();
        _exceptionStack.push_back(ex);
        prune();
    }

    void LixoftObject::addMessage(int level, QString message)
    {
        if (!(level & LixoftObject::ALL))
            level = LixoftObject::UNDEF;
        _messageStack[level].push_back(message);
        prune();
    }

    void LixoftObject::displayMessage(int output, int level)
    {
        if (!(level & LixoftObject::ALL))
            level = LixoftObject::UNDEF;
        QVector<int> displayLevels;
        if (level & LixoftObject::UNDEF)
        {
            displayLevels.push_back(LixoftObject::UNDEF);
        }
        if (level & LixoftObject::INFO)
        {
            displayLevels.push_back(LixoftObject::INFO);
        }
        if (level & LixoftObject::WARNING)
        {
            displayLevels.push_back(LixoftObject::WARNING);
        }
        if (level & LixoftObject::LERROR)
        {
            displayLevels.push_back(LixoftObject::LERROR);
        }
        if (level & LixoftObject::CRITICAL)
        {
            displayLevels.push_back(LixoftObject::CRITICAL);
        }
        if (level & LixoftObject::DEBUG)
        {
            displayLevels.push_back(LixoftObject::DEBUG);
        }
        if (level & LixoftObject::TRACE)
        {
            displayLevels.push_back(LixoftObject::TRACE);
        }
        for (int dl_i=0;dl_i<displayLevels.size();dl_i++)
        {
            QVector<QString>& message = _messageStack[ displayLevels[dl_i] ];
            for (int mi=0;mi<message.size();mi++)
            {
                displayMessage(message[mi],output,displayLevels[dl_i]);
            }
            message.clear();
        }
        prune();
    }

    void LixoftObject::displayMessage(QString message, int output, int level)
    {
        QString levelName="";
        if (level & LixoftObject::INFO)
            levelName+=(QString(":")+"info");
        if (level & LixoftObject::WARNING)
            levelName+=(QString(":")+"warning");
        if (level & LixoftObject::LERROR)
            levelName+=(QString(":")+"error");
        if (level & LixoftObject::CRITICAL)
            levelName+=(QString(":")+"critical");
        if (level & LixoftObject::TRACE)
            levelName+=(QString(":")+"trace");
        if (level & LixoftObject::DEBUG)
            levelName+=(QString(":")+"debug");
        if (level & LixoftObject::UNDEF)
            levelName+=(QString(":")+"undefine");

        if (output & LixoftObject::LIXOFT_STDERR)
        {
         #ifndef WIN32
            int     status;
            char   *realname;
            realname = abi::__cxa_demangle(typeid(*this).name(), 0, 0, &status);
            QString demangledName = realname;
            free(realname); // c allocation
            std::cerr<<"["<<demangledName.toUtf8().data()<<levelName.toUtf8().data()<<"]:"<<message.toUtf8().data()<<std::endl;
          #else
            std::cerr<<"["<<typeid(*this).name()<<levelName.toUtf8().data()<<"]:"<<message.toUtf8().data()<<std::endl;
          #endif


        }
        if (output & LixoftObject::LIXOFT_STDOUT)
        {
          #ifndef WIN32
            int     status;
            char   *realname;
            realname = abi::__cxa_demangle(typeid(*this).name(), 0, 0, &status);
            QString demangledName = realname;
            free(realname); // c allocation
            std::cout<<"["<<demangledName.toUtf8().data()<<levelName.toUtf8().data()<<"]:"<<message.toUtf8().data()<<std::endl;
          #else
            std::cout<<"["<<typeid(*this).name()<<levelName.toUtf8().data()<<"]:"<<message.toUtf8().data()<<std::endl;
          #endif
        }
        if (output & LixoftObject::LIXOFT_LOGGER)
        {
            QString demangledName;
            #ifndef WIN32
              int     status;
              char   *realname;
              realname = abi::__cxa_demangle(typeid(*this).name(), 0, 0, &status);
              demangledName = realname;
              free(realname); // c allocation
            #else
              demangledName=typeid(*this).name();
            #endif

            if (level & LixoftObject::INFO)
                std::cout<<message.toUtf8().data();
            if (level & LixoftObject::WARNING)
                std::cerr<<message.toUtf8().data();
            if (level & LixoftObject::LERROR)
                std::cerr<<message.toUtf8().data();
            if (level & LixoftObject::CRITICAL)
                std::cerr<<message.toUtf8().data();
            if (level & LixoftObject::TRACE)
                std::cerr<<message.toUtf8().data();
            if (level & LixoftObject::DEBUG)
                std::cerr<<message.toUtf8().data();
            if (level & LixoftObject::UNDEF)
               std::cerr<<message.toUtf8().data();
        }
    }

    int LixoftObject::countMessages(int level) const
    {
        int countMess = 0;
        if (level & LixoftObject::UNDEF)
        {
            QMap<int,QVector<QString> >::const_iterator findLevelIt = _messageStack.find(LixoftObject::UNDEF);
            if (findLevelIt != _messageStack.end()) countMess += findLevelIt.value().size();
        }
        if (level & LixoftObject::INFO)
        {
            QMap<int,QVector<QString> >::const_iterator findLevelIt = _messageStack.find(LixoftObject::INFO);
            if (findLevelIt != _messageStack.end()) countMess += findLevelIt.value().size();
        }
        if (level & LixoftObject::WARNING)
        {
            QMap<int,QVector<QString> >::const_iterator findLevelIt = _messageStack.find(LixoftObject::WARNING);
            if (findLevelIt != _messageStack.end()) countMess += findLevelIt.value().size();
        }
        if (level & LixoftObject::LERROR)
        {
            QMap<int,QVector<QString> >::const_iterator findLevelIt = _messageStack.find(LixoftObject::LERROR);
            if (findLevelIt != _messageStack.end()) countMess += findLevelIt.value().size();
        }
        if (level & LixoftObject::CRITICAL)
        {
            QMap<int,QVector<QString> >::const_iterator findLevelIt = _messageStack.find(LixoftObject::CRITICAL);
            if (findLevelIt != _messageStack.end()) countMess += findLevelIt.value().size();
        }
        if (level & LixoftObject::DEBUG)
        {
            QMap<int,QVector<QString> >::const_iterator findLevelIt = _messageStack.find(LixoftObject::DEBUG);
            if (findLevelIt != _messageStack.end()) countMess += findLevelIt.value().size();
        }
        if (level & LixoftObject::TRACE)
        {
            QMap<int,QVector<QString> >::const_iterator findLevelIt = _messageStack.find(LixoftObject::TRACE);
            if (findLevelIt != _messageStack.end()) countMess += findLevelIt.value().size();
        }
        return countMess;
    }

    int LixoftObject::countExceptions() const
    {
        return _exceptionStack.size();
    }

    void LixoftObject::wait() const
    {
        _waiter->lock();
    }

    void LixoftObject::notify() const
    {
        _waiter->unlock();
    }



    QString LixoftObject::toString() const
    {
     #ifndef WIN32
        int     status;
        char   *realname;
        realname = abi::__cxa_demangle(typeid(*this).name(), 0, 0, &status);
        QString demangledName = realname;
        free(realname); // c allocation
        return (QString("Class typename [") + demangledName + "]\n");
      #else

        return (QString("Class typename [") + typeid(*this).name() + "]\n");
      #endif
    }

    const LixoftObject& LixoftObject::null()
    { return _nullObject; }


    bool LixoftObject::isNull() const
    { return _isNull; }

    void LixoftObject::prune() const
    {
        // prune messages
        for (QMap<int,QVector<QString> >::iterator mit = _messageStack.begin();
             mit != _messageStack.end();
             mit++)
        {
            if (mit.value().size() > _pruneLevel)
            {
                int pruneSize = mit.value().size() - _pruneLevel;
                mit.value().remove(0,pruneSize);
            }
        }
        // prune exception
        if (_exceptionStack.size() > _pruneLevel)
        {
            int pruneSize = _exceptionStack.size() - _pruneLevel;
            _exceptionStack.remove(0,pruneSize);
        }
    }
}
}
