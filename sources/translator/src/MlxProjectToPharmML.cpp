#ifndef NDEBUG
#define BOOST_SPIRIT_DEBUG
#endif

#define NOMINMAX

#include "lixoft/translator/MlxProjectToPharmML.h"

#include <sstream>

namespace lixoft {
namespace translator {

MlxProjectToPharmML::MlxProjectToPharmML(const QString& sourceFile,
                                         const QString& output,
                                         const QVector<QPair<QString,QString> >& options,
                                         const  int & outputType):
  LanguageTranslator(sourceFile,
                     output,
                     options,
                     outputType)
{

}

MlxProjectToPharmML::~MlxProjectToPharmML()
{

}

void MlxProjectToPharmML::checkOptions()
{
  LanguageTranslator::checkFileAndDirectory("lixoft::translator::MlxProjectToPharmML");
}

void MlxProjectToPharmML::translate()
{
  /* do nothing : project is directly serialized */
}

class EmptyXML :public mlxPoco::Util::XMLConfiguration
{
public:
  EmptyXML(const std::string rootName)
  { loadEmpty(rootName);}
  mlxPoco::XML::Element* getDocument()
  {
    return  _pDocument->documentElement();
  }
};


void MlxProjectToPharmML::serialize()
{
  EmptyXML mlxEmpyRoot("mlxRoot");
  Node * mlxRoot = static_cast<Node*>(mlxEmpyRoot.getDocument());
  Element * mlxDocument;
  QString outputXML;
  if(! parseMlxtranFile(_sourceFile.toUtf8().data(),mlxDocument,mlxRoot,outputXML)){
    std::istringstream istr(outputXML.toUtf8().data());
    mlxEmpyRoot.load(istr);
    mlxDocument = mlxEmpyRoot.getDocument();
  }
  // convert xmlReader to Pharmml
  PharmXmlConverter xmlWriter(_sourceFile);
  xmlWriter.translate(mlxDocument);
  if (_outputFormat & LanguageTranslator::OUTPUT_IS_A_FILE)
  {
    xmlWriter.saveWithXmlDeclaration(_output.toUtf8().data());
  }
  else
  {
    QString outputProject = _output+"/"+_outBaseName;
    QDir().mkdir(outputProject);
    if (QFileInfo(outputProject).exists() && QFileInfo(outputProject).isDir())
    {
      QString xmlxFile = _output+"/"+_outBaseName + "/" + _outBaseName + ".xml";
      xmlWriter.saveWithXmlDeclaration(xmlxFile.toUtf8().data());
    }
  }
}

using mlxPoco::XML::NodeList;
using mlxPoco::XML::Text;

namespace{

long unsigned int getTokenId(const TokenPairs::const_iterator& iter)
{
  return iter->first;
}

std::string getTokenValue(const TokenPairs::const_iterator& iter)
{
  return iter->second;
}

void copyPkmodelArgument(TokenPairs::const_iterator& tokenBegin, const TokenPairs::const_iterator & tokenEnd, TokenExpression & tokenExpression)
{
  for(TokenPairs::const_iterator token = tokenBegin; token != tokenEnd;  ++token)
  {
    if(token->first != COMMA && token->first != PCLOSE){
      tokenExpression.second.push_back(*token);
    }
    else{
      tokenBegin = token;
      break;
    }
  }
}

void appendComma(TokenExpression & tokenExpression)
{
  if(tokenExpression.second.size()>1){
    tokenExpression.second.push_back(TokenPair(COMMA,","));
  }
}

template <typename iterator>
void checkBinOperation(iterator &localIter0, const iterator &tokenEnd, iterator& iter, const int & nOperations)
{
  localIter0 = iter;
  int iOperations =0;
  if(nOperations)
  {
    for(localIter0 = iter; localIter0!= tokenEnd; ++localIter0)
    {
      if(iOperations== nOperations -1) break;
      iOperations++;
    }
  }
  unsigned long int  id = getTokenId(iter);
  if(id == POPEN||id == BOPEN) { iter++; }
}

template <typename iterator>
void getClosedParenthesis(iterator &localIter, const iterator &tokenBegin, const iterator &tokenEnd, int & parOpen)
{
  for(localIter = tokenBegin; localIter!= tokenEnd; ++localIter)
  {
    long unsigned int localId = getTokenId(localIter);
    if(localId == POPEN|| (localId >= MATHUNARYFUNC && localId <= USERFUNC)) { parOpen++; }
    else if (localId == PCLOSE) { parOpen--; }
    if(!parOpen) {
      break;
    }
  }
}

template <typename iterator>
void findArgumentEnd(iterator& argEnd, const iterator& tokenBegin, const iterator& tokenEnd)
{
  int parOpen = 0;
  for(argEnd = tokenBegin; argEnd!= tokenEnd; ++argEnd)
  {
    long unsigned int argEndId = getTokenId(argEnd);
    if(argEndId==POPEN|| (argEndId >= MATHUNARYFUNC && argEndId <= USERFUNC)){
      parOpen++;
    }
    else if(argEndId==PCLOSE&&parOpen){
      parOpen--;
    }
    if(argEndId == COMMA && !parOpen){
      break;
    }
  }
}

void findCatNames(const Node* mlxtProject, QVector<QString> &catNames)
{
  if(catNames.size())
  {
    catNames = QVector<QString>(0);
  }
  const  Node* mlxCovariateDL  = getChildNode(mlxtProject,"covariateDefinitionList");
  if(mlxCovariateDL)
  {
    const  NodeList* mlxCovariateDefs   = getChildNodesByTagName(mlxCovariateDL,"covariateDefinition");
    if(mlxCovariateDefs)
    {
      for(unsigned iDef = 0; iDef< mlxCovariateDefs->length(); iDef++)
      {
        const Node* mlxCovDef =  mlxCovariateDefs->item(iDef);
        if(mlxCovDef)
        {
          const NamedNodeMap* mlxCovAttr = mlxCovDef->attributes();
          if(mlxCovAttr)
          {
            const Node* type = mlxCovAttr->getNamedItem("type");
            if(type)
            {
              std::string typeName(type->nodeValue());
              if(typeName == "categorical"){
                catNames.push_back(QString(mlxCovAttr->getNamedItem("columnName")->nodeValue().c_str()));
              }
            }
          }
        }
      }
    }
  }
}

void  findDataVariableInObsModel(bool & isCurrentEventNode, NodeList* observationModels, const std::string& observationName, const std::string& dataName, Element*& data, Element*& model)
{//find existing event node for current observation
  for(unsigned obsMod = 0; obsMod < observationModels->length(); obsMod++)
  {
    NodeList*  modelList = static_cast<Element*>(observationModels->item(obsMod))->getElementsByTagName("Discrete");
    if(modelList)
    {
      for(unsigned  iModel = 0; iModel < modelList->length(); iModel++)
      {
        model = static_cast<Element*>(modelList->item(iModel));
        NodeList* dataList  = static_cast<Element*>(model)->getElementsByTagName(dataName);
        if(dataList)
        {
          for(unsigned  iData = 0; iData < dataList->length(); iData++)
          {
            data = static_cast<Element*>(dataList->item(iData));
            Element* variable = data->getElementById(observationName,"symbId");
            if(variable)
            {
              isCurrentEventNode = true;
              data->removeChild(variable);
              break;
            }
          }
          if(isCurrentEventNode){
            break;
          }
        }
      }
      if(isCurrentEventNode){
        break;
      }
    }
  }
}

void insertPopParameter(Node*& lastPopParameter, Element* populationParameter,Element* parameterModel)
{
  if(lastPopParameter)
  {
    Node * NextToLastPopParam = 0;
    if(lastPopParameter){
      NextToLastPopParam = lastPopParameter->nextSibling();
    }
    if(NextToLastPopParam){
      lastPopParameter = parameterModel->insertBefore(populationParameter,NextToLastPopParam);
    }
    else{
      lastPopParameter =  parameterModel->appendChild(populationParameter);
    }
  }
  else
  {
    lastPopParameter =  parameterModel->firstChild();
    if(lastPopParameter){
      lastPopParameter =  parameterModel->insertBefore(populationParameter,lastPopParameter);
    }
    lastPopParameter = parameterModel->appendChild(populationParameter);
  }
}

bool appendDataSetBlkRef(Element*  externalDataSet, const std::string & colName,  const std::string & blkIdRefName)
{
  if(externalDataSet)
  {
    NodeList* columnMappingList = externalDataSet->getElementsByTagName("design:ColumnMapping");
    if(columnMappingList)
    {
      for(unsigned iCol = 0; iCol < columnMappingList->length(); iCol++)
      {
        Element* symbRef = static_cast<Element*>(columnMappingList->item(iCol))->getElementById(colName,"symbIdRef");
        if(symbRef)
        {
          symbRef->setAttribute("blkIdRef",blkIdRefName);
          return true;
        }
        if (colName=="y")
        {
          symbRef = static_cast<Element*>(columnMappingList->item(iCol))->getElementById("Y","symbIdRef");
          if(symbRef)
          {
            symbRef->setAttribute("blkIdRef",blkIdRefName);
            symbRef->setAttribute("symbIdRef","y");
            return true;
          }
        }
      }
    }
  }
  return false;
}

template <typename iterator>
void getOperationsSequence(iterator& localIter, const iterator& tokenEnd, int & nOperations)
{
  int parOpen = 0;
  for(; localIter!= tokenEnd; ++localIter)
  {
    long unsigned int localId = getTokenId(localIter);
    if(localId == POPEN||localId == BOPEN || (localId >= MATHUNARYFUNC && localId <= USERFUNC)) { parOpen++; }
    else if (parOpen &&(localId == PCLOSE || localId == BCLOSE)) { parOpen--; }
    if(!parOpen&& (localId<=RELOP ||localId == PCLOSE || localId == BCLOSE))
    {
      if( localId == PCLOSE || localId == BCLOSE){
        nOperations++;
      }
      break;
    }
    nOperations++;
  }
}

void getVariableName(const TokenExpressions::iterator & expression, std::set<std::string>& variables)
{
  if(expression->first == "equation"){
    variables.insert(expression->second.begin()->second);
  }
}

void mapObsModelParamInStructModel(Element* observationModel, const std::vector<std::string> & declaredVariables)
{
  // find parameters  without  "blkIdRef"  in observation model attributes  and look for their declaration in structural model
  NodeList * children = observationModel->childNodes();
  if(children)
  {
    for(unsigned iChild = 0; iChild < children->length(); iChild++)
    {
      Element *  child  = static_cast<Element*>(children->item(iChild));
      if(child->nodeType() == Node::ELEMENT_NODE)
      {
        NodeList * symbRefList = child->getElementsByTagName("ct:SymbRef");
        if(symbRefList)
        {
          for(unsigned iSymb = 0; iSymb < symbRefList->length(); iSymb++)
          {
            Element * symbol = static_cast<Element*>(symbRefList->item(iSymb));
            if(!symbol->hasAttribute("blkIdRef"))
            {
              std::string symbIdName(symbol->getAttribute("symbIdRef"));
              for(unsigned iVar = 0; iVar<declaredVariables.size(); iVar++)
              {
                if(declaredVariables[iVar]== symbIdName)
                {
                  symbol->setAttribute("blkIdRef","sm");
                  break;
                }
              }
            }
          }
        }
        mapObsModelParamInStructModel(child,declaredVariables);
      }
    }
  }
}

void mapModelParamInDataSet(Element* modelType, Element* externalDataSet, std::set<std::string> &paramMapped, const std::string& modelId)
{
  // find parameters  without  "blkIdRef"  attributes  and look for their declaration in dataset
  NodeList * children = modelType->childNodes();
  if(children)
  {
    for(unsigned iChild = 0; iChild < children->length(); iChild++)
    {
      Element *  child  = static_cast<Element*>(children->item(iChild));
      if(child->nodeType() == Node::ELEMENT_NODE)
      {
        NodeList * symbRefList = child->getElementsByTagName("ct:SymbRef");
        if(symbRefList)
        {
          for(unsigned iSymb = 0; iSymb < symbRefList->length(); iSymb++)
          {
            Element * symbol = static_cast<Element*>(symbRefList->item(iSymb));
            if(!symbol->hasAttribute("blkIdRef"))
            {
              std::string symbIdName(symbol->getAttribute("symbIdRef"));
              if(appendDataSetBlkRef(externalDataSet,symbIdName,modelId))
              {
                paramMapped.insert(symbIdName);
              }
            }
          }
        }
        mapModelParamInDataSet(child,externalDataSet,paramMapped,modelId);
      }
    }
  }
}

bool cleanMlxPath(std::string & path)
{
  QString outpath(path.c_str());
  if (outpath.startsWith("%MLXPROJECT%//"))
  {
    outpath.remove(QString("%MLXPROJECT%//"));
    path = std::string(outpath.toUtf8().data());
    return true;
  }
  else if(outpath.startsWith("%MLXPROJECT%/"))
  {
    outpath.remove(QString("%MLXPROJECT%/"));
    path = std::string(outpath.toUtf8().data());
    return true;
  }
  else if (outpath.startsWith("%MLXPROJECT%"))
  {
    outpath.remove(QString("%MLXPROJECT%"));
    path = std::string(outpath.toUtf8().data());
    return true;
  }
  else {
    return false;
  }
}

}//unnamespace

void  PharmXmlConverter::appendDescription(Element* element, const std::string comments)
{
  Element * description = _pDocument->createElement("ct:Description");
  description->appendChild(_pDocument->createTextNode(comments));
  element->appendChild(description);
}

void  PharmXmlConverter::createStructualModel(Element*modelDefinition, Element* &structuralModel)
{
  structuralModel =  _pDocument->createElement("StructuralModel") ;
  structuralModel->setAttribute("blkId","sm");
  Node *  observationModel = modelDefinition->getChildElement("ObservationModel");
  if(observationModel){
    modelDefinition->insertBefore(structuralModel,observationModel);
  }
  else{
    modelDefinition->appendChild(structuralModel);
  }
}

void PharmXmlConverter::translate(const Element* mlxDocument)
{
  AutoPtr<Element>  pharmml =  _pDocument->documentElement();
  AutoPtr<Element> modellingSteps = _pDocument->createElement("ModellingSteps");
  modellingSteps->setAttribute("xmlns","http://www.pharmml.org/pharmml/0.8/ModellingSteps");
  AutoPtr<Element>  modelDefinition = _pDocument->createElement("ModelDefinition");
  modelDefinition->setAttribute("xmlns","http://www.pharmml.org/pharmml/0.8/ModelDefinition");
  AutoPtr<Element>  trialDesign = _pDocument->createElement("design:TrialDesign");
  trialDesign->setAttribute("xmlns","http://www.pharmml.org/pharmml/0.8/TrialDesign");
  const  Node* mlxProject = getChildNode(mlxDocument,"project");

  if(mlxProject)
  {
    //***** data *****
    const  Node* mlxData = getChildNode(mlxProject,"data");
    QVector<QString> catNamesInData;
    Element*  externalDataSet = 0;
    if(mlxData)
    {
      externalDataSet = _pDocument->createElement("design:ExternalDataSet");
      externalDataSet->setAttribute("toolName","MONOLIX");
      externalDataSet->setAttribute("oid","MLX_ds");
      AutoPtr<Element> dataSet = _pDocument->createElement("DataSet");
      dataSet->setAttribute("xmlns","http://www.pharmml.org/pharmml/0.8/Dataset");
      AutoPtr<Element> definition = _pDocument->createElement("Definition");
      const AutoPtr<NamedNodeMap> dataAttr = mlxData->attributes();
      if(dataAttr)
      {
        bool isValidData = true;
        std::string mlxPath = dataAttr->getNamedItem("uri")->nodeValue();
        if(mlxPath =="/"){
          isValidData = false;
        }
        Node* mlxColumnDelimiter = dataAttr->getNamedItem("columnDelimiter");
        std::string  pharmmlDelimiter;
        if(mlxColumnDelimiter){
          pharmmlDelimiter = setDataDelimiter(mlxColumnDelimiter->nodeValue());
        }
        cleanMlxDataPath(mlxPath);
        std::string mlxHeaders = dataAttr->getNamedItem("headers")->nodeValue();
        if(mlxHeaders =="") {
          isValidData = false;
        }
        if(isValidData)
        {
          QStringList vHeaders(QString(mlxHeaders.c_str()).split(","));
          findDataParam(vHeaders,mlxProject,catNamesInData);
          for(int iHead =0; iHead< vHeaders.size();iHead++)
          {
            std::pair<std::string,std::string> colTypes = pharmmlColTypes(vHeaders[iHead]);
            AutoPtr<Element> column = _pDocument->createElement("Column");
            column->setAttribute("columnId",vHeaders[iHead].toUtf8().data());
            column->setAttribute("columnType",colTypes.first);
            column->setAttribute("valueType",colTypes.second);
            column->setAttribute("columnNum", (QString::number(iHead+1)).toUtf8().data());
            definition->appendChild(column);
            Element* columnMapping  = 0;
            createColumMapping(columnMapping,vHeaders[iHead].toUtf8().data());

            if(_dataSymb.size())
            {
              if(!vHeaders[iHead].compare("COV")||!vHeaders[iHead].compare("CAT")){
                column->setAttribute("columnId",_dataSymb[iHead]);
              }
              else if(_dataSymb[iHead] != std::string(vHeaders[iHead].toUtf8().data()))
              {
                appendSymbRef(columnMapping,_dataSymb[iHead]);
                externalDataSet->appendChild(columnMapping);
              }
            }
            else{

            }
          }
          dataSet->appendChild(definition);
          AutoPtr<Element> externalFile = _pDocument->createElement("ExternalFile");
          externalFile->setAttribute("oid","id");
          AutoPtr<Element> path = _pDocument->createElement("path");
          Text* textNode = _pDocument->createTextNode(mlxPath);
          path->appendChild(textNode);
          externalFile->appendChild(path);
          if(QString(mlxPath.c_str()).toLower().endsWith("csv"))
          {
            AutoPtr<Element> format = _pDocument->createElement("format");
            textNode = _pDocument->createTextNode("CSV");
            format->appendChild(textNode);
            externalFile->appendChild(format);
          }
          AutoPtr<Element> delimiter = _pDocument->createElement("delimiter");
          textNode = _pDocument->createTextNode(pharmmlDelimiter);
          delimiter->appendChild(textNode);
          externalFile->appendChild(delimiter);
          dataSet->appendChild(externalFile);
          externalDataSet->appendChild(dataSet);
          trialDesign->appendChild(externalDataSet);
        }
      }
    }

    //*****  parameter function *****
    const Node* mlxFunctionDL = getChildNode(mlxProject,"functionDefinitionList");
    if(mlxFunctionDL)
    {
      const  NodeList* mlxFunctionDefs   = getChildNodesByTagName(mlxFunctionDL ,"functionDefinition");
      if(mlxFunctionDefs)
      {
        for(unsigned iDef = 0; iDef< mlxFunctionDefs->length(); iDef++)
        {
          const Node* mlxFunctionDefinition =  mlxFunctionDefs->item(iDef);
          if(mlxFunctionDefinition)
          {
            Element* functionDef;
            createFunctionDefNode(functionDef);
            const AutoPtr<NamedNodeMap> functionAttr = mlxFunctionDefinition->attributes();
            if(functionAttr)
            {
              const  Node* name =  functionAttr->getNamedItem("name");
              std::string nameFunc;
              if(name)
              {
                nameFunc = name->nodeValue();
                functionDef->setAttribute("name",nameFunc);
              }
              const  Node* mlxId =  functionAttr->getNamedItem("id");
              if(mlxId)
              {
                std::string id(mlxId->nodeValue());
                functionDef->setAttribute("symbId",id);
                _userFuncIds.push_back(id);
                if(!name){
                  nameFunc = mlxId->nodeValue();
                }
              }
              const Node* function =  functionAttr->getNamedItem("function");
              if(function)
              {
                std::string functionVal(function->nodeValue());
                std::set<std::string> mlxFuncArgs ;
                getMlxtranFuncArgs(functionVal, mlxFuncArgs);
                if(mlxFuncArgs.size())
                {
                  for(std::set<std::string>::const_iterator it = mlxFuncArgs.begin(); it !=mlxFuncArgs.end(); ++it){
                    appendFunctionArg(functionDef,*it);
                  }
                }
                AutoPtr<Element> definition = _pDocument->createElement("Definition");
                functionVal = "=  "+functionVal;
                try{
                  tokenizeToPharmml(functionVal,definition);
                }
                catch(const lixoft::exception::Exception & err)
                {
                  throwNow(lixoft::exception::Exception("lixoft::translator::MlxProjectToPharmML::translate",
                                                        "translating function definition '"+ nameFunc +"'\nfile: " + err.file()+
                                                        ",  line "+ (QString::number(err.line())).toUtf8().data()+ ":\n" +err.message(),
                                                        THROW_LINE_FILE));
                }
                functionDef->appendChild(definition);
              }
              pharmml->appendChild(functionDef);
            }
          }
        }
      }
    }

    //***** variabilityLevelDefinitionList *****
    const Node* mlxVariabilityLevelDL  = getChildNode(mlxProject,"variabilityLevelDefinitionList");
    if(mlxVariabilityLevelDL)
    {
      const  NodeList* mlxVariabilityDefs   = getChildNodesByTagName(mlxVariabilityLevelDL,"variabilityLevelDefinition");
      if(mlxVariabilityDefs)
      {
        AutoPtr<Element> variabilityModel = _pDocument->createElement("VariabilityModel");
        const std::string  blkIdRefName("vm_mdl");
        variabilityModel->setAttribute("blkId",blkIdRefName);
        variabilityModel->setAttribute("type","parameterVariability");
        for(unsigned iDef = 0; iDef< mlxVariabilityDefs->length(); iDef++)
        {
          const NamedNodeMap* mlxVarAttr = mlxVariabilityDefs->item(iDef)->attributes();
          const Node* mlxLevel =  mlxVarAttr->getNamedItem("level");
          std::string mlxLevelType(mlxLevel->getNodeValue());
          AutoPtr<Element> variabilityLevel = _pDocument->createElement("Level");
          std::string levelId("ID");
          if(mlxLevelType != "1")
          {
            QString levNum(QString::number(QString(mlxLevelType.c_str()).toInt()-1));
            levelId = "IOV"+std::string(levNum.toUtf8().data());
            NodeList * previousVarList = variabilityModel->getElementsByTagName("Level");
            if(previousVarList)
            {
              Element* parentVar =  static_cast<Element*>(previousVarList->item(previousVarList->length()-1));
              parentVar->setAttribute("referenceLevel","true");
              AutoPtr<Element> parentLevel = _pDocument->createElement("ParentLevel");
              appendSymbRef(parentLevel,parentVar->getAttribute("symbId"));
              variabilityLevel->appendChild(parentLevel);
            }
          }
          variabilityLevel->setAttribute("symbId",levelId);
          appendDataSetBlkRef(externalDataSet,levelId,blkIdRefName);
          variabilityModel->appendChild(variabilityLevel);
          modelDefinition->appendChild(variabilityModel);
        }
      }
    }
    else
    {
      AutoPtr<Element> variabilityModel = _pDocument->createElement("VariabilityModel");
      const std::string  blkIdRefName("vm_mdl");
      variabilityModel->setAttribute("blkId",blkIdRefName);
      variabilityModel->setAttribute("type","parameterVariability");
      AutoPtr<Element> variabilityLevel = _pDocument->createElement("Level");
      std::string levelId("ID");
      variabilityLevel->setAttribute("symbId",levelId);
      appendDataSetBlkRef(externalDataSet,levelId,blkIdRefName);
      variabilityModel->appendChild(variabilityLevel);
      modelDefinition->appendChild(variabilityModel);
    }
    //***** covariateDefinitionList *****
    const  Node* mlxCovariateDL  = getChildNode(mlxProject,"covariateDefinitionList");
    Element *  structuralModel = 0;
    createStructualModel(modelDefinition, structuralModel);
    bool isVariableList = false;
    bool isCovariate = false;
    std::map<std::string, std::pair<bool,std::map<std::string,bool> > >catCovInfos;
    if(mlxCovariateDL)
    {
      const  NodeList* mlxCovariateDefs   = getChildNodesByTagName(mlxCovariateDL,"covariateDefinition");
      if(mlxCovariateDefs)
      {
        AutoPtr<Element> covariateModel = _pDocument->createElement("CovariateModel");
        const std::string blkIdRefName("cm");
        covariateModel->setAttribute("blkId",blkIdRefName);
        for(unsigned iDef = 0; iDef< mlxCovariateDefs->length(); iDef++)
        {
          const Node* mlxCovDef =  mlxCovariateDefs->item(iDef);
          if(mlxCovDef)
          {
            std::string nameCatStr;
            AutoPtr<Element> covariate = _pDocument->createElement("Covariate");
            const  Node* mlxGroupList = getChildNode(mlxCovDef,"groupList");
            const  NamedNodeMap* mlxCovAttr = mlxCovDef->attributes();
            const   Node* name = mlxCovAttr->getNamedItem("name");
            if(mlxCovAttr)
            {
              const  Node* columnName  =  mlxCovAttr->getNamedItem("columnName");
              QString nameCategory;
              if(columnName)
              {
                nameCategory = QString(columnName->nodeValue().c_str());
                nameCatStr = nameCategory.toUtf8().data();
                covariate->setAttribute("symbId",nameCatStr);
                appendDataSetBlkRef(externalDataSet,nameCatStr,blkIdRefName);
              }
              const  Node* index = mlxCovAttr->getNamedItem("index");
              if(index){
                covariate->setAttribute("index",index->nodeValue());
              }
              const  Node* type = mlxCovAttr->getNamedItem("type");
              if(type)
              {
                isCovariate = true;
                std::string typeName(type->nodeValue());
                bool isCatNameMapped = false;
                Element * columnMapping = 0 ;
                Element * categoryMapping = 0 ;
                if(findCategoryMapping(externalDataSet,columnMapping,nameCatStr)){
                  categoryMapping = _pDocument->createElement("ds:CategoryMapping");
                }
                if(typeName == "categorical")
                {
                  catCovInfos[nameCatStr].first = false;
                  AutoPtr<Element> categorical  = _pDocument->createElement("Categorical");
                  const Node* reference = mlxCovAttr->getNamedItem("reference");
                  if(_categories.size())
                  {
                    for(unsigned catRank = 0; catRank<_categories.size();catRank++)
                    {
                      if(!catNamesInData[catRank].compare(nameCategory))
                      {
                        for(unsigned icat = 0; icat<_categories[catRank].size(); icat++)
                        {
                          AutoPtr<Element> category  = _pDocument->createElement("Category");
                          std::string catName(_categories[catRank][icat]);
                          isCatNameMapped = setCategoricalName(category,categoryMapping,catName,nameCatStr);
                          catCovInfos[nameCatStr].second[catName] = false;
                          if(reference)
                          {
                            appendDescription(category,"reference = "+reference->getNodeValue());
                            catCovInfos[nameCatStr].first = true;
                            catCovInfos[nameCatStr].second[catName] = true;
                          }
                          categorical->appendChild(category);
                        }
                        break;
                      }
                    }
                  }
                  if(isCatNameMapped){
                    columnMapping->appendChild(categoryMapping);
                  }
                  if(mlxGroupList)
                  {
                    const NodeList* mlxGroups   = getChildNodesByTagName(mlxGroupList,"group");
                    if(mlxGroups)
                    {
                      AutoPtr<Element> transCovariate = _pDocument->createElement("Covariate");
                      std::string transCovariateName(name->nodeValue());
                      transCovariate-> setAttribute("symbId",transCovariateName);
                      catCovInfos[transCovariateName].first = false;
                      AutoPtr<Element> transCategorical= _pDocument->createElement("Categorical");
                      transCovariate-> appendChild(transCategorical);
                      covariateModel-> appendChild(transCovariate);
                      for(unsigned iGroup = 0; iGroup< mlxGroups->length(); iGroup++)
                      {
                        AutoPtr<Element> category  = _pDocument->createElement("Category");
                        const Node* mlxGroup =  mlxGroups->item(iGroup);
                        const  NamedNodeMap* groupAttr = mlxGroup->attributes();
                        if(groupAttr)
                        {
                          std::string groupName (groupAttr->getNamedItem("name")->nodeValue());
                          Element*  variable  = 0 ;
                          createVariableNode(variable,groupName,"string");
                          appendDescription(variable,"covIdRef = "+nameCatStr);
                          AutoPtr<Element>  assign  = _pDocument->createElement("ct:Assign");
                          const Node* mlxGroupVal =  groupAttr->getNamedItem("values");
                          if(mlxGroupVal)
                          {
                            QStringList  mlxVals (QString(mlxGroupVal->nodeValue().c_str()).split(",",QString::SkipEmptyParts));
                            if(mlxVals.size()>1)
                            {
                              AutoPtr<Element> vector = _pDocument->createElement("ct:Vector");
                              AutoPtr<Element>  vectorElt = _pDocument->createElement("ct:VectorElements");
                              for(int val = 0; val <  mlxVals.size(); val++)
                              {
                                AutoPtr<Element> catRef =  _pDocument->createElement("ct:String");
                                std::string renamedCatName(std::string(mlxVals[val].toUtf8().data()));
                                if(renamedCatName.size()==1){
                                  renamedCatName = nameCatStr+"_cat_"+renamedCatName;
                                }
                                catRef->appendChild(_pDocument->createTextNode(renamedCatName));
                                vectorElt->appendChild(catRef);
                              }
                              vector->appendChild(vectorElt);
                              assign->appendChild(vector);
                            }
                            else
                            {
                              AutoPtr<Element> catRef =  _pDocument->createElement("ct:String");
                              std::string renamedCatName(std::string(mlxVals[0].toUtf8().data()));
                              if(renamedCatName.size()==1){
                                renamedCatName = nameCatStr +"_cat_"+renamedCatName;
                              }
                              catRef->appendChild(_pDocument->createTextNode(renamedCatName));
                              assign->appendChild(catRef);
                            }
                            variable->appendChild(assign);
                            structuralModel->appendChild(variable);
                            isVariableList = true;
                          }
                          category->setAttribute("catId",groupName);
                          catCovInfos[transCovariateName].second[groupName] = false;
                          const Node* reference = groupAttr->getNamedItem("reference");
                          if(reference)
                          {
                            appendDescription(category,"reference = "+reference->getNodeValue());
                            catCovInfos[transCovariateName].second[groupName] = true;
                            catCovInfos[transCovariateName].first = true;
                          }
                          transCategorical->appendChild(category);
                        }
                      }
                    }
                  }
                  covariate->appendChild(categorical);
                }
                else if(typeName == "latent")
                {
                  AutoPtr<Element> categorical  = _pDocument->createElement("Categorical");
                  catCovInfos[nameCatStr].first = false;
                  const Node* mlxCategories  = mlxCovAttr->getNamedItem("categories");
                  if(mlxCategories)
                  {
                    QStringList catValues(QString(mlxCategories->nodeValue().c_str()).split(",",QString::SkipEmptyParts));
                    for(int cval = 0; cval< catValues.size(); cval++)
                    {
                      AutoPtr<Element> category  = _pDocument->createElement("Category");
                      std::string catName(catValues[cval].toUtf8().data());
                      isCatNameMapped = setCategoricalName(category,categoryMapping,catName,nameCatStr);
                      categorical->appendChild(category);
                    }
                  }
                  covariate->appendChild(categorical);
                  if(isCatNameMapped){
                    columnMapping->appendChild(categoryMapping);
                  }
                }
                else if(typeName == "continuous")
                {
                  AutoPtr<Element> continuous  = _pDocument->createElement("Continuous");
                  const Node* centeredBy = mlxCovAttr->getNamedItem("centeredBy");
                  const Node* mlxTransformation = mlxCovAttr->getNamedItem("transformation");
                  if(mlxTransformation)
                  {
                    AutoPtr<Element> transCovariate = _pDocument->createElement("TransformedCovariate");
                    AutoPtr<Element> transformation = _pDocument->createElement("Transformation");
                    transCovariate->setAttribute("symbId",name->nodeValue());
                    transformation->appendChild(transCovariate);
                    QString mlxTransfValue(mlxTransformation->nodeValue().c_str());
                    if(centeredBy)
                    {
                      QString centeredAttr(centeredBy->nodeValue().c_str());
                      centeredAttr +="(cov)";
                      QString mlxTransfValueCopy(" - " + mlxTransfValue);
                      mlxTransfValueCopy.replace("cov",centeredAttr);
                      mlxTransfValue +=  mlxTransfValueCopy;
                    }
                    mlxTransfValue.replace("cov",nameCategory);
                    try
                    {
                      std::string transfEquation("= "+std::string(mlxTransfValue.toUtf8().data()));
                      tokenizeToPharmml(transfEquation,transformation);
                    }
                    catch(const lixoft::exception::Exception & err)
                    {
                      std::string nameCov = covariate->getAttribute("symbId");
                      throwNow(lixoft::exception::Exception("lixoft::translator::MlxProjectToPharmML::translate",
                                                            "translating covariate "+ nameCov +"\nfile: " + err.file()+
                                                            ",  line "+ (QString::number(err.line())).toUtf8().data()+ ":\n" +err.message(),
                                                            THROW_LINE_FILE));
                    }
                    continuous->appendChild(transformation);
                  }
                  covariate->appendChild(continuous);
                }
              }
            }
            if(!covariateModel->getElementById(nameCatStr,"symbId")){
              covariateModel->appendChild(covariate);
            }
          }
        }
        if(!isVariableList)
        {
          modelDefinition->removeChild(structuralModel);
          structuralModel = 0;
          if(isCovariate){
            modelDefinition->appendChild(covariateModel);
          }
        }
        else{
          modelDefinition->insertBefore(covariateModel,structuralModel);
        }
      }
    }

    //***** models *****
    const Node* mlxModels = getChildNode(mlxProject,"models");
    Element * estimationStep = 0 ;
    if(mlxModels)
    {
      //***** statistical models
      const NodeList* mlxStatisticalModels  = getChildNodesByTagName(mlxModels,"statisticalModels");
      if(mlxStatisticalModels)
      {
        for(unsigned iModel = 0; iModel< mlxStatisticalModels->length();iModel++)
        {
          const Node* statisticalModel =  mlxStatisticalModels->item(iModel);
          if(statisticalModel)
          {
            const Node* mlxParameterList = getChildNode(statisticalModel,"parameterList");
            AutoPtr<Element> parameterModel =_pDocument->createElement("ParameterModel");
            parameterModel->setAttribute("blkId","pm");
            Element * parametersToEstimate = 0;
            Node* lastPopParameter = 0;
            bool isParameterModel = false;
            if(mlxParameterList)
            {
              const NodeList* mlxParameters = getChildNodesByTagName(mlxParameterList,"parameter");
              if(mlxParameters)
              {
                isParameterModel = true;
                createEstimationStepNode(estimationStep,externalDataSet);
                if(!parametersToEstimate){
                  parametersToEstimate =_pDocument->createElement("ParametersToEstimate");
                }
                for(unsigned iparam = 0; iparam < mlxParameters->length(); iparam++)
                {
                  const Node* mlxParam  = mlxParameters->item(iparam);
                  const NamedNodeMap* mlxParamAttr = mlxParam->attributes();
                  const std::string  mlxParamName = mlxParamAttr->getNamedItem("name")->getNodeValue();
                  AutoPtr<Element> populationParameter =_pDocument->createElement("PopulationParameter");
                  std::string  popName = "pop_" +mlxParamName;
                  Node * referenceNode =  mlxParamAttr->getNamedItem("reference");
                  if(referenceNode){
                    popName = referenceNode->getNodeValue();
                  }
                  populationParameter->setAttribute("symbId",popName);
                  AutoPtr<Element> individualParameter =_pDocument->createElement("IndividualParameter");
                  individualParameter->setAttribute("symbId",mlxParamName);
                  _individualParameters.push_back(mlxParamName);
                  AutoPtr<Element>  structuredModel  = _pDocument->createElement("StructuredModel");
                  bool isRandomEffect = false;
                  const Node* mlxParamTransformation = mlxParamAttr->getNamedItem("transformation");
                  writeTransformation(mlxParamTransformation,structuredModel);
                  AutoPtr<Element> linearCovariate =  _pDocument->createElement("LinearCovariate");
                  AutoPtr<Element> populationValue =  _pDocument->createElement("PopulationValue");
                  AutoPtr<Element> assign =  _pDocument->createElement("ct:Assign");
                  appendSymbRef(assign,popName,"pm");
                  populationValue->appendChild(assign);
                  linearCovariate->appendChild(populationValue);
                  const Node* mlxIntercept = getChildNode(mlxParam,"intercept");
                  if(mlxIntercept)
                  {
                    const NamedNodeMap* mlxInterceptAttr = mlxIntercept->attributes();
                    const Node* interceptInit = mlxInterceptAttr->getNamedItem("initialization");
                    const Node* interceptEstimationMethod = mlxInterceptAttr->getNamedItem("estimationMethod");
                    writeInitialization(interceptInit,parametersToEstimate,popName,"estimationMethod",interceptEstimationMethod);
                    // Add population parameter (Bayesian)
                    const Node* interceptPriorTransform  = mlxInterceptAttr->getNamedItem("priorTransform");
                    const Node* interceptPrior  = mlxInterceptAttr->getNamedItem("prior");
                    writeTransformation(interceptPriorTransform,populationParameter,interceptPrior);
                  }
                  insertPopParameter(lastPopParameter, populationParameter,parameterModel);
                  // fixed effect
                  const Node* mlxBetaList = getChildNode(mlxParam,"betaList");
                  if(mlxBetaList)
                  {
                    const NodeList* mlxBetas = getChildNodesByTagName(mlxBetaList,"beta");
                    if(mlxBetas)
                    {
                      for(unsigned ibeta = 0;ibeta < mlxBetas->length(); ibeta++)
                      {
                        const NamedNodeMap* mlxBetaAttr  = mlxBetas->item(ibeta)->attributes();
                        const std::string  mlxBetaCov = mlxBetaAttr->getNamedItem("covariate")->nodeValue();
                        AutoPtr<Element>  covariate  = _pDocument->createElement("Covariate");
                        const Node* mlxBetaPriorTransform  = mlxBetaAttr->getNamedItem("priorTransform");
                        const Node* mlxBetaPrior  = mlxBetaAttr->getNamedItem("prior");
                        writeTransformation(mlxBetaPriorTransform,structuredModel,mlxBetaPrior);
                        appendSymbRef(covariate,mlxBetaCov,"cm");
                        const Node* mlxBetaInit = mlxBetaAttr->getNamedItem("initialization");
                        std::map<std::string, std::pair<bool, std::map<std::string,bool> > >::const_iterator itCov = catCovInfos.find(mlxBetaCov);
                        const Node* mlxBetaEstimationMethod = mlxBetaAttr->getNamedItem("estimationMethod");
                        if(itCov == catCovInfos.end()||!(itCov->second.first))
                        {
                          std::string  betaName = "BETA_"+mlxParamName+"_"+mlxBetaCov;
                          AutoPtr<Element> fixedEffect =  _pDocument->createElement("FixedEffect");
                          appendSymbRef(fixedEffect,betaName,"pm");
                          covariate->appendChild(fixedEffect);
                          writeInitialization(mlxBetaInit,parametersToEstimate,betaName,"estimationMethod",mlxBetaEstimationMethod);
                          writePopParameter(betaName,lastPopParameter,parameterModel);
                        }
                        else
                        {
                          for(std::map<std::string,bool>::const_iterator itCat = (itCov->second).second.begin(); itCat != (itCov->second).second.end(); ++itCat )
                          {
                            if(!itCat->second)
                            {
                              std::string  betaName = "BETA_"+mlxParamName+"_"+itCat->first;
                              AutoPtr<Element> fixedEffect =  _pDocument->createElement("FixedEffect");
                              appendSymbRef(fixedEffect,betaName,"pm");
                              covariate->appendChild(fixedEffect);
                              writeInitialization(mlxBetaInit,parametersToEstimate,betaName,"estimationMethod",mlxBetaEstimationMethod);
                              writePopParameter(betaName,lastPopParameter,parameterModel);
                            }
                          }
                        }
                        linearCovariate->appendChild(covariate);
                      }
                    }
                  }
                  structuredModel->appendChild(linearCovariate);
                  //***** variability
                  const Node* mlxVariabilityList = getChildNode(mlxParam,"variabilityList");
                  if(mlxVariabilityList)
                  {
                    const NodeList* mlxVariability = getChildNodesByTagName(mlxVariabilityList,"variability");
                    if(mlxVariability)
                    {
                      for(unsigned ivar = 0; ivar < mlxVariability->length(); ivar++)
                      {
                        isRandomEffect = true;
                        writeVariability(mlxVariability->item(ivar),parametersToEstimate,structuredModel,parameterModel,lastPopParameter,mlxParamName);
                      }
                    }
                  }
                  else
                  {
                    const Node* mlxVariability = getChildNode(mlxParam,"variability");
                    if(mlxVariability)
                    {
                      isRandomEffect = true;
                      writeVariability( mlxVariability,parametersToEstimate,structuredModel,parameterModel,lastPopParameter,mlxParamName);
                    }
                  }
                  if(!isRandomEffect){
                    individualParameter->appendChild(assign);
                  }
                  else{
                    individualParameter->appendChild(structuredModel);
                  }
                  parameterModel->appendChild(individualParameter);
                }
              }
            }

            //***** correlation
            const Node* mlxCorrelationModelList = getChildNode(statisticalModel,"correlationModelList");
            if(mlxCorrelationModelList)
            {
              const NodeList* mlxCorrelationModels = getChildNodesByTagName(mlxCorrelationModelList,"correlationModel");
              for(unsigned iCorr = 0; iCorr< mlxCorrelationModels->length();iCorr++)
              {
                const Node* mlxCorrelationMod =  mlxCorrelationModels->item(iCorr);
                const Node* mlxCorrAttrLevelName = 0;
                const Node* mlxCorrAttrLevel = 0;
                if(mlxCorrelationMod)
                {
                  const NamedNodeMap* mlxCorrModAttr = mlxCorrelationMod->attributes();
                  if(mlxCorrModAttr)
                  {
                    mlxCorrAttrLevelName = mlxCorrModAttr->getNamedItem("levelName");
                    mlxCorrAttrLevel = mlxCorrModAttr->getNamedItem("level");
                  }
                }
                const Node* mlxBlockList = getChildNode(mlxCorrelationMod,"blockList");
                if(mlxBlockList)
                {
                  const NodeList* mlxBlocks = getChildNodesByTagName(mlxBlockList,"block");
                  isParameterModel = true;
                  for(unsigned iblock = 0; iblock< mlxBlocks->length();iblock++)
                  {
                    const NamedNodeMap* mlxBlockAttr = mlxBlocks->item(iblock)->attributes();
                    const Node* mlxBlockParam = mlxBlockAttr->getNamedItem("parameters");
                    QStringList mlxParameterNames = QString(mlxBlockParam->getNodeValue().c_str()).split(",",QString::SkipEmptyParts);
                    for(int ip1 = 0; ip1 < mlxParameterNames.size()-1; ip1 ++)
                    {
                      for(int ip2 = ip1+1 ; ip2 <mlxParameterNames.size(); ip2++)
                      {
                        AutoPtr<Element> correlation =  _pDocument->createElement("Correlation");
                        if(mlxCorrAttrLevelName || mlxCorrAttrLevel)
                        {
                          std::string descriptionArg;
                          if(mlxCorrAttrLevelName)
                          {
                            descriptionArg += "levelName = " +  mlxCorrAttrLevelName->getNodeValue()  ;
                            if(mlxCorrAttrLevel){
                              descriptionArg += ", level = " +  mlxCorrAttrLevel->getNodeValue();
                            }
                          }
                          else{
                            descriptionArg += "level = " +  mlxCorrAttrLevel->getNodeValue();
                          }
                          appendDescription(correlation,descriptionArg);
                        }
                        AutoPtr<Element> variabilityReference =  _pDocument->createElement("ct:VariabilityReference");
                        appendSymbRef(variabilityReference,"ID","vm_mdl");
                        correlation->appendChild(variabilityReference);
                        AutoPtr<Element>  pairwise = _pDocument->createElement("Pairwise");
                        AutoPtr<Element> randomVariable1 =  _pDocument->createElement("RandomVariable1");
                        AutoPtr<Element> randomVariable2 =  _pDocument->createElement("RandomVariable2");
                        std::string parName1(mlxParameterNames[ip1].toUtf8().data());
                        std::string parName2(mlxParameterNames[ip2].toUtf8().data());
                        appendSymbRef(randomVariable1,"ETA_"+parName1);
                        appendSymbRef(randomVariable2,"ETA_"+parName2);
                        pairwise->appendChild(randomVariable1);
                        pairwise->appendChild(randomVariable2);
                        AutoPtr<Element>  correlationCoefficient = _pDocument->createElement("CorrelationCoefficient");
                        AutoPtr<Element>  assign = _pDocument->createElement("ct:Assign");
                        std::string corrCoefName("CORR_"+parName1+"_"+parName2);
                        writePopParameter(corrCoefName,lastPopParameter,parameterModel);
                        appendSymbRef(assign,corrCoefName,"pm");
                        correlationCoefficient->appendChild(assign);
                        pairwise->appendChild(correlationCoefficient);
                        correlation->appendChild(pairwise);
                        parameterModel->appendChild(correlation);
                      }
                    }
                  }
                }
              }
            }

            //***** residual error (observation model)
            const Node* mlxObservationModelList = getChildNode(statisticalModel,"observationModelList");
            if(mlxObservationModelList)
            {
              const NodeList* mlxObservationModels = getChildNodesByTagName(mlxObservationModelList,"observationModel");
              if(mlxObservationModels)
              {
                unsigned lengthObsModel = mlxObservationModels->length();
                int obModelId = 0;
                for(unsigned imodel = 0; imodel< lengthObsModel;imodel++)
                {
                  AutoPtr<Element> observationModel = _pDocument->createElement("ObservationModel");
                  const Node* mlxObservationMod = mlxObservationModels->item(imodel);
                  const NamedNodeMap* mlxObservationModelAttr =  mlxObservationMod->attributes();
                  AutoPtr<Element> dataType;
                  Element* outStyle = 0;
                  Element* outStyleAssign  = 0;
                  Element* output  = 0;
                  std::string prediction="", observationName;
                  unsigned errModelsSize = 0;
                  std::string iSm("sm");
                  if(mlxObservationModelAttr)
                  {
                    const Node* mlxObsModPred = mlxObservationModelAttr->getNamedItem("pred");
                    prediction = mlxObsModPred->nodeValue();
                  }
                  if(prediction !="")
                  {
                    std::string imodelStr(QString::number(++obModelId).toUtf8().data());
                    observationModel->setAttribute("blkId","om"+imodelStr);
                    const Node* mlxObsModType = mlxObservationModelAttr->getNamedItem("type");
                    const Node* mlxObsModName = mlxObservationModelAttr->getNamedItem("name");
                    observationName = mlxObsModName->nodeValue();
                    std::string obsType(mlxObsModType->nodeValue());
                    if(obsType =="continuous"){
                      dataType = _pDocument->createElement("ContinuousData");
                      outStyle = _pDocument->createElement("Standard");
                      outStyle->setAttribute("symbId",observationName);
                      output = _pDocument->createElement("Output");
                      appendSymbRef(output,prediction,iSm);
                    }
                    else
                    {
                      dataType = _pDocument->createElement("Discrete");
                      if(obsType == "discrete")
                      {
                        outStyle = _pDocument->createElement("CountData");
                        output = _pDocument->createElement("CountVariable");
                        output->setAttribute("symbId",observationName);
                      }
                      else if(obsType == "event")
                      {
                        outStyle = _pDocument->createElement("TimeToEventData");
                        output = _pDocument->createElement("EventVariable");
                        output->setAttribute("symbId",observationName);
                      }
                      else{
                        throwNow(lixoft::exception::Exception("lixoft::translator::MlxProjectToPharmML::translate",
                                                              "unknown observation type '"+ obsType+"' not translated to Pharmml",
                                                              THROW_LINE_FILE));
                      }
                    }

                    const Node* mlxError =  getChildNode(mlxObservationMod,"error");
                    if(mlxError)
                    {
                      const NamedNodeMap* mlxErrorAttr = mlxError->attributes();
                      createEstimationStepNode(estimationStep,externalDataSet);
                      const Node* mlxErrorModAutoCorr = mlxErrorAttr->getNamedItem("autocorr");
                      if(!parametersToEstimate){
                        parametersToEstimate =_pDocument->createElement("ParametersToEstimate");
                      }
                      if(mlxErrorAttr)
                      {
                        Element*  variabilityModel = 0 ;
                        createVariabilityNode(variabilityModel,modelDefinition);
                        const std::string mlxErrorModAlias = mlxErrorAttr->getNamedItem("alias")->nodeValue();
                        const Node* mlxErrModParamList = getChildNode(mlxError,"parameterList");
                        const NodeList* mlxErrModParams = getChildNodesByTagName(mlxErrModParamList,"parameter");
                        std::vector<std::string> errorModels(1);
                        mlxErrorModel(mlxErrorModAlias, errorModels[0]);
                        //call
                        bool isStandardDeviation = false;
                        std::string varname("var");
                        TokenPairs  standardDevPairs;
                        TokenPairs  errorModelPairs;
                        createErrorModelNode(parameterModel,varname,lastPopParameter,parametersToEstimate, modelDefinition, standardDevPairs, isStandardDeviation, lengthObsModel,  mlxErrorModAlias,
                                             iSm, prediction,  observationName,  isParameterModel, mlxErrModParams, mlxErrorModAutoCorr, errorModels, outStyle, outStyleAssign,errorModelPairs);


                      }
                    }
                    if(errModelsSize <=1)
                    {
                      Node* errorModel =  outStyle->getChildElement("ErrorModel");
                      if(errorModel){
                        outStyle->insertBefore(output,errorModel);
                      }
                      else{
                        outStyle->appendChild(output);
                      }
                    }
                    else{
                      outStyle->appendChild(outStyleAssign);
                    }
                    dataType->appendChild(outStyle);
                    observationModel->appendChild(dataType);
                    modelDefinition->appendChild(observationModel);
                  }
                }
              }
            }
            if(isParameterModel)
            {
              if(structuralModel){
                modelDefinition->insertBefore(parameterModel,structuralModel);
              }
              else
              {
                Node* obsModel = modelDefinition->getChildElement("ObservationModel");
                modelDefinition->insertBefore(parameterModel,obsModel);
              }
            }
            if(parametersToEstimate){
              estimationStep->appendChild(parametersToEstimate);
            }
          }
        }
      }
      pharmml->appendChild(modelDefinition);
      pharmml->appendChild(trialDesign);
      //***** structural model
      const Node* mlxStructuralModelList = getChildNode(mlxModels,"structuralModelList");
      if(mlxStructuralModelList && isNotEmpty(mlxStructuralModelList))
      {
        const NodeList* mlxStructuralModels = getChildNodesByTagName(mlxStructuralModelList,"structuralModel");
        if(mlxStructuralModels)
        {
          for(unsigned imodel = 0; imodel< mlxStructuralModels->length();imodel++)
          {
            const NamedNodeMap* mlxStructuralModelAttr = mlxStructuralModels->item(imodel)->attributes();
            const Node* mlxStructuralModelName =  mlxStructuralModelAttr->getNamedItem("name");

            const Node* mlxStructuralModelUri =  mlxStructuralModelAttr->getNamedItem("uri");
            std::string modelDir(mlxStructuralModelUri->getNodeValue());
            QString modelName(mlxStructuralModelName->getNodeValue().c_str());
            if(modelName.startsWith("mlxt:")){
              modelName.replace("mlxt:","");
            }
            else{
              throwNow(lixoft::exception::Exception("lixoft::translator::MlxProjectToPharmML",
                                                    "Could not translate non mlxtran model '"+modelDir+"/"+std::string(modelName.toUtf8().data()) +"' to Pharmml, only mlxtan is managed",
                                                    THROW_LINE_FILE));
            }
            std::string modelPath = modelDir + "/" + std::string(modelName.toUtf8().data())+".txt";
            if(cleanMlxPath(modelPath))
            {
              QFileInfo projectPath(_sourceFile);
              modelPath =  std::string(projectPath.dir().path().toUtf8().data())+"/"+modelPath;
            }
            longitudinalToPharmml(modelPath.c_str(), modelDefinition);
            /***
             * the following are included in  the results of longitudinalToPharmml
               const Node* mlxStructuralModelId =  mlxStructuralModelAttr->getNamedItem("id");
               const Node* mlxStructuralModelOutputName =  mlxStructuralModelAttr->getNamedItem("outputName");
               const Node* mlxStructuralModelOuput =  mlxStructuralModelAttr->getNamedItem("output");
            ***/
          }
        }
      }
      else{
        longitudinalToPharmml(_sourceFile.toUtf8().data(),modelDefinition);
      }
    }
    //***** tasks
    bool isTask = false;
    const Node*  mlxSettings  = getChildNode(mlxProject,"settings");
    if(mlxSettings)
    {
      const  Node* mlxTasks  = getChildNode(mlxSettings,"tasks");
      if(mlxTasks)
      {
        const  Node* mlxScenario  = getChildNode(mlxTasks,"scenario");
        Element* estFIM = 0, *estIndiv = 0, *estPop = 0, * estLl = 0;
        int order = 0;
        Element *  toolName  = _pDocument->createElement("Property");
        toolName->setAttribute("name","software-tool-name");
        Element *  assign  = _pDocument->createElement("ct:Assign");
        Element * string =  _pDocument->createElement("ct:String");
        string->appendChild(_pDocument->createTextNode("MONOLIX"));
        assign->appendChild(string);
        toolName->appendChild(assign);
        Element *  toolVersion  = _pDocument->createElement("Property");
        toolVersion->setAttribute("name","software-tool-version");
        assign  = _pDocument->createElement("ct:Assign");
        string =  _pDocument->createElement("ct:String");
        string->appendChild(_pDocument->createTextNode("4.3.2"));
        assign->appendChild(string);
        toolVersion->appendChild(assign);
        if(mlxScenario)
        {
          const NamedNodeMap* mlxScenarioAttr = mlxScenario->attributes();
          if(mlxScenarioAttr)
          {
            const Node* mlxScenarioPopulationParameters = mlxScenarioAttr->getNamedItem("estimatePopulationParameters");
            if(mlxScenarioPopulationParameters && mlxScenarioPopulationParameters->nodeValue() == "true")
            {
              createOperationNode(estPop, "estPop",order);
              estPop->appendChild(toolName);
              estPop->appendChild(toolVersion);
            }

            const Node* mlxScenarioFisherInformationMatrix = mlxScenarioAttr->getNamedItem("estimateFisherInformationMatrix");
            if(mlxScenarioFisherInformationMatrix && mlxScenarioFisherInformationMatrix->nodeValue() == "true")
            {
              createOperationNode(estFIM, "estFIM",order);
              if(order == 1)
              {
                estFIM->appendChild(toolName);
                estFIM->appendChild(toolVersion);
              }
            }

            const Node* mlxScenarioIndividualParameters = mlxScenarioAttr->getNamedItem("estimateIndividualParameters");
            if(mlxScenarioIndividualParameters && mlxScenarioIndividualParameters->nodeValue() == "true")
            {
              createOperationNode(estIndiv, "estIndiv",order);
              if(order == 1)
              {
                estIndiv->appendChild(toolName);
                estIndiv->appendChild(toolVersion);
              }
            }

            const Node* mlxScenarioLogLikelihood = mlxScenarioAttr->getNamedItem("estimateLogLikelihood");
            if(mlxScenarioLogLikelihood && mlxScenarioLogLikelihood->nodeValue() == "true")
            {
              createOperationNode(estLl,"estLogLikelihood",order);
              AutoPtr<Element> description = _pDocument->createElement("ct:Description");
              description->appendChild(_pDocument->createTextNode("Estimate the log likelihood in the model."));
              estLl->appendChild(description);
              if(order == 1)
              {
                estLl->appendChild(toolName);
                estLl->appendChild(toolVersion);
              }
            }
            const Node* mlxScenarioComputeResults = mlxScenarioAttr->getNamedItem("computeResults");
            if(mlxScenarioComputeResults){
              appendDescription(modellingSteps, "computeResults = "+ mlxScenarioComputeResults->nodeValue());
            }
          }
        }
        const  Node* mlxIndivParamAlgo  = getChildNode(mlxTasks,"individualParameterAlgorithms");
        if(mlxIndivParamAlgo)
        {
          const NamedNodeMap* mlxIndivParamAlgoAttr = mlxIndivParamAlgo->attributes();
          if(mlxIndivParamAlgoAttr)
          {
            const Node* mlxIndivParamAlgoCondDist = mlxIndivParamAlgoAttr->getNamedItem("conditionalDistribution");
            const Node* mlxIndivParamAlgoCondMod = mlxIndivParamAlgoAttr->getNamedItem("conditionalMode");
            if(estIndiv)
            {
              AutoPtr<Element> algorithm =_pDocument->createElement("Algorithm");
              if(mlxIndivParamAlgoCondDist && mlxIndivParamAlgoCondDist->nodeValue() == "true"){
                algorithm->setAttribute("definition","conditionalDistribution");
              }
              else if(mlxIndivParamAlgoCondMod && mlxIndivParamAlgoCondMod->nodeValue() == "true"){
                algorithm->setAttribute("definition","conditionalMode");
              }
              estIndiv->appendChild(algorithm);
            }
          }
        }

        const  Node* mlxLogLikelihoodAlgo  = getChildNode(mlxTasks,"logLikelihoodAlgorithms");
        if(mlxLogLikelihoodAlgo)
        {
          const NamedNodeMap* mlxLogLikelihoodAlgoAttr = mlxLogLikelihoodAlgo->attributes();
          if(mlxLogLikelihoodAlgoAttr)
          {
            const Node* mlxLogLikelihoodImpSamp = mlxLogLikelihoodAlgoAttr->getNamedItem("importantSampling");
            const Node* importantSampling = mlxLogLikelihoodAlgoAttr->getNamedItem("linearization");
            if(estLl)
            {
              AutoPtr<Element> algorithm =_pDocument->createElement("Algorithm");
              if(mlxLogLikelihoodImpSamp && mlxLogLikelihoodImpSamp->nodeValue() == "true"){
                algorithm->setAttribute("definition","importantSampling");
              }
              else if(importantSampling && importantSampling->nodeValue() == "true"){
                algorithm->setAttribute("definition","linearization");
              }
              estLl->appendChild(algorithm);
            }
          }
        }

        const  Node* mlxFIMAlgo  = getChildNode(mlxTasks,"fisherInformationMatrixAlgorithms");
        if(mlxFIMAlgo)
        {
          const NamedNodeMap* mlxFIMAlgoAttr = mlxFIMAlgo->attributes();
          if(mlxFIMAlgoAttr)
          {
            const Node* mlxFIMAlgoStocAppr = mlxFIMAlgoAttr->getNamedItem("stochasticApproximation");
            const Node* mlxFIMAlgoLin = mlxFIMAlgoAttr->getNamedItem("linearization");
            if(estFIM)
            {
              AutoPtr<Element> algorithm =_pDocument->createElement("Algorithm");
              if(mlxFIMAlgoStocAppr && mlxFIMAlgoStocAppr->nodeValue() == "true"){
                algorithm->setAttribute("definition","stochasticApproximation");
              }
              else if(mlxFIMAlgoLin && mlxFIMAlgoLin->nodeValue() == "true"){
                algorithm->setAttribute("definition","linearization");
              }
              estFIM->appendChild(algorithm);
            }
          }
        }

        if(estFIM||estIndiv||estPop||estLl)
        {
          isTask = true;
          createEstimationStepNode(estimationStep,externalDataSet);
          if(estPop){
            estimationStep->appendChild(estPop);
          }
          if(estFIM){
            estimationStep->appendChild(estFIM);
          }
          if(estIndiv){
            estimationStep->appendChild(estIndiv);
          }
          if(estLl){
            estimationStep->appendChild(estLl);
          }
        }
      }
    }
    if(estimationStep && isTask){
      modellingSteps->appendChild(estimationStep);
    }
    // remove description from modelDefinition
    Node * description = modelDefinition->getChildElement("ct:Description");
    if(description)
    {
      modelDefinition->removeChild(description);
      Node * independendVariable  = pharmml->getChildElement("IndependentVariable");
      pharmml->insertBefore(description,independendVariable);
    }
    std::set<std::string> paramMapped ;
    // map tDose and amtDose if present in parameter model
    structuralModel = modelDefinition->getChildElement("StructuralModel");
    if(structuralModel)
    {
      bool isTdose = false, isAmtDose = false;
      setDosingKeywdAttributes(structuralModel, structuralModel, isTdose, isAmtDose);
      if( externalDataSet && (isTdose || isAmtDose)){
        mapDosingKeywdInDataset(externalDataSet,isTdose, isAmtDose);
      }
      // map unmapped parameters of structural model in dataset
      mapModelParamInDataSet(structuralModel,externalDataSet, paramMapped,"sm");
      Node* structuralModelNextToFirstChid = structuralModel->firstChild()->nextSibling();
      for(std::set<std::string>::iterator it = paramMapped.begin(); it != paramMapped.end(); ++it)
      {
        Element * variable = 0;
        createVariableNode(variable,*it,"real");
        structuralModel->insertBefore(variable,structuralModelNextToFirstChid);
      }
      // map unmapped parameters of observation models, existing in strctural model
      // find parameters  without  "blkIdRef"  in observation model attributes  and look for their declaration in structural model
      std::vector<std::string> declaredVariables;
      NodeList * elementVar = structuralModel->getElementsByTagName("ct:Variable");
      for(unsigned iVar = 0 ; iVar <  elementVar->length(); iVar++){
        declaredVariables.push_back(static_cast<Element *> (elementVar->item(iVar))->getAttribute("symbId"));
      }
      elementVar = structuralModel->getElementsByTagName("ct:DerivativeVariable");
      for(unsigned iVar = 0 ; iVar <  elementVar->length(); iVar++){
        declaredVariables.push_back(static_cast<Element *> (elementVar->item(iVar))->getAttribute("symbId"));
      }
      NodeList * obsModelList = modelDefinition->getElementsByTagName("ObservationModel");
      for(unsigned iObsMod = 0; iObsMod < obsModelList->length(); iObsMod++)
      {
        mapObsModelParamInStructModel(static_cast<Element*>(obsModelList->item(iObsMod)), declaredVariables);
      }
    }
    // map unmapped parameters of observation models in dataset
    NodeList *observationModelList = modelDefinition->getElementsByTagName("ObservationModel");
    for(unsigned iObsModel = 0; iObsModel < observationModelList->length(); iObsModel++)
    {
      paramMapped.clear();
      Element * observationModel = static_cast<Element*>(observationModelList->item(iObsModel)) ;
      std::string obsModelId(observationModel->getAttribute("blkId"));
      mapModelParamInDataSet(observationModel,externalDataSet, paramMapped,obsModelId);
      Node * DataType = observationModel->firstChild()->firstChild();
      Node* DataTypeFirstChid = DataType->firstChild();
      for(std::set<std::string>::iterator it = paramMapped.begin(); it != paramMapped.end(); ++it)
      {
        Element * variable = 0;
        createVariableNode(variable,*it,"real");
        // look into observation Model and remove declare variable if not already declared
        Element * existingVar = static_cast<Element *>(DataType)->getElementById(*it,"symbId");
        if(! existingVar){
          DataType->insertBefore(variable,DataTypeFirstChid);
        }
      }
    }
    // clean externalDataSet and remove unused columns
    if(externalDataSet)
    {
      Element * columnMapping = externalDataSet->getChildElement("design:ColumnMapping");
      unsigned columnMappingLength =  externalDataSet->getElementsByTagName("design:ColumnMapping")->length();
      unsigned iColMap = 0;
      while(iColMap < columnMappingLength)
      {
        Element * symbref = columnMapping->getChildElement("ct:SymbRef");
        Element * netxColMapping = static_cast<Element*>(columnMapping->nextSibling());
        if(symbref && ! symbref->hasAttribute("blkIdRef"))
        {
          externalDataSet->removeChild(columnMapping);
        }
        columnMapping = netxColMapping;
        iColMap++;
      }
    }

    pharmml->appendChild(modellingSteps);
  }
  else{
    throwLater(lixoft::exception::Exception("lixoft::translator::MlxProjectToPharmML",
                                            "No project found cannot translate to Pharmml",
                                            THROW_LINE_FILE));
  }
}

void PharmXmlConverter:: mapDosingKeywdInDataset(Element* externalDataSet, const bool & isTdose, const bool & isAmtDose)
{
  Element * DataSet =   externalDataSet->getChildElement("DataSet");
  Element * DataSetDefColDose = DataSet->getChildElement("Definition")->getElementById("dose","columnType");
  std::string  columnIdRefDose(DataSetDefColDose->getAttribute("columnId"));
  if(DataSetDefColDose)
  {
    if(isAmtDose)
    {
      bool isTdoseMapped = false;
      NodeList * columnMappings  =  externalDataSet->getElementsByTagName("design:ColumnMapping");
      for(unsigned iColMap = 0; iColMap < columnMappings->length(); iColMap++)
      {
        Element * tDoseNode = static_cast<Element*> (columnMappings->item(iColMap))->getElementById("amtDose_Var","symbIdRef");
        if(tDoseNode)
        {
          isTdoseMapped =true;
          break;
        }
      }
      if(!isTdoseMapped && DataSetDefColDose)
      {
        Element * columnMapping = 0 ;
        createColumMapping(columnMapping,columnIdRefDose);
        Element * piecewise = _pDocument->createElement("Piecewise");
        piecewise->setAttribute("xmlns","http://www.pharmml.org/pharmml/0.8/Dataset");
        Element * piece = _pDocument->createElement("math:Piece");
        appendSymbRef(piece,"amtDose_Var","sm");
        Element * condition =_pDocument->createElement("math:Condition");
        Element * binop = _pDocument->createElement("math:LogicBinop");
        binop->setAttribute("op","gt");
        Element* columnRef = _pDocument->createElement("ColumnRef");
        columnRef->setAttribute("columnIdRef",columnIdRefDose);
        Element * intNode = _pDocument->createElement("ct:Int");
        intNode->appendChild(_pDocument->createTextNode("0"));
        binop->appendChild(columnRef);
        binop->appendChild(intNode);
        condition->appendChild(binop);
        piece->appendChild(condition);
        piecewise->appendChild(piece);
        columnMapping->appendChild(piecewise);
        externalDataSet->insertBefore(columnMapping,DataSet);
      }
    }
    if(isTdose)
    {
      Element * DataSetDefColTime = externalDataSet->getChildElement("DataSet")->getChildElement("Definition")->getElementById("time","columnType");
      std::string columnIdRefTime(DataSetDefColTime->getAttribute("columnId"));
      Element * columnMapping = 0 ;
      createColumMapping(columnMapping,columnIdRefTime);
      Element * piecewise = _pDocument->createElement("Piecewise");
      piecewise->setAttribute("xmlns","http://www.pharmml.org/pharmml/0.8/Dataset");
      Element * piece = _pDocument->createElement("math:Piece");
      appendSymbRef(piece,"tDose_Var","sm");
      Element * condition =_pDocument->createElement("math:Condition");
      Element * binop = _pDocument->createElement("math:LogicBinop");
      binop->setAttribute("op","gt");
      Element* columnRef = _pDocument->createElement("ColumnRef");
      columnRef->setAttribute("columnIdRef",columnIdRefDose);
      Element * intNode = _pDocument->createElement("ct:Int");
      intNode->appendChild(_pDocument->createTextNode("0"));
      binop->appendChild(columnRef);
      binop->appendChild(intNode);
      condition->appendChild(binop);
      piece->appendChild(condition);
      piecewise->appendChild(piece);
      columnMapping->appendChild(piecewise);
      mlxPoco::XML::Comment * comment = _pDocument->createComment("doseTime=tDose_Var");
      externalDataSet->insertBefore(comment,DataSet);
      externalDataSet->insertBefore(columnMapping,DataSet);
    }
  }
}

void PharmXmlConverter::createColumMapping(Element*& columnMapping, const std::string & columnIdRef)
{
  columnMapping  = _pDocument->createElement("design:ColumnMapping");
  Element* columnRef = _pDocument->createElement("ColumnRef");
  columnRef->setAttribute("xmlns","http://www.pharmml.org/pharmml/0.8/Dataset");
  columnRef->setAttribute("columnIdRef",columnIdRef);
  columnMapping->appendChild(columnRef);
}

void PharmXmlConverter::setDosingKeywdAttributes(Element* structuralModel, Element* structuralModelChild, bool & isTdose, bool & isAmtDose)
{
  // find parameters "tDose" and "amtDose" if present int  structural model
  // and set their "blkIdRef"  attributes to  "sm"
  NodeList * children = structuralModelChild->childNodes();
  if(children)
  {
    for(unsigned iChild = 0; iChild < children->length(); iChild++)
    {
      Element *  child  = static_cast<Element*>(children->item(iChild));
      if(child->nodeType() == Node::ELEMENT_NODE)
      {
        std::string  symbref(child->getAttribute("symbIdRef"));

        if(symbref == "tDose")
        {
          child->setAttribute("blkIdRef","sm");
          child->setAttribute("symbIdRef","tDose_Var");
          if(! isTdose)
          {
            Element * variable;
            createVariableNode(variable,"tDose_Var","real");
            Node * sMFirstChild = structuralModel->firstChild();
            structuralModel->insertBefore(variable, sMFirstChild);
            isTdose = true;
          }
        }
        else if(symbref == "amtDose")
        {
          child->setAttribute("blkIdRef","sm");
          child->setAttribute("symbIdRef","amtDose_Var");
          if(!isAmtDose)
          {
            Element * variable = 0;
            createVariableNode(variable,"amtDose_Var","real");
            Node * sMFirstChild = structuralModel->firstChild();
            structuralModel->insertBefore(variable, sMFirstChild);
            isAmtDose = true;
          }
        }
        setDosingKeywdAttributes(structuralModel,child,isTdose, isAmtDose);
      }
    }
  }
}

QString MlxProjectToPharmML::serializeToString()
{
  return "";
}

PharmXmlConverter::~PharmXmlConverter(){}

PharmXmlConverter::PharmXmlConverter(){}

PharmXmlConverter::PharmXmlConverter(const QString &sourceFile):
  XMLConfiguration(),DOMWriter(),_sourceFile(sourceFile){writeHeader();}

void PharmXmlConverter::writePopParameter(const std::string& parameterName, Node* &lastPopParameter,Element* parameterModel)
{
  AutoPtr<Element> populationParameter =_pDocument->createElement("PopulationParameter");
  populationParameter->setAttribute("symbId",parameterName);
  insertPopParameter(lastPopParameter, populationParameter,parameterModel);
}

void PharmXmlConverter::tokenizeToPharmml(const std::string& strInput, Element* pharmml)
{
  TokenExpressions   results;
  bool iparsed =  parseMlxtranEquationSection(strInput,results);

  if(!iparsed || !results.size())
  {
    throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::tokenizeToPharmml",
                                          std::string("the parse of  section  equation  in longitudinal model failed\ncannot translate to Pharmml"),
                                          THROW_LINE_FILE));
  }
  long unsigned int prevId;
  TokenPairs::iterator tokenPairBegin ;
  TokenPairs::iterator tokenPairEnd ;
  for(TokenExpressions::iterator  expression = results.begin();  expression != results.end(); ++expression)
  {
    tokenPairBegin = expression->second.begin();
    tokenPairEnd = expression->second.end();
    prevId=0;
    writeToPharmml(tokenPairBegin,tokenPairEnd,pharmml,prevId);
  }
}

void PharmXmlConverter::writeProbaDefinition(TokenPairs &expression, Element* countData, bool isTransformed)
{
  Element * probaAssignment =   _pDocument->createElement("ProbabilityAssignment");
  Element * proba =   _pDocument->createElement("Probability");
  if(isTransformed)
  {
    proba->setAttribute("linkFunction",expression[0].second);
    expression.erase(expression.begin());
  }
  expression.begin()->second = "y";
  TokenPairs::iterator tokenPairBegin = expression.begin();
  TokenPairs::iterator tokenPairEnd = expression.end();
  long unsigned int prevId = USERFUNC;
  int parOpen = 1;
  TokenPairs::iterator  probaArgEnd;
  getClosedParenthesis(probaArgEnd, tokenPairBegin,tokenPairEnd, parOpen);
  writeToPharmml(tokenPairBegin, probaArgEnd,proba,prevId);
  // rename assignment to logic equal
  renameAssignToLogicBinop(proba);
  probaAssignment->appendChild(proba);
  ++probaArgEnd;
  prevId = USERFUNC;
  writeToPharmml(probaArgEnd,tokenPairEnd,probaAssignment,prevId);
  countData->appendChild(probaAssignment);
}

void PharmXmlConverter::writeCountProbaDefinition(TokenPairs &expression, Element* countData, bool isTransformed)
{
  Element * pmf =   _pDocument->createElement("PMF");
  if(isTransformed)
  {
    pmf->setAttribute("transform",expression[0].second);
    expression.erase(expression.begin());
  }
  expression.begin()->second = "y";
  TokenPairs::iterator tokenPairBegin = expression.begin();
  TokenPairs::iterator tokenPairEnd = expression.end();
  long unsigned int prevId = USERFUNC;
  int parOpen = 1;
  TokenPairs::iterator  probaArgEnd;
  getClosedParenthesis(probaArgEnd, tokenPairBegin,tokenPairEnd, parOpen);
  writeToPharmml(tokenPairBegin, probaArgEnd,pmf,prevId);
  // rename assignment to logic equal
  renameAssignToLogicBinop(pmf);
  ++probaArgEnd;
  prevId = USERFUNC;
  writeToPharmml(probaArgEnd,tokenPairEnd,pmf,prevId);
  countData->appendChild(pmf);
}

void  PharmXmlConverter::renameAssignToLogicBinop(Element* state)
{
  Node* firstChild = state->firstChild();
  if(firstChild->nodeName() == "ct:AssignStatement")
  {
    NodeList * assignChildren = firstChild->childNodes();
    Element* logicBinop = _pDocument->createElement("math:LogicBinop");
    logicBinop->setAttribute("op","eq");
    while(assignChildren->length()){
      logicBinop->appendChild(assignChildren->item(0));
    }
    state->removeChild(firstChild);
    state->appendChild(logicBinop);
  }
}

void PharmXmlConverter::writeConditionalProbaDef(TokenPairs &expression, Element* countData, bool isTransformed)
{
  Element * probaAssignment =   _pDocument->createElement("ProbabilityAssignment");
  Element * proba =   _pDocument->createElement("Probability");
  if(isTransformed)
  {
    proba->setAttribute("linkFunction",expression[0].second);
    expression.erase(expression.begin());
  }
  expression.insert(expression.begin(),TokenPair(IDANY,"y"));
  TokenPairs::iterator tokenPairBegin = expression.begin();
  TokenPairs::iterator tokenPairEnd = expression.end();
  long unsigned int prevId = 0;
  Element * currentState = _pDocument->createElement("CurrentState");
  TokenPairs::iterator  probaArgEnd;
  for(probaArgEnd = tokenPairBegin; probaArgEnd != tokenPairEnd; ++probaArgEnd)
  {
    if(probaArgEnd->first == CONDOP){
      break;
    }
  }
  if(probaArgEnd->first != CONDOP){
    throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::writeConditionalProbaDef",
                                          std::string("expected conditional operator '"+ probaArgEnd->second+"' cannot translate to Pharmml"),
                                          THROW_LINE_FILE));
  }
  writeToPharmml(tokenPairBegin,probaArgEnd,currentState,prevId);
  Element*  previousState = _pDocument->createElement("PreviousState");
  ++probaArgEnd;
  expression.insert(probaArgEnd,TokenPair(IDANY,"yp"));
  tokenPairEnd = expression.end();
  for(probaArgEnd = expression.begin(); probaArgEnd != tokenPairEnd; ++probaArgEnd)
  {
    if(probaArgEnd->first == CONDOP){
      break;
    }
  }
  if(probaArgEnd->first != CONDOP){
    throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::writeConditionalProbaDef",
                                          std::string("expected conditional operator '"+ probaArgEnd->second+"' cannot translate to Pharmml"),
                                          THROW_LINE_FILE));
  }
  tokenPairBegin = ++probaArgEnd;
  int parOpen = 1;
  getClosedParenthesis(probaArgEnd, tokenPairBegin,tokenPairEnd, parOpen);
  prevId = 0;
  writeToPharmml(tokenPairBegin,probaArgEnd,previousState,prevId);
  // rename assignment to logic equal
  renameAssignToLogicBinop(currentState);
  renameAssignToLogicBinop(previousState);
  proba->appendChild(currentState);
  proba->appendChild(previousState);
  probaAssignment->appendChild(proba);
  ++probaArgEnd;
  prevId = USERFUNC;
  writeToPharmml(probaArgEnd,tokenPairEnd,probaAssignment,prevId);
  countData->appendChild(probaAssignment);
}

void PharmXmlConverter::setIndivParameterAttribute(Element* structuralModel)
{
  // find parameter of structural model  declared in _individual parameters and
  // set their "blkIdRef"  attributes to  "pm"
  NodeList * children = structuralModel->childNodes();
  if(children)
  {
    for(unsigned iChild = 0; iChild < children->length(); iChild++)
    {
      Element *  child  = static_cast<Element*>(children->item(iChild));
      if(child->nodeType() == Node::ELEMENT_NODE)
      {
        std::string  symbref(child->getAttribute("symbIdRef"));
        bool isSet = false;
        for(unsigned iParam = 0;  iParam < _individualParameters.size(); iParam++)
        {
          if(symbref ==  _individualParameters[iParam])
          {
            child->setAttribute("blkIdRef","pm");
            isSet = true;
            break;
          }
        }
        if(!isSet){
          setIndivParameterAttribute(child);
        }
      }
    }
  }
}

void PharmXmlConverter::insertStateCategoryVariable(Element* categoricalData, Element* dependance, Element* & stateVariable, const std::string & stateName)
{
  if(!stateVariable)
  {
    if(stateName == "yp")
    {
      stateVariable = _pDocument->createElement("PreviousStateVariable");
      stateVariable->setAttribute("symbId","yp");
    }
    else if (stateName == "y")
    {
      stateVariable = _pDocument->createElement("CategoryVariable");
      stateVariable->setAttribute("symbId","y");
    }
    if(dependance){
      categoricalData->insertBefore(stateVariable,dependance);
    }
    else{
      categoricalData->appendChild(stateVariable);
    }
  }

}

void PharmXmlConverter::createVariabilityNode(Element * & variabilityModel,Element * modelDefinition)
{
  variabilityModel = _pDocument->createElement("VariabilityModel");
  variabilityModel->setAttribute("blkId","vm_err");
  variabilityModel->setAttribute("type","residualError");
  AutoPtr<Element> variabilityLevel  = _pDocument->createElement("Level");
  variabilityLevel->setAttribute("referenceLevel","false");
  variabilityLevel->setAttribute("symbId","DV");
  variabilityModel->appendChild(variabilityLevel);
  Node * sameVaribilityModel = modelDefinition->getElementById("vm_err","blkId");
  if(!sameVaribilityModel)
  {
    Node * modelDefFirstChild = modelDefinition->firstChild();
    if(modelDefFirstChild){
      modelDefinition->insertBefore(variabilityModel,modelDefFirstChild);
    }
    else{
      modelDefinition->appendChild(variabilityModel);
    }
  }
}

void PharmXmlConverter::createErrorModelNode(Element *parameterModel, const  std::string &varname, Node* &lastPopParameter, Element* parametersToEstimate,
                                             Element* modelDefinition, TokenPairs  &standardDevPairs, bool & isStandardDeviation, const unsigned & lengthObsModel, const std::string& errorModelName,
                                             const std::string& iStructuralModel,const std::string& prediction, const std::string& observationName, bool & isParameterModel, const NodeList* mlxErrModParams,
                                             const Node *mlxErrorModAutoCorr, std::vector<std::string>& errorModels, Element *& outStyle,Element *& outStyleAssign,const TokenPairs &errorModelPairs)
{
  size_t errModelsSize = errorModels.size();
  int iMlxErrArg = 0;
  if(errModelsSize>1)
  {
    outStyle = _pDocument->createElement("General");
    outStyle->setAttribute("symbId",observationName);
    outStyleAssign = _pDocument->createElement("ct:Assign");
    appendSymbRef(outStyleAssign,prediction,iStructuralModel);
  }
  std::string epsPrefix = "EPS_";
  for(unsigned iErr = 0; iErr <errModelsSize; iErr++)
  {
    AutoPtr<Element> assign = _pDocument->createElement("ct:Assign");
    if(mlxErrorModAutoCorr){
      appendDescription(assign,"autocorr = " +mlxErrorModAutoCorr->getNodeValue());
    }
    AutoPtr<Element> errorModel = _pDocument->createElement("ErrorModel");
    AutoPtr<Element> functionCall = _pDocument->createElement("math:FunctionCall");
    Node * lastFunction   = 0 ;
    Element * functionNode =  _pDocument->createElement("tmpFunction");
    try
    {
      tokenizeToPharmml(errorModels[iErr],functionNode);
      Node * indepVar = _pDocument->documentElement()->getChildElement("IndependentVariable");
      if(isNotEmpty(functionNode))
      {
        if(indepVar){
          lastFunction =  _pDocument->documentElement()->insertBefore(functionNode->firstChild(),indepVar->nextSibling());
        }
        else{
          lastFunction = _pDocument->documentElement()->appendChild(functionNode->firstChild());
        }
      }
      else
      {
        std::string errorFunctionName(QString(errorModels[iErr].c_str()).split("(",QString::SkipEmptyParts)[0].toUtf8().data());
        lastFunction = _pDocument->documentElement()->getElementById(errorFunctionName,"symbId");
      }
    }
    catch(const lixoft::exception::Exception & err)
    {
      throwNow(lixoft::exception::Exception("lixoft::translator::MlxProjectToPharmML::translate",
                                            "translating error model definition '"+ errorModelName +"'\nfile: " + err.file()+
                                            ",  line "+ (QString::number(err.line())).toUtf8().data()+ ":\n" +err.message(),
                                            THROW_LINE_FILE));
    }
    if(!lastFunction)
    {
      throwNow(lixoft::exception::Exception("lixoft::translator::MlxProjectToPharmML::translate",
                                            "translating error model definition '"+ errorModelName +"'\n xml node not found\n",
                                            THROW_LINE_FILE));
    }
    const NamedNodeMap* lastFunctionAttr = lastFunction->attributes();
    appendSymbRef(functionCall,lastFunctionAttr->getNamedItem("symbId")->nodeValue());
    const NodeList* pharmmlFunctionArgs = getChildNodesByTagName(lastFunction,"FunctionArgument");
    int iErrParam = 1;
    for(unsigned iArg = 0; iArg < pharmmlFunctionArgs->length();iArg++)
    {
      AutoPtr<Element> functionArgument = _pDocument->createElement("math:FunctionArgument");
      const NamedNodeMap* argsAttr = pharmmlFunctionArgs->item(iArg)->attributes();
      std::string argument(argsAttr->getNamedItem("symbId")->nodeValue());
      functionArgument->setAttribute("symbId",argument);
      if(argument=="f"){
        appendSymbRef(functionArgument,prediction,iStructuralModel);
      }
      else
      {
        std::string errorParamName;
        if(mlxErrModParams)
        {
          const NamedNodeMap* mlxErrModParamAttr = mlxErrModParams->item(iMlxErrArg++)->attributes();
          // assuming that the number of  arguments of pharmmlFunctionArgs different from "f" is exactly the length of mlxErrModParams
          const std::string mlxErrModParamName(mlxErrModParamAttr->getNamedItem("name")->nodeValue());

          if(lengthObsModel>1){
            errorParamName ="ERR_"+ prediction+"_"+mlxErrModParamName;
          }
          else{
            errorParamName ="ERR_"+ mlxErrModParamName;
          }
          errorParamName = std::string(QString(errorParamName.c_str()).toUpper().toUtf8().data());
          if(errorModelPairs.size()>1)
          {
            errorParamName = errorModelPairs[iErrParam].second;
            iErrParam +=2;
          }
          appendSymbRef(functionArgument,errorParamName,"pm");
          const Node* mlxErrModParamInit = mlxErrModParamAttr->getNamedItem("initialization");
          const Node* mlxErrModParamPositive = mlxErrModParamAttr->getNamedItem("positive");
          writeInitialization(mlxErrModParamInit,parametersToEstimate,errorParamName,"positive",mlxErrModParamPositive);
          writePopParameter(errorParamName,lastPopParameter,parameterModel);
        }
        else
        {
          if(lengthObsModel > 1){
            errorParamName ="ERR_"+ prediction+"_"+argument;
          }
          else{
            errorParamName ="ERR_"+ argument;
          }
          errorParamName = std::string(QString(errorParamName.c_str()).toUpper().toUtf8().data());
          if(errorModelPairs.size()>1)
          {
            Element *  popParamError = parameterModel->getElementById(errorParamName,"symbId");
            errorParamName = errorModelPairs[iErrParam].second;
            iErrParam +=2;
            if(popParamError){
              popParamError->setAttribute("symbId",errorParamName);
            }
            else{
            writePopParameter(errorParamName,lastPopParameter,parameterModel);
            }
          }
          if(!isStandardDeviation){
            appendSymbRef(functionArgument,errorParamName,"pm");
          }
          else
          {
            AutoPtr<Element> realNode =  _pDocument->createElement("ct:Real");
            Text * realVal =  _pDocument->createTextNode("1");
            realNode->appendChild(realVal);
            functionArgument->appendChild(realNode);
          }
        }
      }
      functionCall->appendChild(functionArgument);
      assign->appendChild(functionCall);
      errorModel->appendChild(assign);
    }
    AutoPtr<Element> residualError = _pDocument->createElement("ResidualError");
    if(errModelsSize > 1){
      epsPrefix = (QString("EPS")+QString::number(iErr+1)+QString("_")).toUtf8().data();
    }
    std::string errName (epsPrefix + prediction);
    appendSymbRef(residualError,errName,"pm");
    AutoPtr<Element> stdevAssign =  _pDocument->createElement("ct:Assign");
    if(!isStandardDeviation)
    {
      AutoPtr<Element> realNode =  _pDocument->createElement("ct:Real");
      Text * realVal =  _pDocument->createTextNode("1");
      realNode->appendChild(realVal);
      stdevAssign->appendChild(realNode);
    }
    else
    {
      long unsigned prevId = 0 ;
      TokenPairs::iterator tokenPairBegin = standardDevPairs.begin();
      TokenPairs::iterator tokenPairEnd = standardDevPairs.end();
      writeToPharmml(tokenPairBegin,tokenPairEnd,stdevAssign,prevId);
      Element * stdevAssignChild = stdevAssign->getChildElement("ct:SymbRef");
      if(stdevAssignChild){
        stdevAssignChild->setAttribute("blkIdRef","sm");
      }
    }

    Node *  lastPopParameter = 0;
    if(parameterModel)
    {
      Element * errorModVar =  parameterModel->getElementById(errName, "symbId");
      NodeList * popParamList =  parameterModel->getElementsByTagName("PopulationParameter");
      if(popParamList){
        lastPopParameter = popParamList->item(popParamList->length()-1);
      }
      if(errorModVar){
        parameterModel->removeChild(errorModVar);
      }
      writeRandomVariable(errName,"DV","vm_err","mean",varname,stdevAssign, parameterModel,lastPopParameter);
    }
    else
    {
      parameterModel = _pDocument->createElement("ParameterModel");
      parameterModel->setAttribute("blkId","pm");
      writeRandomVariable(errName,"DV","vm_err","mean",varname,stdevAssign, parameterModel,lastPopParameter);
      Node* structuralModel = modelDefinition->getChildElement("StructuralModel");
      if(structuralModel){
        modelDefinition->insertBefore(parameterModel,structuralModel);
      }
      else{
        modelDefinition->appendChild(parameterModel);
      }
    }
    isParameterModel = true;
    if(errModelsSize == 1)
    {
      outStyle->appendChild(errorModel);
      outStyle->appendChild(residualError);
    }
    else
    {
      AutoPtr<Element> plus = _pDocument->createElement("math:Binop");
      plus->setAttribute("op","plus");
      Node*  outStyleContent = outStyleAssign->firstChild();
      plus->appendChild(outStyleContent);
      AutoPtr<Element> times = _pDocument->createElement("math:Binop");
      times->setAttribute("op","times");
      times->appendChild(functionCall);
      times->appendChild(residualError->firstChild());
      plus->appendChild(times);
      outStyleAssign->appendChild(plus);
    }
  }
}

void PharmXmlConverter::longitudinalToPharmml(const char* file, Element* modelDefinition)
{
  QFile fileIn(file);
  if (!fileIn.open(QIODevice::ReadOnly | QIODevice::Text))
  {
    throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::longitudinalToPharmml",
                                          std::string("cannot open  file '"+ std::string(file)+"' cannot translate to Pharmml"),
                                          THROW_LINE_FILE));
  }
  QTextStream fileInStream(&fileIn);
  QString QstrInput(fileInStream.readAll());
  fileIn.close();
  QStringList sectionTmp = QstrInput.split(QRegExp("\\[\\s*LONGITUDINAL\\s*\\]"),QString::SkipEmptyParts);
  QString longitudinal = (sectionTmp.size() > 1 )? sectionTmp[1] : sectionTmp[0];
  sectionTmp = longitudinal.split(QRegExp("\\[\\s*[A-Z_]+\\s*\\]"),QString::SkipEmptyParts);
  longitudinal = sectionTmp[0];
  std::vector<std::pair<std::string, std::string>  > sections;
  std::string longitudinalStr(longitudinal.toUtf8().data());
  splitMlxtranIntoSections(longitudinalStr, sections);
  longitudinal = QString(longitudinalStr.c_str());
  QString longitudinalCopy(longitudinal);
  longitudinalCopy.replace("\n","");
  longitudinalCopy.replace(QRegExp("\\s*"),"");
  if(!longitudinalCopy.isEmpty())
  { //remaining sections in longitudinal
    std::string longRemains(longitudinal.toUtf8().data());
    throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::longitudinalToPharmml",
                                          std::string("unknown model section \n"+ longRemains +"\ncannot translate to Pharmml"),
                                          THROW_LINE_FILE));
  }
  int nSecAlias = 0, indexSecAlias[2] = {-1,-1};
  // remove  output <-- unsed in pharmml model declaration
  for(std::vector<std::pair<std::string, std::string>  >::iterator it = sections.begin() ; it !=  sections.end(); ++it)
  {
    if(it->first == "output")
    {
      sections.erase(it);
      break;
    }
  }
  // rename definition section to observation
  for( unsigned sec = 0; sec <  sections.size(); sec++)
  {
    if(sections[sec].first  == "observation"){
      indexSecAlias[nSecAlias++] = sec;
    }
    else if(sections[sec].first == "definition")
    {
      sections[sec].first = "observation";
      indexSecAlias[nSecAlias++] = sec;
    }
  }
  // collapse  definition section and observation section
  if(nSecAlias == 2)
  {
    sections[indexSecAlias[0]].second +=  "\n" + sections[indexSecAlias[1]].second;
    sections.erase(sections.begin() + indexSecAlias[1]);
  }
  // rename pk section to equation
  nSecAlias = 0;
  indexSecAlias[0] = -1;
  indexSecAlias[1] = -1;
  for( unsigned sec = 0; sec <  sections.size(); sec++)
  {
    if(sections[sec].first  == "equation"){
      indexSecAlias[nSecAlias++] = sec;
    }
    else if(sections[sec].first == "pk"){
      sections[sec].first = "equation";
      indexSecAlias[nSecAlias++] = sec;
    }
  }
  // collapse  pk  section and equation section <-- equation and pk are  parsed the same
  if(nSecAlias == 2)
  {
    //assert(indexSecAlias[0] + 1  == indexSecAlias[1]);
    sections[indexSecAlias[0]].second +=  "\n" + sections[indexSecAlias[1]].second;
    sections.erase(sections.begin() + indexSecAlias[1]);
  }
  //put input section at the end, since must be writed wrt to model defined by equation
  std::pair<std::string, std::string> inputSection;
  for( unsigned sec = 0; sec <  sections.size(); sec++)
  {
    if(sections[sec].first  == "input")
    {
      inputSection= sections[sec];
      sections.erase(sections.begin()+sec);
      break;
    }
  }
  sections.push_back(inputSection);
  // write sections : description, equation  and  observation
  Element* externalDataSet = _pDocument->documentElement()->getChildElement("design:TrialDesign")->getChildElement("design:ExternalDataSet");
  for(unsigned sec = 0; sec < sections.size(); sec++)
  {
    //** description
    if(sections[sec].first == "description")
    {
      AutoPtr<Element> description = _pDocument->createElement("ct:Description") ;
      Text*  value = _pDocument->createTextNode(sections[sec].second);
      description->appendChild(value);
      Node * firstNode = modelDefinition->firstChild();
      if(firstNode){
        modelDefinition->insertBefore(description,firstNode);
      }
      else{
        modelDefinition->appendChild(description);
      }
    }
    //** equation
    else if(sections[sec].first == "equation")
    {
      TokenExpressions   results;
      bool iparsed =  parseMlxtranEquationSection(sections[sec].second,results );
      if(!iparsed || !results.size())
      {
        throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::longitudinalToPharmml",
                                              std::string("the parse of  section  equation  in longitudinal model failed\ncannot translate to Pharmml"),
                                              THROW_LINE_FILE));
      }
      // find and set  derivatives and initial conditions
      std::vector<std::string> derivList;
      for(unsigned i = 0; i <  results.size(); i++)
      {
        if(results[i].first  == "equation")
        {
          QString varName(results[i].second[0].second.c_str());
          if(varName.startsWith("ddt_"))
          {
            results[i].first = "derivative";
            std::string derivName(varName.right(varName.size()-4).toUtf8().data());
            results[i].second[0].second = derivName;
            derivList.push_back(derivName);
          }
          else if(!varName.compare("t_0")||!varName.compare("t0"))
          {
            results[i].first = "initialTime";
            results[i].second.erase(results[i].second.begin());
          }
        }
      }
      std::set<std::string>  initialCondNames;
      for(unsigned i = 0; i <  results.size(); i++)
      {
        if(results[i].first  == "equation")
        {
          QString varName(results[i].second[0].second.c_str());
          if(varName.endsWith("_0"))
          {
            std::string initCondName(varName.left(varName.size()-2).toUtf8().data());
            for(unsigned iDeriv = 0; iDeriv < derivList.size(); iDeriv++)
            {
              if(initCondName == derivList[iDeriv])
              {
                initialCondNames.insert(initCondName);
                break;
              }
            }
          }
        }
        else if(results[i].first  == "functionDefinition")
        {
          QString varName(results[i].second[0].second.c_str());
          if(varName.endsWith("_0"))
          {
            int iPopen = varName.indexOf("_0");
            std::string initCondName(varName.mid(0,iPopen).toUtf8().data());
            for(unsigned iDeriv = 0; iDeriv < derivList.size(); iDeriv++)
            {
              if(initCondName == derivList[iDeriv])
              {
                initialCondNames.insert(initCondName);
                break;
              }
            }
          }
        }
      }
      for(std::set<std::string>::iterator it = initialCondNames.begin(); it != initialCondNames.end(); ++it)
      {
        TokenPairs initCondAssign(1,TokenPair(ASSIGN,"="));
        initCondAssign.push_back(TokenPair(IDANY,*it+"_0"));
        results.push_back(TokenExpression("initialCondition+"+*it,initCondAssign));
      }

      Element *  structuralModel = modelDefinition->getChildElement("StructuralModel");
      if(!structuralModel){
        createStructualModel(modelDefinition, structuralModel);
      }
      TokenExpressions::iterator resultsBegin = results.begin();
      const  TokenExpressions::const_iterator resultsEnd = results.end();
      writeEquationSection(resultsBegin,resultsEnd, structuralModel);
      setIndivParameterAttribute(structuralModel);
    }
    //** observation
    else if(sections[sec].first == "observation")
    {
      TokenExpressions   results;
      bool iparsed =  parseMlxtranEquationSection(sections[sec].second,results );

      if(!iparsed || !results.size())
      {
        throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::longitudinalToPharmml",
                                              std::string("the parse of  section observation  in longitudinal model failed\ncannot translate to Pharmml"),
                                              THROW_LINE_FILE));
      }
      TokenPairs::iterator tokenPairBegin;
      TokenPairs::iterator tokenPairEnd;
      unsigned long int prevId = 0 ;
      for(TokenExpressions::iterator expression =results.begin(); expression != results.end();  )
      {
        if(expression->first == "eventObservation")
        {
          std::string observationName = expression->second[0].second;
          bool isCurrentEventNode = false;
          NodeList * observationModels = modelDefinition->getElementsByTagName("ObservationModel");
          Element * observationModel = 0;
          Element * discrete = 0;
          Element * timeToEventData  = 0;
          Element * eventVariable = 0;
          unsigned nObsModels  = observationModels->length();
          std::string dataName("TimeToEventData");
          //find existing event node for current observation
          findDataVariableInObsModel(isCurrentEventNode,observationModels,observationName,dataName,timeToEventData,discrete);
          if(!isCurrentEventNode)
          {
            observationModel = _pDocument->createElement("ObservationModel");
            std::string imodelStr(QString::number(nObsModels+1).toUtf8().data());
            observationModel->setAttribute("blkId","om"+imodelStr);
            discrete = _pDocument->createElement("Discrete");
            timeToEventData = _pDocument->createElement("TimeToEventData");
          }
          eventVariable = _pDocument->createElement("EventVariable");
          eventVariable->setAttribute("symbId",observationName);
          timeToEventData->appendChild(eventVariable);
          expression->first = "writed+"+expression->first;
          ++expression;
          AutoPtr<Element>  censoring =   _pDocument->createElement("Censoring");
          while(expression->first != "endObservation")
          {
            if(expression->first== "eventType")
            {
              censoring->setAttribute("censoringType",expression->second[0].second);
              timeToEventData->appendChild(censoring);
              expression->first = "writed+"+expression->first;
            }
            else if(expression->first== "maxEventNumber")
            {
              AutoPtr<Element>  maximumNumberEvents =   _pDocument->createElement("MaximumNumberEvents");
              tokenPairBegin = expression->second.begin();
              tokenPairEnd = expression->second.end();
              prevId = IDANY;
              writeToPharmml(tokenPairBegin,tokenPairEnd,maximumNumberEvents,prevId);
              timeToEventData->appendChild(maximumNumberEvents);
              expression->first = "writed+"+expression->first;
            }
            else if(expression->first== "rightCensoringTime")
            {
              AutoPtr<Element>  rightCensoringTime =   _pDocument->createElement("RightCensoringTime");
              tokenPairBegin = expression->second.begin();
              tokenPairEnd = expression->second.end();
              prevId = IDANY;
              writeToPharmml(tokenPairBegin,tokenPairEnd,rightCensoringTime,prevId);
              censoring->appendChild(rightCensoringTime);
              expression->first = "writed+"+expression->first;
            }
            else if(expression->first== "intervalLength")
            {
              AutoPtr<Element>  intervalLength =   _pDocument->createElement("IntervalLength");
              tokenPairBegin = expression->second.begin();
              tokenPairEnd = expression->second.end();
              prevId =IDANY;
              writeToPharmml(tokenPairBegin,tokenPairEnd,intervalLength,prevId);
              censoring->appendChild(intervalLength);
              expression->first = "writed+"+expression->first;
            }
            else if(expression->first== "hazard")
            {
              AutoPtr<Element>  hazardFunction =   _pDocument->createElement("HazardFunction");
              hazardFunction->setAttribute("symbId","h");
              tokenPairBegin = expression->second.begin();
              tokenPairEnd = expression->second.end();
              prevId = USERFUNC;
              writeToPharmml(tokenPairBegin,tokenPairEnd,hazardFunction,prevId);
              timeToEventData->insertBefore(hazardFunction,eventVariable->nextSibling());
              expression->first = "writed+"+expression->first;
            }
            if(expression != results.end()){
              ++expression;
            }
            else{
              break;
            }
          }
          setIndivParameterAttribute(timeToEventData);
          if(!isCurrentEventNode)
          {
            discrete->appendChild(timeToEventData);
            observationModel->appendChild(discrete);
            modelDefinition->appendChild(observationModel);
          }
        }
        else if(expression->first == "countObservation")
        {
          std::string observationName = expression->second[0].second;
          bool isCurrentEventNode = false;
          NodeList * observationModels = modelDefinition->getElementsByTagName("ObservationModel");
          Element * observationModel = 0;
          Element * discrete = 0;
          Element * countData  = 0;
          Element * countVariable = 0;
          Element * previousCountVariable = 0;
          unsigned nObsModels  = observationModels->length();
          const std::string dataName("CountData");
          //find existing event node for current observation
          findDataVariableInObsModel(isCurrentEventNode,observationModels,observationName,dataName,countData,discrete);
          if(!isCurrentEventNode)
          {
            observationModel = _pDocument->createElement("ObservationModel");
            std::string imodelStr(QString::number(nObsModels+1).toUtf8().data());
            observationModel->setAttribute("blkId","om"+imodelStr);
            discrete = _pDocument->createElement("Discrete");
            countData = _pDocument->createElement("CountData");
          }
          expression->first = "writed+"+expression->first;
          ++expression;
          while(expression->first != "endObservation")
          {
            if(expression->first== "transformedProbaDef" || expression->first== "probabilityDefinition")
            {
              if(!countVariable)
              {
                countVariable = _pDocument->createElement("CountVariable");
                countVariable->setAttribute("symbId","y");
                countData->appendChild(countVariable);
                Element * numberCounts =   _pDocument->createElement("NumberCounts");
                numberCounts->setAttribute("symbId","k");
                countData->appendChild(numberCounts);
              }
              if(expression->first== "probabilityDefinition"){
                writeCountProbaDefinition(expression->second,countData);
              }
              else{
                writeCountProbaDefinition(expression->second,countData, true);
              }
              expression->first = "writed+"+expression->first;
            }
            else if(expression->first== "transformedCondProbaDef" || expression->first== "conditionalProbaDef")
            {
              if(!previousCountVariable)
              {
                previousCountVariable = _pDocument->createElement("PreviousCountVariable");
                countVariable = _pDocument->createElement("CountVariable");
                previousCountVariable->setAttribute("symbId","yp");
                countVariable->setAttribute("symbId","y");
                countData->appendChild(countVariable)  ;
                countData->appendChild(previousCountVariable);
              }
              if(expression->first== "transformedCondProbaDef"){
                writeConditionalProbaDef(expression->second,countData, true);
              }
              else{
                writeConditionalProbaDef(expression->second,countData);
              }
              expression->first = "writed+"+expression->first;
            }
            else if(!QString(expression->first.c_str()).startsWith("writed+"))
            {
              const  TokenExpressions::const_iterator resultsEnd = results.end();
              writeEquationSection(expression,resultsEnd,countData);
            }
            if(expression != results.end()){
              ++expression;
            }
            else{
              break;
            }
          }
          setIndivParameterAttribute(countData);
          if(!isCurrentEventNode)
          {
            discrete->appendChild(countData);
            observationModel->appendChild(discrete);
            modelDefinition->appendChild(observationModel);
          }
        }
        else if(expression->first == "categoricalObservation")
        {
          std::string observationName = expression->second[0].second;
          bool isCurrentEventNode = false;
          NodeList * observationModels = modelDefinition->getElementsByTagName("ObservationModel");
          Element * observationModel = 0;
          Element * discrete = 0;
          Element * categoricalData  = 0;
          Element * categoryVariable = 0;
          Element * previousStateVariable = 0 ;
          Element*  dependance = 0 ;
          unsigned nObsModels  = observationModels->length();
          const std::string dataName("CountData");
          //find existing event node for current observation
          findDataVariableInObsModel(isCurrentEventNode,observationModels,observationName,dataName,categoricalData,discrete);
          if(!isCurrentEventNode)
          {
            observationModel = _pDocument->createElement("ObservationModel");
            std::string imodelStr(QString::number(nObsModels+1).toUtf8().data());
            observationModel->setAttribute("blkId","om"+imodelStr);
            discrete = _pDocument->createElement("Discrete");
            categoricalData = _pDocument->createElement("CategoricalData");
          }
          else
          {
            discrete->removeChild(categoricalData);
            categoricalData = _pDocument->createElement("CategoricalData");
          }
          expression->first = "writed+"+expression->first;
          ++expression;
          while(expression->first != "endObservation")
          {
            if(expression->first == "transformedProbaDef" || expression->first== "probabilityDefinition")
            {
              insertStateCategoryVariable(categoricalData,dependance,categoryVariable,"y");
              if(expression->first== "probabilityDefinition"){
                writeProbaDefinition(expression->second,categoricalData);
              }
              else {
                writeProbaDefinition(expression->second,categoricalData,true);
              }
              expression->first = "writed+"+expression->first;
            }
            else if(expression->first == "transformedCondProbaDef" || expression->first== "conditionalProbaDef")
            {
              insertStateCategoryVariable(categoricalData,dependance,categoryVariable,"y");
              insertStateCategoryVariable(categoricalData,dependance,previousStateVariable,"yp");
              if(expression->first == "transformedCondProbaDef"){
                writeConditionalProbaDef(expression->second,categoricalData,true);
              }
              else{
                writeConditionalProbaDef(expression->second, categoricalData);
              }
              expression->first = "writed+"+expression->first;
            }
            else if(expression->first== "categories")
            {
              AutoPtr<Element>  listOfCategories = _pDocument->createElement("ListOfCategories");
              tokenPairBegin = expression->second.begin();
              Element * categoryMapping = 0;
              Element * columnMapping = 0;
              bool isCatNameMapped = false;
              if(findCategoryMapping(externalDataSet,columnMapping,observationName)||findCategoryMapping(externalDataSet,columnMapping,"Y")
                 ||findCategoryMapping(externalDataSet,columnMapping,"y")){
                categoryMapping = _pDocument->createElement("ds:CategoryMapping");
              }
              for(TokenPairs::iterator itt = tokenPairBegin; itt != expression->second.end(); ++itt)
              {
                if(itt->first != COMMA)
                {
                  AutoPtr<Element>  category = _pDocument->createElement("Category");
                  std::string catName(itt->second);
                  isCatNameMapped = setCategoricalName(category,categoryMapping,catName,observationName,false);
                  listOfCategories->appendChild(category);
                }
              }
              if(isCatNameMapped){
                columnMapping->appendChild(categoryMapping);
              }
              Node* firstCatNode = categoricalData->firstChild();
              categoricalData->insertBefore(listOfCategories,firstCatNode);
              expression->first = "writed+"+expression->first;
            }
            else if(expression->first== "dependence")
            {
              dependance = _pDocument->createElement("Dependance");
              dependance->setAttribute("type","discreteMarkov");
              if(previousStateVariable){
                categoricalData->insertBefore(dependance,previousStateVariable->nextSibling());
              }
              else{
                categoricalData->appendChild(dependance);
              }
              expression->first = "writed+"+expression->first;
            }
            else if(expression->first== "transitionRate")
            {
              insertStateCategoryVariable(categoricalData,dependance,categoryVariable,"y");
              insertStateCategoryVariable(categoricalData,dependance,previousStateVariable,"yp");
              AutoPtr<Element> probabilityAssignment =   _pDocument->createElement("ProbabilityAssignment");
              AutoPtr<Element>  transitionRate = _pDocument->createElement("TransitionRate");
              AutoPtr<Element>  currentState = _pDocument->createElement("CurrentState");
              Element *  logicBinOp = _pDocument->createElement("math:LogicBinop");
              tokenPairBegin = expression->second.begin();
              logicBinOp->setAttribute("op","eq");
              appendSymbRef(logicBinOp,"y");
              bool isCatChar = false;
              if(tokenPairBegin->second.size()==1){
                isCatChar =true;
              }
              if(!isCatChar){
                appendSymbRef(logicBinOp,tokenPairBegin->second);
              }
              else{

                appendSymbRef(logicBinOp,observationName+"_cat_"+tokenPairBegin->second);
              }
              currentState->appendChild(logicBinOp);
              ++tokenPairBegin; // comma
              if(tokenPairBegin->first != COMMA){
                throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::longitudinalToPharmml",
                                                      std::string("writing categoricalObservation-> transitionalRate: expected comma instead of  '"+ tokenPairBegin->second +"' cannot translate to Pharmml"),
                                                      THROW_LINE_FILE));
              }
              ++tokenPairBegin;
              AutoPtr<Element>  previousState = _pDocument->createElement("PreviousState");
              logicBinOp = _pDocument->createElement("math:LogicBinop");
              logicBinOp->setAttribute("op","eq");
              appendSymbRef(logicBinOp,"yp");
              if(tokenPairBegin->second.size()==1){
                isCatChar =true;
              }
              if(!isCatChar){
                appendSymbRef(logicBinOp,tokenPairBegin->second);
              }
              else{
                appendSymbRef(logicBinOp,observationName+"_cat_"+tokenPairBegin->second);
              }
              previousState->appendChild(logicBinOp);
              transitionRate->appendChild(currentState);
              transitionRate->appendChild(previousState);
              probabilityAssignment->appendChild(transitionRate);
              ++tokenPairBegin; // equal
              if(tokenPairBegin->first != ASSIGN){
                throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::longitudinalToPharmml",
                                                      std::string("writing categoricalObservation-> transitionalRate: expected assignement instead of  '"+ tokenPairBegin->second +"' cannot translate to Pharmml"),
                                                      THROW_LINE_FILE));
              }
              tokenPairEnd =  expression->second.end();
              prevId = USERFUNC;
              writeToPharmml(tokenPairBegin, tokenPairEnd,probabilityAssignment,prevId);
              categoricalData->appendChild(probabilityAssignment);
              expression->first = "writed+"+expression->first;
            }
            else if(!QString(expression->first.c_str()).startsWith("writed+"))
            {
              const  TokenExpressions::const_iterator resultsEnd = results.end();
              writeEquationSection(expression,resultsEnd,categoricalData);
            }
            if(expression != results.end()){
              ++expression;
            }
            else{
              break;
            }
          }
          setIndivParameterAttribute(categoricalData);
          discrete->appendChild(categoricalData);
          if(!isCurrentEventNode)
          {
            observationModel->appendChild(discrete);
            modelDefinition->appendChild(observationModel);
          }
        }
        else if(expression->first == "continuousObservation")
        {
          std::string observationName = expression->second[0].second;
          expression->first = "writed+"+expression->first;
          ++expression;
          std::string errorModelName, distribution, autocorrelation, prediction, varname = "var";
          TokenPairs  standardDevPairs;
          TokenPairs  errorModelPairs;
          bool isErrorModel = false, isStandardDeviation = false;
          while(expression->first != "endObservation")
          {
            if(expression->first == "autocorrelation")
            {
              autocorrelation = expression->second[0].second;
              expression->first = "writed+"+expression->first;
            }
            else if(expression->first == "errorModel")
            {
              errorModelName =  expression->second[0].second;
              errorModelPairs = expression->second;
              isErrorModel = true;
              expression->first = "writed+"+expression->first;
            }
            else if(expression->first== "distribution")
            {
              distribution = expression->second[0].second;
              expression->first = "writed+"+expression->first;
              //TODO write transformation of the distribution if distribution != normal?
            }
            else if(expression->first== "prediction")
            {
              prediction = expression->second[0].second ;
              expression->first = "writed+"+expression->first;
            }
            else if(expression->first== "sd"|| expression->first== "var")
            {
              standardDevPairs = expression->second;
              isStandardDeviation = true;
              if(!isErrorModel)
              {
                errorModelName = "constant";
                isErrorModel = true;
              }
              if(expression->first== "sd"){
                varname = "stdev";
              }
              expression->first = "writed+"+expression->first;
            }

            if(expression != results.end()){
              ++expression;
            }
            else{
              break;
            }
          }
          bool isCurrentContinuousNode = false;
          NodeList * observationModels = modelDefinition->getElementsByTagName("ObservationModel");
          Element * observationModel = 0;
          Element * continuousData  = 0;
          Element * outStyle = 0;
          Element * outType = 0;
          unsigned nObsModels  = observationModels->length();
          int iObsModel;
          //find existing event node for current observation
          for(unsigned obsMod = 0; obsMod < nObsModels; obsMod++)
          {
            NodeList*  modelList = static_cast<Element*>(observationModels->item(obsMod))->getElementsByTagName("ContinuousData");
            if(modelList)
            {
              iObsModel = obsMod;
              for(unsigned  iModel = 0; iModel < modelList->length(); iModel++)
              {
                continuousData = static_cast<Element*>(modelList->item(iModel));
                outType = continuousData->getElementById(observationName,"symbId");
                if(outType)
                {
                  isCurrentContinuousNode = true;
                  break;
                }
              }
              if(isCurrentContinuousNode){
                break;
              }
            }
          }
          std::string imodelStr;
          if(isCurrentContinuousNode){
            imodelStr = std::string(QString::number(iObsModel).toUtf8().data());
          }
          else
          {
            nObsModels++;
            imodelStr = std::string(QString::number(nObsModels).toUtf8().data());
          }
          Element* outStyleAssign  = 0;
          Element* output  = 0;
          unsigned errModelsSize = 0;
          std::string iSm("sm");
          outStyle = _pDocument->createElement("Standard");
          outStyle->setAttribute("symbId",observationName);
          output = _pDocument->createElement("Output");
          appendSymbRef(output,prediction,iSm);
          if(isErrorModel)
          {
            Element*  variabilityModel = 0 ;
            createVariabilityNode(variabilityModel,modelDefinition);
            std::vector<std::string> errorModels(1);
            mlxErrorModel(errorModelName, errorModels[0]);
            Node *mlxErrorModAutoCorr = 0;
            NodeList* mlxErrModParams = 0;
            Node * lastPopParameter = 0;
            Element* parametersToEstimate = 0;
            bool isParameterModel;
            Element * parameterModel = modelDefinition->getChildElement("ParameterModel");
            createErrorModelNode(parameterModel,varname,lastPopParameter,parametersToEstimate, modelDefinition, standardDevPairs, isStandardDeviation, nObsModels,  errorModelName,
                                 iSm, prediction,  observationName,  isParameterModel, mlxErrModParams, mlxErrorModAutoCorr,  errorModels, outStyle, outStyleAssign, errorModelPairs);

          }
          if(errModelsSize <=1)
          {
            Node* errorModel =  outStyle->getChildElement("ErrorModel");
            if(errorModel){
              outStyle->insertBefore(output,errorModel);
            }
            else{
              outStyle->appendChild(output);
            }
          }
          else{
            outStyle->appendChild(outStyleAssign);
          }
          setIndivParameterAttribute(outStyle);
          if(!isCurrentContinuousNode)
          {
            observationModel = _pDocument->createElement("ObservationModel");
            observationModel->setAttribute("blkId","om"+imodelStr);
            continuousData = _pDocument->createElement("ContinuousData");
            continuousData->appendChild(outStyle);
            observationModel->appendChild(continuousData);
            modelDefinition->appendChild(observationModel);
          }
          else
          {
            continuousData->removeChild(outType);
            continuousData->appendChild(outStyle);
          }
        }
        if(expression->first == "endObservation"){
          expression->first = "writed+"+expression->first;
        }
        if(expression != results.end()){
          ++expression;
        }
      }
    }
    //** input
    else if(sections[sec].first == "input")
    {
      TokenExpressions   results;
      bool iparsed =  parseMlxtranInputSection(sections[sec].second,results);
      if(!iparsed || !results.size())
      {
        throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::longitudinalToPharmml",
                                              std::string("the parse of  section  input  in longitudinal model failed\ncannot translate to Pharmml"),
                                              THROW_LINE_FILE));
      }

      if(!externalDataSet)
      {
        // if no dataSet is defined --> simulx case:  put results of input section  as variables in parameter model
        Element* structuralModel = modelDefinition->getChildElement("StructuralModel");
        if(structuralModel)
        {
          Node* firstVar = structuralModel->firstChild();
          for(unsigned iRes = 0; iRes <  results.size(); iRes++)
          {
            if(results[iRes].first  != "input" && results[iRes].first  != "regressor")
            {
              throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::longitudinalToPharmml",
                                                    std::string("unknown type of input '" + results[iRes].first + "'   in longitudinal model \ncannot translate to Pharmml"),
                                                    THROW_LINE_FILE));
            }
            for(unsigned iReg = 0; iReg < results[iRes].second.size(); iReg++ )
            {
              std::string varName(results[iRes].second[iReg].second);
              Element * variable;
              createVariableNode(variable,varName,"real");
              structuralModel->insertBefore(variable,firstVar);
            }
          }
        }
      }
      else
      {
        //manage only regressors
        Node* externalDataSetFirstNode = externalDataSet->firstChild();
        for(unsigned iRes = 0; iRes <  results.size(); iRes++)
        {
          if(results[iRes].first  == "regressor")
          {
            for(unsigned iReg = 0; iReg < results[iRes].second.size(); iReg++ )
            {
              std::string regressorName(results[iRes].second[iReg].second);
              Element* columnMapping =0;
              if(!findCategoryMapping(externalDataSet,columnMapping,regressorName))
              {
                createColumMapping(columnMapping,"X");
                appendSymbRef(columnMapping,regressorName);
                externalDataSet->insertBefore(columnMapping,externalDataSetFirstNode);
              }
              else{
                externalDataSetFirstNode = columnMapping->nextSibling();
              }
            }
          }
        }
      }
    }
  }
}

bool  PharmXmlConverter::setCategoricalName(Element* category, Element* categoryMapping,
                                            std::string & catName, const std::string& observationName, bool isCatId)
{
  bool isCatChar = false, isCatNameMapped = false;
  std::string catIdName("symbId");
  if(isCatId){
    catIdName = "catId";
  }
  if(catName.size()==1){
    isCatChar = true;
  }
  if(!isCatChar){
    category->setAttribute(catIdName,catName);
  }
  else
  {
    std::string renamedCatName(observationName+"_cat_"+catName);
    category->setAttribute(catIdName,renamedCatName);
    if(categoryMapping)
    {
      Element * map = _pDocument->createElement("ds:Map");
      map->setAttribute("dataSymbol",catName);
      map->setAttribute("modelSymbol",renamedCatName);
      categoryMapping->appendChild(map);
      isCatNameMapped = true;
    }
    catName = renamedCatName;
  }
  return isCatNameMapped ;
}

void PharmXmlConverter::writeMathCondition(const TokenExpression expression, Element* conditionalStatement)
{
  TokenPairs::const_iterator tokenBegin = expression.second.begin();
  TokenPairs::const_iterator tokenEnd = expression.second.end();
  ++tokenBegin;
  unsigned long int prevId = 0;
  AutoPtr<Element> mathCondition = _pDocument->createElement("math:Condition");
  writeToPharmml(tokenBegin, tokenEnd,mathCondition, prevId);
  conditionalStatement->appendChild(mathCondition);
}

void PharmXmlConverter::writeConditionalStatements(TokenExpressions::iterator & expressionsBegin, const TokenExpressions::const_iterator & expressionsEnd, Element* element)
{
  std::set<std::string> variables;
  Node* conditionalStatement = writeConditionalStatements(expressionsBegin,expressionsEnd,element , variables);
  for(std::set<std::string>::const_iterator var = variables.begin(); var != variables.end(); ++var)
  {
    Element* variable = element->getElementById(*var,"symbId");
    if(!variable)
    {
      createVariableNode(variable,*var,"real");
      element->insertBefore(variable,conditionalStatement);
    }
  }

}
Node*  PharmXmlConverter::writeConditionalStatements(TokenExpressions::iterator & expressionsBegin, const TokenExpressions::const_iterator & expressionsEnd, Element* element, std::set<std::string>& variables)
{
  AutoPtr<Element> conditionalStatement = _pDocument->createElement("ConditionalStatement");
  if(!QString(expressionsBegin->first.c_str()).startsWith("if"))
  {
    throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::writeConditionalStatements",
                                          std::string("wrong conditional satement '"+ expressionsBegin->second[0].second +"' cannot translate to Pharmml"),
             THROW_LINE_FILE));
  }
  AutoPtr<Element> mathIf = _pDocument->createElement("math:If");
  TokenPairs::const_iterator tokenBegin;
  TokenPairs::const_iterator tokenEnd;
  writeMathCondition(*expressionsBegin,mathIf);
  expressionsBegin->first = "writed+"+expressionsBegin->first;
  TokenExpressions::iterator expression = ++expressionsBegin;
  unsigned long int prevId;

  for(; expression!= expressionsEnd; ++expression )
  {
    if(expression->first == "if")
    {
      writeConditionalStatements(expression, expressionsEnd, mathIf,variables);
    }
    if(expression->first == "end"||expression->first == "else" ||expression->first == "elseif")
    {
      break;
    }
    else
    {
      tokenBegin = expression->second.begin();
      tokenEnd = expression->second.end();
      getVariableName(expression,variables);
      prevId = 0;
      writeToPharmml(tokenBegin, tokenEnd,mathIf, prevId);
      expression->first = "writed+"+expression->first;
    }
  }
  conditionalStatement->appendChild(mathIf);
  for(; expression!= expressionsEnd; )
  {
    if(expression->first == "elseif")
    {
      AutoPtr<Element> mathElseIf = _pDocument->createElement("math:ElseIf");
      writeMathCondition(*expression,mathElseIf);
      TokenExpressions::iterator subExpression;
      expression->first = "writed+"+expression->first;
      for(subExpression = ++expression; subExpression!= expressionsEnd; ++subExpression)
      {
        if(subExpression->first == "if")
        {
          writeConditionalStatements(subExpression, expressionsEnd, mathElseIf,variables);
          expression = subExpression;
        }
        if(subExpression->first == "end"||subExpression->first == "else" ||subExpression->first == "elseif")
        {
          break;
        }
        else
        {
          tokenBegin = subExpression->second.begin();
          tokenEnd = subExpression->second.end();
          getVariableName(subExpression,variables);
          prevId = 0;
          writeToPharmml(tokenBegin, tokenEnd,mathElseIf, prevId);
          subExpression->first = "writed+"+subExpression->first;
          expression = subExpression;
        }
      }
      conditionalStatement->appendChild(mathElseIf);
    }
    if(expression->first == "else")
    {
      AutoPtr<Element> mathElse = _pDocument->createElement("math:Else");
      TokenExpressions::iterator subExpression;
      expression->first = "writed+"+expression->first;
      for(subExpression = ++expression; subExpression!= expressionsEnd; ++subExpression)
      {
        if(subExpression->first == "if")
        {
          writeConditionalStatements(subExpression, expressionsEnd, mathElse,variables);
          expression = subExpression;
        }
        if(subExpression->first == "end")
        {
          break;
        }
        else
        {
          tokenBegin = subExpression->second.begin();
          tokenEnd = subExpression->second.end();
          getVariableName(subExpression,variables);
          prevId = 0;
          writeToPharmml(tokenBegin, tokenEnd,mathElse, prevId);
          subExpression->first = "writed+"+ subExpression->first;
          expression = subExpression;
        }
      }
      conditionalStatement->appendChild(mathElse);
    }
    if(expression->first == "end")
    {
      expression->first = "writed+"+expression->first;
      break;
    }
    if(expression != expressionsEnd){
      ++expression;
    }
  }
  expressionsBegin = expression;
  return element->appendChild(conditionalStatement);
}

void PharmXmlConverter::writePkMacros(TokenExpressions::iterator & expressionsBegin, const TokenExpressions::const_iterator & expressionsEnd, Element* element, int & cmt)
{
  AutoPtr<Element> pKmacros = _pDocument->createElement("PKmacros");
  if(!QString(expressionsBegin->first.c_str()).startsWith("pkMacro"))
  {
    throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::writePkMacros",
                                          std::string("wrong pk macro '"+ expressionsBegin->second[0].second +"' cannot translate to Pharmml"),
             THROW_LINE_FILE));
  }
  TokenPairs::const_iterator tokenBegin;
  TokenPairs::const_iterator tokenEnd;
  TokenExpressions::iterator expression;
  unsigned long int prevId;
  for(expression = expressionsBegin; expression!= expressionsEnd; ++expression )
  {
    if(expression->first == "pkMacro")
    {
      tokenBegin = expression->second.begin();
      tokenEnd = expression->second.end();
      Element*   macro;
      std::string macroName = tokenBegin->second;
      if (macroName == "iv"){
        macro = _pDocument->createElement("IV");
      }
      else
      {
        std::string::iterator macroBegin =macroName.begin();
        QString firstChar(*macroBegin);
        std::string newMacroName(firstChar.toUpper().toUtf8().data());
        newMacroName += std::string(++macroBegin,macroName.end());
        macro = _pDocument->createElement(newMacroName);
      }
      TokenPairs::const_iterator localIt = tokenBegin;
      int parOpen = 1;
      while(localIt != tokenEnd)
      {
        if(tokenBegin != tokenEnd){
          ++tokenBegin;
        }
        else{
          break;
        }
        if(tokenBegin == tokenEnd){
          break;
        }
        for(localIt = tokenBegin; localIt!= tokenEnd; ++localIt)
        {
          long unsigned int localId = localIt->first;
          if(localId == PCLOSE) parOpen--;
          else if(localId == POPEN) parOpen++;
          if(localId == COMMA || !parOpen){
            break;
          }
        }
        AutoPtr<Element> value = _pDocument->createElement("Value");
        std::string argumentName(tokenBegin->second);
        if(argumentName == "Cl"){
          value->setAttribute("argument","CL");
        }
        else{
          value->setAttribute("argument",argumentName);
        }
        if(tokenBegin != tokenEnd){
          ++tokenBegin;
        }
        if(tokenBegin != localIt)
        {
          if(tokenBegin->first != ASSIGN){
            throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::writePkMacros",
                                                  std::string("expected assignment  instead of  '"+ tokenBegin->second +"' cannot translate to Pharmml"),
                                                  THROW_LINE_FILE));
          }
          ++tokenBegin;
          if(tokenBegin == localIt){
            throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::writePkMacros",
                                                  std::string("unexpected value '"+ tokenBegin->second +"' cannot translate to Pharmml"),
                                                  THROW_LINE_FILE));
          }
          prevId = ASSIGN;
          if(argumentName == "cmt")
          {
            int cmtLocal = QString(tokenBegin->second.c_str()).toInt();
            cmt = std::max(cmt,cmtLocal);
          }
          writeToPharmml(tokenBegin, localIt,value,prevId);
        }
        else{
          appendSymbRef(value,argumentName);
        }
        if(argumentName == "concentration" || argumentName == "amount")
        {
          Element * valueChild = value->getChildElement("ct:SymbRef");
          std::string symbIdVal (valueChild->getAttribute("symbIdRef"));
          bool isVariable = false;
          if(argumentName == "amount")
          {
            NodeList * elementVar = element->getElementsByTagName("ct:DerivativeVariable");
            for(unsigned iVar = 0 ; iVar <  elementVar->length(); iVar++)
            {
              Element * declaredVar = static_cast<Element *> (elementVar->item(iVar));
              if(declaredVar->getAttribute("symbId") == symbIdVal)
              {
                isVariable = true;
                break;
              }
            }
            if(isVariable){
              break;
            }
          }
          if(!isVariable)
          {
            Element * variable = 0 ;
            createVariableNode(variable,symbIdVal,"real");
            Node* elementFirstChild = element->firstChild();
            if(elementFirstChild){
              element->insertBefore(variable,elementFirstChild);
            }
            else{
              element->appendChild(variable);
            }
          }
        }
        macro->appendChild(value);
        tokenBegin = localIt;
      }
      pKmacros->appendChild(macro);
      expression->first = "writed+"+expression->first;
    }
    else{
      break;
    }
  }
  // clean different macros and move out of value node expressions not being single variable, real, or int
  unsigned pKmacrosSize = pKmacros->childNodes()->length();
  unsigned iMacro = 0 ;
  Node * macro = pKmacros->firstChild();
  while(iMacro < pKmacrosSize)
  {
    Node * netxMacro = macro->nextSibling();
    NodeList * values = static_cast<Element*>(macro)->getElementsByTagName("Value");
    if(values)
    {
      for(unsigned iVal = 0; iVal< values->length(); iVal++)
      {
        Element * value = static_cast<Element*>(values->item(iVal));
        Node * valChild = value->firstChild();
        std::string valName(valChild->nodeName());
        if(valName != "ct:Int" && valName != "ct:Real" && valName != "ct:SymbRef")
        {
          AutoPtr<Element>  assign  = _pDocument->createElement("ct:Assign");
          value->removeChild(valChild);
          assign->appendChild(valChild);
          value->appendChild(assign);
        }
      }
    }
    macro = netxMacro;
    iMacro++;
  }

  element->appendChild(pKmacros);
}

void PharmXmlConverter::writeEquationSection(TokenExpressions::iterator & resultsBegin, const TokenExpressions::const_iterator & resultsEnd, Element* structuralModel)
{
  unsigned long int prevId = 0;
  Element*   initialTime = 0;
  TokenPairs::const_iterator tokenBegin;
  TokenPairs::const_iterator tokenEnd;
  // initial time
  for(TokenExpressions::iterator  expression = resultsBegin; expression!= resultsEnd; ++expression)
  {
    if(expression->first == "initialTime")
    {
      initialTime =  _pDocument->createElement("ct:InitialTime") ;
      tokenBegin = expression->second.begin();
      tokenEnd = expression->second.end();
      prevId = 0;
      writeToPharmml(tokenBegin, tokenEnd,initialTime,prevId);
      expression->first = "writed+"+expression->first;
      break;
    }
  }
  if(!initialTime)
  {
    initialTime =  _pDocument->createElement("ct:InitialTime");
    AutoPtr<Element> assign =  _pDocument->createElement("ct:Assign");
    AutoPtr<Element> real  =  _pDocument->createElement("ct:Real");
    Text* nullVal = _pDocument->createTextNode("0");
    real->appendChild(nullVal);
    assign->appendChild(real);
    initialTime->appendChild(assign);
  }
  int cmt = 0; // compartment number in pk models
  for(TokenExpressions::iterator  expression = resultsBegin; expression!= resultsEnd ;)
  {
    /***
     * parse result flags:
     * "equation"  "functionDefinition" "derivative"  "initialCondition+{derivativeName}"
     * "initialConditionFuncDef+{derivativeName}"    "if+{derivativeName}" "if"
     * "elseif"  "else"  "end"    "pkMacro"  "pkEquation"
     *
     ***/
    if(expression->first == "equation")
    {
      tokenBegin = expression->second.begin();
      tokenEnd = expression->second.end();
      std::string varName(tokenBegin->second);
      Element*   variable = 0;
      prevId = tokenBegin->first;
      if(varName =="odeType")
      {
        createVariableNode(variable,varName,"string");
        TokenPairs::const_iterator tokenCopy = ++tokenBegin;
        writeToPharmml(tokenBegin, tokenEnd,variable, prevId);
        Element*   variableOdeType = 0;
        ++tokenCopy;
        createVariableNode(variableOdeType,tokenCopy->second,"string");
        structuralModel->appendChild(variableOdeType);
      }
      else
      {
        createVariableNode(variable,varName,"real");
        writeToPharmml(++tokenBegin, tokenEnd,variable, prevId);
      }
      structuralModel->appendChild(variable);
      expression->first = "writed+"+expression->first;
    }
    else if(expression->first == "functionDefinition")
    {
      tokenBegin = expression->second.begin();
      tokenEnd = expression->second.end();
      Element*  functionNode;
      std::string functionName(tokenBegin->second);
      createFunctionDefNode(functionNode);
      functionNode->setAttribute("symbId",functionName);
      _userFuncIds.push_back(functionName);
      TokenPairs::const_iterator funcDefEnd;
      for(funcDefEnd = ++tokenBegin; funcDefEnd != tokenEnd; ++funcDefEnd)
      {
        if(funcDefEnd->first == ASSIGN){
          break;
        }
      }
      AutoPtr<Element> definition =_pDocument->createElement("Definition");
      for(TokenPairs::const_iterator itt = tokenBegin; itt!= funcDefEnd; ++itt)
      {
        long unsigned int localId = itt->first;
        if(localId != COMMA && localId != PCLOSE){
          appendFunctionArg(definition,getTokenValue(itt));
        }
      }
      tokenBegin = funcDefEnd;
      prevId = PCLOSE;
      writeToPharmml(tokenBegin,tokenEnd,definition,prevId);
      functionNode->appendChild(definition);
      Node * indeptVar = _pDocument->documentElement()->getChildElement("IndependentVariable");
      if(indeptVar){
        _pDocument->documentElement()->insertBefore(functionNode,indeptVar->nextSibling());
      }
      else{
        _pDocument->documentElement()->appendChild(functionNode);
      }
      expression->first = "writed+"+expression->first;
    }
    else if(expression->first == "derivative")
    {
      tokenBegin = expression->second.begin();
      tokenEnd = expression->second.end();
      AutoPtr<Element>   derivativeVariable = _pDocument->createElement("ct:DerivativeVariable");
      derivativeVariable->setAttribute("symbolType","real");
      std::string derivativeName(tokenBegin->second);
      derivativeVariable->setAttribute("symbId",derivativeName);
      // remove declared variables with derivative name
      NodeList * elementVar = structuralModel->getElementsByTagName("ct:Variable");
      for(unsigned iVar = 0 ; iVar <  elementVar->length(); iVar++)
      {
        Element * declaredVar = static_cast<Element *> (elementVar->item(iVar));
        if(declaredVar->getAttribute("symbId") == derivativeName)
        {
          structuralModel->removeChild(declaredVar);
          break;
        }
      }
      prevId = tokenBegin->first;
      writeToPharmml(++tokenBegin,tokenEnd,derivativeVariable,prevId);
      // initial conditions
      Element *initialValue = 0;
      for(TokenExpressions::iterator  condExpression = resultsBegin; condExpression!= resultsEnd; ++condExpression)
      {
        if(condExpression->first == "initialCondition+" + derivativeName)
        {
          if(!initialValue){
            initialValue =  _pDocument->createElement("ct:InitialValue");
          }
          tokenBegin = condExpression->second.begin();
          tokenEnd = condExpression->second.end();
          prevId = IDANY;
          writeToPharmml(tokenBegin,tokenEnd,initialValue,prevId);
        }
        else if(condExpression->first == "initialConditionFuncDef+" + derivativeName)
        {
          if(!initialValue){
            initialValue =  _pDocument->createElement("ct:InitialValue");
          }
          tokenBegin = condExpression->second.begin();
          tokenEnd = condExpression->second.end();
          while(tokenBegin->first != ASSIGN|| tokenBegin != tokenEnd){
            ++tokenBegin;
          }
          prevId = USERFUNC;
          writeToPharmml(tokenBegin,tokenEnd,initialValue,prevId);
        }
        else if(condExpression->first == "if+" + derivativeName)
        {
          if(!initialValue){
            initialValue =  _pDocument->createElement("ct:InitialValue");
          }
          writeConditionalStatements(condExpression,resultsEnd,initialValue);
        }
      }
      if(initialValue)
      {
        AutoPtr<Element> initialCondition = _pDocument->createElement("ct:InitialCondition");
        initialCondition->appendChild(initialValue);
        Node* initTimeCopy = initialTime->cloneNode(true);
        initialCondition->appendChild(initTimeCopy);
        derivativeVariable->appendChild(initialCondition);
      }
      structuralModel->appendChild(derivativeVariable);
      expression->first = "writed+"+expression->first;
    }
    else if(expression->first == "if"){
      writeConditionalStatements(expression,resultsEnd,structuralModel);
    }
    else if(expression->first == "pkMacro"){
      writePkMacros(expression,resultsEnd,structuralModel,cmt);
    }
    // increase iterator
    if(expression != resultsEnd){
      ++expression;
    }
  }
  // write pk equation after, in order to deal with correct cmt from other pk macros
  for(TokenExpressions::iterator  expression = resultsBegin; expression!= resultsEnd ;)
  {
    if(expression->first == "pkEquation")
    {
      /***
       *  translation of pkmodel(...) in to pk macros:
       * Cc = pkmodel(V, Cl)
       *   --->   compartment(cmt=1,concentration=Cc, volume=V)
       *        + iv(cmt=1)
       *        + elimination(cmt=1,Cl)
       * {Cc, Ce} = pkmodel(Tlag, ka, p, V, Vm, Km, k12, k21, k13, k31, ke0)
       *        --->   compartment(volume=V, concentration={Cc: first output})
       *             + absorption(Tk0, ka , Ktr ,Mtt,Tlag ,p )
       *             + elimination(k,Cl,Vm ,Km)
       *             + peripheral( k12,k21)
       *             + peripheral( k13,k31)
       *             + effect(concentrantion={Ce: second output}, ke0)
       ***/
      ++cmt;
      std::string i1cmt (QString::number(cmt).toUtf8().data());
      std::string i2cmt (QString::number(cmt+1).toUtf8().data());
      std::string i3cmt (QString::number(cmt+2).toUtf8().data());
      tokenBegin = expression->second.begin();
      tokenEnd = expression->second.end();
      TokenExpression compartment(TokenExpression("pkMacro",TokenPairs(1,TokenPair(PKFUNC,"compartment")))),
          iv(TokenExpression("pkMacro",TokenPairs(1,TokenPair(PKFUNC,"iv")))),
          elimination(TokenExpression("pkMacro",TokenPairs(1,TokenPair(PKFUNC,"elimination")))),
          absorption(TokenExpression("pkMacro",TokenPairs(1,TokenPair(PKFUNC,"absorption")))),
          peripheral2(TokenExpression("pkMacro",TokenPairs(1,TokenPair(PKFUNC,"peripheral")))),
          peripheral3(TokenExpression("pkMacro",TokenPairs(1,TokenPair(PKFUNC,"peripheral")))),
          effect(TokenExpression("pkMacro",TokenPairs(1,TokenPair(PKFUNC,"effect"))));
      TokenPair Cc, Ce(0,"EMPTY");
      if(tokenBegin->first != BOPEN){
        Cc = *tokenBegin;
      }
      else
      {
        ++tokenBegin;
        Cc = *tokenBegin;
        ++tokenBegin;
        if(tokenBegin->first == COMMA)
        {
          ++tokenBegin;
          Ce = *tokenBegin;
          ++tokenBegin;
        }
        if(tokenBegin->first != BCLOSE){
          throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::writeEquationSection",
                                                std::string("expected closing bracket    instead of  '"+ tokenBegin->second +"' cannot translate to Pharmml"),
                                                THROW_LINE_FILE));
        }
      }
      ++tokenBegin;
      if(tokenBegin->first != ASSIGN){
        throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::writeEquationSection",
                                              std::string("expected assignment   instead of  '"+ tokenBegin->second +"' cannot translate to Pharmml"),
                                              THROW_LINE_FILE));
      }
      ++tokenBegin;
      std::string funcName (tokenBegin->second);
      if(funcName!="pkmodel"){
        throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::writeEquationSection",
                                              std::string("expected assignment   instead of  '"+ funcName +"' cannot translate to Pharmml"),
                                              THROW_LINE_FILE));
      }
      for(TokenPairs::const_iterator token = ++tokenBegin; token != tokenEnd;  )
      {
        if(token->second =="V")
        {
          compartment.second.push_back(TokenPair(IDANY,"volume"));
          ++token;
          if(token->first != COMMA && token->first != BCLOSE) {
            copyPkmodelArgument(token,tokenEnd,compartment);
          }
          else
          {
            compartment.second.push_back(TokenPair(ASSIGN,"="));
            compartment.second.push_back(TokenPair(IDANY,"V"));
          }
          compartment.second.push_back(TokenPair(COMMA,","));
          compartment.second.push_back(TokenPair(IDANY,"concentration"));
          compartment.second.push_back(TokenPair(ASSIGN,"="));
          compartment.second.push_back(Cc);
        }
        else if(token->second =="Tk0" ||token->second =="ka" ||token->second =="Ktr" ||token->second =="Mtt" ||
                token->second =="Tlag" ||token->second =="p")
        {
          appendComma(absorption);
          copyPkmodelArgument(token,tokenEnd, absorption);
        }
        else if(token->second =="k"||token->second =="Cl" ||token->second =="Vm" ||token->second =="Km")
        {
          appendComma(elimination);
          copyPkmodelArgument(token,tokenEnd, elimination);
        }
        else if(token->second =="k12")
        {
          appendComma(peripheral2);
          peripheral2.second.push_back(TokenPair(IDANY,"k"+i1cmt+i2cmt));
          ++token;
          copyPkmodelArgument(token,tokenEnd,peripheral2);
        }
        else if(token->second =="k21")
        {
          appendComma(peripheral2);
          peripheral2.second.push_back(TokenPair(IDANY,"k"+i2cmt+i1cmt));
          ++token;
          copyPkmodelArgument(token,tokenEnd,peripheral2);
        }
        else if(token->second =="k13")
        {
          appendComma(peripheral3);
          peripheral3.second.push_back(TokenPair(IDANY,"k"+i1cmt+i3cmt));
          ++token;
          copyPkmodelArgument(token,tokenEnd,peripheral3);
        }
        else if(token->second =="k31")
        {
          appendComma(peripheral3);
          peripheral3.second.push_back(TokenPair(IDANY,"k"+i3cmt+i1cmt));
          ++token;
          copyPkmodelArgument(token,tokenEnd,peripheral3);
        }
        else if(token->second =="ke0")
        {
          appendComma(effect);
          copyPkmodelArgument(token,tokenEnd,effect);
        }
        else if (token->first != COMMA && token->first != PCLOSE)
        {
          throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::writeEquationSection",
                                                std::string("unknown pkmodel argument '"+ token->second +"' cannot translate to Pharmml"),
                                                THROW_LINE_FILE));
        }
        if(token != tokenEnd){
          ++token;
        }
      }
      if(Ce.first){
        appendComma(effect);
        effect.second.push_back(TokenPair(IDANY,"concentration"));
        effect.second.push_back(TokenPair(ASSIGN,"="));
        effect.second.push_back(Ce);
      }
      TokenExpressions pkModelMacros;
      bool isIv = true;
      if(compartment.second.size()>1){
        compartment.second.push_back(TokenPair(PCLOSE,")"));
        pkModelMacros.push_back(compartment);
      }
      if(elimination.second.size()>1){
        elimination.second.push_back(TokenPair(PCLOSE,")"));
        pkModelMacros.push_back(elimination);
      }
      if(absorption.second.size()>1)
      {
        absorption.second.push_back(TokenPair(PCLOSE,")"));
        pkModelMacros.push_back(absorption);
        isIv = false;
      }
      if(peripheral2.second.size()>1)
      {
        peripheral2.second.push_back(TokenPair(PCLOSE,")"));
        pkModelMacros.push_back(peripheral2);        
      }
      if(peripheral3.second.size()>1)
      {
        peripheral3.second.push_back(TokenPair(PCLOSE,")"));
        pkModelMacros.push_back(peripheral3);        
      }
      if(effect.second.size()>1)
      {
        effect.second.push_back(TokenPair(PCLOSE,")"));
        pkModelMacros.push_back(effect);        
      }
      if(isIv)
      {
        iv.second.push_back(TokenPair(IDANY,"cmt"));
        iv.second.push_back(TokenPair(ASSIGN,"="));
        iv.second.push_back(TokenPair(INT,i1cmt));
        iv.second.push_back(TokenPair(PCLOSE,")"));
        pkModelMacros.push_back(iv);
      }
      TokenExpressions::iterator  pkModelBegin = pkModelMacros.begin();
      TokenExpressions::const_iterator  pkModelEnd =  pkModelMacros.end();
      writePkMacros(pkModelBegin,pkModelEnd,structuralModel,cmt);
      expression->first = "writed+"+expression->first;
    }
    // increase iterator
    if(expression != resultsEnd){
      ++expression;
    }
  }
}

bool  PharmXmlConverter::findCategoryMapping(Element* externalDataSet, Element*& columnMapping, const std::string & varName)
{
  if(externalDataSet)
  {
    NodeList * columnMappingList = externalDataSet->getElementsByTagName("design:ColumnMapping");
    if(columnMappingList)
    {
      for(unsigned icolMap  =0; icolMap <columnMappingList->length(); icolMap++)
      {
        columnMapping = static_cast<Element*>(columnMappingList->item(icolMap));
        if(columnMapping->getElementById(varName,"symbIdRef")){
          return true;
        }
      }
    }
  }
  return false;
}

std::string PharmXmlConverter::pharmmlOpName(const std::string &opName)
{
  if(opName =="<") return "lt";
  else if( opName == "+")  return "plus";
  else if( opName == "-")  return "minus";
  else if( opName == "*")  return "times";
  else if( opName == "/" || opName == "./")  return "divide";
  else if( opName == "<=") return "leq" ;
  else if( opName == ">")  return "gt";
  else if( opName == ">=") return "geq";
  else if( opName == "==") return "eq";
  else if( opName == "!=" || opName == "~=") return "neq";
  else if( opName == "^") return "power";
  else if( opName == "|"||opName == "||") return "or";
  else if( opName == "&"||opName == "&&") return "and";
  else
  {
    throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::pharmmlOpName",
                                          std::string("unknown operator '"+ opName +"' cannot translate to Pharmml"),
                                          THROW_LINE_FILE));
    return "";
  }
}


void PharmXmlConverter::createVariableNode(Element * &variable, const std::string & varName,  const std::string &  varType)
{
  variable = _pDocument->createElement("ct:Variable");
  variable->setAttribute("symbolType",varType);
  variable->setAttribute("symbId",varName);
}

void  PharmXmlConverter::createFunctionDefNode(Element*& functionDef)
{
  functionDef = _pDocument->createElement("ct:FunctionDefinition");
  functionDef->setAttribute("xmlns","http://www.pharmml.org/pharmml/0.8/CommonTypes");
  functionDef->setAttribute("symbolType","real");
}

void  PharmXmlConverter::createOperationNode(Element*& operation, const std::string & opName, int&order)
{
  operation = _pDocument->createElement("Operation");
  operation->setAttribute("order",QString::number(++order).toUtf8().data());
  operation->setAttribute("opType",opName);
}

void  PharmXmlConverter::createEstimationStepNode(Element*& estimationStep, Element* externalDataSet)
{
  if(!estimationStep)
  {
    estimationStep = _pDocument->createElement("EstimationStep");
    estimationStep->setAttribute("oid","estimStep_1");
    if(externalDataSet)
    {
      Element * externalDataSetReference = _pDocument->createElement("ExternalDataSetReference");
      Element * oidRef = _pDocument->createElement("ct:OidRef");
      oidRef->setAttribute("oidRef","MLX_ds");
      externalDataSetReference->appendChild(oidRef);
      estimationStep->appendChild(externalDataSetReference);
    }
  }
}

void  PharmXmlConverter::appendFunctionArg(Element* functionNode, const std::string & argument)
{
  AutoPtr<Element> functionArgument = _pDocument->createElement("FunctionArgument");
  functionArgument->setAttribute("symbolType","real");
  functionArgument->setAttribute("symbId",argument);
  functionNode->appendChild(functionArgument);
}

void  PharmXmlConverter::appendSymbRef(Element* node, const std::string & symbIdRefAttribute, const std::string & blkIdRefAttribute)
{
  AutoPtr<Element> symbRef =  _pDocument->createElement("ct:SymbRef");
  symbRef->setAttribute("symbIdRef",symbIdRefAttribute);
  if(blkIdRefAttribute!=""){
    symbRef->setAttribute("blkIdRef",blkIdRefAttribute);
  }
  node->appendChild(symbRef);
}

template<typename iterator>
void PharmXmlConverter::createBinRelationNode(Element* & binRelation, const iterator & iter)
{
  unsigned long id = getTokenId(iter);
  std::string value = getTokenValue(iter);
  if (id == RELOP|| id  == CONDOP)
  {
    binRelation  =  _pDocument->createElement("math:LogicBinop");
    binRelation->setAttribute("op",pharmmlOpName(value));
  }
  else if (id == BINOP||id == UNOP|| id == PLUS || id == MINUS)
  {
    binRelation  =  _pDocument->createElement("math:Binop");
    binRelation->setAttribute("op",pharmmlOpName(value));
  }
  else
  {
    throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::createBinRelationNode",
                                          std::string("unimplemented case,  current value: '"+value +"' cannot translate to Pharmml"),
                                          THROW_LINE_FILE));
  }
}

template<typename iterator>
void PharmXmlConverter::writeNextToken(iterator & iter, const iterator & tokenEnd, Element* parentElt, Element* currentElt)
{
  long unsigned int  prevId = getTokenId(iter);
  if(iter==tokenEnd)
  {
    parentElt->appendChild(currentElt);
    return;
  }
  iter++;
  if(iter ==tokenEnd){
    parentElt->appendChild(currentElt);
  }
  else
  {
    long unsigned int  id = getTokenId(iter);
    checkId(prevId,id,getTokenValue(iter),__LINE__,__FILE__);
    if( id<= BOPEN ||id == REAL ||id == INT || (id >= MATHUNARYFUNC && id <= USERFUNC)||id == IDANY )
    {
      Element* binRelation = 0;
      createBinRelationNode(binRelation,iter);
      binRelation->appendChild(currentElt);
      iterator localIter = ++iter;
      int nOperations = 0;
      getOperationsSequence(localIter, tokenEnd, nOperations);
      prevId = id;
      iterator localIter0;
      checkBinOperation(localIter0,tokenEnd,iter,nOperations);
      writeToPharmml(iter, localIter,binRelation,prevId);
      parentElt->appendChild(binRelation);
      writeNextToken(localIter0,tokenEnd,parentElt,binRelation);
      iter= localIter0;
    }
    else if (id == BCLOSE ||  id == PCLOSE) {
      parentElt->appendChild(currentElt);
    }
    else if (id ==  ASSIGN)
    {
      AutoPtr<Element> assign = _pDocument->createElement("ct:AssignStatement");
      assign->setAttribute("op","eq");
      prevId = id;
      assign->appendChild(currentElt);
      ++iter;
      writeToPharmml(iter,tokenEnd,assign,prevId);
      parentElt->appendChild(assign);

    }
    else if(id!=END)
    {
      std::string vals(getTokenValue(iter));
      throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::writeNextToken",
                                            std::string("unimplemented case,  current value: '"+vals +"' cannot translate to Pharmml"),
                                            THROW_LINE_FILE));
    }
  }
}

template<typename iterator>
void  PharmXmlConverter::getFunctionArgument(iterator& tokenBegin, const iterator& tokenEnd, Element* argument,
                                             Node* &argumentFirstchild, long unsigned int prevId, const std::string& functionName)
{
  iterator argEnd;
  findArgumentEnd(argEnd, tokenBegin,tokenEnd);
  writeToPharmml(tokenBegin,argEnd,argument,prevId);
  std::string lastValueInArg(getTokenValue(argEnd));
  //check that node argument has exactly one child
  argumentFirstchild = argument->firstChild();
  if(argumentFirstchild != argument->lastChild())
  {
    throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::getFunctionArgumentChild",
                                          std::string("incorect argument before : '"+ lastValueInArg
                                                      +"', in function call  '" + functionName +"' cannot translate to Pharmml"),
                                          THROW_LINE_FILE));
  }
}

template<typename iterator>
void PharmXmlConverter::writeToPharmml(iterator &iter, const iterator & tokenEnd, Element* pharmml, long unsigned int& prevId)
{
  // use parse results  of  grammar or
  // use tokens from lexer on a single line expression --> if multiple lines exists, must use grammar
  for(; iter!= tokenEnd; )
  {
    long unsigned int  id =  getTokenId(iter);
    std::string value(getTokenValue(iter));
    checkId(prevId,id, value,__LINE__,__FILE__);
    if(id == BCLOSE ||  id == PCLOSE|| ((prevId == ASSIGN|| (prevId >= MATHUNARYFUNC && prevId <= USERFUNC)) && id == PLUS)){
      ;//do nothing
    }
    else if (id ==  ASSIGN)
    {
      AutoPtr<Element> assign = _pDocument->createElement("ct:Assign");
      ++iter;
      prevId = id;
      writeToPharmml(iter, tokenEnd,assign,prevId);
      pharmml->appendChild(assign);
    }
    else if (id == REAL)
    {
      AutoPtr<Element> realNode = _pDocument->createElement("ct:Real");
      Text* realValue  =  _pDocument->createTextNode(value);
      realNode->appendChild(realValue);
      writeNextToken(iter,tokenEnd,pharmml,realNode);
    }
    else if (id == INT)
    {
      AutoPtr<Element> intNode = _pDocument->createElement("ct:Int");
      Text* intValue  =  _pDocument->createTextNode(value);
      intNode->appendChild(intValue);
      writeNextToken(iter,tokenEnd,pharmml,intNode);
    }
    else if (id == IDANY)
    {
      AutoPtr<Element> symbref = _pDocument->createElement("ct:SymbRef");
      symbref->setAttribute("symbIdRef",value);
      writeNextToken(iter,tokenEnd,pharmml,symbref);
    }
    else if (id == MATHUNARYFUNC||id == STATFUNC)
    {
      AutoPtr<Element> unop;
      if(id == MATHUNARYFUNC){
        unop = _pDocument->createElement("math:Uniop");
      }
      else{
        unop = _pDocument->createElement("math:Statsop");
      }
      unop->setAttribute("op",value);
      int parOpen = 1;
      iterator localIter;
      ++iter;
      getClosedParenthesis(localIter, iter,tokenEnd, parOpen);
      prevId = id;
      writeToPharmml(iter, localIter,unop,prevId);
      writeNextToken(iter,tokenEnd,pharmml,unop);
    }
    else if (id == MATH_NARYFUNC)
    {
      AutoPtr<Element> naryop = _pDocument->createElement("math:Naryop");
      naryop->setAttribute("op",value);
      int parOpen = 1;
      iterator localIter;
      ++iter;
      getClosedParenthesis(localIter,iter,tokenEnd, parOpen);
      prevId = id;
      AutoPtr<Element> vectorElements = _pDocument->createElement("ct:VectorElements");
      for(iterator itt = iter; itt!= localIter ; )
      {
        Element* argument = _pDocument->createElement("tmp");
        Node* argumentFirstchild;
        getFunctionArgument(itt, localIter,argument, argumentFirstchild, prevId, value);
        if(argumentFirstchild->nodeName() != "ct:Real" && argumentFirstchild->nodeName() != "ct:Int"
           && argumentFirstchild->nodeName() != "ct:SymbRef")
        {
          Element* assign = _pDocument->createElement("ct:Assign");
          assign->appendChild(argumentFirstchild);
          vectorElements->appendChild(assign);
        }
        else{
          vectorElements->appendChild(argumentFirstchild);
        }
        if(itt!= localIter){
          ++itt;
        }
        iter =itt;
      }
      AutoPtr<Element> vector = _pDocument->createElement("ct:Vector");
      vector->appendChild(vectorElements);
      naryop->appendChild(vector);
      writeNextToken(iter,tokenEnd,pharmml,naryop);
    }
    else if (id == DELAY)
    {
      AutoPtr<Element> delay = _pDocument->createElement("ct:Delay");
      int parOpen = 1;
      iterator localIter;
      ++iter;
      getClosedParenthesis(localIter,iter,tokenEnd, parOpen);
      prevId = id;
      int nArgs = 0;
      for(iterator itt = iter; itt!= localIter ; )
      {
        Element* argument = _pDocument->createElement("tmp");
        Node* argumentFirstchild;
        getFunctionArgument(itt, localIter,argument, argumentFirstchild, prevId, value);
        if(!nArgs){
          delay->appendChild(argumentFirstchild);
        }
        else{
          AutoPtr<Element> delayVariable = _pDocument->createElement("ct:DelayVariable");
          delayVariable->appendChild(argumentFirstchild);
          delay->appendChild(delayVariable);
        }
        if(nArgs >= 2){
          std::string nargs(QString::number(nArgs+1).toUtf8().data());
          throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::writeToPharmml",
                                                std::string("expected two arguments for delay instead of  '"+nargs +"' cannot translate to Pharmml"),
                                                THROW_LINE_FILE));
        }
        nArgs++;
        if(itt!= localIter){
          ++itt;
        }
        iter =itt;
      }
      writeNextToken(iter,tokenEnd,pharmml,delay);
    }
    else if (id == USERFUNC)
    {
      bool isUndefFunc = true;
      Element* functionNode = 0;
      int parOpen = 1;
      iterator localIter;
      ++iter;
      getClosedParenthesis(localIter,iter,tokenEnd, parOpen);
      //check if the next iter to localIter  is assignment, then it defines the user function
      iterator nextLocalIter;
      for(nextLocalIter = localIter; nextLocalIter!= tokenEnd; )
      {
        ++nextLocalIter;
        break;
      }
      if(getTokenId(nextLocalIter) == ASSIGN)
      {
        createFunctionDefNode(functionNode);
        functionNode->setAttribute("symbId",value);
        for(unsigned ii = 0; ii< _userFuncIds.size(); ii++)
        {
          if(_userFuncIds[ii] == value)
          {
            isUndefFunc = false;
            break;
          }
        }
        if(isUndefFunc)
        {
          _userFuncIds.push_back(value);
          AutoPtr<Element> definition =_pDocument->createElement("Definition");
          for(iterator itt = iter; itt!= localIter; ++itt)
          {
            long unsigned int localId = getTokenId(itt);
            if(localId != COMMA && localId != PCLOSE){
              appendFunctionArg(functionNode,getTokenValue(itt));
            }
          }
          iter = nextLocalIter;
          prevId = id;
          writeToPharmml(iter,tokenEnd,definition,prevId);
          functionNode->appendChild(definition);
        }
        else {
          iter = tokenEnd;
        }
      }
      else
      {
        functionNode = _pDocument->createElement("math:FunctionCall");
        Element * functionDef = _pDocument->documentElement()->getElementById(value,"symbId");
        if(!functionDef)
        {
          throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::writeToPharmml",
                                                std::string("undefined function  '" + value+"' cannot translate to Pharmml"),
                                                THROW_LINE_FILE));
        }
        appendSymbRef(functionNode,value);
        const NodeList* pharmmlFunctionArgs = getChildNodesByTagName(functionDef,"FunctionArgument");
        unsigned pharmmlFunctionArgsLength = pharmmlFunctionArgs->length();
        unsigned iArg = 0;
        prevId = id;
        for(iterator itt = iter; itt!= localIter ; )
        {
          AutoPtr<Element> argument = _pDocument->createElement("tmp");
          Node* argumentFirstchild;
          getFunctionArgument(itt, localIter,argument, argumentFirstchild, prevId, value);
          AutoPtr<Element> functionArgument = _pDocument->createElement("math:FunctionArgument");
          //check the number of arguments of the function
          if(iArg == pharmmlFunctionArgsLength)
          {
            throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::writeToPharmml",
                                                  std::string("incorect number of arguments in function call '" + value+"' cannot translate to Pharmml"),
                                                  THROW_LINE_FILE));
          }
          const NamedNodeMap* argsAttr = pharmmlFunctionArgs->item(iArg++)->attributes();
          std::string argumentName(argsAttr->getNamedItem("symbId")->nodeValue());
          functionArgument->setAttribute("symbId",argumentName);
          functionArgument->appendChild(argumentFirstchild);
          prevId = getTokenId(itt);
          if(itt!= localIter){
            ++itt;
          }
          functionNode->appendChild(functionArgument);
          iter = itt;
        }
      }
      if(isUndefFunc)
      {
        writeNextToken(iter,tokenEnd,pharmml,functionNode);
      }
    }
    else if(id == BOPEN||id == POPEN)
    {
      int parOpen = 1;
      iterator localIter;
      ++iter;
      getClosedParenthesis(localIter, iter,tokenEnd, parOpen);
      //get the next iter to localIter
      iterator nextLocalIter;
      for(nextLocalIter = localIter; nextLocalIter!= tokenEnd; )
      {
        ++nextLocalIter;
        break;
      }
      prevId = id;
      if(nextLocalIter == tokenEnd) {
        writeToPharmml(iter, localIter,pharmml,prevId);
      }
      else
      {
        id =  getTokenId(nextLocalIter);
        value = getTokenValue(nextLocalIter);
        if( id<= BOPEN ||id == REAL ||id == INT || (id >= MATHUNARYFUNC && id <= USERFUNC)  ||id == IDANY )
        {
          Element* binRelation = 0;
          createBinRelationNode(binRelation,nextLocalIter);
          writeToPharmml(iter,localIter,binRelation,prevId);
          ++iter;
          prevId = getTokenId(iter);
          iterator localIter = ++iter;
          int nOperations = 0;
          getOperationsSequence(localIter, tokenEnd, nOperations);
          prevId = id;
          iterator localIter0;
          checkBinOperation(localIter0,tokenEnd,iter,nOperations);
          writeToPharmml(iter, localIter,binRelation,prevId);
          writeNextToken(localIter0,tokenEnd,pharmml,binRelation);
          iter = localIter0;
        }
        else
        {
          throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::writeToPharmml",
                                                std::string("unexpected   case  '"+ value +"' cannot translate to Pharmml"),
                                                THROW_LINE_FILE));
        }
      }
    }
    else if(( prevId == ASSIGN || prevId == POPEN  ||(prevId >= MATHUNARYFUNC && prevId <= USERFUNC)) && id == MINUS)
    {
      AutoPtr<Element>  unop = _pDocument->createElement("math:Uniop");
      unop->setAttribute("op","minus");
      int parOpen = 1;
      if(prevId == ASSIGN){
        parOpen = 0;
      }
      iterator localIter;
      int nOperations = 0;
      ++iter;
      getClosedParenthesis(localIter,iter,tokenEnd, parOpen);
      prevId = id;
      iterator localIter0;
      checkBinOperation(localIter0,tokenEnd,iter,nOperations);
      if(localIter == iter)
      {
        localIter0 = iter;
        ++localIter;
        writeToPharmml(iter,localIter,unop,prevId);
        iter = localIter0;
      }
      else{
        writeToPharmml(iter, localIter,unop,prevId);
      }
      writeNextToken(iter,tokenEnd,pharmml,unop);

    }
    else{
      throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::writeToPharmml",
                                            std::string("unexpected   case  '"+ value +"' cannot translate to Pharmml"),
                                            THROW_LINE_FILE));
    }
    if(iter!=tokenEnd)
    {
      prevId = getTokenId(iter);
      iter++;
    }
  }
}

void PharmXmlConverter::checkId(const long unsigned int& previousId,const  long unsigned int &id, const std::string tokenValue, const int& line, const char* file)
{
  if(id ==UNKNOWN)
  {
    throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::checkId",
                                          std::string("unknown string '"+ tokenValue +"' cannot translate to Pharmml"),
                                          line, file));
  }
  else if(( (id <= BINOP) &&  (previousId <= BINOP) )|| ((id == REAL|| id == INT) &&  (previousId ==  REAL||previousId ==INT)) )
  {
    throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::checkId",
                                          std::string("incorrect syntax before '"+ tokenValue +"' cannot translate to Pharmml"),
                                          line, file));
  }
}

void PharmXmlConverter::writeTransformation(const Node *mlxPriorTransformation,  Element*structuredModel,const Node *mlxPrior)
{
  if(mlxPriorTransformation)
  {
    std::string transformationName;
    if(pharmmlTransformationName(mlxPriorTransformation->nodeValue(),transformationName))
    {
      AutoPtr<Element>  transformation  = _pDocument->createElement("Transformation");
      transformation->setAttribute("type",transformationName);
      structuredModel->appendChild(transformation);
    }
  }
  if(mlxPrior)
  {
    QString transformation(mlxPrior->nodeValue().c_str());
    QStringList argPrior = transformation.split("prior(",QString::SkipEmptyParts);
    AutoPtr<Element> distribution =  _pDocument->createElement("Distribution");
    AutoPtr<Element>  probOnto = _pDocument->createElement("ProbOnto");
    probOnto->setAttribute("xmlns","http://www.pharmml.org/probonto/ProbOnto");
    probOnto->setAttribute("name","Normal1");
    AutoPtr<Element>  meanParameter= _pDocument->createElement("Parameter");
    meanParameter->setAttribute("name","median");
    AutoPtr<Element> meanAssign =  _pDocument->createElement("ct:Assign");
    AutoPtr<Element> meanReal =  _pDocument->createElement("ct:Real");
    Text* value =_pDocument->createTextNode(argPrior[0].trimmed().toUtf8().data());
    meanReal->appendChild(value);
    meanAssign->appendChild(meanReal);
    meanParameter->appendChild(meanAssign);
    AutoPtr<Element>  stdevParameter= _pDocument->createElement("Parameter");
    stdevParameter->setAttribute("name","var");
    AutoPtr<Element> stdevAssign =  _pDocument->createElement("ct:Assign");
    AutoPtr<Element> stdevReal =  _pDocument->createElement("ct:Real");
    QStringList argVariance = argPrior[1].split(")",QString::SkipEmptyParts);
    Text* stdevValue =_pDocument->createTextNode(argVariance[0].toUtf8().data());
    stdevReal->appendChild(stdevValue);
    stdevAssign->appendChild(stdevReal);
    stdevParameter->appendChild(stdevAssign);
    probOnto->appendChild(meanParameter);
    probOnto->appendChild(stdevParameter);
    distribution->appendChild(probOnto);
    structuredModel->appendChild(distribution);
  }
}

void PharmXmlConverter::writeInitialization(const Node*interceptInit,Element* parametersToEstimate, const std::string& mlxParamName, const std::string& descriptionArgName, const  Node* descriptionArg)
{
  if(interceptInit)
  {
    AutoPtr<Element> parameterEstimation =  _pDocument->createElement("ParameterEstimation");
    appendSymbRef(parameterEstimation,mlxParamName,"pm");
    AutoPtr<Element> initialEstimate =  _pDocument->createElement("InitialEstimate");
    initialEstimate->setAttribute("fixed","false");
    AutoPtr<Element> real =  _pDocument->createElement("ct:Real");
    QStringList  initialValArg(QString(interceptInit->nodeValue().c_str()).split(QRegExp("\\s+"),QString::SkipEmptyParts));
    bool estimation = true;
    if(initialValArg.size()>1)
    {
      if(!initialValArg[1].compare("fixed"))
      {
        initialEstimate->setAttribute("fixed","true");
        estimation = false;
      }
    }
    //    if(estimation &&descriptionArg){
    //      appendDescription(initialEstimate, descriptionArgName +" = "+descriptionArg->getNodeValue());
    //    }
    Text* initialValue =_pDocument->createTextNode(initialValArg[0].toUtf8().data());
    real->appendChild(initialValue);
    initialEstimate->appendChild(real);
    parameterEstimation->appendChild(initialEstimate);
    parametersToEstimate->appendChild(parameterEstimation);
  }
}

void PharmXmlConverter::writeRandomVariable(const std::string & symbIdName, std::string  symbIdRefAttribute,  std::string  blkIdRefAttribute,
                                            std::string  meanName,  std::string  stdevName, Element* stdevAssign,
                                            Element* parameterModel, Node* & lastPopParameter)
{
  AutoPtr<Element> randomVariable =  _pDocument->createElement("RandomVariable");
  randomVariable->setAttribute("symbId",symbIdName);
  AutoPtr<Element> variabilityReference =  _pDocument->createElement("ct:VariabilityReference");
  appendSymbRef(variabilityReference,symbIdRefAttribute,blkIdRefAttribute);
  AutoPtr<Element> distribution =  _pDocument->createElement("Distribution");
  AutoPtr<Element>  probOnto = _pDocument->createElement("ProbOnto");
  probOnto->setAttribute("xmlns","http://www.pharmml.org/probonto/ProbOnto");
  if(stdevName == "var"){
    probOnto->setAttribute("name","Normal2");
  }
  else if (stdevName == "stdev"){
    probOnto->setAttribute("name","Normal1");
  }
  AutoPtr<Element>  meanParameter= _pDocument->createElement("Parameter");
  meanParameter->setAttribute("name",meanName);
  AutoPtr<Element> meanAssign =  _pDocument->createElement("ct:Assign");
  AutoPtr<Element> meanReal =  _pDocument->createElement("ct:Real");
  Text* zero =_pDocument->createTextNode("0");
  meanReal->appendChild(zero);
  meanAssign->appendChild(meanReal);
  meanParameter->appendChild(meanAssign);
  AutoPtr<Element>  stdevParameter= _pDocument->createElement("Parameter");
  stdevParameter->setAttribute("name",stdevName);
  stdevParameter->appendChild(stdevAssign);
  probOnto->appendChild(meanParameter);
  probOnto->appendChild(stdevParameter);
  distribution->appendChild(probOnto);
  randomVariable->appendChild(variabilityReference);
  randomVariable->appendChild(distribution);
  Node * NextToLastPopParam = 0;
  if(lastPopParameter){
    NextToLastPopParam = lastPopParameter->nextSibling();
  }
  if(NextToLastPopParam){
    parameterModel->insertBefore(randomVariable,NextToLastPopParam);
  }
  else{
    parameterModel->appendChild(randomVariable);
  }
}

void PharmXmlConverter::writeVariability(const Node* mlxVariability, Element* parametersToEstimate,
                                         Element* structuredModel, Element* parameterModel,Node* &lastPopParameter, const std::string& mlxParamName)
{
  const NamedNodeMap* mlxVarAttr  = mlxVariability->attributes();
  const Node* mlxVarAttrInit = mlxVarAttr->getNamedItem("initialization");
  const Node* mlxVarLevel = mlxVarAttr->getNamedItem("level");
  std::string mlxVarLevelType(mlxVarLevel->getNodeValue());
  const Node* mlxVarLevelName  = mlxVarAttr->getNamedItem("levelName");
  const Node* mlxVarAttrCov  = mlxVarAttr->getNamedItem("covariate");
  std::string randomEffetName("ETA");
  std::string covarianceName("omega");
  bool isVariance = false;
  if(mlxVarLevelType != "1")
  {
    randomEffetName += mlxVarLevelType;
    covarianceName  += mlxVarLevelType;
  }
  randomEffetName += "_"+ mlxParamName;
  covarianceName  += "_"+ mlxParamName;
  const Node* sdNode = mlxVarAttr->getNamedItem("standardDeviation");
  if(sdNode){
    covarianceName = sdNode->getNodeValue();
    QStringList covSplit(QString(covarianceName.c_str()).split("(",QString::SkipEmptyParts));
    if(covSplit.size()>1)
    {
      covarianceName = std::string(covSplit[1].split(")",QString::SkipEmptyParts)[0].toUtf8().data());
      isVariance = true;
    }
  }
  AutoPtr<Element> randomEffects =  _pDocument->createElement("RandomEffects");
  if(mlxVarLevelName || mlxVarAttrCov)
  {
    std::string description;
    if(mlxVarLevelName)
    {
      description = "levelName = "+mlxVarLevelName->getNodeValue();
      if(mlxVarAttrCov){
        description += ", covariate = "+mlxVarAttrCov->getNodeValue();
      }
    }
    else{
      description =  "covariate = "+mlxVarAttrCov->getNodeValue();
    }
    appendDescription(randomEffects,description);
  }
  appendSymbRef(randomEffects,randomEffetName,"pm");
  structuredModel->appendChild(randomEffects);
  AutoPtr<Element> stdevAssign =  _pDocument->createElement("ct:Assign");
  appendSymbRef(stdevAssign,covarianceName);
  if(isVariance){
    writeRandomVariable(randomEffetName, "ID","vm_mdl","mean","var",stdevAssign,parameterModel,lastPopParameter);
  }
  else{
  writeRandomVariable(randomEffetName, "ID","vm_mdl","mean","stdev",stdevAssign,parameterModel,lastPopParameter);
  }
  writeInitialization(mlxVarAttrInit,parametersToEstimate,covarianceName);
  writePopParameter(covarianceName,lastPopParameter,parameterModel);
}

void PharmXmlConverter::writeHeader(){
  if (!_pRoot.get()) loadEmpty("PharmML");
  Element* pharmml = _pDocument->documentElement();
  pharmml->setAttribute("xmlns","http://www.pharmml.org/pharmml/0.8/PharmML");
  pharmml->setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
  pharmml->setAttribute("xsi:schemaLocation","http://www.pharmml.org/pharmml/0.8/PharmML  "
                                             "http://www.pharmml.org/pharmml/0.8/PharmML");
  pharmml->setAttribute("xmlns:math","http://www.pharmml.org/pharmml/0.8/Maths");
  pharmml->setAttribute("xmlns:ct","http://www.pharmml.org/pharmml/0.8/CommonTypes");
  pharmml->setAttribute("xmlns:ds","http://www.pharmml.org/pharmml/0.8/Dataset");
  pharmml->setAttribute("xmlns:design","http://www.pharmml.org/pharmml/0.8/TrialDesign");
  pharmml->setAttribute("xmlns:po","http://www.pharmml.org/probonto/ProbOnto");
  pharmml->setAttribute("writtenVersion","0.8");
  Element* name = _pDocument->createElement("ct:Name");
  Text* textNode =  _pDocument->createTextNode("generated by lixoftLanguageTranslator v.1.0   implementedBy LIXOFT");
  name->appendChild(textNode);
  pharmml->appendChild(name);
  Element*  independentVariable = _pDocument->createElement("IndependentVariable");
  independentVariable->setAttribute("symbId","t");
  pharmml->appendChild(independentVariable);
}

void PharmXmlConverter::saveWithXmlDeclaration(const std::string& path) const
{
  //combines xml header declaration and  mlxPoco::XML::XMLWriter::PRETTY_PRINT format
  mlxPoco::FileOutputStream ostr(path);
  if (ostr.good())
  {
    XMLWriter headerDoc(ostr,mlxPoco::XML::XMLWriter::WRITE_XML_DECLARATION);
    headerDoc.startDocument();
    headerDoc.characters("\n");
    DOMWriter writer;
    writer.setNewLine("\n");
    writer.setOptions(mlxPoco::XML::XMLWriter::PRETTY_PRINT);
    writer.writeNode(ostr, _pDocument);
  }
  else{
    throw mlxPoco::CreateFileException(path);
  }
}



mlxPoco::XML::Node* getChildNode(const mlxPoco::XML::Node* node,const std::string& name)
{
  mlxPoco::XML::Node* pNode = node->firstChild();
  while (pNode && !(pNode->nodeType() == mlxPoco::XML::Node::ELEMENT_NODE && pNode->nodeName() == name))
    pNode = pNode->nextSibling();
  return  pNode;
}

mlxPoco::XML::Node* getChildNode(const mlxPoco::XML::Element* element,const std::string& name)
{
  mlxPoco::XML::Node* pNode = element->firstChild();
  while (pNode && !(pNode->nodeType() == mlxPoco::XML::Node::ELEMENT_NODE && pNode->nodeName() == name))
    pNode = pNode->nextSibling();
  return  pNode;
}

mlxPoco::XML::NodeList* getChildNodesByTagName(const mlxPoco::XML::Node* node,const std::string& name)
{
  return  (dynamic_cast<mlxPoco::XML::Element*>(const_cast<mlxPoco::XML::Node*>(node)))->getElementsByTagName(name);
}

bool isNotEmpty(const mlxPoco::XML::Node* node)
{
  return  (node->hasAttributes()||node->hasChildNodes());
}

std::pair<std::string,std::string> PharmXmlConverter::pharmmlColTypes(const QString & colname)
{
  QString colnameTolower(colname.toLower());
  if(!colnameTolower.compare("id") || !colnameTolower.compare("#id") || !colnameTolower.compare("i") ||
     !colnameTolower.compare("tiempo")){
    return std::pair<std::string,std::string>("id","string");
  }
  else  if(!colnameTolower.compare("time") || !colnameTolower.compare("t")){
    return std::pair<std::string,std::string>("time","real");
  }
  else  if(!colnameTolower.compare("amt") || !colnameTolower.compare("dose") ||
           !colnameTolower.compare("d")){
    return std::pair<std::string,std::string>("dose","real");
  }
  else  if(!colnameTolower.compare("x") || !colnameTolower.compare("x_") ||
           !colnameTolower.compare("reg") || !colnameTolower.compare("xx")){
    return std::pair<std::string,std::string>("reg","real");
  }
  else  if(!colnameTolower.compare("y") || !colnameTolower.compare("dv") ||
           !colnameTolower.compare("conc") || !colnameTolower.compare("obs"))
  {
    return std::pair<std::string,std::string>("dv","real");
  }
  else  if( !colnameTolower.compare("ytype") || !colnameTolower.compare("itype") ||
            !colnameTolower.compare("type") || !colnameTolower.compare("flag") ||
            !colnameTolower.compare("dvid")){
    return std::pair<std::string,std::string>("dvid","int");
  }
  else  if(!colnameTolower.compare("cov") || !colnameTolower.compare("c")||
           !colnameTolower.compare("cov_")){
    return std::pair<std::string,std::string>("covariate","real");
  }
  else  if(!colnameTolower.compare("cat") || !colnameTolower.compare("cat_")){
    return std::pair<std::string,std::string>("covariate","string");
  }
  else  if(!colnameTolower.compare("adm") || !colnameTolower.compare("dpt")){
    return std::pair<std::string,std::string>("adm","int");
  }
  else  if(!colnameTolower.compare("mdv")){
    return std::pair<std::string,std::string>("mdv","int");
  }
  else  if(!colnameTolower.compare("evid") || !colnameTolower.compare("evd")){
    return std::pair<std::string,std::string>("evid","int");
  }
  else  if(!colnameTolower.compare("cens")){
    return std::pair<std::string,std::string>("censoring","real");
  }
  else  if(!colnameTolower.compare("limit")){
    return std::pair<std::string,std::string>("limit","real");
  }
  else  if(!colnameTolower.compare("rate") || !colnameTolower.compare("r")){
    return std::pair<std::string,std::string>("rate","real");
  }
  else  if(!colnameTolower.compare("ii") || !colnameTolower.compare("int")||
           !colnameTolower.compare("tau")){
    return std::pair<std::string,std::string>("ii","real");
  }
  else  if(!colnameTolower.compare("addl") || !colnameTolower.compare("add")){
    return std::pair<std::string,std::string>("addl","int");
  }
  else  if(!colnameTolower.compare("ss")){
    return std::pair<std::string,std::string>("ss","real");
  }
  else  if(!colnameTolower.compare("occasion") || !colnameTolower.compare("occ")){
    return std::pair<std::string,std::string>("occasion","real");
  }
  else  if(!colnameTolower.compare("cmt") || !colnameTolower.compare("cmp")){
    return std::pair<std::string,std::string>("cmt","int");
  }
  else  if(!colnameTolower.compare("ignore")){
    return std::pair<std::string,std::string>("undefined","string");
  }
  else{
    throwLater(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::pharmmlColTypes",
                                            QString("unknown type of colummn '"+ colname+"' not translated to Pharmml").toUtf8().data(),
                                            THROW_LINE_FILE));
    //TODO remains "tinf" ("xdose" "xd")  ("repl" "jeu")
    return std::pair<std::string,std::string>("UNKNOWN","UNKNOWN");
  }
}

std::string PharmXmlConverter::pharmmlDelimiterTypes(const std::string & delimiter)
{
  if(delimiter == "\\t"){
    return "TAB";
  }
  else  if(delimiter == ","){
    return "COMMA";
  }
  else  if(delimiter == ";"){
    return "SEMICOLON";
  }
  else if(delimiter == " "){
    return "SPACE";
  }
  else{
    throwLater(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::pharmmlDelimiterTypes",
                                            std::string("unknown type of delimiter '"+delimiter+"' not translated to Pharmml"),
                                            THROW_LINE_FILE));
    return "UNKNOWN";
  }
}

bool PharmXmlConverter::pharmmlTransformationName(const std::string & mlxTransformation, std::string& pharmmlTransformation)
{
  if(mlxTransformation == "L")
  {
    pharmmlTransformation = "log";
    return true;
  }
  else  if(mlxTransformation == "G")
  {
    pharmmlTransformation = "logit";
    return true;
  }
  else  if(mlxTransformation == "P")
  {
    pharmmlTransformation = "probit";
    return true;
  }
  else if(mlxTransformation == "N"){
    return  false;
  }
  else
  {
    bool isIdDefined = false;
    for(unsigned i =0; i < _userFuncIds.size();i++)
    {
      std::string id(_userFuncIds[i]);
      if( mlxTransformation == id)
      {
        pharmmlTransformation = id;
        isIdDefined = true;
        break;
      }
    }
    if(isIdDefined){
      return  true;
    }
    else
    {
      throwLater(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::pharmmlDelimiterTypes",
                                              std::string("unknown type of transformation '"+mlxTransformation+"' not translated to Pharmml"),
                                              THROW_LINE_FILE));
      pharmmlTransformation = "UNKNOWN";
      return true;
    }
  }
}

void PharmXmlConverter::mlxErrorModel(const std::string &name, std::string& errorModel)
{  
  if (name =="const"|| name == "constant"){
    errorModel = "constantErrorModel(a)=a\n";
  }
  else if (name=="prop"|| name =="proportional"){
    errorModel = "proportionalError(b,f)=b*f\n";
  }
  else if (name=="comb1"|| name =="combined1"){
    errorModel = "combinedError1(a,b,f)=a+b*f\n";
  }
  else if (name=="comb2"|| name =="combined2")  {
    errorModel = "combinedError2(a,b,f)=sqrt(a^2 + b^2*f)\n";
  }
  else if (name=="propc"|| name =="proporionalC"){
    errorModel = "powerError(b,c,f)=b*(f^c)\n";
  }
  else if (name=="comb1c"|| name =="combined1C"){
    errorModel = "combinedPowerError1(a,b,c,f)=a+ b*(f^c)\n";
  }
  else if (name=="comb2c"|| name =="combined2c"){
    errorModel = "combinedPowerError2(a,b,c,f)= a^2 + b^2*(f^(2*c))\n";
  }
  else if (name=="exp"|| name =="exponential"){
    errorModel = "exponentialError(a)=exp(a)\n";
  }
  else if (name=="logit") {
    errorModel = "logitError(y)=y/(1-y)\n";
  }
  else if (name=="band(0,10)"){
    errorModel = "bandError1(y)=y/(10.0-y)\n";
  }
  else if (name=="band(0,100)"){
    errorModel = "bandError2(y)=y/(100.0-y)\n";
  }
  else
  {
    throwLater(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::mlxErrorModel",
                                            std::string("unknown type of error model '"+name+"' not translated to Pharmml"),
                                            THROW_LINE_FILE));
    errorModel = "UNKNOWN";
  }
}

void PharmXmlConverter::cleanMlxDataPath(std::string & path)
{
  cleanMlxPath(path);
  _dataPath = path;
}

std::string PharmXmlConverter::setDataDelimiter(const std::string & delimiter)
{
  std::string pharmmlDelimiter =  pharmmlDelimiterTypes(delimiter);
  if(pharmmlDelimiter == "SPACE"){
    _dataDelimiter = QRegExp("\\s+");
  }
  else{
    _dataDelimiter = QRegExp(delimiter.c_str()) ;
  }
  return pharmmlDelimiter;
}

void PharmXmlConverter::checklineSize(const int & lineSize, const  int & firstLineSize )
{
  if(lineSize != firstLineSize)
  {
    throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::findDataParam",
                                          std::string("numbers of column in  '"+ _dataPath+"' differs from DATA headers, cannot translate to Pharmml"),
                                          THROW_LINE_FILE));
    return ;
  }
}

void  PharmXmlConverter::findDataParam(const QStringList & vHeaders,const Node*mlxtProject,QVector<QString>& catNamesInData)
{// find unparsed data parameters: fill _categories and _dataSymb
  int vHeadersSize = vHeaders.size();
  QFile data(_dataPath.c_str());
  QString currentPath(QDir::current().absolutePath());
  QVector<QString> catNames;
  findCatNames(mlxtProject,catNames);
  if (!data.open(QFile::ReadOnly | QFile::Text))
  {
    QDir::setCurrent(QFileInfo(_sourceFile).path());
    data.setFileName(QDir::current().absolutePath()+"/"+ QString(_dataPath.c_str()) );
    if (!data.open(QFile::ReadOnly | QFile::Text))
    {
      throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::findDataParam",
                                            std::string("cannot open data file '"+ _dataPath+"' cannot translate to Pharmml"),
                                            THROW_LINE_FILE));
    }
  }

  QTextStream dataStream(&data);
  QString line (dataStream.readLine().trimmed());
  QStringList lineSplit (line.split(_dataDelimiter));
  int firstLineSize = lineSplit.size();
  if(firstLineSize != vHeadersSize)
  {
    if(! lineSplit[0].compare("#"))
    {
      QString  firstSymb(line.split(lineSplit[1])[0]+lineSplit[1]);
      lineSplit.removeFirst();
      lineSplit[0] = firstSymb;
      firstLineSize--;
    }
    if(firstLineSize < vHeadersSize)
    {
      throwNow(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::findDataParam",
                                            std::string("numbers of column in  '"+ _dataPath+"' differs from DATA headers, cannot translate to Pharmml"),
                                            THROW_LINE_FILE));
    }
  }
  bool isDataSymb = false;
  std::vector<int> categoricalLines;
  // look  types of first    line  to detect if there is no header
  // assumming that at least observations are present and are real numbers or integers (categorical)
  // and that user's names in header (if there is header) contains at least one non numeric character
  for(int iHead = 0; iHead< vHeadersSize;iHead++)
  {
    std::pair<std::string, std::string> colType = pharmmlColTypes(vHeaders[iHead]);
    if(colType.first == "dv" || colType.first== "time")
    {
      bool ok;
      lineSplit[iHead].toDouble(&ok);
      if(!ok){
        isDataSymb  = true ;
      }
    }
  }
  int nbLines = 0;
  if(isDataSymb)
  {
    for(int iHead =0; iHead< vHeadersSize;iHead++){
      _dataSymb.push_back(lineSplit[iHead].trimmed().toUtf8().data());
    }
    line  = dataStream.readLine().trimmed();
    while(line.isEmpty()){
      line  = dataStream.readLine().trimmed();
    }
    lineSplit  = line.split(_dataDelimiter);
    checklineSize(lineSplit.size(),firstLineSize);
    nbLines++;
    for(int icat = 0; icat < catNames.size(); icat++)
    {
      for(int iHead =0; iHead< vHeadersSize;iHead++)
      {
        if(!catNames[icat].compare(QString(_dataSymb[iHead].c_str())))
        {
          categoricalLines.push_back(iHead);
          catNamesInData.push_back(catNames[icat]);
          break;
        }
      }
    }
  }
  if(categoricalLines.size())
  {
    QVector<QMap<QString,int> > mapCat(categoricalLines.size());
    // read categorical from data file
    for (; ;)
    {
      for(unsigned iCat = 0; iCat < categoricalLines.size(); iCat++)
      {
        QString lineSplitTrimmed = lineSplit[categoricalLines[iCat]].trimmed();
        if(mapCat[iCat].value(lineSplitTrimmed,-1) == -1){
          mapCat[iCat][lineSplitTrimmed] = 1;
        }
        else{
          mapCat[iCat][lineSplitTrimmed]++;
        }
      }
      line  = dataStream.readLine().trimmed();
      if(!line.isNull())
      {
        while(line.isEmpty())
        {
          line  = dataStream.readLine().trimmed();
          if(line.isNull()) break;
        }
        lineSplit  = line.split(_dataDelimiter);
        checklineSize(lineSplit.size(),firstLineSize);
        nbLines++;
      }
      else{
        break;
      }
    }
    _categories = std::vector<std::vector<std::string> >(categoricalLines.size());
    // check that the number of lines readed is exactly the number of values readed for each categorical column;
    for(unsigned iCat = 0; iCat < categoricalLines.size(); iCat++)
    {
      int nbValReaded = 0;
      for(QMap<QString,int>::const_iterator it = mapCat[iCat].begin(); it!= mapCat[iCat].end(); ++it)
      {
        nbValReaded += it.value();
        _categories[iCat].push_back(std::string(it.key().toUtf8().data()));
      }
      if(nbLines!=nbValReaded)
      {
        std::string strICat(QString::number(iCat).toUtf8().data());
        throwLater(lixoft::exception::Exception("lixoft::translator::PharmXmlConverter::findDataParam",
                                                std::string("numbers of values for categorical number "+strICat+ " in  '"+ _dataPath+"' differs from number of data lines, cannot translate to Pharmml"),
                                                THROW_LINE_FILE));
      }
    }
  }
  QDir::setCurrent(currentPath);
}

} //namespace translator
}//namespace lixoft

#undef BOOST_SPIRIT_DEBUG
