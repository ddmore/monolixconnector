#include <QSet>
#include <QString>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QDomDocument>
#include <QtAlgorithms>
#include <QDebug>

#include "lixoft/exceptions/Exception.h"
#include "lixoft/translator/PharmML08ToMlxProject.h"
#include <limits>
#include <cmath>
#include <math.h>
#include <iostream>

namespace lixoft {
namespace translator {

PharmML08ToMlxProject::PharmML08ToMlxProject(const QString& sourceFile,
                                         const QString& output,
                                         const QVector<QPair<QString,QString> >& options,
                                         int outputFormat):
  PharmMLLoader08(sourceFile,
                output,
                options,
                outputFormat)

{}

PharmML08ToMlxProject::~PharmML08ToMlxProject()
{

}


void PharmML08ToMlxProject::checkOptions()
{
  LanguageTranslator::checkFileAndDirectory("lixoft::translator::PharmML08ToMlxProject");
}

void PharmML08ToMlxProject::translate()
{
  QDomDocument domDocument;
  QString errorStr;
  int errorLine;
  int errorColumn;
  QFile file(_sourceFile);
  if (!domDocument.setContent(&file, true, &errorStr, &errorLine,
                              &errorColumn))
  {
    QString errorMessage = "On file '" + _sourceFile
                           + QString("'\nParse error at line '")+QString::number(errorLine)
                           + QString("' at column '")+QString::number(errorColumn)
                           + QString("' error:") + errorStr;
    throw lixoft::exception::Exception("PharmML08ToMlxProject::pharmlToMlxtran",errorMessage.toUtf8().data(),THROW_LINE_FILE);
  }
  QDomElement root = domDocument.documentElement();
  QDomNodeList modelDefinition = root.elementsByTagName("ModelDefinition");
  QDomNodeList modellingSteps = root.elementsByTagName("ModellingSteps");
  QDomNodeList rootNodes = root.childNodes();

  QDomNodeList trialDesign = root.elementsByTagName("TrialDesign");
  // first load trial design to get dosing variables
  for (int tdi = 0;tdi<trialDesign.size();tdi++)
  {
    pharmMLDataToMlxtran(trialDesign.at(tdi));
  }
  for (int msi = 0;msi<modellingSteps.size();msi++)
  {
    pharmMLEstimationTaskToMlxtran(modellingSteps.at(msi));
  }

  loadFromRoot(root);
}

void PharmML08ToMlxProject::serialize()
{
  QString outputProject;
  if (LanguageTranslator::OUTPUT_IS_A_FILE){
    outputProject = QFileInfo(_output).absolutePath() + "/" + _outBaseName;
  }
  else
    outputProject = _output+"/"+_outBaseName;
  outputProject = outputProject.trimmed();
  QDir().mkdir(outputProject);
  if (QFileInfo(outputProject).exists() && QFileInfo(outputProject).isDir())
  {
    QFile projectFile(outputProject+"/"+_outBaseName+"_project.mlxtran");
    QFile modelFile(outputProject+"/"+_outBaseName+"_model.txt");
    QFile pharmmlNamesFile(outputProject+"/"+_outBaseName+"_pharmmlnames.txt");
    QFile odataFile(outputProject+"/"+_outBaseName+"_data.txt");
    QFile otableFile(outputProject+"/tables.xmlx");

    // test if there is 'Import Data' from  Pharmml
    QString dataFileHeader;
    QSet<QString> regressorsEntries;
    QMap<int,QString> regressorsIndexes;
    if (_dataSet.columns.size()
        && !_dataSet.dataUrl.isEmpty())
    {
      // test url
      bool dataPathIsCorrect = false;
      QString dataFilename = _dataSet.dataUrl;
      if (_dataSet.dataUrl.contains(QRegExp("^file://")))
      {
        dataFilename.remove(QRegExp("^file://"));
        dataPathIsCorrect =true;
      }
      else
      {
        dataPathIsCorrect = true;
      }
      if (dataPathIsCorrect)
      {
        if (dataFilename.at(0) != '/')
        {
          QFileInfo src(_sourceFile);
          dataFilename = src.absoluteDir().absolutePath() + "/" + dataFilename;
        }
        QFile testIfDataExist(dataFilename);
        if (!testIfDataExist.exists())
          throwLater(lixoft::exception::Exception("lixoft::translator::PharmML08ToMlxProject",
                                                  (QString("Cannot open file '")+dataFilename+QString("'")).toUtf8().data(),
                                                  THROW_LINE_FILE));
        else
        {
          QString tfile = outputProject+"/"+ QFileInfo(testIfDataExist).fileName();
          if (!QFile::copy(dataFilename,tfile))
          {
            addMessage(LixoftObject::WARNING,QString("File '")
                       + dataFilename
                       + QString("'")
                       + QString(" overwrites '") + tfile + QString("'"));
            if (!QFile::remove(tfile))
            {
              throwLater(lixoft::exception::Exception("PharmML08ToMlxProject::serialize",
                                                      (QString("Cannot remove file '")
                                                       + tfile + QString("'")).toUtf8().data(),THROW_LINE_FILE));
            }
            if (!QFile::copy(dataFilename,tfile))
            {
              throwLater(lixoft::exception::Exception("PharmML08ToMlxProject::serialize",
                                                      (QString("Cannot copy file '")
                                                       + dataFilename
                                                       + QString("'")
                                                       + QString(" to '") + tfile + QString("'")).toUtf8().data(),THROW_LINE_FILE));
            }

          }
          dataFileHeader = "DATA:\n";
          dataFileHeader += "path = \"%MLXPROJECT%/\",\n";
          dataFileHeader += (QString("file  =\"") + QFileInfo(testIfDataExist).fileName()  + QString("\",\n"));

          dataFileHeader+= "headers = {";
          for (QMap<int,QPair<QString,QString> >::const_iterator dIt = _dataSet.columns.begin();
               dIt != _dataSet.columns.end();
               dIt++)
          {
            QMap<int,QPair<QString,QString> >::const_iterator nextIt = dIt;
            nextIt++;
            // check if it is a categeorical
            QString catOrCov = dIt.value().first;
            bool isReg = true;
            bool inCov = false;
            if (dIt.value().first == "COV") {
              inCov = true;
              if (columnMapping.contains(dIt.value().second)) {
                for (int ci = 0;ci<covariatesSection.size();ci++)
                {
                  if (covariatesSection[ci]->type() == PharmMLLoader08::VariableDefinition::IS_CATEGORICAL_VARIABLE
                      && covariatesSection[ci]->name == dIt.value().second)
                  {
                    catOrCov="CAT";
                    isReg=false;
                    break;
                  }
                }
              }
              // maybe a reg
              for (int ii = 0;ii<individualParametersSection.size();ii++) {
                if (individualParametersSection[ii]->type() == PharmMLLoader08::VariableDefinition::IS_STATISTICAL_DEFINITION) {
                  PharmMLLoader08::StatisticalDefinition* sd = static_cast<PharmMLLoader08::StatisticalDefinition*>(individualParametersSection[ii]);
                  if (sd->covariates.size())
                  {
                    for (QMap<QString,QString>::const_iterator cit = sd->covariates.begin();cit!=sd->covariates.end();cit++)
                    {
                      QMap<QString,QString>::const_iterator nextIt=cit;
                      nextIt++;
                      QString citkey = cit.key();
                      citkey.remove(QRegExp("\\:.*$"));
                      citkey.remove(QRegExp("#.*$"));
                      if (citkey == dIt.value().second) {
                        isReg = false;
                      }
                    }
                  }
                }
              }
              // check covariate dependencies
              if (isReg) {
                for (int ci = 0;ci<covariatesSection.size();ci++)
                {

                  if (covariatesSection[ci]->dependentVariables.contains(dIt.value().second+"\\:")
                      || covariatesSection[ci]->dependentVariables.contains(dIt.value().second))     {
                    isReg = false;
                  }
                }
              }
            }
            if (dIt.value().first == "REG") {
              catOrCov = "X";
              regressorsEntries.insert(dIt.value().second);
              regressorsIndexes[regressorsIndexes.size()] = dIt.value().second;
            }
            if (inCov && isReg) {
              catOrCov = "X";
              regressorsEntries.insert(dIt.value().second);
              regressorsIndexes[dIt.key()] = dIt.value().second;
            }
            dataFileHeader+=catOrCov;
            if (nextIt != _dataSet.columns.end())
            {
              dataFileHeader+=",";
            }
          }
          dataFileHeader+="}\n";
          QString delim = "COMMA";
          if (_dataSet.columnDelimiter == "COMMA") delim=",";
          else if (_dataSet.columnDelimiter == "SPACE") delim=" ";
          else if (_dataSet.columnDelimiter == "SEMICOLUMN" || _dataSet.columnDelimiter == "SEMICOLON") delim=";";
          else if (_dataSet.columnDelimiter == "TABULATION" || _dataSet.columnDelimiter == "TAB") delim="\\t";
          else throwLater(lixoft::exception::Exception("lixoft::translator::PharmML08ToMlxProject",
                                                       (QString("Unknown delimiter '")+_dataSet.columnDelimiter+QString("'")).toUtf8().data(),
                                                       THROW_LINE_FILE));
          dataFileHeader += (QString("columnDelimiter = \"")+delim+QString("\"\n"));
        }
      }
      else
      {
        throwLater(lixoft::exception::Exception("lixoft::translator::PharmML08ToMlxProject",
                                                (QString("Unknown protocol for url '") + _dataSet.dataUrl+QString("'")).toUtf8().data(),
                                                THROW_LINE_FILE));
      }
    }
    else // create fake data file
    {

    }

    projectFile.open(QFile::WriteOnly | QFile::Text);
    modelFile.open(QFile::WriteOnly | QFile::Text);
    pharmmlNamesFile.open(QFile::WriteOnly | QFile::Text);
    otableFile.open(QFile::WriteOnly | QFile::Text);

    bool doSerialization = true;
    if (!projectFile.isOpen())
    {
      throwLater(lixoft::exception::Exception("lixoft::translator::PharmML08ToMlxProject",
                                              (QString("[ERROR] Cannot open or create project file '")
                                               + projectFile.fileName() + QString("'")).toUtf8().data(),
                                              THROW_LINE_FILE));
      doSerialization = false;
    }
    else if (!modelFile.isOpen())
    {
      throwLater(lixoft::exception::Exception("lixoft::translator::PharmML08ToMlxProject",
                                              (QString("[ERROR] Cannot open or create model file '")+modelFile.fileName() + QString("'")).toUtf8().data(),
                                              THROW_LINE_FILE));
      doSerialization = false;
    }
    else if (!modelFile.isOpen())
    {
      throwLater(lixoft::exception::Exception("lixoft::translator::PharmML08ToMlxProject",
                                              (QString("[ERROR] Cannot open or create data file '")+odataFile.fileName() + QString("'")).toUtf8().data(),
                                              THROW_LINE_FILE));
      doSerialization = false;
    }
    else if (!pharmmlNamesFile.isOpen())
    {
      throwLater(lixoft::exception::Exception("lixoft::translator::PharmML08ToMlxProject",
                                              (QString("[ERROR] Cannot open or create data file '")+odataFile.fileName() + QString("'")).toUtf8().data(),
                                              THROW_LINE_FILE));
      doSerialization = false;
    }
    else if (!otableFile.isOpen())
    {
      throwLater(lixoft::exception::Exception("lixoft::translator::PharmML08ToMlxProject",
                                              (QString("[ERROR] Cannot open or create data file '")+otableFile.fileName() + QString("'")).toUtf8().data(),
                                              THROW_LINE_FILE));
      doSerialization = false;
    }

    if (doSerialization)
    {
      QTextStream projectStream(&projectFile);
      QTextStream modelStream(&modelFile);
      QTextStream pharmmlNamesStream(&pharmmlNamesFile);
      QTextStream tableStream(&otableFile);

      QVector<QString> projectToSerialize;
      QMap<QString,int> individualParametersToSerializeId;


      bool allWithVariance = true;
      projectToSerialize.push_back(dataFileHeader+"\n");
      QStringList variableStream;
      QSet<QString> definedCovariates;
      // Manage Covariates
      for (int ci = 0;ci<covariatesSection.size();ci++)
      {
        if (covariatesSection[ci]->type() == PharmMLLoader08::VariableDefinition::IS_CATEGORICAL_VARIABLE)
        {
          if(!regressorsEntries.contains(covariatesSection[ci]->name)) {
            variableStream<<covariatesSection[ci]->name<<" [use=cov,type=cat],\n";
          }
        }
        else
        {
          if (covariatesSection[ci]->equation.length())
          {
            if (covariatesSection[ci]->centeredBy.length()) {
                variableStream<<covariatesSection[ci]->equation<<" [use=cov,centeredBy="<<covariatesSection[ci]->centeredBy<<"],\n";
            }
            else {
                variableStream<<covariatesSection[ci]->equation<<" [use=cov],\n";
            }
            QString cname = covariatesSection[ci]->name;
            QMap<QString,QString>::const_iterator findIt = columnNameTypeMapping.find(cname);
            bool itIsCovariate = true;
            if (findIt != columnNameTypeMapping.end()) {
              if (findIt.value() == "REG") {
                itIsCovariate = false;
                addMessage(LixoftObject::WARNING,QString("The variable '")
                           + cname
                           + QString("' is not tagged as covariate in data set. Monolix will not work properly."));
              }
            }
            if (itIsCovariate) {
              if (!covariatesSection[ci]->equation.startsWith(cname)) {
                for (QMap<QString,QString>::const_iterator cit = columnMapping.begin();
                     cit != columnMapping.end();
                     cit++)
                {
                  if (cit.value() == cname)
                  {
                    cname =cit.key();
                    break;
                  }
                }
                variableStream<<cname<<",\n";
              }
              else {
                for (QSet<QString>::const_iterator depCov = covariatesSection[ci]->dependentVariables.begin();
                     depCov != covariatesSection[ci]->dependentVariables.end();
                     depCov++) {
                  QString cname = *depCov;
                  for (QMap<QString,QString>::const_iterator cit = columnMapping.begin();
                       cit != columnMapping.end();
                       cit++)
                  {
                    if (cit.value() == cname)
                    {
                      cname =cit.key();
                      break;
                    }
                  }
                  variableStream<<cname<<",\n";
                  findIt = columnNameTypeMapping.find(cname);
                  bool itIsCovariate = true;
                  if (findIt != columnNameTypeMapping.end()) {
                    if (findIt.value() == "REG") {
                      itIsCovariate = false;
                      addMessage(LixoftObject::WARNING,QString("The variable '")
                                 + cname
                                 + QString("' is not tagged as covariate (but as regressor) in data set. Monolix will not work properly."));
                    }
                  }
                }
              }
            }
          }
          else
          {

            QMap<QString,QString>::const_iterator findIt = columnNameTypeMapping.find(covariatesSection[ci]->name);
            if (findIt != columnNameTypeMapping.end() && findIt.value() != "REG") {
                if (covariatesSection[ci]->centeredBy.length()) {
                    variableStream<<covariatesSection[ci]->name<<" [use=cov, centeredBy="<<covariatesSection[ci]->centeredBy<<"],\n";
                }
                else {
                    variableStream<<covariatesSection[ci]->name<<" [use=cov],\n";
                }
            }

            /*bool itIsCovariate = true;
                        if (findIt != columnNameTypeMapping.end()) {
                            if (findIt.value() == "REG") {
                                itIsCovariate = false;
                                addMessage(LixoftObject::WARNING,QString("The variable '")
                                                                         + covariatesSection[ci]->name
                                                                         + QString("' is not tagged as covariate (but as regressor) in data set. Monolix will not work properly."));
                             }
                       }*/
          }
        }
        for (QSet<QString>::const_iterator depCov = covariatesSection[ci]->dependentVariables.begin();
             depCov != covariatesSection[ci]->dependentVariables.end();
             depCov++)
        {
          definedCovariates.insert(*depCov);
        }
      }

      if (variableStream.size())
      {
        projectToSerialize.push_back("\nVARIABLES:\n");
        for (int vsi = 0;vsi<variableStream.size();vsi++)
        {
          projectToSerialize.push_back(variableStream.at(vsi));
        }
      }

      QVector<int> transfertIndividualParameterToStructuralModel;
      projectToSerialize.push_back("\nINDIVIDUAL:\n");
      // get possibles etas (equation case)
      QSet<QString> randomVariables;
      for (int ii=0;ii<individualParametersSection.size();ii++)
      {
        if (individualParametersSection[ii]->type() == PharmMLLoader08::VariableDefinition::IS_RANDOM_VARIABLE)
        {
          PharmMLLoader08::RandomVariableDefinition* rvd = static_cast<PharmMLLoader08::RandomVariableDefinition*>(individualParametersSection[ii]);
          randomVariables.insert(rvd->name);
        }
      }

      for (int ii=0;ii<individualParametersSection.size();ii++)
      {
        QString currentIP;
        QString currentIPName;
        if (individualParametersSection[ii]->type() == PharmMLLoader08::VariableDefinition::IS_STATISTICAL_DEFINITION)
        {
          PharmMLLoader08::StatisticalDefinition* sd = static_cast<PharmMLLoader08::StatisticalDefinition*>(individualParametersSection[ii]);

          if (sd->equation.length())
          {
            if (sd->dependentVariables.size() == 1 && !(sd->equation.contains("-")
                                                        || sd->equation.contains("+")
                                                        || sd->equation.contains("*")
                                                        || sd->equation.contains("/")
                                                        ))
            {
              currentIPName = sd->name;
              currentIP+=sd->name + QString(" = { distribution=normal, iiv=no },\n");
              sd->equation="";

            }
            else
            {
              addMessage(LixoftObject::WARNING,QString("Individual parameter '")
                         + sd->name
                         +QString("' is write with equation, Monolix does not support correctly this feature and the parameters will be not correctly estimated"));
              transfertIndividualParameterToStructuralModel.push_back(ii);
              for (QSet<QString>::const_iterator depIt = sd->dependentVariables.begin();
                   depIt != sd->dependentVariables.end();
                   depIt++)
              {
                QSet<QString>::const_iterator findrvIt = randomVariables.find(*depIt);
                if (findrvIt != randomVariables.end())
                {
                  currentIP += *findrvIt+QString(" = { distribution=")+QString(sd->transformation)+QString("normal, iiv=yes },\n");
                  currentIPName = *findrvIt;
                }
              }
            }
          }
          else
          {
            if (sd->omegaNames.size())
                allWithVariance = allWithVariance && sd->withVariance;
            QString pname = sd->name;
            pname = pname.trimmed();
            if (pname.length()) {
                currentIP+=sd->name+QString(" = { distribution=")+QString(sd->transformation)+QString("normal");
                currentIPName=sd->name;
                if (sd->covariates.size())
                {
                  currentIP+=", covariate={";
                  for (QMap<QString,QString>::const_iterator cit = sd->covariates.begin();cit!=sd->covariates.end();cit++)
                  {
                    QMap<QString,QString>::const_iterator nextIt=cit;
                    nextIt++;
                    QString cname=cit.key();
                    cname = cname.remove(QRegExp("\\:.*$"));
                    if (nextIt == sd->covariates.end())
                    {
                      currentIP+=cname;
                    }
                    else currentIP+=cname+QString(", ");
                  }
                  currentIP+="} ";
                }
                if (sd->omegas.size()) {
                    currentIP+=", iiv=yes ";
                    for (int iov_i = 1; iov_i < sd->omegaNames.size(); iov_i++) {
                        if (iov_i == 1) {
                            currentIP+=", iov=yes ";
                        }
                        else {
                            currentIP+=QString(", iov") + QString::number(iov_i)+ QString("=yes ");
                        }
                    }
                }
                else currentIP+=QString(", iiv=no ");
                currentIP+=QString("},\n");
            }
          }
        }
        if (currentIP.length()) {
            projectToSerialize.push_back(currentIP);
            individualParametersToSerializeId[currentIPName] = projectToSerialize.size()-1;

        }
      }
      if (_parameterCorrelation.size())
      {
        projectToSerialize.push_back(QString("\nCORRELATION:\n"));

        QVector<QString> correlationBlocks;

        for (int cori=0;cori<_parameterCorrelation.size();cori++)
        {
          int level = _parameterCorrelation[cori].level;
          while (level>=correlationBlocks.size()) {

              correlationBlocks.push_back("");
          }
          if (level == 0 && correlationBlocks[level].length() == 0) {
              correlationBlocks[level] = QString("correlationIIV={");
          }
          else if (correlationBlocks[level].length() == 0) {
              correlationBlocks[level] = QString("correlationIOV") + QString::number(level) + QString("={");
          }
          correlationBlocks[level]+=QString("{")+_parameterCorrelation[cori].randomVariable1+QString(",")
                                       +_parameterCorrelation[cori].randomVariable2+QString("}");
          if (cori != (_parameterCorrelation.size()-1))
          {
            correlationBlocks[level] += QString(",");
          }
        }

        for (int cori = 0;cori<correlationBlocks.size();cori++) {
            if (correlationBlocks[cori].length()) {
                correlationBlocks[cori] += QString("}\n");
                projectToSerialize.push_back(correlationBlocks[cori]);
            }
        }
      }



      projectToSerialize.push_back(QString("\nOBSERVATION:\n"));
      QVector<QString> observationPredictions;
      QMap<QString,QString> fixedErrorModelParams;
      for (int oi = 0;oi<observationModelSection.size();oi++)
      {
        if (observationModelSection[oi]->type() == PharmMLLoader08::VariableDefinition::IS_STATISTICAL_DEFINITION)
        {
          PharmMLLoader08::StatisticalDefinition* sd = static_cast<PharmMLLoader08::StatisticalDefinition*>(observationModelSection[oi]);
          if (sd->equation.length())
          {
            throwLater(lixoft::exception::Exception("PharmML08ToMlxProject",std::string("For Monolix, observation ''")
                                                    + std::string(observationModelSection[oi]->name.toUtf8().data())
                                                    + std::string("' cannot be defined as equation.")));
          }
          else if (!sd->errorFunction || sd->omegas.size() != 1)
          {
            throwLater(lixoft::exception::Exception("PharmML08ToMlxProject",std::string("For Monolix, observation ''")
                                                    + std::string(observationModelSection[oi]->name.toUtf8().data())
                                                    + std::string("' should have predefined error model.")));
          }
          else
          {
            QString errorModel = sd->omegas[0];
            if ( errorModel == "additiveError")
            {
              errorModel = "constant";
            }
            else if (errorModel == "combinedAdditiveProportionalModel1"
                     || errorModel == "combinedErrorModel"
                     || errorModel == "combinedError1"
                     || errorModel=="combined1")
            {
              errorModel = "combined1";
            }
            else if (errorModel == "combinedAdditiveProportionalModel2" || errorModel=="combinedError2")
            {
              errorModel = "combined2";
            }
            else if (errorModel == "combinedAdditiveProportionalModel1")
            {
              errorModel = "proportional";

            }
            else if (errorModel == "additiveErrorModel" || errorModel == "constantErrorModel")
            {
              errorModel = "constant";
            }
            else if (errorModel == "proportionalError")
            {
              errorModel = "proportional";
              QString changeName="zError_a_{"+sd->name+"}";
              if(_initialValues.contains(changeName))
              {
                _initialValues["zError_b_{"+sd->name+"}"] = _initialValues["zError_a_{"+sd->name+"}"];
                _initialValues.remove(changeName);
              }
            }
            else if (errorModel=="powerError")
            {
              errorModel="proportionalc";
              QString changeName="zError_b_{"+sd->name+"}";
              if(_initialValues.contains(changeName))
              {
                _initialValues["zError_c_{"+sd->name+"}"] = _initialValues["zError_b_{"+sd->name+"}"];
                _initialValues.remove(changeName);
                _initialValues["zError_b_{"+sd->name+"}"] = _initialValues["zError_a_{"+sd->name+"}"];
                _initialValues.remove("zError_a_{"+sd->name+"}");
              }
            }
            else if (errorModel=="combinedPowerError1")
            {
              errorModel="combined1c";
            }
            else if (errorModel=="combinedPowerError2")
            {
              errorModel="combined2c";
            }
            else if (errorModel=="exponentialError")
            {
              errorModel="exponential";
            }
            else throwLater(lixoft::exception::Exception("PharmMLToMlxtran::serialize",std::string("Error model '")
                                                         + std::string(errorModel.toUtf8().data())
                                                         + std::string("' is unknown"),THROW_LINE_FILE));
            projectToSerialize.push_back(sd->name+QString(" = { type=continuous, prediction=")
                                         + sd->population+QString(", error=")
                                         + errorModel+QString("},\n"));
            observationPredictions.push_back(sd->population);
          }
        }
        else addMessage(LixoftObject::WARNING,QString("For Monolix, observation ''")
                        + observationModelSection[oi]->name
                        + QString("' should be a statistical definition."));
      }
      for(QMap<QString,Categorical>::const_iterator discreteIt = _discreteModels.begin();
          discreteIt != _discreteModels.end();
          discreteIt++) {
        for (QMap<QString,QString>::const_iterator dataNameIt = columnMapping.begin();
             dataNameIt !=  columnMapping.end();
             dataNameIt++) {
          if (dataNameIt.value() == discreteIt.value().categoryVariable) {
            observationPredictions.push_back(discreteIt.value().categoryVariable);
          }
        }
      }

      if (observationPredictions.size() == 0 && _discreteModels.size() ) {
          for(QMap<QString,Categorical>::const_iterator discreteIt = _discreteModels.begin();
              discreteIt != _discreteModels.end();
              discreteIt++) {
              observationPredictions.push_back(discreteIt.value().categoryVariable);
          }
      }

      projectToSerialize.push_back("\nSTRUCTURAL_MODEL:\n");
      projectToSerialize.push_back(QString("\tfile=\"mlxt:")+_outBaseName+QString("_model\",\n"));
      projectToSerialize.push_back(QString("\tpath=\"%MLXPROJECT%\",\n"));
      projectToSerialize.push_back(QString("\toutput={"));
      qSort(observationPredictions.begin(),observationPredictions.end());

      for (int smoi = 0;smoi<observationPredictions.size();smoi++)
      {
        if (smoi != (observationPredictions.size()-1))
        {
          projectToSerialize.push_back(observationPredictions[smoi] + QString(", "));
        }
        else
        {
          projectToSerialize.push_back(observationPredictions[smoi]);
        }
      }
      projectToSerialize.push_back(QString("}\n"));
      projectToSerialize.push_back(QString("\nTASKS:\n"));
      projectToSerialize.push_back(QString("globalSettings={\n"));
      if (allWithVariance) {
        projectToSerialize.push_back(QString("\twithVariance=yes,\n"));
      }
      else {
        projectToSerialize.push_back(QString("\twithVariance=no,\n"));
      }
      projectToSerialize.push_back(QString("\tsettingsGraphics=\"%MLXPROJECT%/tables.xmlx\",\n"));
      projectToSerialize.push_back(QString("},\n"));
      projectToSerialize.push_back(QString("; workflow\n"));
      if (_initialValues.size())
      {
        projectToSerialize.push_back(QString("\testimatePopulationParameters(\n\t\tinitialValues={\n"));
        QSet<QString> savedVariables;
        for (QMap<QString,QString>::const_iterator tit = _initialValues.begin();tit!=_initialValues.end();tit++)
        {
          QString param = tit.key();
          param.remove(QRegExp("_\\{[^}]+\\}$"));
          param.remove("zError_");
          QString paramName = tit.key();
          paramName.remove("zError_");
          if (!_predictions.contains(param))
          {
            projectToSerialize.push_back(QString("\t\t\t")
                                         + paramName
                                         + QString(" = ")
                                         + tit.value()
                                         + QString(",\n"));
            savedVariables.insert(paramName);
          }

        }
        projectToSerialize.push_back(QString("\t                            })\n"));
      }
      else
      {
        projectToSerialize.push_back(QString("\testimatePopulationParameters(),\n"));
      }
      if (_discreteModels.size()) {
          projectToSerialize.push_back(QString("estimateFisherInformationMatrix( method={stochasticApproximation} ),\n"));
          projectToSerialize.push_back(QString("estimateIndividualParameters( method={conditionalDistribution} ),\n"));
          projectToSerialize.push_back(QString("estimateLogLikelihood(method={importantSampling}),\n"));
      }
      else {
          projectToSerialize.push_back(QString("estimateFisherInformationMatrix( method={linearization} ),\n"));
          projectToSerialize.push_back(QString("estimateIndividualParameters( method={conditionalDistribution} ),\n"));
          projectToSerialize.push_back(QString("estimateLogLikelihood(method={linearization}),\n"));
      }
      projectToSerialize.push_back(QString("displayGraphics(),\n"));


      tableStream<<"<monolix>\n";
      tableStream<<"\t<graphics>\n";
      tableStream<<"\t<version value=\"420\"/>\n";
      tableStream<<"\t<graphicsToPrint>\n";
      tableStream<<"\t<obsTimes value=\"1\"/>\n";
      tableStream<<"\t<intTimes value=\"1\"/>\n";
      tableStream<<"\t<fullTimes value=\"1\"/>\n";
      tableStream<<"\t<indContTable value=\"1\"/>\n";
      tableStream<<"\t<covTable value=\"1\"/>\n";
      tableStream<<"\t</graphicsToPrint>\n";
      tableStream<<"\t</graphics>\n";
      tableStream<<"</monolix>\n";


      for (QMap<QString,QString>::const_iterator pharmmlNamesIt = pharmMLNamesMapping.begin();
           pharmmlNamesIt != pharmMLNamesMapping.end();
           pharmmlNamesIt++)
      {
        pharmmlNamesStream<<pharmmlNamesIt.key()<<"="<<pharmmlNamesIt.value()<<"\n";
      }

      // get input from individual parameter build with equation
      QSet<QString> structuralModelInput = _macroParameters;
      QSet<QString> allocatedFromIndividualParameters;
      for (int ii = 0; ii<transfertIndividualParameterToStructuralModel.size();ii++)
      {
        const QSet<QString>& dependantVariables = individualParametersSection[ transfertIndividualParameterToStructuralModel[ii] ]->dependentVariables;
        structuralModelInput.remove(individualParametersSection[ transfertIndividualParameterToStructuralModel[ii] ]->name);
        structuralModelInput.unite(dependantVariables);
        allocatedFromIndividualParameters.insert(individualParametersSection[ transfertIndividualParameterToStructuralModel[ii] ]->name);
      }
      // get input from dependant variables of structural model sections
      QString doseAmountVariable;
      for (int is = 0;is<structuralModelSection.size();is++)
      {
        structuralModelInput.unite(structuralModelSection[is]->dependentVariables);
        for (QMap<QString,QString>::const_iterator colMapIt = columnMapping.begin();
             colMapIt!=columnMapping.end();
             colMapIt++) {
          if(structuralModelSection[is]->dependentVariables.contains(colMapIt.value()))
          {
            for (QMap<int,QPair<QString,QString> >::const_iterator colsIt = _dataSet.columns.begin();
                 colsIt != _dataSet.columns.end();colsIt++) {
              if (colMapIt.key() == colsIt.value().second && colsIt.value().first=="DOSE") {
                doseAmountVariable = colMapIt.value();
              }
            }
          }
        }
      }
      // supress dependent variable mute by pk macros
      for (QSet<QString>::const_iterator pkamountIt = _macroPKAmount.begin();
           pkamountIt != _macroPKAmount.end();
           pkamountIt++) {
        structuralModelInput.remove(*pkamountIt);
      }
      bool doseAmountVariableInPKBlock = false;
      if (doseAmountVariable.length()) {
        for (int is = 0;is<structuralModelSection.size();is++) {
          if (doseAmountVariable == structuralModelSection[is]->name) {
            doseAmountVariable = "";
          }
          if (QString("ddt_"+doseAmountVariable) ==  structuralModelSection[is]->name)
          {
            doseAmountVariableInPKBlock = true;
          }
        }
      }
      // Supress dosing parameters
      for (QMap<QString,PharmML08ToMlxProject::Dosing >::const_iterator dit = _dosingVariables.begin();
           dit != _dosingVariables.end();
           dit++)
      {
        if (structuralModelInput.contains(dit.key()))
          structuralModelInput.remove(dit.key());
      }
      // Supress time dosing parameters
      for (QMap<QString,QString >::const_iterator dit = doseTimeValues.begin();
           dit != doseTimeValues.end();
           dit++)
      {
        if (structuralModelInput.contains(dit.key()))
          structuralModelInput.remove(dit.key());
      }
      for (QMap<QString,QPair<QString,QString> >::const_iterator admIt = administrationMapping.begin();
           admIt != administrationMapping.end();
           admIt++) {
        structuralModelInput.remove(admIt.key());
      }
      for (QSet<QString>::const_iterator mpkIt = _macroInputFilter.begin();
           mpkIt!=_macroInputFilter.end();
           mpkIt++) {
            structuralModelInput.remove(*mpkIt);
      }
      // Suppress affected variables
      for (int is = 0;is<structuralModelSection.size();is++)
      {
        structuralModelInput.remove(structuralModelSection[is]->name);
        if (structuralModelSection[is]->name.contains(QRegExp("^ddt_"))) // manage derivative variables
        {
          QString derivativeVariable = structuralModelSection[is]->name;
          derivativeVariable.remove(QRegExp("^ddt_"));
          structuralModelInput.remove(derivativeVariable);
        }
      }
      for (QSet<QString>::const_iterator iit = allocatedFromIndividualParameters.begin();
           iit != allocatedFromIndividualParameters.end();
           iit++)
      {
        structuralModelInput.remove(*iit);
      }
      if (doseAmountVariable.length()) {
        structuralModelInput.remove(doseAmountVariable);
      }
      // Suppress time
      structuralModelInput.remove("t");

      QVector<QString> paramsInStructuralModel;
      modelStream<<"INPUT:\n\tparameter={";

      QSet<QString> filteredInputWithoutRegressors;
      for (QSet<QString>::const_iterator pit = structuralModelInput.begin();
           pit != structuralModelInput.end();
           pit++)
      {
        if (!regressorsEntries.contains(*pit)) {
          filteredInputWithoutRegressors.insert(*pit);
        }
      }
      for (QSet<QString>::const_iterator pit = filteredInputWithoutRegressors.begin();
           pit !=filteredInputWithoutRegressors.end();
           pit++)
      {
        QSet<QString>::const_iterator pitend = pit;
        pitend++;
        if (!regressorsEntries.contains(*pit)) {
          bool isParam = false;
          for (int parami = 0;parami<individualParametersSection.size();parami++) {
            if (individualParametersSection[parami]->name == *pit) {
              isParam = true;
            }
          }
          if (!isParam) {
            this->addMessage(LixoftObject::WARNING,QString("The input parameter '")
                             + *pit
                             + QString("' is not defined as individual parameter, with statistical definition. The Monolix project will be not runnable"));
          }
          modelStream<<*pit;
          paramsInStructuralModel.push_back(*pit);
        }
        if (pitend != filteredInputWithoutRegressors.end()) modelStream<<", ";
      }
      modelStream<<"}\n";
      if (regressorsEntries.size()) {
        modelStream<<"\tregressor={";
        // create regressor table
        QVector<QString> regTable(regressorsEntries.size());
        for (QSet<QString>::const_iterator regIt = regressorsEntries.begin();regIt!=regressorsEntries.end();regIt++) {
            int ri = 0;
            for (QMap<int,QString>::const_iterator regIndIt = regressorsIndexes.begin(); regIndIt != regressorsIndexes.end();
                 regIndIt++) {
                if (regIndIt.value() == *regIt) {
                    regTable[ri] = *regIt;
                }
                if (regressorsEntries.contains(regIndIt.value())) {
                    ri++;
                }
            }
        }

        for (QVector<QString>::const_iterator regIt = regTable.begin();regIt!=regTable.end();regIt++) {
          modelStream<<*regIt;
          QVector<QString>::const_iterator nextRegIt = regIt;
          nextRegIt++;
          if (nextRegIt != regTable.end()) {
            modelStream<<",";
          }
        }
        modelStream<<"}\n";
      }

      modelStream<<"\n";
      if (!_pkBlock.isEmpty() || administrationMapping.size() || doseAmountVariableInPKBlock)
      {
        modelStream<<"PK:\n";
        bool addIV = false;
        for (QMap<QString,QPair<QString,QString> >::const_iterator admIt = administrationMapping.begin();
             admIt != administrationMapping.end();
             admIt++)
        {
          if (admIt.value().second.size()) {
            QMap<QString,QString>::const_iterator findNameTypeIt = columnNameTypeMapping.find(admIt.value().second);
            if (findNameTypeIt != columnNameTypeMapping.end()) {
              if (findNameTypeIt.value() == "DOSE") {
                bool isaDepot = false;
                for (int si = 0;si<structuralModelSection.size();si++) {
                  if (structuralModelSection[si]->equation.contains("ddt_"+admIt.key())
                      || structuralModelSection[si]->name.contains("ddt_"+admIt.key()) ) {
                    isaDepot = true;
                  }
                }
                if (isaDepot) {
                  modelStream<<"depot(target="<<admIt.key()<<")\n";
                }
                else {
                  doseAmountVariable = admIt.key();
                  doseAmountVariableInPKBlock = false;
                }

              }
              else if (findNameTypeIt.value() == "CMT") {
                  bool isaDepot = false;
                  for (int si = 0;si<structuralModelSection.size();si++) {
                    if (structuralModelSection[si]->equation.contains("ddt_"+admIt.key())
                        || structuralModelSection[si]->name.contains("ddt_"+admIt.key()) ) {
                      isaDepot = true;
                    }
                  }
                  if (!isaDepot) {
                      modelStream<<"compartment(cmt="<<admIt.value().first<<", amount="<<admIt.key()<<")\n";
                      addIV = true;
                  }
                  else {
                      modelStream<<"depot(adm="<<admIt.value().first<<", target="<<admIt.key()<<")\n";
                  }
              }
            }
          }
          else
          {
            //modelStream<<"compartment(cmt="<<admIt.value().first<<", amount="<<admIt.key()<<")\n";
            bool isaDepot = false;
            for (int si = 0;si<structuralModelSection.size();si++) {
                if (structuralModelSection[si]->equation.contains("ddt_"+admIt.key())
                    || structuralModelSection[si]->name.contains("ddt_"+admIt.key()) ) {
                  isaDepot = true;
                }
              }
              if (isaDepot) {
                modelStream<<"depot(target="<<admIt.key()<<")\n";
              }
              else {
                  doseAmountVariable = admIt.key();
                  doseAmountVariableInPKBlock = false;
              }
          }
        }
        if (doseAmountVariableInPKBlock) {
          modelStream<<"compartment(cmt=1"<<", amount="<<doseAmountVariable<<")\n";
          modelStream<<"iv()\n";
        }
        if (addIV) {
          modelStream<<"iv()\n";
        }
        modelStream<<_pkBlock <<"\n";
      }
      modelStream<<"EQUATION:\n";
      if (doseAmountVariable.length() && !doseAmountVariableInPKBlock) {
        modelStream<<doseAmountVariable<<" = amtDose\n";
      }
      if (doseTimeValues.size()) {
        for (QMap<QString,QString>::const_iterator dtit = doseTimeValues.begin();
             dtit != doseTimeValues.end();
             dtit++) {
          modelStream<<dtit.key()<<" = "<<dtit.value()<<"\n";
        }
      }
      // write individual equations
      for (int ii = 0;ii<individualParametersSection.size();ii++)
      {
        if (individualParametersSection[ii]->equation.length())
        {
          if (individualParametersSection[ii]->equation.contains("=") ||  individualParametersSection[ii]->name.length()==0)
            modelStream<<individualParametersSection[ii]->equation<<"\n";
          else modelStream<<individualParametersSection[ii]->name<<" = "<<individualParametersSection[ii]->equation<<"\n";
        }
      }
      // write structural model equations
      QVector<QString> ddts;
      for (int si = 0;si<structuralModelSection.size();si++)
      {
        if (structuralModelSection[si]->equation.contains("=")
            || structuralModelSection[si]->name.length()==0
            || structuralModelSection[si]->equation.contains(QRegExp("\\s*if"))
            || structuralModelSection[si]->equation.contains(QRegExp("\\s*else"))
            || structuralModelSection[si]->equation.contains(QRegExp("\\s*end\\s*\\n*$")))
        {
            if (structuralModelSection[si]->equation.contains(QRegExp("^\\s*ddt_"))) {
                // ddts put at the end of script
                ddts.push_back(structuralModelSection[si]->equation);
            }
            else {
                modelStream<<structuralModelSection[si]->equation<<"\n";
            }
        }
        else
        {

          if (structuralModelSection[si]->name.contains(QRegExp("^\\s*ddt_"))) {
              ddts.push_back(structuralModelSection[si]->name + " = " + structuralModelSection[si]->equation);
          }
          else
          {
            modelStream<<structuralModelSection[si]->name<<" = "<<structuralModelSection[si]->equation<<"\n";
          }
        }
      }
      for (int ddti = 0;ddti<ddts.size();ddti++) {
          modelStream<<ddts[ddti]<<"\n";
      }
      if (_discreteModels.size()) {
        modelStream<<"\nOBSERVATION:\n";
        for(QMap<QString,Categorical>::const_iterator catObsIt = _discreteModels.begin();
            catObsIt != _discreteModels.end();
            catObsIt++) {
          if (catObsIt.value().equations.length())
          {
            modelStream<<catObsIt.value().equations<<"\n";
          }
        }
      }
      modelStream<<"\nOUTPUT:\n";
      modelStream<<"\toutput={";
      for (int smoi = 0;smoi<observationPredictions.size();smoi++)
      {
        if (smoi != (observationPredictions.size()-1))
        {
          modelStream<<observationPredictions[smoi]<<", ";
        }
        else
        {
          modelStream<<observationPredictions[smoi];
        }
      }

      modelStream<<"}\n";

      for (int smPi = 0;smPi<paramsInStructuralModel.size();smPi++) {
          if(individualParametersToSerializeId.contains(paramsInStructuralModel[smPi])) {
              individualParametersToSerializeId.remove(paramsInStructuralModel[smPi]);
          }
      }
      /*QVector<int> idxs;
      for(QMap<QString,int>::const_iterator iptsIt = individualParametersToSerializeId.begin();
          iptsIt != individualParametersToSerializeId.end();
          iptsIt++) {
          idxs.push_back(iptsIt.value());
      }
      qSort(idxs.begin(),idxs.end());
      for (int i = 0;i<idxs.size();i++) {
          projectToSerialize.remove(idxs[idxs.size()-1-i]);
      }*/
      for (int i = 0;i<projectToSerialize.size();i++) {
          projectStream<<projectToSerialize[i];
      }
    }
  }
  else throwLater(lixoft::exception::Exception("lixoft::translator::PharmML08ToMlxProject",
                                               (QString("[ERROR] Cannot create '")+_output+"/"+_outBaseName+"_project.mlxtran'").toUtf8().data(),
                                               THROW_LINE_FILE));
}

QString PharmML08ToMlxProject::serializeToString()
{
  return "";
}

}
}
