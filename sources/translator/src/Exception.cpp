#include "lixoft/exceptions/Exception.h"

#include <stdio.h>

namespace lixoft {
namespace exception {

    Exception::Exception() throw():std::exception(),
                                   _source("undefine source"),
                                   _message(""),
                                   _line(-1),
                                   _file("")
    {

    }

    Exception::Exception(const Exception& cpy) throw() : std::exception(cpy),
                                               _source(cpy._source),
                                               _message(cpy._message),
                                               _line(cpy._line),
                                               _file(cpy._file)
    {}

    Exception::Exception(const std::string& source,
                         const std::string& message,
                         int line,
                         const char* file) throw() : _source(source),
                                                           _message(message),
                                                           _line(line),
                                                           _file(file?file:"")

    {}

    Exception::~Exception() throw()
    {}

    const char* Exception::what() const throw()
    { return "lixoft:Exception"; }

    const std::string& Exception::source() const
    { return _source; }

    const std::string& Exception::message() const
    { return _message; }

    int Exception::line() const
    { return _line; }

    const std::string& Exception::file() const
    { return _file; }

    // DEPRECATED
    void Exception::display(std::string& displayOut) const
    {
        char buffer[32];
        sprintf(buffer,"%d",_line);
        displayOut = std::string("id:") + std::string(this->what()) + std::string("\n")
                     + std::string("source:") + _source + std::string("\n")
                     + std::string("message:") + _message + std::string("\n")
                     + std::string("file:") + _file + std::string("\n")
                     + std::string("line:") + std::string(buffer) + std::string("\n");
    }

    std::string Exception::displayAll() const
    {
        char buffer[32];
        sprintf(buffer,"%d",_line);
        std::string displayOut = std::string("id:") + std::string(this->what()) + std::string("\n")
                                 + std::string("source:") + _source + std::string("\n")
                                 + std::string("message:") + _message + std::string("\n")
                                 + std::string("file:") + _file + std::string("\n")
                                 + std::string("line:") + std::string(buffer) + std::string("\n");
        return displayOut;
    }

    std::string Exception::displayText() const
    {
        char buffer[32];
        sprintf(buffer,"%d",_line);
        std::string displayOut = std::string("id:") + std::string(this->what()) + std::string("\n")
                                 + std::string("source:") + _source + std::string("\n")
                                 + std::string("message:") + _message + std::string("\n");
        return displayOut;
    }

    std::string Exception::source(const std::string& functionDecl)
    {
      const std::string::size_type sourceEnd = functionDecl.rfind('(');
      const std::string::size_type sourceBegin = functionDecl.rfind(' ', sourceEnd) + 1;

      return functionDecl.substr(sourceBegin, sourceEnd-sourceBegin);
    }
}
} // namespace lixoft
