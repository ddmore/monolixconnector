#include <QDir>
#include <QFile>
#include <QDebug>

#include <QDomElement>
#include <QDomNodeList>

#include <iostream>

#include "lixoft/translator/PharmML08ToMlxtran.h"
#include "lixoft/translator/PharmML08ToMlxProject.h"

namespace lixoft {
namespace translator {

    PharmML08ToMlxtran::PharmML08ToMlxtran(const QString& sourceFile,
                        const QString& outDirectory,
                        const QVector<QPair<QString,QString> >& options,
                        int outputFormat):
        PharmMLLoader08(sourceFile,
                      outDirectory,
                      options,
                      outputFormat)
    {}

    PharmML08ToMlxtran::~PharmML08ToMlxtran()
    {}

    void PharmML08ToMlxtran::checkOptions()
    {
        LanguageTranslator::checkFileAndDirectory("lixoft::translator::PharmMLToMlxTran");
    }

    void PharmML08ToMlxtran::translate()
    {
        QDomDocument domDocument;
        QString errorStr="undef";
        int errorLine;
        int errorColumn;
        QFile file(_sourceFile);
        if (!domDocument.setContent(&file, true, &errorStr, &errorLine,
                                    &errorColumn))
        {
            QString errorMessage = QString("Parse error at line %1, column %2:").arg(errorLine)
                    .arg(errorColumn)
                    + errorStr;
            throw lixoft::exception::Exception("pharmlToMlxtran",errorMessage.toUtf8().data(),THROW_LINE_FILE);
        }




        QDomElement root = domDocument.documentElement();

        QDomNodeList trialDesign = root.elementsByTagName("TrialDesign");
        // first load trial design to get dosing variables
        for (int tdi = 0;tdi<trialDesign.size();tdi++)
        {
            pharmMLDataToMlxtran(trialDesign.at(tdi));
        }


        loadFromRoot(root);

        return;

        QDomNodeList modelDefinition = root.elementsByTagName("ModelDefinition");

        QDomNodeList modellingSteps = root.elementsByTagName("ModellingSteps");
        QDomNodeList rootNodes = root.childNodes();


        for (int mdi = 0;mdi <modelDefinition.size();mdi++)
        {
            QDomNode definitionNode = modelDefinition.at(mdi);
            for (int definitionModel_i = 0;definitionModel_i<definitionNode.childNodes().size();definitionModel_i++)
            {
                if(definitionNode.childNodes().at(definitionModel_i).nodeName() == "CovariateModel")
                {
                    pharmMLCovariateModelToMlxtran(definitionNode.childNodes().at(definitionModel_i));
                }
                else if(definitionNode.childNodes().at(definitionModel_i).nodeName() == "ParameterModel")
                {
                    pharmMLParameterModelToMlxtran(definitionNode.childNodes().at(definitionModel_i));
                }
                else if(definitionNode.childNodes().at(definitionModel_i).nodeName() == "StructuralModel")
                {
                    pharmMLStructuralModelToMlxtran(definitionNode.childNodes().at(definitionModel_i));
                }
                else if(definitionNode.childNodes().at(definitionModel_i).nodeName() == "ObservationModel")
                {
                    pharmMLObservationModelToMlxtran(definitionNode.childNodes().at(definitionModel_i));
                }
            }
        }
        for (int msi = 0;msi<modellingSteps.size();msi++)
        {
            pharmMLEstimationTaskToMlxtran(modellingSteps.at(msi));
        }
        for (int rni = 0;rni<rootNodes.size();rni++)
        {
            if (rootNodes.at(rni).nodeName() == "ct:Name")
            {
                if (rootNodes.at(rni).childNodes().size())
                    _modelDescription+=rootNodes.at(rni).firstChild().nodeValue();
            }
        }
        for (int trial_i=0;trial_i<trialDesign.size();trial_i++)
        {

        }
    }

    void PharmML08ToMlxtran::extractInputs(QSet<QString>& mergedInputs, QSet<QString>& removedInputs, QVector<VariableDefinition*>& variables)
    {
        for (int i = 0;i<variables.size();i++)
        {
            if (variables[i]->type() == VariableDefinition::IS_RANDOM_VARIABLE)
            {
                RandomVariableDefinition* rvd = static_cast<RandomVariableDefinition*>(variables[i]);
                if (!rvd->needWrite) continue;
            }

            for (QSet<QString>::const_iterator it = variables[i]->dependentVariables.begin();
                 it != variables[i]->dependentVariables.end();
                 it++)
            {
                if (
                    !removedInputs.contains(*it) &&
                    !_dosingVariables.contains(*it) // dosing variables (amtDose and tDose aliases) cannot be inputs
                    && (*it != "t")                 // time cannot be input
                   )
                {
                    QString insertedVariable = *it;
                    if (variables[i]->type() == VariableDefinition::IS_STATISTICAL_DEFINITION) {
                        StatisticalDefinition* sd = static_cast<StatisticalDefinition*>(variables[i]);
                        if (this->errorModelParameterMapping.contains(sd->name)) {
                            QVector<QPair<QString,QString> >mapParams = this->errorModelParameterMapping[sd->name];
                            for (int mi = 0;mi<mapParams.size();mi++) {
                                if (insertedVariable == mapParams[mi].second) {
                                    insertedVariable = mapParams[mi].first;
                                }
                            }
                        }
                    }
                    insertedVariable = insertedVariable.remove(QRegExp("\\:.*$"));
                    insertedVariable = insertedVariable.remove(QRegExp("#.*$"));
                    mergedInputs.insert(insertedVariable);
                }
            }
            //_macroParameters.remove(variables[i]->name);
        }
        // suppress affected variables;
        for (int i = 0;i<variables.size();i++)
        {
            QString name  = variables[i]->name;
            name = name.remove(QRegExp("\\:.*$"));
            name = name.remove(QRegExp("#.*$"));
            name.replace("ddt_","");
            mergedInputs.remove(name);
            removedInputs.insert(name);
        }
    }


    QString PharmML08ToMlxtran::contextualSerialize(QVector<VariableDefinition*>& variables)
    {
        bool inEquation = false;
        bool inDefinition = false;
        bool inPK = false;
        QString returnedString = "";
        QVector<VariableDefinition *> derivativeAtEnd;
        QMap<QString,QStringList> categoricalCovariatesInput;
        for (int i = 0;i<variables.size();i++)
        {
            bool firstAddDefinition = false;
            if ((variables[i]->type() == PharmMLLoader08::VariableDefinition::IS_RANDOM_VARIABLE
                || variables[i]->type() == PharmMLLoader08::VariableDefinition::IS_STATISTICAL_DEFINITION)
                )
            {
                if (variables[i]->type() == PharmMLLoader08::VariableDefinition::IS_RANDOM_VARIABLE)
                {
                    PharmMLLoader08::RandomVariableDefinition* rvd = static_cast<PharmMLLoader08::RandomVariableDefinition*>(variables[i]);
                    if (!rvd->equation.length())
                    {
                        if(!inDefinition && !returnedString.contains(QRegExp("DEFINITION:[\\n\\s]+$"))) returnedString+="\nDEFINITION:\n";
                        inDefinition = true;
                        inEquation = false;
                        inPK = false;
                    }
                    else
                    {
                        if(!inEquation && !rvd->equation.contains("PK:")) returnedString+="\nEQUATION:\n";
                        inDefinition = false;
                        inEquation = false;
                        inPK = false;
                        if (!rvd->equation.contains("PK:")) {
                            inEquation =  true;
                        }
                        else {
                            inPK = true;
                        }
                    }

                }
                else
                {
                    PharmMLLoader08::StatisticalDefinition* sd = static_cast<PharmMLLoader08::StatisticalDefinition*>(variables[i]);
                    if (!sd->equation.length())
                    {
                        if(!inDefinition && !returnedString.contains(QRegExp("DEFINITION:[\\n\\s]+$"))) {
                            returnedString+="\nDEFINITION:\n";
                            firstAddDefinition = true;
                        }
                        inDefinition = true;
                        inEquation = false;
                        inPK = false;
                    }
                    else
                    {
                        if(!inEquation && !sd->equation.contains("PK:")) returnedString+="\nEQUATION:\n";
                        inDefinition = false;
                        inEquation = false;
                        inPK = false;
                        if (!sd->equation.contains("PK:")) {
                            inEquation =  true;
                        }
                        else {
                            inPK = true;
                        }
                    }

                }
            }
            else if (!inEquation && !inPK)
            {
                if (!variables[i]->equation.contains("PK:") && variables[i]->equation.length())
                {
                    returnedString+="\nEQUATION:\n";
                    inDefinition = false;
                    inEquation =  true;
                }
            }

            if (inDefinition)
            {                                                                                               
               if (variables[i]->type() == PharmMLLoader08::VariableDefinition::IS_RANDOM_VARIABLE)
               {
                   PharmMLLoader08::RandomVariableDefinition* rvd = static_cast<PharmMLLoader08::RandomVariableDefinition*>(variables[i]);
                   if (rvd->needWrite)
                   {
                       if (rvd->equation.length() == 0)
                       {
                           if (rvd->name.length()) {
                               returnedString+=(rvd->name + QString(" = { distribution = normal, ")
                                                + QString("prediction = ") + rvd->mean + QString(","));
                               if (!rvd->isVariance)
                               {
                                  returnedString+=(QString("sd = ") + QString(rvd->stddev) + QString(" }\n"));
                               }
                               else
                               {
                                  returnedString+=(QString("var = ") + QString(rvd->stddev) + QString(" }\n"));
                               }
                           }
                           else if (firstAddDefinition) {
                               returnedString.remove("\nDEFINITION:\n");
                           }
                       }
                       else returnedString+=(rvd->equation + QString("\n"));
                   }
               }
               else
               {
                    PharmMLLoader08::StatisticalDefinition* sd = static_cast<PharmMLLoader08::StatisticalDefinition*>(variables[i]);
                    if (sd->equation.length() == 0 && sd->name.length())
                    {
                        returnedString += QString(sd->name) + QString(" = { distribution = normal, ");
                        if (sd->transformation.length())
                            returnedString += QString("transformation = ") + sd->transformation + QString(", ");
                        if (sd->errorFunction)
                        {
                            returnedString += (QString("prediction = ") + sd->population);
                        }
                        else
                        {
                            returnedString += QString("reference = ") + sd->population;
                        }
                        if (sd->covariates.size() || sd->omegas.size())
                            returnedString += QString(", ");
                        if (sd->covariates.size())
                        {
                            QString covariateList = "";
                            QString coefficientList = "";
                            bool extraBrackets=false;
                            if (sd->covariates.size() > 1) extraBrackets=true;
                            for (QMap<QString,QString>::const_iterator cit = sd->covariates.begin();
                                 cit != sd->covariates.end();
                                 cit++)
                            {
                                QString citkey = cit.key();
                                QString citvalue = cit.value();
                                QStringList catmod = cit.key().split(":");
                                if (catmod.size() == 2 && catmod[1].length()) {
                                    categoricalCovariatesInput[catmod[0]].push_back(catmod[1]);
                                }
                                citvalue = citvalue.remove(QRegExp("\\:.*$"));
                                citvalue = citvalue.remove(QRegExp("#.*$"));
                                citkey = citkey.remove(QRegExp("\\:.*$"));
                                citkey = citkey.remove(QRegExp("#.*$"));
                                QMap<QString,QString>::const_iterator nextIt = cit;
                                nextIt++;
                                if (nextIt == (sd->covariates.end() ))
                                {
                                    covariateList += citkey;
                                    if (extraBrackets) coefficientList+="{";
                                    coefficientList += citvalue;
                                    if (extraBrackets) coefficientList+="}";
                                }
                                else
                                {
                                    covariateList += (citkey + ",");
                                    if (extraBrackets) coefficientList+="{";
                                    coefficientList += citvalue;
                                    if (extraBrackets) coefficientList+="}";
                                    coefficientList += ",";
                                }
                            }
                            returnedString += (QString("covariate={") + covariateList + QString("}, "));
                            returnedString += (QString("coefficient={") + coefficientList + QString("},"));
                        }
                        if (sd->omegas.size())
                        {
                            if (sd->errorFunction)
                            {
                                if (sd->omegas.size())
                                {
                                    QString errorModel = sd->omegas[0];
                                    QString errorModelTransf = "";
                                    if (errorModel == "additiveErrorModel" || errorModel=="additiveError")
                                    {
                                        errorModel = "constant";
                                    }
                                    else if (errorModel == "combinedAdditiveProportionalModel1" || errorModel == "combinedErrorModel" || errorModel=="combinedError1")
                                    {
                                        errorModel = "combined1";
                                    }
                                    else if (errorModel == "combinedAdditiveProportionalModel1Log" || errorModel == "combinedErrorModelLog" || errorModel=="combinedError1Log")
                                    {
                                        errorModel = "combined1";
                                        errorModelTransf = ", transformation=log";
                                    }
                                    else if (errorModel == "combinedAdditiveProportionalModel2Log" || errorModel == "combinedErrorMode2Log" || errorModel=="combinedError2Log")
                                    {
                                        errorModel = "combined2";
                                        errorModelTransf = ", transformation=log";
                                    }
                                    else if (errorModel == "combinedAdditiveProportionalModel2" || errorModel == "combinedError2")
                                    {
                                        errorModel = "combined2";
                                    }
                                    else if (errorModel == "combinedAdditiveProportionalModel1" || errorModel == "proportionalErrorModel" || errorModel == "proportionalError")
                                    {
                                        errorModel = "proportional";
                                    }
                                    else if (errorModel == "constantErrorModel")
                                    {
                                        errorModel = "constant";
                                    }
                                    else throwLater(lixoft::exception::Exception("PharmML08ToMlxtran::serialize",std::string("Error model '")
                                                                                                               + std::string(errorModel.toUtf8().data())
                                                                                                               + std::string("' is unknown"),THROW_LINE_FILE));

                                    returnedString += QString(" errorModel=") + errorModel+ QString("(");
                                    for (int epi = 0;epi<sd->errorParameters.size();epi++)
                                    {
                                        QVector<QPair<QString,QString> >mapParams = this->errorModelParameterMapping[sd->name];
                                        QString mappedErrorParam = sd->errorParameters[epi];
                                        for (int mi = 0;mi<mapParams.size();mi++) {
                                            if (mappedErrorParam == mapParams[mi].second) {
                                                mappedErrorParam = mapParams[mi].first;
                                            }
                                        }
                                        if (epi == (sd->errorParameters.size()-1))
                                            returnedString +=  mappedErrorParam;
                                        else returnedString += ( mappedErrorParam + ", ");
                                    }
                                    returnedString += ")";
                                    returnedString += errorModelTransf;
                                }
                                else
                                    throwLater(lixoft::exception::Exception("PharmML08ToMlxtran::serialize","There is no error model",THROW_LINE_FILE));

                            }
                            else
                            {
                                QString varlevel = "";
                                if (sd->withVariance) returnedString += "var={";
                                else returnedString += "sd={";
                                for (int sdi = 0;sdi<sd->omegas.size();sdi++)
                                {
                                    if (sd->omegas.size() > 1) {
                                        if (sdi == 0) {
                                            varlevel="varlevel={id";
                                        }
                                        else
                                        {
                                            varlevel += ",id";
                                            for (int occi=0;occi<sdi;occi++) {
                                                varlevel += "*occ";
                                            }
                                        }
                                    }

                                    if (sdi == (sd->omegas.size()-1))
                                    {
                                        //varRef+= sd->
                                        returnedString += sd->omegas[sdi];
                                    }
                                    else returnedString += (sd->omegas[sdi] + ", ");
                                }
                                returnedString += "}";
                                if (varlevel.length()) {
                                    varlevel+="}";
                                    returnedString+=(", "+varlevel);
                                }
                            }
                        }
                        else {
                            returnedString+=" ,sd=0.0 ";
                        }

                        returnedString +=  "}\n";
                    }
                    else
                    {
                        QString sdeq = sd->toString();                        
                        sdeq = sdeq.trimmed();
                        if (!sdeq.isEmpty())
                        {
                            if (sdeq.contains("="))
                                returnedString += (sdeq + "\n");
                            else if (sd->name.length())
                               returnedString += (sd->name +" = "+sdeq + "\n");
                        }
                        else if (firstAddDefinition) {
                            returnedString.remove("\nDEFINITION:\n");
                        }

                    }
               }
            }
            else // equation part
            {
                if (variables[i]->equation.contains(QRegExp("\\s*if"))
                    || variables[i]->equation.contains(QRegExp("\\s*else"))
                    || variables[i]->equation.contains(QRegExp("\\s*end\\s*\\n*$")))
                {
                    returnedString+=(variables[i]->equation+"\n");
                }
                else
                {
                    if (variables[i]->equation.contains("ddt_"))
                    {
                        derivativeAtEnd.push_back(variables[i]);
                    }
                    else{
                        QString sdeq = variables[i]->toString();
                        sdeq=sdeq.trimmed();
                        if (!sdeq.isEmpty())
                        {
                            if (sdeq.contains("="))
                                returnedString += (sdeq + "\n");
                            else
                                returnedString += (variables[i]->name + " = " + sdeq + "\n");
                        }
                    }
                }
            }
        }
        for (int di = 0;di<derivativeAtEnd.size();di++)
        {
            returnedString += (derivativeAtEnd[di]->toString() + "\n");
        }
        returnedString.replace(QRegExp("DEFINITION:\\n[\\n\\s]*"),"DEFINITION:\n");
        returnedString.replace(QRegExp("DEFINITION:\\n[\\n\\s]*EQUATION:"),"EQUATION:\n");
        return returnedString.trimmed();

    }

    void PharmML08ToMlxtran::serialize()
    {
        QFile modelFile(_output);
        modelFile.open(QFile::WriteOnly | QFile::Text);
        if (modelFile.isOpen())
        {
            QTextStream modelStream(&modelFile);

            QSet<QString> inputs;
            QSet<QString> removedInputs;
            if (covariatesSection.size())
            {
                QString covariateData = contextualSerialize(covariatesSection);
                if (covariateData.length())
                {
                    modelStream<<"[COVARIATE]\n";
                    // serialize covariate
                    extractInputs(inputs,removedInputs,covariatesSection);
                    if (inputs.size())
                        modelStream<<"input={"<<QStringList(inputs.toList()).join(",")<<"}\n\n";
                    modelStream<<covariateData;
                }
            }

            // serialize individual,

            if (individualParametersSection.size())
            {
                inputs.clear();
                removedInputs.clear();
                extractInputs(inputs,removedInputs,individualParametersSection);
                QString individualData = contextualSerialize(individualParametersSection);
                QVector<QString> correlations;
                if (_parameterCorrelation.size())
                {
                   for (int cori=0;cori<_parameterCorrelation.size();cori++)
                   {
                       if (_parameterCorrelation[cori].asInput) {
                           if (_parameterCorrelation[cori].equation.length() && _parameterCorrelation[cori].covCoefName.length())
                           {
                              inputs.insert(_parameterCorrelation[cori].covCoefName);
                              if (individualData.contains("EQUATION:")) {
                                    individualData = individualData.replace("EQUATION:","EQUATION:\n"+_parameterCorrelation[cori].equation+"\n");
                              }
                              else {
                                  individualData = "EQUATION:\n"+_parameterCorrelation[cori].equation+"\n"+individualData;
                              }

                           }
                           else {
                            inputs.insert(_parameterCorrelation[cori].coefficient);
                           }
                       }
                       int level = _parameterCorrelation[cori].level;
                       while(level >= correlations.size()) {
                           correlations.push_back("");
                       }
                       if (correlations[level].length() == 0) {
                           correlations[level] = "\ncorrelation={";
                       }
                       QString corCoef = _parameterCorrelation[cori].coefficient;
                       if (corCoef.length() == 0) { corCoef = "1.0"; }
                       correlations[level]+=(QString("r(")+_parameterCorrelation[cori].randomVariable1+QString(",")
                                     +_parameterCorrelation[cori].randomVariable2+QString(")=")+corCoef);
                       if (cori != (_parameterCorrelation.size()-1))
                       {
                           correlations[level]+=QString(",");
                       }
                   }
                   for (int cori=0;cori<correlations.size();cori++)
                   {
                       if (correlations[cori].length()) {
                            correlations[cori]+="}\n";
                       }
                   }
                }
                QString correlation="";
                for (int cori = 0;cori<correlations.size();cori++) {
                    if (correlations[cori].length()) {
                        correlation+=correlations[cori];
                    }
                }
                if (individualData.length())
                {
                    modelStream<<"\n\n[INDIVIDUAL]\n";
                    if (inputs.size())
                        modelStream<<"input={"<<QStringList(inputs.toList()).join(",")<<"}\n\n";

                    if (individualData.contains("EQUATION:"))
                    {
                        QString eq("EQUATION");
                        if (individualData.indexOf(eq) > individualData.indexOf("DEFINITION"))
                        {
                            individualData.replace(individualData.indexOf(eq),eq.length(),correlation+"\n"+eq);
                        }
                        else {
                            individualData+=correlation;
                        }
                    }
                    else {
                        individualData+=correlation;
                    }
                    modelStream<<individualData;
                }

             }

            // serialize longitudinal
            if (structuralModelSection.size() || observationModelSection.size())
            {
                QString structuralData = contextualSerialize(structuralModelSection);
                QString observationData = contextualSerialize(observationModelSection);
                if (observationData.length() == 0 && _discreteModels.size()) {
                    observationData+="DEFINITION:\n";
                }
                for(QMap<QString,Categorical>::const_iterator catObsIt = _discreteModels.begin();
                    catObsIt != _discreteModels.end();
                    catObsIt++) {
                  if (catObsIt.value().equations.length())
                  {
                    observationData+=(catObsIt.value().equations+"\n");
                  }
                }
                if (structuralData.length() || observationData.length())
                {
                    modelStream<<"\n\n[LONGITUDINAL]\n";
                    inputs.clear();
                    removedInputs.clear();
                    QString pkSer = "";
                    inputs=_macroParameters;
                    extractInputs(inputs,removedInputs,structuralModelSection);
                    extractInputs(inputs,removedInputs,observationModelSection);

                    bool doseAmountVariableInPKBlock = false;
                    QString doseAmountVariable;
                    QSet<QString> structuralModelInput = inputs;
                    for (int is = 0;is<structuralModelSection.size();is++)
                    {
                        structuralModelInput.unite(structuralModelSection[is]->dependentVariables);
                        for (QMap<QString,QString>::const_iterator colMapIt = columnMapping.begin();
                             colMapIt!=columnMapping.end();
                             colMapIt++) {
                            if(structuralModelSection[is]->dependentVariables.contains(colMapIt.value()))
                            {
                                for (QMap<int,QPair<QString,QString> >::const_iterator colsIt = _dataSet.columns.begin();
                                     colsIt != _dataSet.columns.end();colsIt++) {
                                    if (colMapIt.key() == colsIt.value().second && colsIt.value().first=="DOSE") {
                                        doseAmountVariable = colMapIt.value();
                                    }
                                }
                            }
                        }
                    }
                    // supress dependent variable mute by pk macros
                    for (QSet<QString>::const_iterator pkamountIt = _macroPKAmount.begin();
                         pkamountIt != _macroPKAmount.end();
                         pkamountIt++) {
                      structuralModelInput.remove(*pkamountIt);
                      int amountIsIndParam = false;
                      for (int is = 0;is<individualParametersSection.size();is++) {
                          if (individualParametersSection[is]->name == *pkamountIt)
                              amountIsIndParam = true;
                      }
                      if (!amountIsIndParam) inputs.remove(*pkamountIt);
                    }
                    if (doseAmountVariable.length()) {
                        for (int is = 0;is<structuralModelSection.size();is++) {
                            if (doseAmountVariable == structuralModelSection[is]->name) {
                                doseAmountVariable = "";
                            }
                            if (QString("ddt_"+doseAmountVariable) ==  structuralModelSection[is]->name)
                            {
                                doseAmountVariableInPKBlock = true;
                            }
                        }
                    }

                    //******//

                    // Supress dosing parameters
                    for (QMap<QString,PharmML08ToMlxProject::Dosing >::const_iterator dit = _dosingVariables.begin();
                         dit != _dosingVariables.end();
                         dit++)
                    {
                      if (structuralModelInput.contains(dit.key()))
                        structuralModelInput.remove(dit.key());
                    }
                    // Supress time dosing parameters
                    for (QMap<QString,QString >::const_iterator dit = doseTimeValues.begin();
                         dit != doseTimeValues.end();
                         dit++)
                    {
                      if (structuralModelInput.contains(dit.key()))
                        structuralModelInput.remove(dit.key());
                    }
                    for (QMap<QString,QPair<QString,QString> >::const_iterator admIt = administrationMapping.begin();
                         admIt != administrationMapping.end();
                         admIt++) {
                      structuralModelInput.remove(admIt.key());
                    }
                    // Suppress affected variables
                    for (int is = 0;is<structuralModelSection.size();is++)
                    {
                      structuralModelInput.remove(structuralModelSection[is]->name);
                      if (structuralModelSection[is]->name.contains(QRegExp("^ddt_"))) // manage derivative variables
                      {
                        QString derivativeVariable = structuralModelSection[is]->name;
                        derivativeVariable.remove(QRegExp("^ddt_"));
                        structuralModelInput.remove(derivativeVariable);
                      }
                    }

                    if (doseAmountVariable.length()) {
                      structuralModelInput.remove(doseAmountVariable);
                    }
                    // Suppress time
                    structuralModelInput.remove("t");


                    //******//


                    if (!_pkBlock.isEmpty() || administrationMapping.size() || doseAmountVariableInPKBlock)
                    {
                        pkSer+="PK:\n";
                        bool addIV = false;
                        for (QMap<QString,QPair<QString,QString> >::const_iterator admIt = administrationMapping.begin();
                             admIt != administrationMapping.end();
                             admIt++)
                        {
                            if (admIt.value().second.size()) {
                                QMap<QString,QString>::const_iterator findNameTypeIt = columnNameTypeMapping.find(admIt.value().second);
                                if (findNameTypeIt != columnNameTypeMapping.end()) {
                                    if (findNameTypeIt.value() == "DOSE") {
                                        bool isaDepot = false;
                                        for (int si = 0;si<structuralModelSection.size();si++) {
                                            if (structuralModelSection[si]->equation.contains("ddt_"+admIt.key())
                                                || structuralModelSection[si]->name.contains("ddt_"+admIt.key()) ) {
                                               isaDepot = true;
                                            }
                                        }
                                        if (isaDepot) {
                                           pkSer+=("depot(target="+admIt.key()+")\n");
                                        }
                                        else {
                                          doseAmountVariable = admIt.key();
                                          doseAmountVariableInPKBlock = false;
                                        }
                                    }
                                    else if (findNameTypeIt.value() == "CMT") {                                                                            
                                      bool isaDepot = false;
                                      for (int si = 0;si<structuralModelSection.size();si++) {
                                        if (structuralModelSection[si]->equation.contains("ddt_"+admIt.key())
                                            || structuralModelSection[si]->name.contains("ddt_"+admIt.key()) ) {
                                          isaDepot = true;
                                        }
                                      }
                                      if (!isaDepot) {
                                          pkSer+=("compartment(cmt="+admIt.value().first+", amount="+admIt.key()+")\n");
                                          addIV = true;
                                      }
                                      else {
                                          pkSer+=("depot(adm="+admIt.value().first+", target="+admIt.key()+")\n");
                                      }
                                    }
                                }
                            }
                            else
                            {
                                //pkSer+=("depot(target="+admIt.key()+")\n");
                                bool isaDepot = false;
                                for (int si = 0;si<structuralModelSection.size();si++) {
                                    if (structuralModelSection[si]->equation.contains("ddt_"+admIt.key())
                                        || structuralModelSection[si]->name.contains("ddt_"+admIt.key()) ) {
                                      isaDepot = true;
                                    }
                                  }
                                  if (isaDepot) {
                                    pkSer+=("depot(target="+admIt.key()+")\n");
                                  }
                                  else {
                                      doseAmountVariable = admIt.key();
                                      doseAmountVariableInPKBlock = false;
                                  }
                            }
                        }
                        if (doseAmountVariableInPKBlock) {
                           pkSer+=("compartment(cmt=1"+QString(", amount=")+doseAmountVariable+")\n");
                           pkSer+=("iv()\n");
                        }
                        if (addIV) {
                            pkSer+=("iv()\n");
                        }
                        pkSer+=(_pkBlock +"\n");
                    }
                    //  - serialize structural model
                    if (structuralData.length()) {
                        QString dtStr;
                        if (doseTimeValues.size()) {

                          for (QMap<QString,QString>::const_iterator dtit = doseTimeValues.begin();
                               dtit != doseTimeValues.end();
                               dtit++) {
                            dtStr+=(dtit.key()+QString(" = ")+dtit.value()+QString("\n"));
                            inputs.remove(dtit.key());
                          }
                        }
                        if (doseAmountVariable.length() && !doseAmountVariableInPKBlock)
                        {
                            if (structuralData.contains("EQUATION:")) {
                                QString eq = "EQUATION:";
                                QString dosePatch = QString("\n")+doseAmountVariable+" = amtDose\n" + dtStr;
                                structuralData.replace(structuralData.indexOf(eq),eq.length(),eq+dosePatch);
                                if (inputs.size()) {
                                    inputs.remove(doseAmountVariable);
                                }
                            }
                            else
                            {
                                if (inputs.size()) {
                                    inputs.remove(doseAmountVariable);
                                }
                                structuralData=QString("EQUATION:\n")+doseAmountVariable+" = amtDose\n" + dtStr + structuralData;
                            }
                        }
                        else if (dtStr.length()) {
                            QString eqsec="";
                            if (!structuralData.contains("EQUATION:")) {
                                structuralData=QString("EQUATION:\n")+dtStr+structuralData;
                            }
                            else {
                                structuralData.replace("EQUATION:",QString("EQUATION:\n")+dtStr);
                            }
                        }


                        modelStream<<"input={"<<QStringList(inputs.toList()).join(",")<<"}\n\n";
                        if (pkSer.length()) {
                            modelStream<<pkSer<<"\n\n";
                        }

                        modelStream<<structuralData<<"\n\n";
                    }
                    else
                    {
                        if (inputs.size())
                            modelStream<<"input={"<<QStringList(inputs.toList()).join(",")<<"}\n\n";
                        if (pkSer.length()) {
                            modelStream<<pkSer<<"\n";
                        }
                    }
                    //  - serialize observation
                    if (observationData.length()) {
                        modelStream<<observationData<<"\n\n";
                    }
                }
            }

        }
        else throwNow(lixoft::exception::Exception("PharmML08ToMlxtran::serialize",std::string("Cannot open (write) file '")
                                                                                 + std::string(_output.toUtf8().data())
                                                                                 + std::string("'."),
                                                    THROW_LINE_FILE));
    }

    void PharmML08ToMlxtran::serializeOLD()
    {
        QFile modelFile(_output);
        modelFile.open(QFile::WriteOnly | QFile::Text);
        if (modelFile.isOpen())
        {
            QTextStream modelStream(&modelFile);

            // serialize covariate
            if (_continuousCovariates.size())
            {
                modelStream<<"[COVARIATE]\n";
                for (QMap<QString,QString>::const_iterator covIt = _continuousCovariates.begin();
                     covIt != _continuousCovariates.end();
                     covIt++)
                {
                    QString newCov = covIt.key(); newCov.replace(QRegExp("[^a-zA-Z0-9_]+"),"");
                    QString eq = covIt.value();
                    modelStream<<newCov<<" = "<<eq<<"\n";
                }
            }

            // serialize individual parameters
            QString equationSectionAsParameter="";
            QString equationSection="";
            QString definitionSection="";
            QString preDefinitionSection="";
            for (QMap<QString,PharmMLLoader08::StatisticalDefinition>::const_iterator ipIt = _individualParameters.begin();
                 ipIt != _individualParameters.end();
                 ipIt++)
            {
                if (ipIt.value().equation.length())
                {
                    QString currentEq;
                    if (ipIt.value().dependentVariables.size())
                    {
                        // search in random effects;
                        for (QMap<QString,RandomVariableData>::const_iterator rit = _randomVariables.begin();
                             rit != _randomVariables.end();
                             rit++)
                        {
                            if (ipIt.value().dependentVariables.contains(rit.key()))
                            {
                                preDefinitionSection += (QString("__X_")+rit.key() + QString(" = { distribution=normal, mean=0, sd=1.0 }\n"));
                                currentEq+=(  rit.key() + QString(" = ")+ rit.value().mean
                                              + QString(" + __X_")+rit.key() + QString("*sqrt(") + rit.value().stddev
                                              +QString(")\n"));
                                if (ipIt.value().isParameterModel)
                                    equationSectionAsParameter += currentEq;
                                else equationSection += currentEq;
                            }
                        }
                    }
                    currentEq = (ipIt.value().equation+QString("\n"));
                    if (ipIt.value().isParameterModel)
                        equationSectionAsParameter += currentEq;
                    else equationSection += currentEq;

                }
                else
                {
                    definitionSection+=(ipIt.key() + QString(" = {") +
                                  QString(" distribution=") + ipIt.value().transformation
                                  + QString("Normal, "));
                    if (ipIt.value().covariates.size())
                    {
                        definitionSection+="covariate={";
                        for (QMap<QString,QString>::const_iterator cit = ipIt.value().covariates.begin();
                             cit != ipIt.value().covariates.end();
                             cit++)
                        {
                            QMap<QString,QString>::const_iterator citend = cit;
                            citend++;
                            if (citend != ipIt.value().covariates.end())
                                definitionSection+= (cit.key() + QString(","));
                            else definitionSection+=cit.key();
                        }
                        definitionSection+="} ";
                    }
                    definitionSection+="}\n";
                }
            }
            preDefinitionSection = preDefinitionSection.trimmed();
            if (preDefinitionSection.length()) preDefinitionSection = (QString("\nDEFINITION:\n")+preDefinitionSection+QString("\n"));
            if (equationSection.length()) equationSection = (QString("EQUATION:\n") + equationSectionAsParameter + QString("\n") + equationSection);
            if (definitionSection.length()) definitionSection = (QString("DEFINITION:\n") + definitionSection + QString("\n"));

            QRegExp captureAssignment("([a-zA-Z0-9_]+)\\s*=");
            QStringList list;
            int pos = 0;
            while ((pos = captureAssignment.indexIn(equationSection, pos)) != -1) {
                 list << captureAssignment.cap(1);
                 pos += captureAssignment.matchedLength();
            }
            pos = 0;
            while ((pos = captureAssignment.indexIn(definitionSection, pos)) != -1) {
                 list << captureAssignment.cap(1);
                 pos += captureAssignment.matchedLength();
            }
            for (int supW=0;supW<list.size();supW++)
            {
                _individualParametersSymbols.remove(list.at(supW));
            }

            if (_individualParameters.size())
            {
                modelStream<<"[INDIVIDUAL]\n";
                modelStream<<"input={";
                for (QSet<QString>::const_iterator inputIt = _individualParametersSymbols.begin();
                     inputIt != _individualParametersSymbols.end();
                     inputIt++)
                {
                    QSet<QString>::const_iterator nextInputIt = inputIt;
                    nextInputIt++;
                    if (nextInputIt != _individualParametersSymbols.end())
                    {
                        modelStream<<(*inputIt)<<",";
                    }
                    else
                    {
                        modelStream<<(*inputIt);
                    }
                }
                modelStream<<"}\n";
            }
            modelStream<<preDefinitionSection;
            modelStream<<equationSection;
            modelStream<<definitionSection;

            // serialize observation model
            QSet<QString> longitudinalInput;
            QString preLongitudinalDefinition;
            QString longitudinalEquation;
            QString longitudinalDefinition;

            // Supress dosing parameters
            for (QMap<QString,PharmML08ToMlxProject::Dosing >::const_iterator dit = _dosingVariables.begin();
                 dit != _dosingVariables.end();
                 dit++)
            {
                if (_structuralModelParameters.contains(dit.key()))
                    _structuralModelParameters.remove(dit.key());
            }

            for (QMap<QString,QVector<QString> >::const_iterator pit = _structuralModelParameters.begin();
                 pit != _structuralModelParameters.end();
                 pit++)
            {
                QMap<QString,QVector<QString> >::const_iterator pitend = pit;
                pitend++;
                if (pit.value().size())
                {
                    longitudinalInput.insert(pit.key());
                }
            }


            if (_observationRandomEffects.size())
            {
                for (int ri = 0;ri<_observationRandomEffects.size();ri++)
                {
                    preLongitudinalDefinition+=(QString("\t")+_observationRandomEffects[ri].first+QString("\n"));
                    longitudinalEquation+=(QString("\t")+_observationRandomEffects[ri].second+QString("\n"));
                    pos = 0;
                    list.clear();
                    while ((pos = captureAssignment.indexIn(_observationRandomEffects[ri].second, pos)) != -1)
                    {
                         list << captureAssignment.cap(1);
                         pos += captureAssignment.matchedLength();
                    }
                    for (int li = 0;li<list.size();li++)
                    {
                        longitudinalInput.remove(list.at(li));
                    }
                }
            }
            for (QMap<QString,PharmML08ToMlxProject::SMEquation>::const_iterator it = _structuralModelEquations.begin();
                 it != _structuralModelEquations.end();
                 it++)
            {
                if (it.value().equation.contains(QRegExp("\\s*if"))
                    || it.value().equation.contains(QRegExp("\\s*else"))
                    || it.value().equation.contains(QRegExp("\\s*end\\s*\n$")))
                {
                    modelStream<<it.value().equation<<"\n";
                }
                else
                {

                    //possible DDE => supress equals 0.0;
                    if (!it.value().description.isEmpty())
                    {
                         longitudinalEquation+=(QString("\t;")
                                                + QString(it.value().description)
                                                + QString("\n")
                                               );
                    }
                    if (it.key().contains("#ISASSIGNED"))
                    {
                         longitudinalEquation+=(
                                                QString("\t")
                                                + it.value().equation
                                                + QString("\n")
                                               );
                    }
                    else
                    {
                        if (!it.value().initialODEParameter.isEmpty())
                        {
                            longitudinalEquation+=(QString("\t")+
                                                   it.value().initialODEParameter
                                                   + QString(" = 0.0\n"));
                        }
                        longitudinalEquation+=(QString("\t")
                                               + it.key()
                                               + QString(" = ")
                                               + it.value().equation
                                               + QString("\n"));
                    }
                    pos = 0;
                    list.clear();
                    while ((pos = captureAssignment.indexIn(it.value().equation, pos)) != -1)
                    {
                         list << captureAssignment.cap(1);
                         pos += captureAssignment.matchedLength();
                    }
                    for (int li = 0;li<list.size();li++)
                    {
                        longitudinalInput.remove(list.at(li));
                    }
                }
            }
            if (_errorModel.size())
            {
                modelStream<<"\nDEFINITION:\n";
            }
            for (QMap<QString,QString>::const_iterator obsit = _errorModel.begin();obsit!=_errorModel.end();obsit++)
            {
                QStringList observationPrediction = obsit.key().split("::");
                if (observationPrediction.size() == 2 && !observationPrediction.at(0).isEmpty() && !observationPrediction.at(1).isEmpty())
                {
                    QString errorModel = obsit.value();
                    QString transformation="";
                    if (errorModel == "additiveErrorModel")
                    {
                        errorModel = "constant";
                    }
                    else if (errorModel == "combinedAdditiveProportionalModel1" || errorModel == "combinedErrorModel")
                    {
                        errorModel = "combined1";
                    }
                    else if (errorModel == "combinedErrorModel2Log") {
                        errorModel = "combined2";
                        transformation = ", transformation=log";
                    }
                    else if (errorModel == "combinedErrorModelLog") {
                        errorModel = "combined";
                        transformation=", transformation=log";
                    }
                    else if (errorModel == "combinedAdditiveProportionalModel2")
                    {
                        errorModel = "combined2";
                    }
                    else if (errorModel == "combinedAdditiveProportionalModel1")
                    {
                        errorModel = "proportional";
                    }
                    else if (errorModel == "constantErrorModel")
                    {
                        errorModel = "constant";
                    }
                    else if (errorModel == "exponentialError")
                    {
                        errorModel = "exponential";
                    }
                    longitudinalDefinition+= (observationPrediction.at(0)
                                              + QString(" = { type=continuous, prediction=")
                                              + observationPrediction.at(1)
                                              + transformation
                                              + QString(", error=")
                                              + errorModel
                                              + QString("},\n"));
                    _predictions.insert(observationPrediction.at(1));
                }
            }

            modelStream<<"\n[LONGITUDINAL]\n";
            modelStream<<"input={";
            for (QSet<QString>::const_iterator inputIt = longitudinalInput.begin();
                 inputIt != longitudinalInput.end();
                 inputIt++)
            {
                QSet<QString>::const_iterator nextIt = inputIt;
                nextIt++;
                modelStream<<(*inputIt);
                if (nextIt != longitudinalInput.end())
                    modelStream<<", ";
            }

            modelStream<<"}\n\n";

            QSet<QString> structuralModelInput = _macroParameters;
            // get input from dependant variables of structural model sections
            QString doseAmountVariable;
            for (int is = 0;is<structuralModelSection.size();is++)
            {
              structuralModelInput.unite(structuralModelSection[is]->dependentVariables);
              for (QMap<QString,QString>::const_iterator colMapIt = columnMapping.begin();
                   colMapIt!=columnMapping.end();
                   colMapIt++) {
                if(structuralModelSection[is]->dependentVariables.contains(colMapIt.value()))
                {
                  for (QMap<int,QPair<QString,QString> >::const_iterator colsIt = _dataSet.columns.begin();
                       colsIt != _dataSet.columns.end();colsIt++) {
                    if (colMapIt.key() == colsIt.value().second && colsIt.value().first=="DOSE") {
                      doseAmountVariable = colMapIt.value();
                    }
                  }
                }
              }
            }
            // supress dependent variable mute by pk macros
            for (QSet<QString>::const_iterator pkamountIt = _macroPKAmount.begin();
                 pkamountIt != _macroPKAmount.end();
                 pkamountIt++) {
              structuralModelInput.remove(*pkamountIt);
            }
            bool doseAmountVariableInPKBlock = false;
            if (doseAmountVariable.length()) {
              for (int is = 0;is<structuralModelSection.size();is++) {
                if (doseAmountVariable == structuralModelSection[is]->name) {
                  doseAmountVariable == "";
                }
                if (QString("ddt_"+doseAmountVariable) ==  structuralModelSection[is]->name)
                {
                  doseAmountVariableInPKBlock = true;
                }
              }
            }
            // Supress dosing parameters
            for (QMap<QString,PharmML08ToMlxProject::Dosing >::const_iterator dit = _dosingVariables.begin();
                 dit != _dosingVariables.end();
                 dit++)
            {
              if (structuralModelInput.contains(dit.key()))
                structuralModelInput.remove(dit.key());
            }
            // Supress time dosing parameters
            for (QMap<QString,QString >::const_iterator dit = doseTimeValues.begin();
                 dit != doseTimeValues.end();
                 dit++)
            {
              if (structuralModelInput.contains(dit.key()))
                structuralModelInput.remove(dit.key());
            }
            for (QMap<QString,QPair<QString,QString> >::const_iterator admIt = administrationMapping.begin();
                 admIt != administrationMapping.end();
                 admIt++) {
              structuralModelInput.remove(admIt.key());
            }


            if (!_pkBlock.isEmpty() || administrationMapping.size() || doseAmountVariableInPKBlock)
            {
              modelStream<<"PK:\n";
              bool addIV = false;
              for (QMap<QString,QPair<QString,QString> >::const_iterator admIt = administrationMapping.begin();
                   admIt != administrationMapping.end();
                   admIt++)
              {
                if (admIt.value().second.size()) {
                  QMap<QString,QString>::const_iterator findNameTypeIt = columnNameTypeMapping.find(admIt.value().second);
                  if (findNameTypeIt != columnNameTypeMapping.end()) {
                    if (findNameTypeIt.value() == "DOSE") {
                      bool isaDepot = false;
                      for (int si = 0;si<structuralModelSection.size();si++) {
                        if (structuralModelSection[si]->equation.contains("ddt_"+admIt.key())
                            || structuralModelSection[si]->name.contains("ddt_"+admIt.key()) ) {
                          isaDepot = true;
                        }
                      }
                      if (isaDepot) {
                        modelStream<<"depot(target="<<admIt.key()<<")\n";
                      }
                      else {
                        doseAmountVariable = admIt.key();
                        doseAmountVariableInPKBlock = false;
                      }

                    }
                    else if (findNameTypeIt.value() == "CMT") {
                      modelStream<<"compartment(cmt="<<admIt.value().first<<", amount="<<admIt.key()<<")\n";
                      addIV = true;
                    }
                  }
                }
                else
                {
                  modelStream<<"compartment(cmt="<<admIt.value().first<<", amount="<<admIt.key()<<")\n";
                }
              }
              if (doseAmountVariableInPKBlock) {
                modelStream<<"compartment(cmt=1"<<", amount="<<doseAmountVariable<<")\n";
                modelStream<<"iv()\n";
              }
              if (addIV) {
                modelStream<<"iv()\n";
              }
              modelStream<<_pkBlock <<"\n";
            }
            if (preLongitudinalDefinition.length())
                modelStream<<"\nDEFINITION:\n"<<preLongitudinalDefinition<<"\n";
            if (longitudinalEquation.length())
            {
                modelStream<<"\nEQUATION:\n";
                modelStream<<longitudinalEquation<<"\n";
            }
            if (longitudinalDefinition.size())
            {
                modelStream<<"\nDEFINITION:\n";
                modelStream<<longitudinalDefinition<<"\n";
            }
        }
        else
        {
            throwLater(lixoft::exception::Exception("lixoft::translator::PharmMToMlxtran",
                                                    std::string("Cannot open or create model file '")
                                                    + std::string(modelFile.fileName().toUtf8().data()) + std::string("'")
                                                    , THROW_LINE_FILE));
        }
    }

    QString PharmML08ToMlxtran::serializeToString()
    {
        return "";
    }
}
}
