#include "lixoft/translator/LanguageTranslatorObject.h"


namespace lixoft {
namespace translator {

    LanguageTranslatorObject::LanguageTranslatorObject()
    {}
    LanguageTranslatorObject::~LanguageTranslatorObject()
    {}
    QStringList& LanguageTranslatorObject::errorStack()
    { return _errorStack; }
    const QStringList& LanguageTranslatorObject::errorStack() const
    { return _errorStack; }
    QStringList& LanguageTranslatorObject::warningStack()
    { return _warningStack;}
    const QStringList& LanguageTranslatorObject::warningStack() const
    { return _warningStack; }
    void LanguageTranslatorObject::reset()
    { 
      _errorStack.clear();
      _warningStack.clear();
    }

}
}

