#include <QSet>
#include <QFileInfo>
#include <QDir>
#include <QDomDocument>
#include <QDomElement>

#include <QDebug>

#include <math.h>
#include <limits>

#include "lixoft/translator/LanguageTranslator.h"
#include "lixoft/exceptions/Exception.h"

namespace lixoft {
namespace translator {

  LanguageTranslator::LanguageTranslator(const QString& sourceFile,
                                         const QString& output,
                                         const QVector<QPair<QString,QString> >& options,
                                         int outputFormat):
      _sourceFile(sourceFile),
      _output(output),
      _outputFormat(outputFormat),
      _options(options)
  {
      // compute baseName
      QFileInfo inputFileName(sourceFile);
      _outBaseName = inputFileName.baseName();
  }

  LanguageTranslator::~LanguageTranslator()
  {}

  void LanguageTranslator::checkFileAndDirectory(const QString& translatorClass)
  {
      QFileInfo testSource(_sourceFile);
      if (!testSource.exists() || !testSource.isFile() || !testSource.isReadable())
          throw lixoft::exception::Exception(translatorClass.toUtf8().data(),
                                             std::string("Source file '") + std::string(_sourceFile.toUtf8().data()) + std::string("' ")
                                             + std::string(" does not exist or is not a file or is not readable"),
                                             THROW_LINE_FILE
                                            );
      if (_outputFormat & LanguageTranslator::OUTPUT_IS_A_DIRECTORY)
      {
        QDir().mkdir(_output);
        QFileInfo testOuputDir(_output);
        if (!testOuputDir.exists() ||  !testOuputDir.isDir())
              throw lixoft::exception::Exception(translatorClass.toUtf8().data(),
                                             std::string("Output directory '") + std::string(_output.toUtf8().data()) + std::string("' ")
                                             + std::string(" does not exist or is not a directory"),
                                             THROW_LINE_FILE
                                            );
      }
      else
      {
          QFile createEmpty(_output);
          createEmpty.open(QFile::WriteOnly | QFile::Text);
          createEmpty.close();
          QFileInfo testOuputFile(_output);
          if (!testOuputFile.exists() ||  !testOuputFile.isFile())
                throw lixoft::exception::Exception(translatorClass.toUtf8().data(),
                                               std::string("Output directory '") + std::string(_output.toUtf8().data()) + std::string("' ")
                                               + std::string(" does not exist or is not a file"),
                                               THROW_LINE_FILE
                                              );

      }
      if (_outBaseName.isEmpty()) _outBaseName="default";
  }

  QString LanguageTranslator::toString() const
  {
      QString objectString = LixoftObject::toString();
      objectString+=(
                        QString("Source file:") + _sourceFile + QString("\n")
                        + QString("Output:") + _output + QString("\n")
                        + QString("Output Format:") + QString::number(_outputFormat) + QString("\n")
                        + QString("Options:\n")
                    );
      for (int opti = 0;opti<_options.size();opti++)
      {
          objectString+=( QString("\t") + _options[opti].first + QString(":") + _options[opti].second + QString("\n"));
      }
      return objectString;
  }

}
}
