#include "lixoft/translator/LixoftTranslatorFactory.h"
#include "lixoft/translator/MlxProjectToPharmML.h"
#include "lixoft/translator/PharmMLToMlxProject.h"
#include "lixoft/translator/PharmML08ToMlxProject.h"
#include "lixoft/translator/PharmMLToMlxtran.h"
#include "lixoft/translator/PharmML08ToMlxtran.h"

#include "lixoft/exceptions/Exception.h"


#define PROJECT_PHARML_STR      "pharmml"
#define PROJECT_XMLX_STR        "xmlx"
#define PROJECT_MLXPROJECT_STR  "mlxproject"
#define PROJECT_MLXTRAN_STR     "mlxtran"


namespace lixoft {
namespace translator {


QSharedPointer<LanguageTranslator> LixoftTranslatorFactory::getTranslator(const QString& fromLanguage,
                                                                          const QString& toLanguage,
                                                                          const QString& sourceFile,
                                                                          const QString& output,                                                                          
                                                                          const QVector<QPair<QString,QString> >& options,
                                                                          int outputFormat,
                                                                          const QString& pharmmlversion
                                                                          )
{
    if (fromLanguage.toLower() == PROJECT_PHARML_STR)
    {
        if (toLanguage.toLower() == PROJECT_MLXPROJECT_STR)
        {
            if (pharmmlversion == "0.8") {
                return QSharedPointer<LanguageTranslator>(new PharmML08ToMlxProject(sourceFile,
                                                                              output,
                                                                              options,
                                                                              outputFormat));
            }
            else {
                return QSharedPointer<LanguageTranslator>(new PharmMLToMlxProject(sourceFile,
                                                                              output,                                                                              
                                                                              options,
                                                                              outputFormat));
            }
        }
        else if (toLanguage.toLower() == PROJECT_MLXTRAN_STR)
        {
            if (pharmmlversion == "0.8") {
              return QSharedPointer<LanguageTranslator>(new PharmML08ToMlxtran(sourceFile,
                                                                             output,
                                                                             options,
                                                                             outputFormat));
            }
            else {
              return QSharedPointer<LanguageTranslator>(new PharmMLToMlxtran(sourceFile,
                                                                           output,                                                                           
                                                                           options,
                                                                           outputFormat));
            }
        }
    }
    else if (fromLanguage.toLower() == PROJECT_MLXPROJECT_STR)
    {

        if (toLanguage.toLower() == PROJECT_PHARML_STR)
        {
            return QSharedPointer<LanguageTranslator>(new MlxProjectToPharmML(sourceFile,
                                                                              output,
                                                                              options,
                                                                              outputFormat));
        }


    }
    LixoftObject::null().throwNow(lixoft::exception::Exception("lixoft::translator::LixoftTranslatorFactory",
                                       std::string("Translator from '") + std::string(fromLanguage.toUtf8().data()) + std::string("' ")
                                       + std::string("to '") + std::string(toLanguage.toUtf8().data()) + std::string("' ")
                                       + std::string("does not exists"),THROW_LINE_FILE));
    return QSharedPointer<LanguageTranslator>(0); // never reached (make compiler happy)

}

}
}
