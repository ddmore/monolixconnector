#include "lixoft/translator/PharmMLLoader08.h"

#include <QTextStream>
#include <QFile>
#include <QFileInfo>
#include <QPair>
#include <QString>
#include <QStringList>
#include <QDomNodeList>
#include <QDebug>


#include "lixoft/exceptions/Exception.h"
#include <limits>
#include <cmath>
#include <math.h>
#include <iostream>


namespace lixoft {
namespace  translator {

PharmMLLoader08::PharmMLLoader08(const QString& sourceFile,
                             const QString& output,
                             const QVector<QPair<QString,QString> >& options,
                             int outputFormat):
    LanguageTranslator(sourceFile,
                       output,
                       options,
                       outputFormat),
    isMonolixCompliant(true)
{}

PharmMLLoader08::~PharmMLLoader08()
{
    for (int di = 0;di<covariatesSection.size();di++)           delete covariatesSection[di];
    for (int di = 0;di<individualParametersSection.size();di++) delete individualParametersSection[di];
    for (int di = 0;di<structuralModelSection.size();di++)      delete structuralModelSection[di];
    for (int di = 0;di<observationModelSection.size();di++)     delete observationModelSection[di];
}

void PharmMLLoader08::getCovariateModel(QDomElement& root, QVector<VariableDefinition*>& covariates,QSet<QString> & globalParameterRegistered)
{
    for (int ri = 0;ri<root.childNodes().size();ri++)
    {
        QVector<PharmMLLoader08::LNodeSibling> nodeCovariates;
        QStringList blkRefsCovariate;
        traverseTo(nodeCovariates,
                   blkRefsCovariate,
                   root.childNodes().at(ri),
                   QStringList()<<"ModelDefinition"<<"CovariateModel"<<"Covariate");        
        if (nodeCovariates.size())
        {
            for (int ci = 0;ci<nodeCovariates.size();ci++)
            {
                QString covariateName = parseQDomNodeAttribute(nodeCovariates[ci].siblingNode,"symbId");
                QVector<PharmMLLoader08::LNodeSibling> nodeContinuousCovariatesTransformation;
                QVector<PharmMLLoader08::LNodeSibling> nodeContinuousCovariatesTransformationAssigned;
                QStringList blkRefsCC;
                QVector<PharmMLLoader08::LNodeSibling> nodeCategoricalCovariates;
                QStringList blkRefsCatC;

                traverseTo(nodeContinuousCovariatesTransformation,
                           blkRefsCC,
                           nodeCovariates[ci].siblingNode,
                           QStringList()<<"Covariate"<<"Continuous"<<"Transformation"<<"Assign");
                traverseTo(nodeContinuousCovariatesTransformationAssigned,
                           blkRefsCC,
                           nodeCovariates[ci].siblingNode,
                           QStringList()<<"Covariate"<<"Continuous"<<"Transformation"<<"TransformedCovariate");
                traverseTo(nodeCategoricalCovariates,
                           blkRefsCatC,
                           nodeCovariates[ci].siblingNode,
                           QStringList()<<"Covariate"<<"Categorical");
                QString assignedValue;
                if (nodeContinuousCovariatesTransformationAssigned.size()) {
                    assignedValue = parseQDomNodeAttribute(nodeContinuousCovariatesTransformationAssigned[0].siblingNode,"symbId");
                }
                if (nodeContinuousCovariatesTransformation.size())
                {
                    PharmMLLoader08::VariableDefinition* continuousCovariate = new PharmMLLoader08::VariableDefinition;
                    QSet<QString> assigned;
                    equationToVariableDefinition(PharmMLLoader08::COVARIATE_SECTION,
                                                 *continuousCovariate,
                                                 assignedValue,
                                                 assigned,
                                                 covariateName,
                                                 nodeCovariates[ci].blockId,
                                                 nodeContinuousCovariatesTransformation[0]
                                                 );
                    if (assignedValue.length())
                        continuousCovariate->name = assignedValue;
                    else if (assigned.size())
                        continuousCovariate->name = *assigned.begin();
                    covariates.push_back(continuousCovariate);
                }
                else
                {
                    traverseTo(nodeContinuousCovariatesTransformation,
                               blkRefsCC,
                               nodeCovariates[ci].siblingNode,
                               QStringList()<<"Covariate"<<"Continuous");
                    if (nodeContinuousCovariatesTransformation.size())
                    {
                       PharmMLLoader08::VariableDefinition* continuousCovariate = new PharmMLLoader08::VariableDefinition;
                       continuousCovariate->name=covariateName;
                       covariates.push_back(continuousCovariate);
                    }
                }
                if (nodeCategoricalCovariates.size())
                {                    
                    PharmMLLoader08::CatagoricalVariableDefinition* categoricalCovariate = new PharmMLLoader08::CatagoricalVariableDefinition;
                    QDomNodeList modalities = nodeCategoricalCovariates[0].siblingNode.childNodes();
                    categoricalCovariate->name = covariateName;
                    for (int mod_i = 0;mod_i<modalities.size();mod_i++)
                    {
                        if (modalities.at(mod_i).nodeName() == "Category")
                        {
                            QString covariateName = parseQDomNodeAttribute(modalities.at(mod_i),"catId");
                            categoricalCovariate->modalities.push_back(covariateName);
                        }
                    }
                    covariates.push_back(categoricalCovariate);
                }
            }
        }
    }
}

void PharmMLLoader08::getIndividualParameter(const QDomNode& parameterElements,
                                           const QMap<QString,RandomVariableDefinition*>& randomVariables,
                                           const QSet<QString>& registeredSimpleParameters,
                                           PharmMLLoader08::StatisticalDefinition& statisticalDefinitionVariable)
{
    QString readIndividualParameter = parseQDomNodeAttribute(parameterElements,"symbId");
    QString readTransformation = "";
    QString readPopulationParameter = "";
    QMap<QString,QString> readCovariates;
    QVector<QString> readEtas;

    QDomNodeList individualParameterElement = parameterElements.childNodes();
    for (int ipei = 0;ipei<individualParameterElement.size();ipei++)
    {

       if (individualParameterElement.at(ipei).nodeName() == "StructuredModel")
        {
            QDomNodeList gaussianModelElements = individualParameterElement.at(ipei).childNodes();
            for (int gmei = 0;gmei<gaussianModelElements.size();gmei++)
            {
                if (gaussianModelElements.at(gmei).nodeName() == "Transformation")
                {
                    QString transformation = parseQDomNodeAttribute(gaussianModelElements.at(gmei),"type");
                    if (transformation != "log" && transformation != "logit" && transformation != "probit")
                    {
                        throwLater(lixoft::exception::Exception("lixoft::translator::PharmMLLoader08",
                                                                (QString("[Individual Parameter bad model]: bad transformation '")
                                                                 + transformation + QString("' at line #")
                                                                 + QString::number(gaussianModelElements.at(gmei).lineNumber())
                                                                 + QString(". The managed transformations are log, logit and probit")).toUtf8().data(),
                                                                 THROW_LINE_FILE));
                    }
                    else readTransformation = transformation;
                }
                else if (gaussianModelElements.at(gmei).nodeName() == "LinearCovariate")
                {
                    QDomNodeList linearCovariates = gaussianModelElements.at(gmei).childNodes();
                    for (int lci = 0;lci<linearCovariates.size();lci++)
                    {
                        if (linearCovariates.at(lci).nodeName().contains("PopulationValue"))
                        {
                            QDomNodeList populationParameter = linearCovariates.at(lci).childNodes();
                            for (int popi = 0;popi < populationParameter.size();popi++)
                            {
                                if (populationParameter.at(popi).nodeName().contains("Assign"))
                                {
                                    QDomNodeList populationParameterNode = populationParameter.at(popi).childNodes();
                                    if (populationParameterNode.size() == 1)
                                    {
                                        if ((populationParameterNode.at(0).nodeName() == "Equation"
                                                  || populationParameterNode.at(0).nodeName() == "math:Equation")
                                                 && (populationParameterNode.at(0).firstChild().nodeName()=="ct:SymbRef"
                                                     || populationParameterNode.at(0).firstChild().nodeName()=="SymbRef")
                                                 )
                                        {
                                            QDomNodeList populationEqParameterNode = populationParameterNode.at(0).childNodes();

                                            if (populationEqParameterNode.size() == 1)
                                            {
                                                readPopulationParameter = parseQDomNodeAttribute(populationEqParameterNode.at(0),"symbIdRef");
                                            }
                                            else
                                            {
                                                addMessage(LixoftObject::WARNING,
                                                           QString("[Individual Parameter bad model]: to many population parameters, section '")
                                                           + linearCovariates.at(lci).nodeName() + QString("' at line #")
                                                           + QString::number(individualParameterElement.at(ipei).lineNumber()));
                                            }
                                        }
                                        else
                                        {
                                         readPopulationParameter = parseQDomNodeAttribute(populationParameterNode.at(0),"symbIdRef");
                                        }
                                    }
                                    else
                                    {
                                        addMessage(LixoftObject::WARNING,
                                                   QString("[Individual Parameter bad model]: to many population parameters, section '")
                                                   + linearCovariates.at(lci).nodeName() + QString("' at line #")
                                                   + QString::number(individualParameterElement.at(ipei).lineNumber()));
                                    }
                                }
                                else
                                {
                                    addMessage(LixoftObject::WARNING,
                                               QString("[Individual Parameter bad model]: incorrect definition '")
                                               + linearCovariates.at(lci).nodeName() + QString("' at line #")
                                               + QString::number(individualParameterElement.at(ipei).lineNumber()));
                                }
                            }
                        }
                        else if (linearCovariates.at(lci).nodeName() == "Covariate")
                        {
                            QDomNodeList covariateEltments =  linearCovariates.at(lci).childNodes();
                            QString rcov;
                            QVector<QPair<QString,QString> > rbeta;

                            for (int ci = 0;ci<covariateEltments.size();ci++)
                            {
                                if (covariateEltments.at(ci).nodeName() == "ct:SymbRef")
                                {
                                    //rcov = parseQDomNodeAttribute(covariateEltments.at(ci),"blkIdRef").toLower()+parseQDomNodeAttribute(covariateEltments.at(ci),"symbIdRef").toLower();
                                    rcov = parseQDomNodeAttribute(covariateEltments.at(ci),"symbIdRef");
                                    if (_symbolIds.contains(rcov))
                                    {
                                        const PharmMLLoader08::SymbolId& symIdRef = _symbolIds[rcov];
                                        if (symIdRef.transformed.length())
                                            rcov = symIdRef.transformed;

                                    }
                                }
                                else if (covariateEltments.at(ci).nodeName() == "FixedEffect")
                                {
                                    QDomNodeList fixedEffectNodes = covariateEltments.at(ci).childNodes();
                                    QPair<QString,QString> catBeta;
                                    for (int fi = 0;fi<fixedEffectNodes.size();fi++)
                                    {
                                        if (fixedEffectNodes.at(fi).nodeName() == "ct:SymbRef")
                                        {
                                            catBeta.second = parseQDomNodeAttribute(fixedEffectNodes.at(fi),"symbIdRef");
                                        }
                                        else if (fixedEffectNodes.at(fi).nodeName() == "Category")
                                        {
                                                 catBeta.first = parseQDomNodeAttribute(fixedEffectNodes.at(fi),"catId");
                                        }
                                    }
                                    rbeta.push_back(catBeta);
                                }
                            }
                            for (int rbetai = 0;rbetai<rbeta.size();rbetai++)
                            {
                                QString cmapping="__UNDEF__";
                                if (covariateCategoryMapping.contains(rcov)) {
                                    if (covariateCategoryMapping[rcov].contains(rbeta[rbetai].first)) {
                                        cmapping = covariateCategoryMapping[rcov][rbeta[rbetai].first];
                                    }
                                }
                                readCovariates[rcov+QString(":")+rbeta[rbetai].first ] = rbeta[rbetai].second+"#"+cmapping;
                                //readCovariates[ rcov ] = rbeta[rbetai].second;
                            }
                        }
                        else
                        {
                            addMessage(LixoftObject::WARNING,
                                       QString("[Individual Parameter bad model]: unmanaged section '")
                                       + linearCovariates.at(lci).nodeName() + QString("' at line #")
                                       + QString::number(individualParameterElement.at(ipei).lineNumber()));

                        }
                    }
                }
                else if (gaussianModelElements.at(gmei).nodeName().contains("PopulationValue"))
                {
                    QDomNodeList populationParameter = gaussianModelElements.at(gmei).childNodes();
                    for (int popi = 0;popi < populationParameter.size();popi++)
                    {
                        if (populationParameter.at(popi).nodeName().contains("Assign"))
                        {
                            QDomNodeList populationParameterNode = populationParameter.at(popi).childNodes();
                            if (populationParameterNode.size() == 1)
                            {
                                if ((populationParameterNode.at(0).nodeName() == "Equation"
                                          || populationParameterNode.at(0).nodeName() == "math:Equation")
                                         && (populationParameterNode.at(0).firstChild().nodeName()=="ct:SymbRef"
                                             || populationParameterNode.at(0).firstChild().nodeName()=="SymbRef")
                                         )
                                {
                                    QDomNodeList populationEqParameterNode = populationParameterNode.at(0).childNodes();

                                    if (populationEqParameterNode.size() == 1)
                                    {
                                        readPopulationParameter = parseQDomNodeAttribute(populationEqParameterNode.at(0),"symbIdRef");
                                    }
                                    else
                                    {
                                        addMessage(LixoftObject::WARNING,
                                                   QString("[Individual Parameter bad model]: to many population parameters, section '")
                                                   + gaussianModelElements.at(gmei).nodeName() + QString("' at line #")
                                                   + QString::number(individualParameterElement.at(ipei).lineNumber()));
                                    }
                                }
                                else
                                {
                                 readPopulationParameter = parseQDomNodeAttribute(populationParameterNode.at(0),"symbIdRef");
                                }
                            }
                            else
                            {
                                addMessage(LixoftObject::WARNING,
                                           QString("[Individual Parameter bad model]: to many population parameters, section '")
                                           + gaussianModelElements.at(gmei).nodeName() + QString("' at line #")
                                           + QString::number(individualParameterElement.at(ipei).lineNumber()));
                            }
                        }
                        else
                        {
                            addMessage(LixoftObject::WARNING,
                                       QString("[Individual Parameter bad model]: incorrect definition '")
                                       + gaussianModelElements.at(gmei).nodeName() + QString("' at line #")
                                       + QString::number(individualParameterElement.at(ipei).lineNumber()));
                        }
                    }
                }
                else if (gaussianModelElements.at(gmei).nodeName().contains("Assign")) { // assign only Pop value
                    QDomNodeList populationParameterNode = gaussianModelElements.at(gmei).childNodes();
                    if (populationParameterNode.size() == 1)
                    {
                        if (populationParameterNode.at(0).nodeName().contains("SymbRef"))
                        {
                            QDomNodeList populationEqParameterNode = populationParameterNode.at(0).childNodes();

                            if (populationEqParameterNode.size() == 1)
                            {
                                readPopulationParameter = parseQDomNodeAttribute(populationEqParameterNode.at(0),"symbIdRef");
                            }
                            else
                            {
                                addMessage(LixoftObject::WARNING,
                                           QString("[Individual Parameter bad model]: to many population parameters, section '")
                                           + gaussianModelElements.at(gmei).nodeName() + QString("' at line #")
                                           + QString::number(individualParameterElement.at(ipei).lineNumber()));
                            }
                        }
                        else
                        {
                         readPopulationParameter = parseQDomNodeAttribute(populationParameterNode.at(0),"symbIdRef");
                        }
                    }
                }
                else if (gaussianModelElements.at(gmei).nodeName() == "RandomEffects")
                {
                    QDomNodeList randomEffects = gaussianModelElements.at(gmei).childNodes();
                    for (int rei = 0;rei<randomEffects.size();rei++)
                    {
                        if (randomEffects.at(rei).nodeName() == "ct:SymbRef")
                        {
                            QString etaName = parseQDomNodeAttribute(randomEffects.at(rei),"symbIdRef");
                            readEtas.push_back(etaName);
                            pharmMLNamesMapping[readIndividualParameter+QString(":eta")] = etaName;
                        }
                    }
                }
                else if (gaussianModelElements.at(gmei).nodeName() != "#comment")
                {
                    throwLater(lixoft::exception::Exception("lixoft::translator::PharmMLLoader08",
                                                            (QString("[Individual Parameter bad model]: not managed '")
                                                             + gaussianModelElements.at(gmei).nodeName() + QString("' at line #")
                                                             + QString::number(individualParameterElement.at(ipei).lineNumber())
                                                             + QString(".")).toUtf8().data(),
                                                            THROW_LINE_FILE));
                }
            }
        }
       else if (individualParameterElement.at(ipei).nodeName().contains("Assign")
                 && individualParameterElement.at(ipei).childNodes().size()
                 && individualParameterElement.at(ipei).firstChild().nodeName().contains("SymbRef"))
        {
           QString assignRef = parseQDomNodeAttribute(individualParameterElement.at(ipei).firstChild(),"symbIdRef");
           statisticalDefinitionVariable.name = readIndividualParameter;
           if (!registeredSimpleParameters.contains(assignRef))
           {
                statisticalDefinitionVariable.equation += (assignRef + QString("\n"));
                statisticalDefinitionVariable.dependentVariables.insert(assignRef);
           }
           else readPopulationParameter = assignRef;

        }
        else if ((individualParameterElement.at(ipei).nodeName() == "ct:Assign" || individualParameterElement.at(ipei).nodeName() == "Assign")
                 && individualParameterElement.at(ipei).childNodes().size()
                 && (individualParameterElement.at(ipei).firstChild().nodeName()=="Equation"
                     || individualParameterElement.at(ipei).firstChild().nodeName()=="math:Equation"))
        {
            isMonolixCompliant = false;
            QMap<QString,QVector<QString> > blockRef;
            QMap<QString,QString> aliases;
            QSet<QString> dependentVariables;
            QVector<QString> eq = pharmlEquationToMlxtran(PharmMLLoader08::INDIVIDUAL_SECTION,individualParameterElement.at(ipei).firstChild(),
                                                          readIndividualParameter,
                                                          blockRef,
                                                          dependentVariables,
                                                          "",
                                                          aliases);
            for (int eqi = 0;eqi<eq.size();eqi++)
            {
                if (!eq[eqi].contains(QRegExp(readIndividualParameter+QString("\\s*="))))
                {
                    eq[eqi] =QString(/*";" +*/ eq[eqi]);
                }
                statisticalDefinitionVariable.equation += (eq[eqi]);
                statisticalDefinitionVariable.dependentVariables=dependentVariables;
                statisticalDefinitionVariable.name = readIndividualParameter;
            }
        }
       else if (individualParameterElement.at(ipei).nodeName().contains("Assign")) {
           QMap<QString,QVector<QString> > blockRef;
           QSet<QString> dependentVariables;
           QString blkIdRefTmp;
           QMap<QString,QString> aliasesTmp;
           QVector<QString> equation = pharmlEquationToMlxtran(PharmMLLoader08::LONGITUDINAL_SECTION,individualParameterElement.at(ipei),"",blockRef,dependentVariables, blkIdRefTmp, aliasesTmp);
           for (int eqi=0;eqi<equation.size();eqi++) {
                statisticalDefinitionVariable.equation += (equation[eqi]);
           }
           statisticalDefinitionVariable.dependentVariables=dependentVariables;
           statisticalDefinitionVariable.name = readIndividualParameter;
       }
    }
    // TODO : out
    if (readPopulationParameter.length())
    {
        statisticalDefinitionVariable.transformation = readTransformation;
        statisticalDefinitionVariable.population = readPopulationParameter;
        statisticalDefinitionVariable.dependentVariables.insert(readPopulationParameter);
        for (int ri = 0;ri<readEtas.size();ri++)
        {
            QMap<QString,RandomVariableDefinition*>::const_iterator findIt = randomVariables.find(readEtas[ri]);
            if (findIt != randomVariables.end())
            {
                bool doubleOK = false;
                double meanValue = findIt.value()->mean.toDouble(&doubleOK);
                if (!doubleOK)
                {
                    throwLater(lixoft::exception::Exception("PharmMLLoader08::getIndividualParameter",std::string("RandomVariable '")
                                                                                                    +readEtas[ri].toUtf8().data()
                                                                                                    +std::string("' mean is a symbol (it should be 0.0).")));

                }
                if (meanValue>std::numeric_limits<double>::epsilon()
                    || meanValue<(-1*std::numeric_limits<double>::epsilon()))
                {
                    throwLater(lixoft::exception::Exception("PharmMLLoader08::getIndividualParameter",std::string("RandomVariable '")
                                                                                                    +readEtas[ri].toUtf8().data()
                                                                                                    +std::string("' mean is a value different of 0.0.")));
                }
                if (readEtas.size())
                {
                    RandomVariableDefinition* rvtmp = findIt.value();
                    QString varLevel = "";
                    if (rvtmp->variabilityReferences.size())
                        varLevel=rvtmp->variabilityReferences[0].second;
                    statisticalDefinitionVariable.omegas.push_back(findIt.value()->stddev);
                    statisticalDefinitionVariable.omegaNames.push_back(QPair<QString,QString>(findIt.value()->omegaName,varLevel));
                    statisticalDefinitionVariable.withVariance = findIt.value()->isVariance;
                    statisticalDefinitionVariable.dependentVariables.insert(findIt.value()->stddev);
                    QString omegaOrGamma = ":omega";
                    if ( statisticalDefinitionVariable.omegaNames.size() > 1) {
                        omegaOrGamma= ":gamma";
                    }
                    if (statisticalDefinitionVariable.withVariance)
                        pharmMLNamesMapping[readIndividualParameter+omegaOrGamma+QString("2")] = findIt.value()->omegaName;
                    else {
                        pharmMLNamesMapping[readIndividualParameter+omegaOrGamma] = findIt.value()->omegaName;
                    }
                }

            }
            else if (randomVariables.size())
                throwLater(lixoft::exception::Exception("PharmMLLoader08::getIndividualParameter",std::string("RandomVariable '")
                                                                                                +readEtas[ri].toUtf8().data()
                                                                                                +std::string("' is not defined.")));
            // search eta in defined RandomVariables
        }
        statisticalDefinitionVariable.etas = readEtas;
        statisticalDefinitionVariable.name = readIndividualParameter;
        statisticalDefinitionVariable.covariates = readCovariates;
        QSet<QString>& depV = statisticalDefinitionVariable.dependentVariables;
        for (QMap<QString,QString>::const_iterator readCovIt = readCovariates.begin();
             readCovIt != readCovariates.end();
             readCovIt++)
        {
            pharmMLNamesMapping[readIndividualParameter+QString(":beta:")+readCovIt.key()] = readCovIt.value();
            depV.insert(readCovIt.key());
            depV.insert(readCovIt.value());
        }
        pharmMLNamesMapping[readIndividualParameter+QString(":pop")] = readPopulationParameter;
    }
}

void PharmMLLoader08::getIndividualParameterModel(QDomElement& root, QVector<VariableDefinition*>& individualParameters,QSet<QString> & globalParameterRegistered)
{
    for (int ri = 0;ri<root.childNodes().size();ri++)
    {
        QVector<PharmMLLoader08::LNodeSibling> nodePopulationParameters;
        QStringList blkRefsPopulationParameter;
        QVector<PharmMLLoader08::LNodeSibling> nodeSimpleParameters;
        QStringList blkRefsSimpleParameter;
        QVector<PharmMLLoader08::LNodeSibling> nodeRandomVariables;
        QStringList blkRefsRandomVariables;
        QVector<PharmMLLoader08::LNodeSibling> nodeIndividualParameters;
        QStringList blkRefsIndividualParameters;
        QVector<PharmMLLoader08::LNodeSibling> nodeCorrelationLevels;
        QStringList blkRefsCorrelationLevels;
        QVector<PharmMLLoader08::LNodeSibling> nodeCorrelation;
        QStringList blkRefsCorrelation;
        QSet<QString> registeredSimpleParameters;
        traverseTo(nodePopulationParameters,
                   blkRefsPopulationParameter,
                   root.childNodes().at(ri),
                   QStringList()<<"ModelDefinition"<<"ParameterModel"<<"PopulationParameter");
        traverseTo(nodeRandomVariables,
                   blkRefsRandomVariables,
                   root.childNodes().at(ri),
                   QStringList()<<"ModelDefinition"<<"ParameterModel"<<"RandomVariable");
        traverseTo(nodeIndividualParameters,
                   blkRefsIndividualParameters,
                   root.childNodes().at(ri),
                   QStringList()<<"ModelDefinition"<<"ParameterModel"<<"IndividualParameter");
        traverseTo(nodeCorrelationLevels,
                   blkRefsCorrelationLevels,
                   root.childNodes().at(ri),
                   QStringList()<<"ModelDefinition"<<"VariabilityModel");
        traverseTo(nodeCorrelation,
                   blkRefsCorrelation,
                   root.childNodes().at(ri),
                   QStringList()<<"ModelDefinition"<<"ParameterModel"<<"Correlation");
        for (int pi = 0;pi<nodePopulationParameters.size();pi++)
        {
            QString parameterName = parseQDomNodeAttribute(nodePopulationParameters[pi].siblingNode,"symbId");
            registeredSimpleParameters.insert(parameterName);
        }

        for (int ci = 0;ci<nodeSimpleParameters.size();ci++)
        {
            QString parameterName = parseQDomNodeAttribute(nodeSimpleParameters[ci].siblingNode,"symbId");
            QVector<PharmMLLoader08::LNodeSibling> nodeAssign;
            QStringList blkRefsCC;
            traverseTo(nodeAssign,
                       blkRefsCC,
                       nodeSimpleParameters[ci].siblingNode,
                       QStringList()<<"SimpleParameter"<<"Assign"<<"Equation");
            if (nodeAssign.size())
            {
                PharmMLLoader08::VariableDefinition* simpleParameter = new PharmMLLoader08::VariableDefinition;
                QSet<QString> assigned;
                equationToVariableDefinition(PharmMLLoader08::INDIVIDUAL_SECTION,
                                             *simpleParameter,
                                             "",
                                             assigned,
                                             parameterName,
                                             nodeSimpleParameters[ci].blockId,
                                             nodeAssign[0]
                                            );
                if (assigned.size())
                {
                    simpleParameter->name = *assigned.begin();
                }
                individualParameters.push_back(simpleParameter);
            }
            else
            {
                nodeAssign.clear();
                blkRefsCC.clear();
                traverseTo(nodeAssign,
                           blkRefsCC,
                           nodeSimpleParameters[ci].siblingNode,
                           QStringList()<<"Parameter"<<"Assign"<<"Equation");
                if (nodeAssign.size())
                {
                    PharmMLLoader08::VariableDefinition* simpleParameter = new PharmMLLoader08::VariableDefinition;
                    QSet<QString> assigned;
                    equationToVariableDefinition(PharmMLLoader08::INDIVIDUAL_SECTION,
                                                 *simpleParameter,
                                                 "",
                                                 assigned,
                                                 parameterName,
                                                 nodeSimpleParameters[ci].blockId,
                                                 nodeAssign[0]
                                                );
                    if (assigned.size())
                    {
                        simpleParameter->name = *assigned.begin();
                    }
                    individualParameters.push_back(simpleParameter);
                }
                else if (nodeSimpleParameters[ci].siblingNode.childNodes().size() == 0)
                {
                   QString symb = parseQDomNodeAttribute(nodeSimpleParameters[ci].siblingNode,"symbId");
                   if ( symb.length()) { registeredSimpleParameters.insert(symb); globalParameterRegistered.insert(symb); }
                }
            }
        }
        QMap<QString,RandomVariableDefinition*> randomVariables;
        for (int ci = 0;ci<nodeRandomVariables.size();ci++)
        {
            PharmMLLoader08::RandomVariableDefinition* rvd = new PharmMLLoader08::RandomVariableDefinition;
            getRandomVariable(nodeRandomVariables[ci].siblingNode,*rvd);
            individualParameters.push_back(rvd);
            randomVariables[rvd->name] = rvd;
        }
        for (int ci = 0;ci<nodeIndividualParameters.size();ci++)
        {
            PharmMLLoader08::StatisticalDefinition* sd = new PharmMLLoader08::StatisticalDefinition;
            getIndividualParameter(nodeIndividualParameters[ci].siblingNode,randomVariables,registeredSimpleParameters,*sd);
            individualParameters.push_back(sd);
        }
        for (int ci = 0;ci<nodeCorrelationLevels.size();ci++)
        {
            QDomNodeList levelList = nodeCorrelationLevels[ci].siblingNode.childNodes();
            for (int li = 0;li<levelList.size();li++) {
                QString levelCol = parseQDomNodeAttribute(levelList.at(li),"symbId");
                QDomNodeList parentLevel = levelList.at(li).childNodes();
                if (!parentLevel.size()) {
                    variabilityLevelsIds[levelCol] = 0;
                }
                QString parentLevelName = "";
                for (int pli = 0;pli<parentLevel.size();pli++) {
                    if (parentLevel.at(pli).firstChild().nodeName().contains("SymbRef")) {
                        parentLevelName = parseQDomNodeAttribute(parentLevel.at(pli).firstChild(),"symbIdRef");
                    }
                }
                if (parentLevelName.length() && variabilityLevelsIds.contains(parentLevelName)) {
                    variabilityLevelsIds[levelCol] = variabilityLevelsIds[parentLevelName] + 1;
                }
            }
        }
        for (int ci = 0;ci<nodeCorrelation.size();ci++)
        {
            QString eta1;
            QString eta2;
            QString corCoef;
            QString covCoef;
            bool corCoefIsSymbol = false;

            QString varBlkIdRef;
            QString varSymIdRef;
            int level = 0;
            QDomNodeList correlationElement = nodeCorrelation[ci].siblingNode.childNodes();
            for (int cei = 0;cei<correlationElement.size();cei++)
            {
                if (correlationElement.at(cei).nodeName()=="ct:VariabilityReference")
                {
                    if (correlationElement.at(cei).childNodes().size()==1
                            && correlationElement.at(cei).firstChild().nodeName() == "ct:SymbRef")
                    {
                        QDomNode etaNode = correlationElement.at(cei).firstChild();
                        varBlkIdRef = parseQDomNodeAttribute(etaNode,"blkIdRef");
                        varSymIdRef = parseQDomNodeAttribute(etaNode,"symbIdRef");

                    }
                }
                else if (correlationElement.at(cei).nodeName()=="Pairwise")
                {
                    QDomNodeList correlationPart = correlationElement.at(cei).childNodes();
                    QVector<bool> isCov;
                    for (int cpi = 0;cpi<correlationPart.size();cpi++)
                    {
                        isCov.push_back(false);
                        if (correlationPart.at(cpi).nodeName()=="RandomVariable1")
                        {
                            if (correlationPart.at(cpi).childNodes().size()==1
                                    && correlationPart.at(cpi).firstChild().nodeName() == "ct:SymbRef")
                            {
                                QDomNode etaNode = correlationPart.at(cpi).firstChild();
                                eta1 = parseQDomNodeAttribute(etaNode,"symbIdRef");

                            }
                        }
                        else if (correlationPart.at(cpi).nodeName()=="RandomVariable2")
                        {
                            if (correlationPart.at(cpi).childNodes().size()==1
                                    && correlationPart.at(cpi).firstChild().nodeName() == "ct:SymbRef")
                            {
                                QDomNode etaNode = correlationPart.at(cpi).firstChild();
                                eta2 = parseQDomNodeAttribute(etaNode,"symbIdRef");
                            }
                        }
                        else if (correlationPart.at(cpi).nodeName()=="CorrelationCoefficient")
                        {
                            QDomNodeList realNode = correlationPart.at(cpi).childNodes();
                            for (int rni = 0;rni<realNode.size();rni++) {
                                QString nn = realNode.at(rni).nodeName();
                                if (realNode.at(rni).nodeName().contains("Real"))
                                {
                                    corCoef = realNode.at(rni).firstChild().nodeValue();
                                }
                                else if (realNode.at(rni).nodeName().contains("Assign")
                                   && realNode.at(rni).firstChild().nodeName().contains("Real")) {
                                     corCoef = realNode.at(rni).firstChild().firstChild().nodeValue();
                                }
                                else if (realNode.at(rni).nodeName().contains("Assign")
                                   && realNode.at(rni).firstChild().nodeName().contains("SymbRef")) {
                                     corCoef = parseQDomNodeAttribute(realNode.at(rni).firstChild(),"symbIdRef");
                                     corCoefIsSymbol = true;
                                }

                            }
                        }
                        else if (correlationPart.at(cpi).nodeName()=="Covariance") {
                            QDomNodeList realNode = correlationPart.at(cpi).childNodes();
                            isCov[isCov.size()-1] = true;
                            for (int rni = 0;rni<realNode.size();rni++) {
                                QString nn = realNode.at(rni).nodeName();
                                if (realNode.at(rni).nodeName().contains("Real"))
                                {
                                    covCoef = realNode.at(rni).firstChild().nodeValue();
                                }
                                else if (realNode.at(rni).nodeName().contains("Assign")
                                   && realNode.at(rni).firstChild().nodeName().contains("Real")) {
                                     covCoef = realNode.at(rni).firstChild().firstChild().nodeValue();
                                }
                                else if (realNode.at(rni).nodeName().contains("Assign")
                                   && realNode.at(rni).firstChild().nodeName().contains("SymbRef")) {
                                     covCoef = parseQDomNodeAttribute(realNode.at(rni).firstChild(),"symbIdRef");
                                     corCoefIsSymbol = true;
                                }
                            }
                        }
                    }
                    PharmMLLoader08::ParameterCorrelation pc;
                    QString omega1,omega2;
                    // search pi with eta
                    for (int is = 0;is<individualParametersSection.size();is++)
                    {
                        if (individualParametersSection[is]->type() == PharmMLLoader08::VariableDefinition::IS_STATISTICAL_DEFINITION)
                        {
                            PharmMLLoader08::StatisticalDefinition* sd = static_cast<PharmMLLoader08::StatisticalDefinition*>(individualParametersSection[is]);
                            for (int etai = 0;etai<sd->etas.size();etai++)
                            {
                                if (sd->etas[etai] == eta1)
                                {
                                    eta1 = sd->name;
                                    if (sd->withVariance) {
                                        omega1 = "sqrt("+sd->omegas[etai]+")";
                                    }
                                    else {
                                        omega1 = sd->omegas[etai];
                                    }

                                }
                                if (sd->etas[etai] == eta2)
                                {
                                    eta2 = sd->name;
                                    if (sd->withVariance) {
                                        omega2 = "sqrt("+sd->omegas[etai]+")";
                                    }
                                    else {
                                        omega2 = sd->omegas[etai];
                                    }

                                }
                            }
                        }
                    }
                    if (covCoef.length()) {
                        corCoef = "CORTMP__"+QString::number(ci) +"_"+eta1+"_"+eta2;
                        pc.equation = corCoef+" = " + covCoef + "/(" + omega1 + "*" + omega2 + ")\n";
                    }

                    pc.randomVariable1 = eta1;
                    pc.randomVariable2 = eta2;
                    if (variabilityLevelsIds.contains(varSymIdRef)) {
                        level = variabilityLevelsIds[varSymIdRef];
                    }
                    pc.level = level;
                    pc.coefficient = corCoef;
                    pc.covCoefName = covCoef;
                    pc.blkRef = varBlkIdRef;
                    pc.blkSymb = varSymIdRef;
                    if (corCoefIsSymbol) {
                        if (pc.equation.length() && pc.covCoefName.length())
                        {
                            pharmMLNamesMapping[eta1+QString("#")+eta2+QString("#")+QString::number(level)
                                            +QString(":correlation_as_cov")]=covCoef;
                        }
                        else {
                            pharmMLNamesMapping[eta1+QString("#")+eta2+QString("#")+QString::number(level)
                                            +QString(":correlation")]=corCoef;
                        }
                        pc.asInput = true;
                    }
                    _parameterCorrelation.push_back(pc);


                }
            }
        }
    }
}

void PharmMLLoader08::getStructuralModel(QDomElement& root,
                                       QVector<VariableDefinition*>& structural,
                                       QSet<QString> & globalParameterRegistered)
{
    for (int ri = 0;ri<root.childNodes().size();ri++)
    {
        QVector<PharmMLLoader08::LNodeSibling> nodeStructuralModel;
        QVector<PharmMLLoader08::LNodeSibling> pkMacros;
        QStringList blkRefsStructuralModel;
        traverseTo(nodeStructuralModel,
                   blkRefsStructuralModel,
                   root.childNodes().at(ri),
                   QStringList()<<"ModelDefinition"<<"StructuralModel");
        traverseTo(pkMacros,
                   blkRefsStructuralModel,
                   root.childNodes().at(ri),
                   QStringList()<<"ModelDefinition"<<"StructuralModel"<<"PKmacros");

        for (int ci = 0;ci<nodeStructuralModel.size();ci++)
        {

            QMap<QString,QString> dosingVariables;
            QString blkIdRefTmp;
            QMap<QString,QString> aliasesTmp;


            for (QMap<QString,PharmMLLoader08::Dosing>::const_iterator dit = _dosingVariables.begin();dit!=_dosingVariables.end();dit++)
            {
                if (dit.value().dosingTypes == "doseAmount" && (dit.value().inputTarget=="dose" || dit.value().inputTarget=="parameter"))
                {
                    aliasesTmp[dit.key()] = "amtDose";
                }
                else if (dit.value().dosingTypes == "dosingTimes")
                {
                    aliasesTmp[dit.key()] = "tDose";
                }
            }

            // first parse variables
            QDomNodeList structuralEquations = nodeStructuralModel[ci].siblingNode.childNodes();
            for (int seqi = 0;seqi<structuralEquations.size();seqi++)
            {
                if(structuralEquations.at(seqi).nodeName() == "ct:Variable"
                        || structuralEquations.at(seqi).nodeName() == "SimpleParameter"
                        ||  structuralEquations.at(seqi).nodeName() == "Parameter")
                {
                    QString variableAssigned = parseQDomNodeAttribute(structuralEquations.at(seqi),"symbId");
                    if ((structuralEquations.at(seqi).nodeName() == "SimpleParameter" || structuralEquations.at(seqi).nodeName() == "Parameter")
                         && structuralEquations.at(seqi).childNodes().size() == 0)
                    {
                        globalParameterRegistered.insert(variableAssigned);
                    }
                    QDomNodeList variableElements = structuralEquations.at(seqi).childNodes();
                    if (variableElements.size() == 0) // inputs or dosing
                    {
                        if (_dosingVariables.contains(variableAssigned))
                        {
                            QMap<QString,PharmMLLoader08::Dosing>::const_iterator findIt = _dosingVariables.find(variableAssigned);
                            if (findIt.value().dosingTypes == "dosingTimes") dosingVariables[variableAssigned] = "";
                            else if (findIt.value().dosingTypes == "doseAmount") dosingVariables[variableAssigned] = "";
                        }
                    }
                }
            }
            int countSM = 0;
            for (int seqi = 0;seqi<structuralEquations.size();seqi++)
            {
                if (structuralEquations.at(seqi).nodeName() == "ct:DerivativeVariable")
                {
                    QDomNodeList derivativeElements = structuralEquations.at(seqi).childNodes();
                    QString odeComponent = parseQDomNodeAttribute(structuralEquations.at(seqi),"symbId");
                    for (int dei = 0;dei < derivativeElements.size();dei++)
                    {
                        if (derivativeElements.at(dei).nodeName() == "ct:Description")
                        {
                            QDomNodeList descNodes = derivativeElements.at(dei).childNodes();
                            if (descNodes.size())
                            {
                                _structuralModelEquations[QString("ddt_")+odeComponent].description = descNodes.at(0).nodeValue();
                            }
                        }
                        else if (derivativeElements.at(dei).nodeName() == "ct:IndependentVariable")
                        {

                        }
                        else if (derivativeElements.at(dei).nodeName() == "ct:InitialCondition")
                        {

                            QDomNodeList initialConditionElements = derivativeElements.at(dei).childNodes();
                            for (int ici = 0;ici < initialConditionElements.size();ici++)
                            {
                                if (initialConditionElements.at(ici).nodeName() == "ct:Assign")
                                {
                                    QDomNodeList initialValue = initialConditionElements.at(ici).childNodes();
                                    QMap<QString,QVector<QString> > blockRef;
                                    QSet<QString> dependentVariables;
                                    QVector<QString> equation = pharmlEquationToMlxtran(PharmMLLoader08::LONGITUDINAL_SECTION,initialConditionElements.at(ici),"",blockRef, dependentVariables, blkIdRefTmp, aliasesTmp);
                                    if (equation.size())
                                    {

                                        QString i1 =  (odeComponent+QString("_0")).trimmed();
                                        QString i2 = equation[0].trimmed();
                                        if (i1 != i2) {
                                            VariableDefinition* vdd = new VariableDefinition;
                                            vdd->equation = odeComponent+QString("_0 = ") + equation[0];
                                            vdd->dependentVariables = dependentVariables;
                                            structural.push_back(vdd);
                                        }
                                    }/*
                                    for (int ivi = 0;ivi < initialValue.size();ivi++)
                                    {
                                        if (initialValue.at(ivi).nodeName() == "ct:Real")
                                        {
                                            QDomNodeList realNode = initialValue.at(ivi).childNodes();
                                            if (realNode.size())
                                            {
                                                VariableDefinition* vd = new VariableDefinition;
                                                vd->equation = odeComponent+QString("_0 = ") + realNode.at(0).nodeValue();
                                                structural.push_back(vd);
                                            }

                                        }
                                        else if (initialValue.at(ivi).nodeName() == "ct:SymbRef")
                                        {
                                            QString derivativeInitialCondSymb = parseQDomNodeAttribute(initialValue.at(ivi),"symbIdRef");
                                            VariableDefinition* vdd = new VariableDefinition;
                                            vdd->equation = odeComponent+QString("_0 = ") + derivativeInitialCondSymb;
                                            vdd->dependentVariables.insert(derivativeInitialCondSymb);
                                            structural.push_back(vdd);

                                        }
                                        else if (initialValue.at(ivi).nodeName() == "Equation" || initialValue.at(ivi).nodeName() == "math:Equation")
                                        {

                                        }

                                    }*/
                                }
                                if (initialConditionElements.at(ici).nodeName() == "ct:InitialValue")
                                {
                                    QDomNodeList initialValue= initialConditionElements.at(ici).childNodes();
                                    for (int ivi = 0;ivi<initialValue.size();ivi++)
                                    {
                                        if (initialValue.at(ivi).nodeName() == "ct:Assign")
                                        {
                                            QMap<QString,QVector<QString> > blockRef;
                                            QSet<QString> dependentVariables;
                                            QVector<QString> equation = pharmlEquationToMlxtran(PharmMLLoader08::LONGITUDINAL_SECTION,initialValue.at(ivi),"",blockRef,dependentVariables, blkIdRefTmp, aliasesTmp);
                                            if (equation.size())
                                            {
                                                QString i1 =  (odeComponent+QString("_0")).trimmed();
                                                QString i2 = equation[0].trimmed();
                                                if (i1 != i2) {
                                                    VariableDefinition* vdd = new VariableDefinition;
                                                    vdd->equation = odeComponent+QString("_0 = ") + equation[0];
                                                    vdd->dependentVariables = dependentVariables;
                                                    structural.push_back(vdd);
                                                }
                                            }
                                            /*QDomNodeList theValue = initialValue.at(ivi).childNodes();
                                            for (int tvi = 0;tvi < theValue.size();tvi++)
                                            {
                                                if (theValue.at(tvi).nodeName() == "ct:Real")
                                                {
                                                    QDomNodeList realNode = theValue.at(tvi).childNodes();
                                                    if (realNode.size())
                                                    {
                                                        VariableDefinition* vdd = new VariableDefinition;
                                                        vdd->equation = odeComponent+QString("_0 = ") + realNode.at(0).nodeValue();
                                                        structural.push_back(vdd);
                                                    }

                                                }
                                                else if (theValue.at(tvi).nodeName() == "ct:SymbRef")
                                                {
                                                    QString derivativeInitialCondSymb = parseQDomNodeAttribute(theValue.at(tvi),"symbIdRef");
                                                    VariableDefinition* vdd = new VariableDefinition;
                                                    vdd->equation = odeComponent+QString("_0 = ") + derivativeInitialCondSymb;
                                                    vdd->dependentVariables.insert(derivativeInitialCondSymb);
                                                    structural.push_back(vdd);
                                                }
                                                else if (theValue.at(tvi).nodeName() == "Equation" || theValue.at(tvi).nodeName() == "math:Equation")
                                                {

                                                }

                                            }*/
                                        }
                                    }
                                }
                            }
                        }
                        else if (derivativeElements.at(dei).nodeName() == "ct:Assign")
                        {

                            QSet<QString> dependentVariables;
                            QVector<QString> equation = pharmlEquationToMlxtran(PharmMLLoader08::LONGITUDINAL_SECTION,derivativeElements.at(dei),"",_structuralModelParameters,dependentVariables, blkIdRefTmp, aliasesTmp);
                            for (int ei = 0;ei<equation.size();ei++)
                            {
                                QString addBolus = "";
                                if (_dosingVariables.contains(odeComponent))
                                {
                                    QMap<QString,PharmMLLoader08::Dosing>::iterator findIt = _dosingVariables.find(odeComponent);
                                    if (findIt.value().dosingTypes == "doseAmount" && (findIt.value().inputTarget == "target" || findIt.value().inputTarget == "derivativeVariable")) // add PK
                                    {
                                        dosingVariables[odeComponent] = "done";
                                        _pkBlock+=(QString("depot(target=") + odeComponent + QString(")\n"));
                                    }
                                }
                                else
                                {
                                    for (QMap<QString,QString>::iterator mapIt = columnMapping.begin();
                                         mapIt!=columnMapping.end();
                                         mapIt++) {
                                        if (mapIt.value() == odeComponent) {
                                            for (QMap<int,QPair<QString,QString> >::const_iterator cit =_dataSet.columns.begin();
                                                 cit != _dataSet.columns.end();
                                                 cit++) {
                                                if (cit.value().first == "DOSE" && cit.value().second==mapIt.key()) {
                                                  _pkBlock+=(QString("depot(target=") + odeComponent + QString(")\n"));
                                                }
                                            }
                                        }
                                    }
                                }
                                QString assignedVariable = QString("ddt_")+odeComponent;
                                VariableDefinition* vdd = new VariableDefinition;
                                vdd->equation = assignedVariable + QString(" = ") + equation[ei] + addBolus;
                                vdd->dependentVariables = dependentVariables;
                                vdd->name = assignedVariable;
                                structural.push_back(vdd);
                            }
                            /*
                            QDomNodeList assignedElements = derivativeElements.at(dei).childNodes();
                            for (int aei = 0;aei<assignedElements.size();aei++)
                            {
                                if (assignedElements.at(aei).nodeName() == "Equation" || assignedElements.at(aei).nodeName() == "math:Equation")
                                {

                                }
                                else if (assignedElements.at(aei).nodeName() == "ct:Real")
                                {
                                    QString readRealValue;
                                    QString assignedVariable = QString("ddt_")+odeComponent;
                                    readRealValue = assignedElements.at(aei).firstChild().firstChild().nodeValue();
                                    QString addBolus = "";
                                    if (_dosingVariables.contains(odeComponent))
                                    {
                                        QMap<QString,PharmMLLoader08::Dosing>::const_iterator findIt = _dosingVariables.find(odeComponent);
                                        if (findIt.value().dosingTypes == "doseAmount" && (findIt.value().inputTarget == "target" || findIt.value().inputTarget == "derivativeVariable"))
                                        {
                                            dosingVariables[odeComponent] = "done";
                                            _pkBlock+=(QString("depot(target=") + odeComponent + QString(")\n"));
                                        }

                                    }
                                    VariableDefinition* vdd = new VariableDefinition;
                                    vdd->equation = assignedVariable + QString(" = ") + readRealValue + addBolus;
                                    vdd->name = assignedVariable;
                                }
                            }*/
                        }
                    }
                }
                else if(structuralEquations.at(seqi).nodeName().contains("Variable")
                        || structuralEquations.at(seqi).nodeName() == "SimpleParameter"
                        || structuralEquations.at(seqi).nodeName() == "Parameter" )
                {
                    QDomNodeList variableElements = structuralEquations.at(seqi).childNodes();
                    QString variableAssigned = parseQDomNodeAttribute(structuralEquations.at(seqi),"symbId");
                    for (int vei = 0;vei < variableElements.size();vei++)
                    {
                        if (variableElements.at(vei).nodeName().contains("Assign"))
                        {
                            QSet<QString> dependentVariables;
                            QVector<QString> equation = pharmlEquationToMlxtran(PharmMLLoader08::LONGITUDINAL_SECTION,variableElements.at(vei),variableAssigned,_structuralModelParameters,dependentVariables,blkIdRefTmp, aliasesTmp);
                            for (int ei = 0;ei<equation.size();ei++)
                            {
                                VariableDefinition* vdd = new VariableDefinition;
                                vdd->equation = /*"#ISASSIGNED"+QString::number(countSM)+variableAssigned + QString(" = ") +*/ equation[ei];
                                structural.push_back(vdd);
                                vdd->dependentVariables = dependentVariables;
                                vdd->name = variableAssigned;
                                countSM++;
                            }
                            /*QDomNodeList assignedElements = variableElements.at(vei).childNodes();
                            for (int aei = 0;aei<assignedElements.size();aei++)
                            {
                                if (assignedElements.at(aei).nodeName() == "Equation"
                                        || assignedElements.at(aei).nodeName() == "math:Equation")
                                {

                                }
                                else if (assignedElements.at(aei).nodeName() == "ct:Real")
                                {
                                    QString readRealValue;
                                    readRealValue = assignedElements.at(aei).firstChild().nodeValue();
                                    VariableDefinition* vdd = new VariableDefinition;
                                    vdd->equation = variableAssigned + QString(" = ") + readRealValue;
                                    vdd->name = variableAssigned;
                                    structural.push_back(vdd);
                                }
                                else if (assignedElements.at(aei).nodeName() == "ct:SymbRef")
                                {
                                    QString readValue;
                                    readValue = parseQDomNodeAttribute(assignedElements.at(aei),"symbIdRef");
                                    VariableDefinition* vdd = new VariableDefinition;
                                    vdd->equation = variableAssigned + QString(" = ") + readValue;
                                    vdd->name = variableAssigned;
                                    structural.push_back(vdd);
                                }
                            }*/
                        }
                    }
                }
            }
            //
            for (QMap<QString,PharmMLLoader08::Dosing>::const_iterator dit = _dosingVariables.begin();
                 dit != _dosingVariables.end();dit++)
            {
                QString dv = dosingVariables[dit.key()];
                if (dv.isEmpty() && (dit.value().inputTarget == "target" || dit.value().inputTarget == "derivativeVariable"))
                {
                    VariableDefinition* vdd = new VariableDefinition;
                    vdd->equation = QString("ddt_")+dit.key() + QString(" = 0");
                    _pkBlock+=(QString("depot(target=") + dit.key() + QString(")\n"));
                }
            }
        }
        QMap<QString,QString> orderedPkMacro;
        int countCompartment=0;
        int countIV=0;
        int countElim=0;
        int countAbsorp=0;
        int countDepot=0;
        int countPeripheral=0;
        int countOral=0;
        for (int pki = 0;pki<pkMacros.size();pki++) {
            QDomNodeList pkMacrosList = pkMacros[pki].siblingNode.childNodes();
            for (int pkListi = 0;pkListi<pkMacrosList.size();pkListi++) {
                if (pkMacrosList.at(pkListi).nodeName() == "Compartment") {
                    QDomNodeList cptArg = pkMacrosList.at(pkListi).childNodes();
                    QString argAmount,argCmt,argVolume,argP;
                    QString argCC;
                    for (int cptArgi = 0;cptArgi<cptArg.size();cptArgi++) {
                        if (cptArg.at(cptArgi).nodeName() == "Value") {
                            QString argument = parseQDomNodeAttribute(cptArg.at(cptArgi),"argument");
                            if (argument == "amount") {
                                if (cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef"))
                                {
                                    argAmount = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                    _macroPKAmount.insert(argAmount);
                                }
                            }
                            if (argument == "concentration") {
                                if (cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef"))
                                {
                                    argCC = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                }
                            }
                            else if (argument == "cmt") {

                                if (cptArg.at(cptArgi).firstChild().nodeName().contains("Int"))
                                {
                                    argCmt = cptArg.at(cptArgi).firstChild().firstChild().nodeValue();
                                }
                            }
                            else if (argument == "volume" || argument == "V") {
                                if (cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef"))
                                {
                                    argVolume = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                    _macroParameters.insert(argVolume);
                                }
                            }
                            else if (argument == "p") {
                                if (cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef"))
                                {
                                    argP = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                    _macroParameters.insert(argP);
                                }
                            }
                         }
                    }
                    if (argCmt.length()==0) {
                        argCmt = "1";
                    }
                    if (argP.length()) {
                        argP = ", p="+argP;
                    }
                    if (argCC.length()) {
                        _macroInputFilter.insert(argCC);
                        argCC = ", concentration="+argCC;
                    }
                    if (argVolume.length()) {
                        if (argAmount.length()) {
                            QString cmtId = "#" + QString::number(countCompartment);
                            orderedPkMacro["compartment"+cmtId]="compartment(amount="+argAmount+", volume="+argVolume+", cmt="+argCmt+argCC+argP+")\n";
                            countCompartment++;
                        }
                        else {
                            QString cmtId = "#" + QString::number(countCompartment);
                            orderedPkMacro["compartment"+cmtId]="compartment(volume="+argVolume+", cmt="+argCmt+argCC+argP+")\n";
                            countCompartment++;
                        }

                    }
                    else {
                      if (argAmount.length()) {
                        QString cmtId = "#" + QString::number(countCompartment);
                        orderedPkMacro["compartment"+cmtId]="compartment(amount="+argAmount+", cmt="+argCmt+argCC+argP+")\n";
                        countCompartment++;
                      }
                      else {
                          QString cmtId = "#" + QString::number(countCompartment);
                          orderedPkMacro["compartment"+cmtId]="compartment(cmt="+argCmt+argCC+argP+")\n";
                          countCompartment++;
                      }
                    }
                }
                else if (pkMacrosList.at(pkListi).nodeName() == "IV") {
                    QDomNodeList cptArg = pkMacrosList.at(pkListi).childNodes();
                    QString argAdm,argCmt,argP="";
                    for (int cptArgi = 0;cptArgi<cptArg.size();cptArgi++) {
                        if (cptArg.at(cptArgi).nodeName() == "Value") {
                            QString argument = parseQDomNodeAttribute(cptArg.at(cptArgi),"argument");
                            if (argument == "adm" || argument=="type") {
                                if (cptArg.at(cptArgi).firstChild().nodeName().contains("Int"))
                                {
                                    argAdm = cptArg.at(cptArgi).firstChild().firstChild().nodeValue();
                                }
                            }
                            else if (argument == "cmt") {
                                if (cptArg.at(cptArgi).firstChild().nodeName().contains("Int"))
                                {
                                    argCmt = cptArg.at(cptArgi).firstChild().firstChild().nodeValue();
                                }
                            }
                            else if (argument == "p") {
                                if (cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef"))
                                {
                                    argP = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                    _macroParameters.insert(argP);
                                }
                            }
                         }
                    }
                    if (argCmt.length() == 0) {
                        argCmt="1";
                    }
                    if (argP.length()) {
                        argP = ", p="+argP;
                    }
                    if (argAdm.length()) {
                        argAdm = ", adm="+argAdm;
                    }
                    QString ivid = "#" + QString::number(countIV++);
                    orderedPkMacro["iv"+ivid]="iv(cmt="+argCmt+argAdm+argP+")\n";
                }
                else if (pkMacrosList.at(pkListi).nodeName() == "Elimination") {
                    QDomNodeList cptArg = pkMacrosList.at(pkListi).childNodes();
                    QString argCmt = "1";
                    QStringList elim;
                    QString param_k,param_V,param_Cl,param_Vm,param_Km;
                    for (int cptArgi = 0;cptArgi<cptArg.size();cptArgi++) {
                        if (cptArg.at(cptArgi).nodeName() == "Value") {
                            QString argument = parseQDomNodeAttribute(cptArg.at(cptArgi),"argument");
                            if (argument.isEmpty()) {
                                if(cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef")) {
                                    QString theElim = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                    elim.push_back(theElim);
                                    _macroParameters.insert(theElim);
                                }
                            }
                            else if (argument.toLower() == "cl") {
                                if(cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef")) {
                                    param_Cl = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                    //elim.push_back("Cl="+theElim);
                                    //_macroParameters.insert(theElim);
                                }
                                else if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                   QDomNodeList aList = cptArg.at(cptArgi).firstChild().childNodes();
                                   for (int ail = 0;ail<aList.size();ail++) {

                                       if (aList.at(ail).nodeName().contains("SymbRef")) {
                                           param_Cl = parseQDomNodeAttribute(aList.at(ail),"symbIdRef");
                                           //elim.push_back("Cl="+thecl);
                                           //_macroParameters.insert(thecl);
                                       }
                                       /*
                                       if (aList.at(ail).nodeName().contains("Equation")) {
                                           QDomNodeList aeList = aList.at(ail).childNodes();
                                           for (int aeil = 0;aeil<aeList.size();aeil++) {

                                           }
                                       }*/
                                   }
                                }
                            }
                            else if (argument.toLower() == "vm") {
                                if(cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef")) {
                                    param_Vm = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                }
                                else if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                   QDomNodeList aList = cptArg.at(cptArgi).firstChild().childNodes();
                                   for (int ail = 0;ail<aList.size();ail++) {

                                       if (aList.at(ail).nodeName().contains("SymbRef")) {
                                           param_Vm = parseQDomNodeAttribute(aList.at(ail),"symbIdRef");
                                       }
                                   }
                                }
                            }
                            else if (argument.toLower() == "km") {
                                if(cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef")) {
                                    param_Km = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                }
                                else if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                   QDomNodeList aList = cptArg.at(cptArgi).firstChild().childNodes();
                                   for (int ail = 0;ail<aList.size();ail++) {

                                       if (aList.at(ail).nodeName().contains("SymbRef")) {
                                           param_Km = parseQDomNodeAttribute(aList.at(ail),"symbIdRef");
                                       }
                                   }
                                }
                            }
                            else if (argument.toLower() == "v" || argument.toLower() == "volume") {
                                if(cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef")) {
                                    param_V = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                   // elim.push_back("V="+theElim);
                                   // _macroParameters.insert(theElim);
                                }
                                else if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                   QDomNodeList aList = cptArg.at(cptArgi).firstChild().childNodes();
                                   for (int ail = 0;ail<aList.size();ail++) {
                                       if (aList.at(ail).nodeName().contains("SymbRef")) {
                                           param_V = parseQDomNodeAttribute(aList.at(ail),"symbIdRef");
                                           //elim.push_back("V="+thev);
                                           //_macroParameters.insert(thev);
                                       }
                                       /*if (aList.at(ail).nodeName().contains("Equation")) {


                                           QDomNodeList aeList = aList.at(ail).childNodes();
                                           for (int aeil = 0;aeil<aeList.size();aeil++) {
                                               if (aeList.at(aeil).nodeName().contains("SymbRef")) {
                                                   param_V = parseQDomNodeAttribute(aeList.at(aeil),"symbIdRef");
                                                   //elim.push_back("V="+thev);
                                                   //_macroParameters.insert(thev);
                                               }
                                           }
                                       }*/
                                   }
                                }
                            }
                            else if (argument.toLower() == "k") {
                                if(cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef")) {
                                    param_k = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                   // elim.push_back("V="+theElim);
                                   // _macroParameters.insert(theElim);
                                }


                                else if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                    QSet<QString> dependentVariables;
                                    QMap<QString,QString> aliasesTmp;
                                    QVector<QString> equation = pharmlEquationToMlxtran(PharmMLLoader08::LONGITUDINAL_SECTION,
                                                                                        cptArg.at(cptArgi).firstChild(),
                                                                                        "",
                                                                                        _structuralModelParameters,
                                                                                        dependentVariables,"", aliasesTmp);
                                    if (equation.size() == 1) {
                                        _macroParameters.unite(dependentVariables);
                                       elim.push_back("k="+equation[0]);
                                    }
                                    /*QDomNodeList aList = cptArg.at(cptArgi).firstChild().childNodes();
                                   for (int ail = 0;ail<aList.size();ail++) {
                                       if (aList.at(ail).nodeName().contains("SymbRef")) {
                                           param_k= parseQDomNodeAttribute(aList.at(ail),"symbIdRef");
                                           //elim.push_back("V="+thev);
                                           //_macroParameters.insert(thev);
                                       }
                                       //
                                       //if (aList.at(ail).nodeName().contains("Equation")) {
                                       //    QDomNodeList aeList = aList.at(ail).childNodes();
                                       //    for (int aeil = 0;aeil<aeList.size();aeil++) {
                                       //    }
                                       // }
                                   }
                                */}
                            }
                            else if (argument == "cmt") {
                                if (cptArg.at(cptArgi).firstChild().nodeName().contains("Int"))
                                {
                                    argCmt = cptArg.at(cptArgi).firstChild().firstChild().nodeValue();
                                }
                                else if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                   QDomNodeList aList = cptArg.at(cptArgi).firstChild().childNodes();
                                   for (int ail = 0;ail<aList.size();ail++) {
                                       if (aList.at(ail).nodeName().contains("Int")) {
                                           argCmt = aList.at(ail).firstChild().nodeValue();
                                       }
                                   /*    if (aList.at(ail).nodeName().contains("Equation")) {
                                           QDomNodeList aeList = aList.at(ail).childNodes();
                                           for (int aeil = 0;aeil<aeList.size();aeil++) {

                                           }
                                       }*/
                                   }
                                }
                            }
                         }
                    }

                    if (param_k.length()) {
                        elim.push_back("k="+param_k);
                        _macroParameters.insert(param_k);
                    }
                    else if (param_Cl.length() && param_V.length()) {
                        elim.push_back("k="+param_Cl + "/"+param_V);
                        _macroParameters.insert(param_V);
                        _macroParameters.insert(param_Cl);
                    }
                    else if (param_Cl.length() && !param_V.length()) {
                        elim.push_back("Cl="+param_Cl);
                        _macroParameters.insert(param_Cl);
                    }
                    if (param_Km.length()) {
                        elim.push_back("Km="+param_Km);
                        _macroParameters.insert(param_Km);
                    }
                    if (param_Vm.length()) {
                        elim.push_back("Vm="+param_Vm);
                        _macroParameters.insert(param_Vm);
                    }

                    if (argCmt.length()==0) {
                        argCmt = "1";
                    }
                    QString elimID = QString("#") + QString::number(countElim++);
                    orderedPkMacro["elimination"+elimID]=("elimination(cmt="+argCmt+", "+elim.join(",")+")\n");
                }
                else if (pkMacrosList.at(pkListi).nodeName() == "Absorption") {
                    QDomNodeList cptArg = pkMacrosList.at(pkListi).childNodes();
                    QString argCmt = "1";
                    QString argAdm = "1";
                    QString bioA="";
                    QStringList elim;
                    for (int cptArgi = 0;cptArgi<cptArg.size();cptArgi++) {
                        if (cptArg.at(cptArgi).nodeName() == "Value") {
                           QString argument = parseQDomNodeAttribute(cptArg.at(cptArgi),"argument");
                           if (argument == "cmt") {
                                if (cptArg.at(cptArgi).firstChild().nodeName().contains("Int"))
                                {
                                    argCmt = cptArg.at(cptArgi).firstChild().firstChild().nodeValue();
                                }
                                else if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                   QDomNodeList aList = cptArg.at(cptArgi).firstChild().childNodes();
                                   for (int ail = 0;ail<aList.size();ail++) {
                                       if (aList.at(ail).nodeName().contains("Int")) {
                                           argCmt = aList.at(ail).firstChild().nodeValue();
                                       }
                                   }
                                   /*
                                   for (int ail = 0;ail<aList.size();ail++) {
                                       if (aList.at(ail).nodeName().contains("Equation")) {
                                           QDomNodeList aeList = aList.at(ail).childNodes();
                                       }
                                   }*/
                                }
                           }
                           if (argument=="adm" || argument=="type")
                           {
                               if (cptArg.at(cptArgi).firstChild().nodeName().contains("Int"))
                               {
                                   argAdm = cptArg.at(cptArgi).firstChild().firstChild().nodeValue();
                               }
                               else if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                  QDomNodeList aList = cptArg.at(cptArgi).firstChild().childNodes();
                                  for (int ail = 0;ail<aList.size();ail++) {
                                      if (aList.at(ail).nodeName().contains("Int")) {
                                          argAdm = aList.at(ail).firstChild().nodeValue();
                                      }
                                      /*
                                      if (aList.at(ail).nodeName().contains("Equation")) {
                                          QDomNodeList aeList = aList.at(ail).childNodes();
                                          for (int aeil = 0;aeil<aeList.size();aeil++) {
                                          }
                                      }*/
                                  }
                               }
                           }
                           else if (argument.toLower() == "tlag") {
                                if(cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef")) {
                                    QString theTlag = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                    elim.push_back("Tlag="+theTlag);
                                    _macroParameters.insert(theTlag);
                                }
                                else if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                   QDomNodeList aList = cptArg.at(cptArgi).firstChild().childNodes();
                                   for (int ail = 0;ail<aList.size();ail++) {
                                       if (aList.at(ail).nodeName().contains("SymbRef")) {
                                           QString theTlag = parseQDomNodeAttribute(aList.at(ail),"symbIdRef");
                                           elim.push_back("Tlag="+theTlag);
                                           _macroParameters.insert(theTlag);
                                       }
                                       /*
                                       if (aList.at(ail).nodeName().contains("Equation")) {
                                           QDomNodeList aeList = aList.at(ail).childNodes();
                                           for (int aeil = 0;aeil<aeList.size();aeil++) {

                                           }
                                       }*/
                                   }
                                }
                           }
                           else if (argument == "KA" ||argument=="ka") {
                                if(cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef")) {
                                    QString theElim = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                    elim.push_back("ka="+theElim);
                                    _macroParameters.insert(theElim);
                                }
                                else if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                   QDomNodeList aList = cptArg.at(cptArgi).firstChild().childNodes();
                                   for (int ail = 0;ail<aList.size();ail++) {
                                       if (aList.at(ail).nodeName().contains("SymbRef")) {
                                           QString theKa = parseQDomNodeAttribute(aList.at(ail),"symbIdRef");
                                           elim.push_back("ka="+theKa);
                                           _macroParameters.insert(theKa);
                                       }
                                       /*
                                       if (aList.at(ail).nodeName().contains("Equation")) {
                                           QDomNodeList aeList = aList.at(ail).childNodes();
                                           for (int aeil = 0;aeil<aeList.size();aeil++) {
                                           }
                                       }*/
                                   }
                                }
                           }
                           else if (argument == "TK0" ||argument=="Tk0") {
                                 if(cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef")) {
                                     QString theElim = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                     elim.push_back("Tk0="+theElim);
                                     _macroParameters.insert(theElim);
                                 }
                                 else if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                    QDomNodeList aList = cptArg.at(cptArgi).firstChild().childNodes();
                                    for (int ail = 0;ail<aList.size();ail++) {
                                        if (aList.at(ail).nodeName().contains("SymbRef")) {
                                            QString theTk0 = parseQDomNodeAttribute(aList.at(ail),"symbIdRef");
                                            elim.push_back("Tk0="+theTk0);
                                            _macroParameters.insert(theTk0);
                                        }
                                        /*
                                        if (aList.at(ail).nodeName().contains("Equation")) {
                                            QDomNodeList aeList = aList.at(ail).childNodes();
                                            for (int aeil = 0;aeil<aeList.size();aeil++) {
                                            }
                                        }*/
                                    }
                                 }
                           }
                           else if (argument.toLower() == "ktr") {
                                 if(cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef")) {
                                     QString theElim = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                     elim.push_back("Ktr="+theElim);
                                     _macroParameters.insert(theElim);
                                 }
                                 else if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                    QDomNodeList aList = cptArg.at(cptArgi).firstChild().childNodes();
                                    for (int ail = 0;ail<aList.size();ail++) {
                                        if (aList.at(ail).nodeName().contains("SymbRef")) {
                                            QString theKtr = parseQDomNodeAttribute(aList.at(ail),"symbIdRef");
                                            elim.push_back("Ktr="+theKtr);
                                            _macroParameters.insert(theKtr);
                                        }
                                        /*
                                        if (aList.at(ail).nodeName().contains("Equation")) {
                                            QDomNodeList aeList = aList.at(ail).childNodes();
                                            for (int aeil = 0;aeil<aeList.size();aeil++) {
                                            }
                                        }*/
                                    }
                                 }
                           }
                           else if (argument.toLower() == "mtt") {
                                 if(cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef")) {
                                     QString theElim = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                     elim.push_back("Mtt="+theElim);
                                     _macroParameters.insert(theElim);
                                 }
                                 else if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                    QDomNodeList aList = cptArg.at(cptArgi).firstChild().childNodes();
                                    for (int ail = 0;ail<aList.size();ail++) {
                                        if (aList.at(ail).nodeName().contains("SymbRef")) {
                                            QString theMtt = parseQDomNodeAttribute(aList.at(ail),"symbIdRef");
                                            elim.push_back("Mtt="+theMtt);
                                            _macroParameters.insert(theMtt);
                                        }
                                        /*
                                        if (aList.at(ail).nodeName().contains("Equation")) {
                                            QDomNodeList aeList = aList.at(ail).childNodes();
                                            for (int aeil = 0;aeil<aeList.size();aeil++) {
                                            }
                                        }*/
                                    }
                                 }
                           }
                           else if (argument == "p" ||argument=="P") {
                                  if(cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef")) {
                                      QString theBioA = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                      bioA = ",p="+theBioA;
                                      _macroParameters.insert(theBioA);
                                  }
                                  else if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                     QDomNodeList aList = cptArg.at(cptArgi).firstChild().childNodes();
                                     for (int ail = 0;ail<aList.size();ail++) {
                                         if (aList.at(ail).nodeName().contains("SymbRef")) {
                                             QString thep = parseQDomNodeAttribute(aList.at(ail),"symbIdRef");
                                             elim.push_back("p="+thep);
                                             _macroParameters.insert(thep);
                                         }
                                         /*
                                         if (aList.at(ail).nodeName().contains("Equation")) {
                                             QDomNodeList aeList = aList.at(ail).childNodes();
                                             for (int aeil = 0;aeil<aeList.size();aeil++) {

                                             }
                                         }*/
                                     }
                                  }
                            }
                      }
                  }
                  QString absID = "#"+QString::number(countAbsorp++);
                  if (argCmt.length() && argAdm.length()){

                        orderedPkMacro["absorption"+absID]=("absorption(cmt="+argCmt+",adm="+argAdm+bioA+", "+elim.join(",")+")\n");
                  }
                  else
                  {
                       orderedPkMacro["absorption"+absID]=("absorption("+elim.join(",")+")\n");
                  }
                }

                else if (pkMacrosList.at(pkListi).nodeName() == "Depot") {
                    QDomNodeList cptArg = pkMacrosList.at(pkListi).childNodes();
                    QString argCmt = "1";
                    QString argAdm = "1";
                    QString bioA="";
                    QStringList elim;
                    for (int cptArgi = 0;cptArgi<cptArg.size();cptArgi++) {
                        if (cptArg.at(cptArgi).nodeName() == "Value") {
                           QString argument = parseQDomNodeAttribute(cptArg.at(cptArgi),"argument");
                           if (argument == "cmt") {
                                if (cptArg.at(cptArgi).firstChild().nodeName().contains("Int"))
                                {
                                    argCmt = cptArg.at(cptArgi).firstChild().firstChild().nodeValue();
                                }
                                else if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                   QDomNodeList aList = cptArg.at(cptArgi).firstChild().childNodes();
                                   for (int ail = 0;ail<aList.size();ail++) {
                                       if (aList.at(ail).nodeName().contains("Int")) {
                                           argCmt = aList.at(ail).firstChild().nodeValue();
                                       }
                                   }
                                }
                           }
                           if (argument=="adm" || argument=="type")
                           {
                               if (cptArg.at(cptArgi).firstChild().nodeName().contains("Int"))
                               {
                                   argAdm = cptArg.at(cptArgi).firstChild().firstChild().nodeValue();
                               }
                               else if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                  QDomNodeList aList = cptArg.at(cptArgi).firstChild().childNodes();
                                  for (int ail = 0;ail<aList.size();ail++) {
                                      if (aList.at(ail).nodeName().contains("Int")) {
                                          argAdm = aList.at(ail).firstChild().nodeValue();
                                      }
                                  }
                               }
                           }
                           else if (argument.toLower() == "tlag") {
                                if(cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef")) {
                                    QString theTlag = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                    elim.push_back("Tlag="+theTlag);
                                    _macroParameters.insert(theTlag);
                                }
                                else if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                   QDomNodeList aList = cptArg.at(cptArgi).firstChild().childNodes();
                                   for (int ail = 0;ail<aList.size();ail++) {
                                       if (aList.at(ail).nodeName().contains("SymbRef")) {
                                           QString theTlag = parseQDomNodeAttribute(aList.at(ail),"symbIdRef");
                                           elim.push_back("Tlag="+theTlag);
                                           _macroParameters.insert(theTlag);
                                       }
                                   }
                                }
                           }
                           else if (argument == "KA" ||argument=="ka") {
                                if(cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef")) {
                                    QString theElim = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                    elim.push_back("ka="+theElim);
                                    _macroParameters.insert(theElim);
                                }
                                else if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                   QDomNodeList aList = cptArg.at(cptArgi).firstChild().childNodes();
                                   for (int ail = 0;ail<aList.size();ail++) {
                                       if (aList.at(ail).nodeName().contains("SymbRef")) {
                                           QString theKa = parseQDomNodeAttribute(aList.at(ail),"symbIdRef");
                                           elim.push_back("ka="+theKa);
                                           _macroParameters.insert(theKa);
                                       }
                                   }
                                }
                           }
                           else if (argument == "TK0" ||argument=="Tk0") {
                                 if(cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef")) {
                                     QString theElim = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                     elim.push_back("Tk0="+theElim);
                                     _macroParameters.insert(theElim);
                                 }
                                 else if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                    QDomNodeList aList = cptArg.at(cptArgi).firstChild().childNodes();
                                    for (int ail = 0;ail<aList.size();ail++) {
                                        if (aList.at(ail).nodeName().contains("SymbRef")) {
                                            QString theTk0 = parseQDomNodeAttribute(aList.at(ail),"symbIdRef");
                                            elim.push_back("Tk0="+theTk0);
                                            _macroParameters.insert(theTk0);
                                        }
                                        /*
                                        if (aList.at(ail).nodeName().contains("Equation")) {
                                            QDomNodeList aeList = aList.at(ail).childNodes();
                                            for (int aeil = 0;aeil<aeList.size();aeil++) {
                                            }
                                        }*/
                                    }
                                 }
                           }
                           else if (argument.toLower() == "ktr") {
                                 if(cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef")) {
                                     QString theElim = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                     elim.push_back("Ktr="+theElim);
                                     _macroParameters.insert(theElim);
                                 }
                                 else if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                    QDomNodeList aList = cptArg.at(cptArgi).firstChild().childNodes();
                                    for (int ail = 0;ail<aList.size();ail++) {
                                        if (aList.at(ail).nodeName().contains("SymbRef")) {
                                            QString theKtr = parseQDomNodeAttribute(aList.at(ail),"symbIdRef");
                                            elim.push_back("Ktr="+theKtr);
                                            _macroParameters.insert(theKtr);
                                        }
                                        /*
                                        if (aList.at(ail).nodeName().contains("Equation")) {
                                            QDomNodeList aeList = aList.at(ail).childNodes();
                                            for (int aeil = 0;aeil<aeList.size();aeil++) {
                                            }
                                        }*/
                                    }
                                 }
                           }
                           else if (argument.toLower() == "mtt") {
                                 if(cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef")) {
                                     QString theElim = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                     elim.push_back("Mtt="+theElim);
                                     _macroParameters.insert(theElim);
                                 }
                                 else if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                    QDomNodeList aList = cptArg.at(cptArgi).firstChild().childNodes();
                                    for (int ail = 0;ail<aList.size();ail++) {
                                        if (aList.at(ail).nodeName().contains("SymbRef")) {
                                            QString theMtt = parseQDomNodeAttribute(aList.at(ail),"symbIdRef");
                                            elim.push_back("Mtt="+theMtt);
                                            _macroParameters.insert(theMtt);
                                        }
                                    }
                                 }
                           }
                           else if (argument.toLower() == "target") {
                                 if(cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef")) {
                                     QString thetgt = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                     elim.push_back("target="+thetgt);
                                     _macroParameters.insert(thetgt);
                                 }
                                 else if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                    QDomNodeList aList = cptArg.at(cptArgi).firstChild().childNodes();
                                    for (int ail = 0;ail<aList.size();ail++) {
                                        if (aList.at(ail).nodeName().contains("SymbRef")) {
                                            QString thetgt = parseQDomNodeAttribute(aList.at(ail),"symbIdRef");
                                            elim.push_back("target="+thetgt);
                                            _macroParameters.insert(thetgt);
                                        }
                                    }
                                 }
                           }
                           else if (argument.toLower() == "p") {
                                  if(cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef")) {
                                      QString theBioA = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                      bioA = ",p="+theBioA;
                                      _macroParameters.insert(theBioA);
                                  }
                                  else if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                     QDomNodeList aList = cptArg.at(cptArgi).firstChild().childNodes();
                                     for (int ail = 0;ail<aList.size();ail++) {
                                         if (aList.at(ail).nodeName().contains("SymbRef")) {
                                             QString thep = parseQDomNodeAttribute(aList.at(ail),"symbIdRef");
                                             elim.push_back("p="+thep);
                                             _macroParameters.insert(thep);
                                         }
                                         /*
                                         if (aList.at(ail).nodeName().contains("Equation")) {
                                             QDomNodeList aeList = aList.at(ail).childNodes();
                                             for (int aeil = 0;aeil<aeList.size();aeil++) {

                                             }
                                         }*/
                                     }
                                  }
                            }
                      }
                  }
                  QString dptId = "#"+QString::number(countDepot++);
                  if (argCmt.length() && argAdm.length()){
                        orderedPkMacro["depot"+dptId]+=("depot(adm="+argAdm+bioA+", "+elim.join(",")+")\n");
                  }
                  else
                  {
                       orderedPkMacro["depot"+dptId]+=("depot("+elim.join(",")+")\n");
                  }
                }


                else if (pkMacrosList.at(pkListi).nodeName() == "Peripheral") {
                    QDomNodeList cptArg = pkMacrosList.at(pkListi).childNodes();
                    QString argAmount;
                    QStringList periph;
                    for (int cptArgi = 0;cptArgi<cptArg.size();cptArgi++) {
                        if (cptArg.at(cptArgi).nodeName() == "Value") {
                            QString argument = parseQDomNodeAttribute(cptArg.at(cptArgi),"argument");
                            if (argument.isEmpty()) {
                                if(cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef")) {
                                    QString thePeriph = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                    periph.push_back(thePeriph);
                                    //_macroParameters.insert(thePeriph);
                                }
                            }
                            else if (argument == "amount") {
                                if (cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef"))
                                {
                                    argAmount = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                    //_macroParameters.insert(argAmount);
                                }
                            }
                            else if (argument.toLower()=="k_1_2" || argument.toLower()=="k12") {
                               if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                   QSet<QString> dependentVariables;
                                   QMap<QString,QString> aliasesTmp;
                                   QVector<QString> equation = pharmlEquationToMlxtran(PharmMLLoader08::LONGITUDINAL_SECTION,
                                                                                       cptArg.at(cptArgi).firstChild(),
                                                                                       "",
                                                                                       _structuralModelParameters,
                                                                                       dependentVariables,"", aliasesTmp);
                                   if (equation.size() == 1) {
                                       _macroParameters.unite(dependentVariables);
                                       periph.push_back("k12="+equation[0]);
                                   }
                               }


                            }
                            else if(argument.toLower()=="k_2_1" || argument.toLower()=="k21") {
                                if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                    QSet<QString> dependentVariables;
                                    QMap<QString,QString> aliasesTmp;
                                    QVector<QString> equation = pharmlEquationToMlxtran(PharmMLLoader08::LONGITUDINAL_SECTION,
                                                                                        cptArg.at(cptArgi).firstChild(),
                                                                                        "",
                                                                                        _structuralModelParameters,
                                                                                        dependentVariables,"", aliasesTmp);
                                    if (equation.size() == 1) {
                                        _macroParameters.unite(dependentVariables);
                                        periph.push_back("k21="+equation[0]);
                                    }
                                }

                            }
                            else if (argument.toLower()=="k_2_3" || argument.toLower()=="k23") {
                                if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                    QSet<QString> dependentVariables;
                                    QMap<QString,QString> aliasesTmp;
                                    QVector<QString> equation = pharmlEquationToMlxtran(PharmMLLoader08::LONGITUDINAL_SECTION,
                                                                                        cptArg.at(cptArgi).firstChild(),
                                                                                        "",
                                                                                        _structuralModelParameters,
                                                                                        dependentVariables,"", aliasesTmp);
                                    if (equation.size() == 1) {
                                        _macroParameters.unite(dependentVariables);
                                        periph.push_back("k23="+equation[0]);
                                    }
                               }

                            }
                            else if(argument.toLower()=="k_3_2" || argument.toLower()=="k32") {
                                if (cptArg.at(cptArgi).firstChild().nodeName().contains("Assign")) {
                                    QSet<QString> dependentVariables;
                                    QMap<QString,QString> aliasesTmp;
                                    QVector<QString> equation = pharmlEquationToMlxtran(PharmMLLoader08::LONGITUDINAL_SECTION,
                                                                                        cptArg.at(cptArgi).firstChild(),
                                                                                        "",
                                                                                        _structuralModelParameters,
                                                                                        dependentVariables,"", aliasesTmp);
                                    if (equation.size() == 1) {
                                        _macroParameters.unite(dependentVariables);
                                        periph.push_back("k32="+equation[0]);
                                    }

                                }

                            }
                         }
                    }
                    QString periphId = "#"+QString::number(countPeripheral++);
                    if (argAmount.length()) {
                        orderedPkMacro["peripheral"+periphId]+=("peripheral(amount="+argAmount+", "+periph.join(",")+")\n");
                    }
                    else {
                        orderedPkMacro["peripheral"+periphId]+=("peripheral("+periph.join(",")+")\n");
                    }
                }
                else if (pkMacrosList.at(pkListi).nodeName() == "Oral") {
                    QDomNodeList cptArg = pkMacrosList.at(pkListi).childNodes();
                    QString argAdm,argCmt;
                    QStringList oralArgs;
                    for (int cptArgi = 0;cptArgi<cptArg.size();cptArgi++) {
                        if (cptArg.at(cptArgi).nodeName() == "Value") {
                            QString argument = parseQDomNodeAttribute(cptArg.at(cptArgi),"argument");
                            if (argument == "adm" || argument=="type") {
                                if (cptArg.at(cptArgi).firstChild().nodeName().contains("Int"))
                                {
                                    argAdm = cptArg.at(cptArgi).firstChild().firstChild().nodeValue();
                                }
                            }
                            else if (argument == "cmt") {
                                if (cptArg.at(cptArgi).firstChild().nodeName().contains("Int"))
                                {
                                    argCmt = cptArg.at(cptArgi).firstChild().firstChild().nodeValue();
                                }
                            }
                            if (argument.isEmpty()) {
                                if(cptArg.at(cptArgi).firstChild().nodeName().contains("SymbRef")) {
                                    QString theOral = parseQDomNodeAttribute(cptArg.at(cptArgi).firstChild(),"symbIdRef");
                                    oralArgs.push_back(theOral);
                                    _macroParameters.insert(theOral);
                                }
                            }
                         }
                    }
                    QString oralID = "#"+QString::number(countOral++);
                    orderedPkMacro["oral"+oralID]=("oral(adm="+argAdm+", type="+argCmt+", "+oralArgs.join(",")+")\n");
                }
            }
        }
        QMap<QString,QString>::iterator findIt;
        for (int c_i = 0;c_i<countDepot;c_i++) {
            QString countId = "#" + QString::number(c_i);
            if ((findIt = orderedPkMacro.find("depot"+countId)) != orderedPkMacro.end()) {
                _pkBlock +=  findIt.value();
            }
        }
        for (int c_i = 0;c_i<countCompartment;c_i++) {
            QString countId = "#" + QString::number(c_i);
            if ((findIt = orderedPkMacro.find("compartment"+countId)) != orderedPkMacro.end()) {
                _pkBlock +=  findIt.value();
            }
        }
        for (int c_i = 0;c_i<countAbsorp;c_i++) {
            QString countId = "#" + QString::number(c_i);
            if ((findIt = orderedPkMacro.find("absorption"+countId)) != orderedPkMacro.end()) {
                _pkBlock +=  findIt.value();
            }
        }
        for (int c_i = 0;c_i<countOral;c_i++) {
            QString countId = "#" + QString::number(c_i);
            if ((findIt = orderedPkMacro.find("oral"+countId)) != orderedPkMacro.end()) {
                _pkBlock +=  findIt.value();
            }
        }
        for (int c_i = 0;c_i<countIV;c_i++) {
            QString countId = "#" + QString::number(c_i);
            if ((findIt = orderedPkMacro.find("iv"+countId)) != orderedPkMacro.end()) {
                _pkBlock +=  findIt.value();
            }
        }
        for (int c_i = 0;c_i<countPeripheral;c_i++) {
            QString countId = "#" + QString::number(c_i);
            if ((findIt = orderedPkMacro.find("peripheral"+countId)) != orderedPkMacro.end()) {
                _pkBlock +=  findIt.value();
            }
        }
        for (int c_i = 0;c_i<countElim;c_i++) {
            QString countId = "#" + QString::number(c_i);
            if ((findIt = orderedPkMacro.find("elimination"+countId)) != orderedPkMacro.end()) {
                _pkBlock +=  findIt.value();
            }
        }

    }
    /*if (_pkBlock.length())
    {
       VariableDefinition* vdd = new VariableDefinition;
       vdd->equation = QString("PK:\n") + _pkBlock;
       structural.push_front(vdd);
    }*/

}

void PharmMLLoader08::getObservationModel(QDomElement& root,QVector<VariableDefinition*>& observations,QSet<QString> & globalParameterRegistered)
{
    QSet<QString> simpleParameterRegistered = globalParameterRegistered;
    for (int ri = 0;ri<root.childNodes().size();ri++)
    {
        QVector<PharmMLLoader08::LNodeSibling> nodeObsModel;
        QStringList blkRefsObsModel;
        traverseTo(nodeObsModel,
                   blkRefsObsModel,
                   root.childNodes().at(ri),
                   QStringList()<<"ModelDefinition"<<"ObservationModel");

        for (int ci = 0;ci<nodeObsModel.size();ci++)
        {
            QDomNodeList observationModelNodes = nodeObsModel[ci].siblingNode.childNodes();
            QString observationModelRef = parseQDomNodeAttribute(nodeObsModel[ci].siblingNode,"blkId");
            QString blkIdRefTmp;
            for (int oi = 0;oi<observationModelNodes.size();oi++)
            {
                if (observationModelNodes.at(oi).nodeName() == "SimpleParameter" || observationModelNodes.at(oi).nodeName() == "Parameter")
                {
                    QString simpleParam = parseQDomNodeAttribute(observationModelNodes.at(oi),"symbId");
                    if (simpleParam.length())
                    {
                        simpleParameterRegistered.insert(simpleParam);
                    }
                }
                else if (observationModelNodes.at(oi).nodeName() == "RandomVariable")
                {
                    RandomVariableDefinition* rvd = new RandomVariableDefinition;
                    getRandomVariable(observationModelNodes.at(oi),*rvd);
                    observations.push_back(rvd);
                }
                else if (observationModelNodes.at(oi).nodeName() == "General" )
                {
                    isMonolixCompliant = false;
                    addMessage(LixoftObject::WARNING,
                                QString("[Observation model]: General error model is not managed by Monolix. Section '")
                                + observationModelNodes.at(oi).nodeName() + QString("' at line #")
                                + QString::number(observationModelNodes.at(oi).lineNumber())
                               );
                    QString obsModelName = parseQDomNodeAttribute(observationModelNodes.at(oi),"symbId");
                    QMap<QString,QString> aliasesTmp;
                    QDomNodeList assignList = observationModelNodes.at(oi).childNodes();
                    for (int assi = 0;assi<assignList.size();assi++)
                    {
                        if (assignList.at(assi).nodeName() == "ct:Assign")
                        {
                            QDomNodeList assignedElements = assignList.at(assi).childNodes();
                            for (int aei = 0;aei<assignedElements.size();aei++)
                            {
                                if (assignedElements.at(aei).nodeName() == "Equation"
                                        || assignedElements.at(aei).nodeName() == "math:Equation")
                                {
                                    QSet<QString> dependentVariables;
                                    QVector<QString> equation = pharmlEquationToMlxtran(PharmMLLoader08::LONGITUDINAL_SECTION,
                                                                                        assignedElements.at(aei),
                                                                                        obsModelName,
                                                                                        _structuralModelParameters,
                                                                                        dependentVariables,blkIdRefTmp, aliasesTmp);
                                    for (int ei = 0;ei<equation.size();ei++)
                                    {
                                        VariableDefinition* vd = new VariableDefinition;
                                        vd->equation = equation[ei];
                                        vd->dependentVariables = dependentVariables;
                                        observations.push_back(vd);

                                    }
                                }
                                else if (assignedElements.at(aei).nodeName() == "ct:Real")
                                {
                                    QString readRealValue;
                                    readRealValue = assignedElements.at(aei).firstChild().nodeValue();
                                    VariableDefinition *vd = new VariableDefinition;
                                    vd->equation = obsModelName+QString(" = ") + readRealValue;
                                    observations.push_back(vd);
                                }
                            }
                        }
                    }
                }
                else if (observationModelNodes.at(oi).nodeName() == "Standard" || observationModelNodes.at(oi).nodeName() == "ContinuousData")
                {
                    QDomNodeList standadObsNodesRoot = observationModelNodes.at(oi).childNodes();
                    QVector<QString> errorModelParameters;
                    QString pharmMLErrorModel;
                    QString outputVariable;
                    for  (int soir = 0;soir<standadObsNodesRoot.size();soir++)
                    {
                        if (standadObsNodesRoot.at(soir).nodeName() == "Standard")
                        {
                            QDomNodeList standadObsNodes = standadObsNodesRoot.at(soir).childNodes();
                            QString obsModelName = parseQDomNodeAttribute(standadObsNodesRoot.at(soir),"symbId");

                            for (int soi = 0;soi<standadObsNodes.size();soi++)
                            {
                                if (standadObsNodes.at(soi).nodeName() == "Output")
                                {
                                    QDomNodeList outputNodes = standadObsNodes.at(soi).childNodes();
                                    for (int outi = 0;outi<outputNodes.size();outi++)
                                    {
                                        if (outputNodes.at(outi).nodeName() == "ct:SymbRef")
                                        {
                                            outputVariable = parseQDomNodeAttribute(outputNodes.at(outi),"symbIdRef");
                                        }
                                    }
                                }
                                else if (standadObsNodes.at(soi).nodeName() == "ErrorModel")
                                {
                                    QDomNodeList errorModel = standadObsNodes.at(soi).childNodes();
                                    QVector<QString> fctArgs;
                                    fctArgs.push_back("a");
                                    fctArgs.push_back("b");
                                    fctArgs.push_back("c");
                                    int countArguments=0;
                                    for (int emi = 0;emi<errorModel.size();emi++)
                                    {
                                        if (errorModel.at(emi).nodeName() == "ct:Assign")
                                        {
                                            QDomNodeList errorModelEq = errorModel.at(emi).childNodes();
                                            //for (int emai = 0;emai<errorModelAssign.size();emai++)
                                            {
                                                //QDomNodeList errorModelEq = errorModelAssign.at(emai).childNodes();
                                                for (int emei = 0;emei<errorModelEq.size();emei++)
                                                {
                                                    if (errorModelEq.at(emei).nodeName().contains("FunctionCall"))
                                                    {
                                                        QDomNodeList errorModelFunCall = errorModelEq.at(emei).childNodes();
                                                        for (int emfci = 0;emfci<errorModelFunCall.size();emfci++)
                                                        {
                                                            if (errorModelFunCall.at(emfci).nodeName()=="ct:SymbRef")
                                                            {
                                                                pharmMLErrorModel = parseQDomNodeAttribute(errorModelFunCall.at(emfci),"symbIdRef");
                                                            }
                                                            else if (errorModelFunCall.at(emfci).nodeName() == "math:FunctionArgument"
                                                                     || errorModelFunCall.at(emfci).nodeName() == "FunctionArgument")
                                                            {
                                                                QString outputArgument = parseQDomNodeAttribute(errorModelFunCall.at(emfci),"symbId");
                                                                QDomNodeList functionArguments = errorModelFunCall.at(emfci).childNodes();
                                                                QString blockRef;

                                                                for (int fai = 0;fai<functionArguments.size();fai++)
                                                                {
                                                                    blockRef = parseQDomNodeAttribute(functionArguments.at(fai),"blkIdRef");
                                                                    if (functionArguments.at(fai).nodeName()=="ct:SymbRef"
                                                                          || functionArguments.at(fai).nodeName()=="SymbRef")
                                                                    {

                                                                        if (countArguments<fctArgs.size())
                                                                        {
                                                                          QString pharmMLParam = parseQDomNodeAttribute(functionArguments.at(fai),"symbIdRef");
                                                                          if (pharmMLParam.length() == 0) pharmMLParam == outputArgument;
                                                                          if (pharmMLParam == outputArgument) // "f" suppression
                                                                          {
                                                                              errorModelParameterMapping[obsModelName].push_back(QPair<QString,QString>(pharmMLParam,fctArgs[countArguments]));
                                                                              errorModelParameters.push_back(fctArgs[countArguments]);
                                                                              pharmMLNamesMapping[obsModelName+QString(":errorParameter:")+fctArgs[countArguments]] = pharmMLParam;
                                                                              countArguments++;
                                                                          }
                                                                          else if (simpleParameterRegistered.contains(pharmMLParam))
                                                                          {
                                                                              errorModelParameterMapping[obsModelName].push_back(QPair<QString,QString>(pharmMLParam,fctArgs[countArguments]));
                                                                              errorModelParameters.push_back(pharmMLParam);
                                                                              pharmMLNamesMapping[obsModelName+QString(":errorParameter:")+fctArgs[countArguments]] = pharmMLParam;
                                                                              countArguments++;
                                                                          }
                                                                          else if (outputArgument == "additive") {
                                                                              errorModelParameterMapping[obsModelName].push_back(QPair<QString,QString>(pharmMLParam,fctArgs[0]));
                                                                              errorModelParameters.push_back(fctArgs[0]);
                                                                              pharmMLNamesMapping[obsModelName+QString(":errorParameter:")+fctArgs[0]] = pharmMLParam;
                                                                              countArguments++;
                                                                          }
                                                                          else if (outputArgument == "proportional") {
                                                                              errorModelParameterMapping[obsModelName].push_back(QPair<QString,QString>(pharmMLParam,fctArgs[1]));
                                                                              errorModelParameters.push_back(fctArgs[1]);
                                                                              pharmMLNamesMapping[obsModelName+QString(":errorParameter:")+fctArgs[1]] = pharmMLParam;
                                                                              countArguments++;
                                                                          }
                                                                          else if (outputArgument == "exponential") {
                                                                              errorModelParameterMapping[obsModelName].push_back(QPair<QString,QString>(pharmMLParam,fctArgs[2]));
                                                                              errorModelParameters.push_back(fctArgs[2]);
                                                                              pharmMLNamesMapping[obsModelName+QString(":errorParameter:")+fctArgs[2]] = pharmMLParam;
                                                                              countArguments++;
                                                                          }
                                                                        }
                                                                        else
                                                                        {
                                                                          throwLater(lixoft::exception::Exception("PharmMLLoader08::getObservation",
                                                                                                                  "To many argument into errorModel",THROW_LINE_FILE));
                                                                        }
                                                                    }
                                                                }
                                                                //if (blockRef.isEmpty())
                                                                //    errorModelParameters.push_back(outputArgument);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else if (standadObsNodes.at(soi).nodeName() == "ResidualError")
                                {

                                }
                                _errorModel[observationModelRef+"::"+outputVariable] = pharmMLErrorModel;
                            }
                            PharmMLLoader08::StatisticalDefinition* sd = new PharmMLLoader08::StatisticalDefinition;
                            sd->errorFunction = true;
                            sd->omegas.push_back(pharmMLErrorModel);
                            sd->name = obsModelName;
                            sd->population = outputVariable;
                            sd->errorParameters = errorModelParameters;
                            for (int epi = 0;epi<errorModelParameters.size();epi++)
                                sd->dependentVariables.insert(errorModelParameters[epi]);
                            observations.push_back(sd);
                        }
                    }

                }
                else if (observationModelNodes.at(oi).nodeName() == "Discrete")
                {
                    Categorical& categoricalModel = _discreteModels[observationModelRef];
                    QDomNodeList discreteModelsNodes = observationModelNodes.at(oi).childNodes();
                    for (int dmi = 0;dmi<discreteModelsNodes.size();dmi++)
                    {
                        if (discreteModelsNodes.at(dmi).nodeName() == "CategoricalData")
                        {
                            QDomNodeList categoricalDataList = discreteModelsNodes.at(dmi).childNodes();
                            QVector<QString> categoricalM;
                            for (int cdi = 0;cdi<categoricalDataList.size();cdi++)
                            {
                                if (categoricalDataList.at(cdi).nodeName() == "ListOfCategories")
                                {
                                    QDomNodeList listOfCategories = categoricalDataList.at(cdi).childNodes();
                                    for (int loci = 0;loci<listOfCategories.size();loci++)
                                    {
                                        if (listOfCategories.at(loci).nodeName()=="Category")
                                        {
                                            QString category = parseQDomNodeAttribute(listOfCategories.at(loci),"symbId");
                                            if (!categoricalModel.modalitiesAlias.contains(category))
                                            {
                                                categoricalModel.modalitiesAlias[category] = category;
                                                categoricalM.push_back(category);
                                            }
                                        }
                                    }
                                }
                                else if (categoricalDataList.at(cdi).nodeName() == "SimpleParameter" || categoricalDataList.at(cdi).nodeName() == "Parameter")
                                {
                                    QString simpleParameter = parseQDomNodeAttribute(categoricalDataList.at(cdi),"symbId");
                                    categoricalModel.simpleParameters.insert(simpleParameter);
                                }
                                else if (categoricalDataList.at(cdi).nodeName() == "CategoryVariable")
                                {
                                    QString categoryVariable = parseQDomNodeAttribute(categoricalDataList.at(cdi),"symbId");
                                    categoricalModel.categoryVariable = categoryVariable;
                                }
                                else if (categoricalDataList.at(cdi).nodeName().contains("PMF")
                                         && categoricalDataList.at(cdi).firstChild().nodeName() == "Distribution") {
                                        QDomNodeList pmfList = categoricalDataList.at(cdi).firstChild().childNodes();
                                        QString dist;
                                        QVector<QString> params;
                                        for (int pmfi = 0;pmfi<pmfList.count();pmfi++) {
                                            if (pmfList.at(pmfi).nodeName()== "ProbOnto") {
                                             dist = parseQDomNodeAttribute(pmfList.at(pmfi),"name");
                                             if (dist.toLower() == "bernoulli1") {
                                                 QDomNodeList poissonParameterList = pmfList.at(pmfi).childNodes();
                                                 for (int ppi = 0;ppi<poissonParameterList.count();ppi++) {
                                                   if (poissonParameterList.at(ppi).nodeName() == "Parameter") {
                                                       QString name=parseQDomNodeAttribute(poissonParameterList.at(ppi),"name");
                                                       QDomNodeList rateList = poissonParameterList.at(ppi).childNodes();
                                                       for (int ri = 0;ri<rateList.count();ri++) {
                                                           QString nn = rateList.at(ri).nodeName();
                                                           if (nn.contains("Assign")) {
                                                             QString rate=parseQDomNodeAttribute(rateList.at(ri).firstChild(),"symbIdRef");
                                                             params.push_back(rate);
                                                           }
                                                       }
                                                    }
                                                 }
                                              }
                                             }
                                             if (dist.toLower() == "bernoulli1") {
                                                //categoricalModel.categoryVariable = countVariable;
                                                categoricalModel.equations = "P("+categoricalModel.categoryVariable+"=1)=" +params[0]+"\n";
                                             }
                                        }
                                }
                                else if (categoricalDataList.at(cdi).nodeName() == "ProbabilityAssignment")
                                {
                                    QDomNodeList probabilityList = categoricalDataList.at(cdi).childNodes();
                                    QString op;
                                    QVector<QString> pargs;
                                    QString assignProb = "";
                                    for (int pli = 0;pli<probabilityList.count();pli++) {
                                        if (probabilityList.at(pli).nodeName() == "Probability") {
                                            QDomNodeList probItemList = probabilityList.at(pli).childNodes();
                                            for (int pili = 0;pili<probItemList.count();pili++) {
                                                if (probItemList.at(pili).nodeName().contains("LogicBinop")) {
                                                    op = parseQDomNodeAttribute(probItemList.at(pili),"op");
                                                    QDomNodeList parglist = probItemList.at(pili).childNodes();
                                                    for (int pargi = 0;pargi<parglist.count();pargi++) {
                                                      QString argi = parseQDomNodeAttribute(parglist.at(pargi),"symbIdRef");
                                                      pargs.push_back(argi);
                                                    }
                                                }
                                            }
                                        }
                                        if (probabilityList.at(pli).nodeName().contains("Assign")) {
                                           QDomNodeList aList = probabilityList.at(pli).childNodes();
                                           for (int ai = 0;ai<aList.count();ai++) {
                                               assignProb = parseQDomNodeAttribute(aList.at(ai).firstChild(),"symbIdRef");

                                               /*if (aList.at(ai).nodeName().contains("Equation")) {

                                               }
                                               else if (aList.at(ai).nodeName().contains("SymbRef")) {
                                                     assignProb = parseQDomNodeAttribute(aList.at(ai),"symbIdRef");
                                               }*/
                                           }
                                        }
                                    }
                                    if (pargs.size() == 2) {
                                        if (op == "lt")    op = "<";
                                        else if (op == "leq")   op = "<=";
                                        else if (op == "gt")    op = ">";
                                        else if (op == "geq")   op = ">=";
                                        else if (op == "eq")    op = "=";
                                        else if (op == "neq")   op = "~=";
                                        categoricalModel.equations += "P("+pargs[0]+op+pargs[1]+")="+assignProb+"\n";
                                        categoricalModel.categoryVariable = pargs[0];
                                    }
                                }

                            }
                            categoricalModel.equations = categoricalModel.categoryVariable + " = { type= categorical\n"
                                                         + "categories = {" + QStringList(categoricalModel.modalitiesAlias.keys()).join(",") + "}\n"
                                                         + categoricalModel.equations
                                                         + "}";
                        }
                        else if (discreteModelsNodes.at(dmi).nodeName() == "CountData")
                        {
                            QDomNodeList countDataList = discreteModelsNodes.at(dmi).childNodes();
                            QString countVariable;
                            for (int cdli = 0;cdli<countDataList.size();cdli++) {
                                if (countDataList.at(cdli).nodeName().contains("CountVariable")) {
                                    countVariable = parseQDomNodeAttribute(countDataList.at(cdli),"symbId");
                                }
                            }

                            for (int cdli = 0;cdli<countDataList.size();cdli++) {
                                if (countDataList.at(cdli).nodeName().contains("PMF")
                                    && countDataList.at(cdli).firstChild().nodeName() == "Distribution") {
                                   QDomNodeList pmfList = countDataList.at(cdli).firstChild().childNodes();
                                   QString dist;
                                   QVector<QString> params;
                                   for (int pmfi = 0;pmfi<pmfList.count();pmfi++) {
                                       if (pmfList.at(pmfi).nodeName()== "ProbOnto") {
                                        dist = parseQDomNodeAttribute(pmfList.at(pmfi),"name");
                                        if (dist.toLower() == "poisson1") {
                                            QDomNodeList poissonParameterList = pmfList.at(pmfi).childNodes();
                                            for (int ppi = 0;ppi<poissonParameterList.count();ppi++) {
                                              if (poissonParameterList.at(ppi).nodeName() == "Parameter") {
                                                  QString name=parseQDomNodeAttribute(poissonParameterList.at(ppi),"name");
                                                  QDomNodeList rateList = poissonParameterList.at(ppi).childNodes();
                                                  for (int ri = 0;ri<rateList.count();ri++) {
                                                      QString nn = rateList.at(ri).nodeName();
                                                      if (nn.contains("Assign")) {
                                                        QString rate=parseQDomNodeAttribute(rateList.at(ri).firstChild(),"symbIdRef");
                                                        params.push_back(rate);
                                                      }
                                                  }
                                               }
                                            }
                                         }
                                         else if (dist.toLower()  == "categoricalnonordered1") {

                                         }
                                        }                                       

                                       if (pmfList.at(pmfi).nodeName()== "UncertML" &&
                                           pmfList.at(pmfi).firstChild().nodeName() == "PoissonDistribution")
                                       {
                                        dist = "poisson1";
                                        QDomNodeList poissonParameterList = pmfList.at(pmfi).firstChild().childNodes();
                                        for (int ppi = 0;ppi<poissonParameterList.count();ppi++) {
                                          if (poissonParameterList.at(ppi).nodeName() == "rate") {
                                              QDomNodeList rateList = poissonParameterList.at(ppi).childNodes();
                                              for (int ri = 0;ri<rateList.count();ri++) {
                                                  QString nn = rateList.at(ri).nodeName();
                                                  QString name;
                                                  if (nn.toLower().contains("var")) {
                                                    name=parseQDomNodeAttribute(rateList.at(ri),"varId");
                                                    params.push_back(name);
                                                  }
                                              }
                                           }
                                          }
                                        }
                                        if (dist.toLower() == "poisson1") {
                                           categoricalModel.categoryVariable = countVariable;
                                           categoricalModel.equations = countVariable + "={\ntype=count\nlog(P("+countVariable+"=k)) = -"+params[0]+" + k*log("+params[0]+") - factln(k)\n}\n";
                                        }

                                   }
                                }
                            }
                        }
                        else if (discreteModelsNodes.at(dmi).nodeName() == "TimeToEventData")
                        {
                            QDomNodeList countDataList = discreteModelsNodes.at(dmi).childNodes();
                            QString eventVariable;
                            QString hazardFunction;
                            for (int cdli = 0;cdli<countDataList.size();cdli++) {
                                if (countDataList.at(cdli).nodeName().contains("EventVariable")) {
                                    eventVariable = parseQDomNodeAttribute(countDataList.at(cdli),"symbId");
                                }
                                if (countDataList.at(cdli).nodeName().contains("HazardFunction")) {
                                     hazardFunction = parseQDomNodeAttribute(countDataList.at(cdli),"symbId");
                                     if (countDataList.at(cdli).firstChild().nodeName().contains("Assign")) {
                                         QSet<QString> dependentVariables;
                                         QString blkIdRefTmp;
                                         QMap<QString,QString> aliasesTmp;
                                         QVector<QString> equation = pharmlEquationToMlxtran(PharmMLLoader08::LONGITUDINAL_SECTION,countDataList.at(cdli).firstChild(),"",_structuralModelParameters,dependentVariables,blkIdRefTmp, aliasesTmp);
                                         hazardFunction = equation[0];
                                         for (QSet<QString>::const_iterator dit = dependentVariables.begin();dit!=dependentVariables.end();dit++) {
                                             _macroParameters.insert(*dit);
                                         }
                                     }
                                }
                            }
                            categoricalModel.categoryVariable = eventVariable;
                            categoricalModel.equations = eventVariable+"={type=event, maxEventNumber=1, hazard="+hazardFunction+"}";
                        }
                    }
                }
            }
        }
    }
}

void PharmMLLoader08::getInitialValue(QDomElement root)
{
    for (int ri = 0;ri<root.childNodes().size();ri++)
    {
        QVector<PharmMLLoader08::LNodeSibling> nodeEstimationStep;
        QStringList blkRefsEstimationStep;
        traverseTo(nodeEstimationStep,
                   blkRefsEstimationStep,
                  root.childNodes().at(ri),
                  QStringList()<<"ModellingSteps"<<"EstimationStep");
        for (int esti = 0;esti<nodeEstimationStep.size();esti++)
        {
            QDomNodeList estimationStep = nodeEstimationStep[esti].siblingNode.childNodes();
            for (int ei = 0;ei<estimationStep.size();ei++)
            {
                if (estimationStep.at(ei).nodeName() == "ParametersToEstimate")
                {
                    QDomNodeList parameterToEstimate = estimationStep.at(ei).childNodes();
                    for (int pi = 0;pi<parameterToEstimate.size();pi++)
                    {
                        if (parameterToEstimate.at(pi).nodeName() == "ParameterEstimation")
                        {
                            QString readParameter;
                            QString blockRef;
                            QString fixed;
                            QString readValue = "0.0";
                            QDomNodeList parameterEstimation = parameterToEstimate.at(pi).childNodes();
                            for (int pei = 0;pei<parameterEstimation.size();pei++)
                            {
                                if (parameterEstimation.at(pei).nodeName()=="ct:SymbRef")
                                {
                                    readParameter = parseQDomNodeAttribute(parameterEstimation.at(pei),"symbIdRef");
                                    blockRef = parseQDomNodeAttribute(parameterEstimation.at(pei),"blkIdRef");
                                }
                                else if (parameterEstimation.at(pei).nodeName().contains("InitialEstimate"))
                                {
                                    fixed = parseQDomNodeAttribute(parameterEstimation.at(pei),"fixed");
                                    if (parameterEstimation.at(pei).firstChild().nodeName().contains("Real"))
                                    {
                                        readValue = parameterEstimation.at(pei).firstChild().firstChild().nodeValue();
                                    }
                                    else if (parameterEstimation.at(pei).firstChild().nodeName().contains("Int")) {
                                        readValue = parameterEstimation.at(pei).firstChild().firstChild().nodeValue();
                                    }
                                    else if (parameterEstimation.at(pei).firstChild().nodeName().contains("Equation")
                                             && (parameterEstimation.at(pei).firstChild().firstChild().nodeName().contains("Real")
                                                 || parameterEstimation.at(pei).firstChild().firstChild().nodeName().contains("Int"))) {
                                        readValue = parameterEstimation.at(pei).firstChild().firstChild().firstChild().nodeValue();
                                    }
                                    else if (parameterEstimation.at(pei).firstChild().nodeName().contains("Equation")
                                             && parameterEstimation.at(pei).firstChild().firstChild().nodeName().contains("Uniop")
                                             && (parameterEstimation.at(pei).firstChild().firstChild().firstChild().nodeName().contains("Real")
                                                 || parameterEstimation.at(pei).firstChild().firstChild().firstChild().nodeName().contains("Int"))) {
                                        readValue = "-"+parameterEstimation.at(pei).firstChild().firstChild().firstChild().firstChild().nodeValue();
                                    }
                                }
                            }

                            for (int iip = 0;iip<individualParametersSection.size();iip++)
                            {
                                if (individualParametersSection[iip]->type() == PharmMLLoader08::VariableDefinition::IS_STATISTICAL_DEFINITION)
                                {
                                    StatisticalDefinition* sd =static_cast<StatisticalDefinition*>(individualParametersSection[iip]);
                                    sd->equation = sd->equation.trimmed();
                                    if (sd->equation==readParameter && sd->equation.length() && sd->dependentVariables.size() == 1 && !(sd->equation.contains("-")
                                                                                    || sd->equation.contains("+")
                                                                                    || sd->equation.contains("*")
                                                                                    || sd->equation.contains("/")
                                                                                    )) {
                                        if (!fixed.isEmpty() && fixed=="true") readValue += " [medthod=FIXED]";
                                        _initialValues["pop_{"+sd->name+"}"] = readValue;

                                    }
                                    if (sd->population == readParameter)
                                    {
                                        if (!fixed.isEmpty() && fixed=="true") readValue += " [medthod=FIXED]";
                                        _initialValues["pop_{"+sd->name+"}"] = readValue;
                                    }
                                    if (sd->omegaNames.size())
                                    {
                                        for (int omega_i = 0;omega_i<sd->omegaNames.size();omega_i++)
                                        {
                                            QString omegaOrGamma = "omega";
                                            if (omega_i>0) {
                                                omegaOrGamma = "gamma";
                                            }
                                            if (sd->omegaNames[omega_i].first == readParameter)
                                            {
                                                if (sd->withVariance)
                                                {
                                                    bool convertIsOk = false;
                                                    double rval = readValue.toDouble(&convertIsOk);
                                                    readValue = QString::number(rval);
                                                }
                                                else {
                                                    bool convertIsOk=true;
                                                    double rval = readValue.toDouble(&convertIsOk);

                                                    if (convertIsOk)
                                                    {
                                                        //rval*=rval;
                                                    }
                                                    readValue = QString::number(rval);
                                                }

                                                if (!fixed.isEmpty() && fixed=="true") readValue += " [method=FIXED]";
                                                if (sd->withVariance)
                                                {
                                                   _initialValues[omegaOrGamma+"2_{"+sd->name+"}"] = readValue;
                                                }
                                                else
                                                {
                                                    _initialValues[omegaOrGamma+"_{"+sd->name+"}"] = readValue;
                                                }
                                            }
                                        }
                                    }
                                    if(sd->covariates.size())
                                    {
                                        for (QMap<QString,QString>::const_iterator cov_it=sd->covariates.begin();
                                             cov_it!=sd->covariates.end();cov_it++)
                                        {
                                            QString covName = cov_it.value();
                                            covName = covName.remove(QRegExp("\\:.*$"));
                                            covName = covName.remove(QRegExp("#.*$"));
                                            if (covName == readParameter)
                                            {
                                                QString modName = cov_it.key();
                                                modName = modName.remove(QRegExp("\\:.*$"));
                                                modName = modName.remove(QRegExp("#.*$"));
                                                QString beta = "beta_{"+sd->name+","+modName+"}";
                                                if (!fixed.isEmpty() && fixed=="true") readValue += " [method=FIXED]";
                                                _initialValues[beta] = readValue;
                                            }
                                        }
                                    }
                                }
                            }

                            for (QMap<QString,QVector<QPair<QString,QString> > >::const_iterator errIt =  errorModelParameterMapping.begin();
                                 errIt != errorModelParameterMapping.end();
                                 errIt++)
                            {
                                for (int mapPi = 0;mapPi<errIt.value().size();mapPi++)
                                {
                                    if (errIt.value()[mapPi].first == readParameter && readValue.size())
                                    {
                                        QString dispModelParam = errIt.value()[mapPi].second+QString("_{")+errIt.key()+QString("}");
                                        if (!fixed.isEmpty() && fixed=="true") readValue += " [method=FIXED]";
                                        _initialValues["zError_"+dispModelParam] = readValue;
                                    }
                                }
                            }
                            /*QMap<QString,QVector<QString> >::const_iterator findObsModel = errorModelParameterMapping.find(readParameter);
                            if (findObsModel != errorModelParameterMapping.end())
                            {
                                for (int poi = 0;poi<findObsModel.value().size();poi++)
                                {
                                    _initialValues[findObsModel.value()[poi]+"_{"+blockRef+"}"] = readValue;
                                }
                            }*/
                        }
                    }
                }
            }
        }
    }
}

void PharmMLLoader08::getColumnDepotRef(QVector<QPair<QString,QString> >& dtdes,QDomNode& node) {

    QString columnRef="";
    QString columnValue ="";
    for (int ni = 0;ni<node.childNodes().size();ni++) {

        if (node.childNodes().at(ni).nodeName().contains("ColumnRef")) {
            columnRef = parseQDomNodeAttribute(node.childNodes().at(ni),"columnIdRef");
        }
        if (node.childNodes().at(ni).nodeName().contains("Int")) {
            columnValue = node.childNodes().at(ni).firstChild().nodeValue();
        }
        QDomNode nn = node.childNodes().at(ni);
        getColumnDepotRef(dtdes,nn);
    }
    if (columnRef.length() && columnValue.length()) {
        dtdes.push_back(QPair<QString,QString>(columnRef,columnValue));
    }
}

void PharmMLLoader08::getDataSet(QDomElement& root)
{
    for (int ri = 0;ri<root.childNodes().size();ri++)
    {

        QVector<PharmMLLoader08::LNodeSibling> nodeData;
        QStringList blkRefsData;
        traverseTo(nodeData,
                   blkRefsData,
                   root.childNodes().at(ri),
                   QStringList()<<"TrialDesign"<<"ExternalDataSet");
        QString doseTimedValue;        
        for (int di = 0;di<nodeData.size();di++)
        {
            QMap<QString,QVector<QPair<QString,QString> > > depotToDesambiguate;
            QDomNodeList dataNodes = nodeData[di].siblingNode.childNodes();
            for (int dnodei = 0;dnodei<dataNodes.size();dnodei++)
            {
                QString nn = dataNodes.at(dnodei).nodeName();
                // Stuart Patch
                if (dataNodes.at(dnodei).isComment()) {
                    QString comment = dataNodes.at(dnodei).toComment().nodeValue();
                    if (comment.contains("doseTime=")) {
                        doseTimedValue = comment.split("=").at(1).trimmed();
                    }
                }
                if (dataNodes.at(dnodei).nodeName().contains("ColumnMapping") || dataNodes.at(dnodei).nodeName().contains("MultipleDVMapping"))
                {
                    QDomNodeList columnMappingNode = dataNodes.at(dnodei).childNodes();
                    bool undefTag = false;
                    bool isMultipleDV = dataNodes.at(dnodei).nodeName().contains("MultipleDVMapping");

                    QString columnRef;
                    QString symbRef;
                    for (int cmi = 0;cmi<columnMappingNode.size();cmi++)
                    {
                        if (columnMappingNode.at(cmi).nodeName().contains("ColumnRef"))
                        {
                            columnRef = parseQDomNodeAttribute(columnMappingNode.at(cmi),"columnIdRef");
                        }
                        else if (columnMappingNode.at(cmi).nodeName().contains("SymbRef"))
                        {
                            symbRef = parseQDomNodeAttribute(columnMappingNode.at(cmi),"symbIdRef");
                        }
                        else if (columnMappingNode.at(cmi).nodeName().contains("CategoryMapping")) {
                           QDomNodeList categories = columnMappingNode.at(cmi).childNodes();
                           for (int cati = 0;cati<categories.size();cati++) {
                                QString msymb = parseQDomNodeAttribute(categories.at(cati),"modelSymbol");
                                QString dsymb = parseQDomNodeAttribute(categories.at(cati),"dataSymbol");
                                covariateCategoryMapping[columnRef][msymb] = dsymb;
                           }
                        }
                        else if (columnMappingNode.at(cmi).nodeName().contains("Piecewise")) {
                            QDomNodeList columnPiece = columnMappingNode.at(cmi).childNodes();
                            for (int cpi = 0;cpi<columnPiece.size();cpi++) {
                                if (columnPiece.at(cpi).nodeName().contains("Piece")) {
                                   QDomNode cmtNodeName = columnPiece.at(cpi).firstChild();

                                   QString cmtName  = parseQDomNodeAttribute(cmtNodeName,"symbIdRef");
                                   if (cmtName == doseTimedValue) {
                                       doseTimeValues[doseTimedValue] = "tDose";
                                   }
                                   else {

                                       QString mappedName = "";
                                       QString cmtId;
                                       QDomNodeList conditionList = columnPiece.at(cpi).childNodes();
                                       for (int condListi = 0;condListi<conditionList.size();condListi++) {
                                           QDomNodeList mathList = conditionList.at(condListi).childNodes();


                                           for (int mathi = 0;mathi<mathList.size();mathi++)
                                           {
                                               QDomNodeList seekList = mathList.at(mathi).childNodes();
                                               for (int seekId = 0;seekId<seekList.size();seekId++)
                                               {
                                                 if (seekList.at(seekId).nodeName() == "ColumnRef") {
                                                     mappedName =  parseQDomNodeAttribute(seekList.at(seekId),"columnIdRef");
                                                 }

                                                 if (seekList.at(seekId).nodeName().contains("Int")
                                                     || seekList.at(seekId).nodeName().contains("String")) {
                                                   cmtId = seekList.at(seekId).firstChild().nodeValue();
                                                 }
                                               }
                                           }
                                       }
                                       if (cmtName.length() && cmtId.length()) {
                                           if (isMultipleDV) mappedName = columnRef;
                                           administrationMapping[cmtName] = QPair<QString,QString>(cmtId,mappedName);
                                       }
                                       else {
                                           QDomNode dn = columnPiece.at(cpi);
                                           getColumnDepotRef(depotToDesambiguate[cmtName],dn);
                                       }
                                   }
                                }
                                else
                                {/*
                                    addMessage(LixoftObject::LERROR,QString("Tag '")
                                                                    + QString(columnMappingNode.at(cmi).nodeName().toUtf8().data())
                                                                    + QString("' is no managed.")
                                               );*/
                                }
                            }
                        }
                        else
                        {
                            /*
                            addMessage(LixoftObject::WARNING,QString("Tag '")
                                                             + QString(columnMappingNode.at(cmi).nodeName().toUtf8().data())
                                                             + QString("' is no managed.")
                                                             );*/
                            undefTag=true;
                        }
                    }
                    if (!undefTag && columnRef.length() && symbRef.length())
                    {
                        columnMapping[columnRef] = symbRef;
                    }
                }
            }
            for (int dnodei = 0;dnodei<dataNodes.size();dnodei++)
            {
                if (dataNodes.at(dnodei).nodeName().contains("DataSet"))
                {
                    QDomNodeList columnDefinition = dataNodes.at(dnodei).childNodes();
                    int durationGiven = -1;
                    int rateGiven = -1;
                    for (int cdi = 0;cdi<columnDefinition.size();cdi++)
                    {
                        if (columnDefinition.at(cdi).nodeName().contains("Definition"))
                        {
                            QDomNodeList columns = columnDefinition.at(cdi).childNodes();
                            for (int coli = 0;coli<columns.size();coli++)
                            {
                                if (columns.at(coli).nodeName() == "ds:Column"
                                    || columns.at(coli).nodeName() == "Column")
                                {
                                    QString columnId = parseQDomNodeAttribute(columns.at(coli),"columnId");
                                    QString columnType = parseQDomNodeAttribute(columns.at(coli),"columnType");
                                    QString valueType = parseQDomNodeAttribute(columns.at(coli),"valueType");
                                    QString columnNum = parseQDomNodeAttribute(columns.at(coli),"columnNum");
                                    if (columnType == "id" )             columnType="ID";
                                    else if (columnType == "time" )      columnType = "TIME";
                                    else if (columnType == "idv" )       columnType = "TIME";
                                    else if (columnType == "undefined" ) columnType = "IGNORE";
                                    else if (columnType == "dose" )      columnType = "DOSE";
                                    else if (columnType == "adm" )       columnType = "DPT";
                                    else if (columnType == "cmt" )       columnType = "CMT";
                                    else if (columnType == "covariate" ) columnType = "COV";
                                    else if (columnType == "reg" || columnType=="variable") columnType = "REG";
                                    else if (columnType == "rate" )      columnType ="RATE";
                                    else if (columnType == "duration" )  columnType ="TINF";
                                    else if (columnType == "evid" )      columnType ="EVID";
                                    else if (columnType == "ii" )        columnType = "II";
                                    else if (columnType == "ss" )        columnType = "SS";
                                    else if (columnType == "limit" )     columnType = "LIMIT";
                                    else if (columnType == "addl" )        columnType = "ADDL";
                                    else if (columnType == "censoring" || columnType=="cens")   columnType = "CENS";
                                    else if (columnType == "dv" )        columnType = "Y";
                                    else if (columnType == "dvid" )        columnType = "YTYPE";
                                    else if (columnType == "occasion" )  columnType = "OCC";
                                    else if (columnType == "mdv" )  columnType = "MDV";
                                    else
                                    {
                                        QString errorString = QString("getDataSet:Column type '")+columnType + QString("' is not managed.")
                                                              + QString( "Line in XML file:")
                                                              + QString::number(columns.at(coli).lineNumber());
                                        columnType="IGNORE";
                                        addMessage(LixoftObject::WARNING,errorString);
                                    }
                                    if (columnType == "TINF") { durationGiven = columnNum.toInt(); }
                                    if (columnType == "RATE") { rateGiven = columnNum.toInt(); }

                                    _dataSet.columns[columnNum.toInt()] = QPair<QString,QString>(columnType,columnId);
                                    columnNameTypeMapping[columnId] = columnType;
                                    if (columnType == "Y")
                                    {
                                        QVector<QString> keyRemover;
                                        for (QMap<QString,QPair<QString,QString> >::const_iterator adIt = administrationMapping.begin();
                                             adIt != administrationMapping.end();
                                             adIt++) {
                                            if (adIt.value().second == columnId) {
                                                keyRemover.push_back(adIt.key());
                                            }
                                         }
                                         for (int kri = 0;kri<keyRemover.size();kri++) {
                                            administrationMapping.remove(keyRemover[kri]);
                                         }
                                    }
                                    if (columnType == "OCC") {
                                        occColumnNames[columnNum.toInt()] = columnId;
                                        pharmMLNamesMapping[columnNum+":"+columnId+":occasion"] = valueType;
                                    }
                                }
                            }
                            if (durationGiven != -1 && rateGiven != -1)
                            {
                                _dataSet.columns[durationGiven].first = "IGNORE";
                            }
                        }
                        else if (columnDefinition.at(cdi).nodeName().contains("ImportData")
                                 || columnDefinition.at(cdi).nodeName().contains("ExternalFile"))
                        {
                            QDomNodeList importData = columnDefinition.at(cdi).childNodes();
                            for (int idi = 0;idi<importData.size();idi++)
                            {
                                if (importData.at(idi).nodeName().contains("path"))
                                {
                                    _dataSet.dataUrl = "file://"+importData.at(idi).firstChild().nodeValue();
                                }
                                else if (importData.at(idi).nodeName() == "format"
                                    || importData.at(idi).nodeName() == "ds:format" )
                                {
                                    _dataSet.dataFormat = importData.at(idi).firstChild().nodeValue();
                                }
                                else if (importData.at(idi).nodeName() == "delimiter"
                                    || importData.at(idi).nodeName() == "ds:delimiter" )
                                {
                                    _dataSet.columnDelimiter = importData.at(idi).firstChild().nodeValue();
                                }
                            }
                        }
                        else
                        {
                          /* addMessage(LixoftObject::WARNING,QString("Tag '")
                                                             + QString(columnDefinition.at(cdi).nodeName().toUtf8().data())
                                                             + QString("' is no managed.")
                                                             );*/
                        }
                    }
                }
            }
            for (QMap<QString,QVector<QPair<QString,QString> > >::const_iterator it = depotToDesambiguate.begin();
                 it != depotToDesambiguate.end();
                 it++) {
                if (!administrationMapping.contains(it.key())) {
                    for (int cref = 0;cref<it.value().size();cref++) {
                        for (QMap<int,QPair<QString,QString> >::const_iterator dsit = _dataSet.columns.begin();
                             dsit != _dataSet.columns.end();
                             dsit++) {
                            QString dbg1 = it.value()[cref].first;
                            QString dbg2 = dsit.value().second;
                            QString dbg3 = dsit.value().first;
                            if (it.value()[cref].first == dsit.value().second && dsit.value().first=="CMT") {
                                administrationMapping[it.key()] = QPair<QString,QString>(it.value()[cref].second,it.value()[cref].first);
                            }
                        }
                    }
                }
            }
        }
    }
}

void PharmMLLoader08::loadFromRoot(QDomElement& root)
{
    getDataSet(root);
    // Data preload

    QVector<QString> headerNames;
    QString delimiter = _dataSet.columnDelimiter;
    QString filename = _dataSet.dataUrl.remove("file://");

    /*
    QVector<lixoft::data::HeaderType> headerTypes;
    for (QMap<int,QPair<QString,QString> >::const_iterator it = _dataSet.columns.begin();
         it!=_dataSet.columns.end(); it++){
      headerTypes.push_back(lixoft::data::LixoftDataType::headerStringToType()[it.value().first]);
      headerNames.push_back(it.value().second);
    }

    QString delim;
    if (delimiter.toLower()=="tab")
    {
      delim="\t";
    }
    else if (delimiter.toLower()=="space")
    {
      delim=" ";
    }
    else if (delimiter.toLower()=="comma")
    {
      delim=",";
    }
    else if (delimiter.toLower()=="semicolumn")
    {
      delim=";";
    }
    else {
        addMessage(LixoftObject::LERROR,QString("Data delimiter '"+ delimiter +"'unrecognized"));
        return;
    }

    lixoft::data::builder::DataManagerBuilderFileLoader dMBuilder(filename, headerNames, headerTypes, 2, "COLUMN DELIMITER", &delim);
    */

    QSet<QString> globalParameterRegistered;
    getCovariateModel(root,covariatesSection,globalParameterRegistered);

    getIndividualParameterModel(root,individualParametersSection,globalParameterRegistered);
    getStructuralModel(root,structuralModelSection,globalParameterRegistered);
    getObservationModel(root,observationModelSection,globalParameterRegistered);
    getInitialValue(root);
    //throwNow();
}


QString PharmMLLoader08::parseQDomNodeAttribute(const QDomNode& node,
                                                    const QString& attributeName)
{
    QDomNamedNodeMap nodeAttributes = node.attributes();
    for (int attr_i = 0;attr_i<static_cast<int>(nodeAttributes.length());attr_i++)
    {
        if (!nodeAttributes.item(attr_i).isNull())
        {
            QDomAttr attributeNode = nodeAttributes.item(attr_i).toAttr();
            if (attributeNode.name() == attributeName) return attributeNode.value();
        }
    }
    return QString("");
}

void PharmMLLoader08::traverseTo(QVector<PharmMLLoader08::LNodeSibling>& selectedNodes,
                               QStringList blockRefs,
                               const QDomNode& fromNode, const QStringList& nodePaths)
{
    if (nodePaths.size())
    {
        if (nodePaths.size())
        {
            if (fromNode.nodeName().contains(QRegExp(nodePaths.at(0))))
            {
                if (nodePaths.size() == 1)
                {
                    PharmMLLoader08::LNodeSibling node;
                    node.siblingNode = fromNode;
                    node.blockId = blockRefs;
                    selectedNodes.push_back(node);
                }
                else
                {
                    QStringList newNodePath = nodePaths;
                    newNodePath.pop_front();
                    QString blkRef = parseQDomNodeAttribute(fromNode,"blkId");
                    if (blkRef.length()) blockRefs.push_back(blkRef);
                    for (int ci = 0;ci<fromNode.childNodes().size();ci++)
                    {
                        traverseTo(selectedNodes,
                                   blockRefs,
                                   fromNode.childNodes().at(ci),
                                   newNodePath);
                    }
                    if (blkRef.length()) blockRefs.pop_back();
                }
            }
        }
    }
}

void PharmMLLoader08::getRandomVariable(const QDomNode& randomVariable, PharmMLLoader08::RandomVariableDefinition& rvd)
{
    if (randomVariable.nodeName() == "RandomVariable")
    {
        QDomNodeList randomVariableElement = randomVariable.childNodes();
        rvd.name = parseQDomNodeAttribute(randomVariable,"symbId");
        QVector<QPair<QString,QString> >& variabilityReferences = rvd.variabilityReferences;
        for (int rvei = 0;rvei<randomVariableElement.size();rvei++)
        {
            if (randomVariableElement.at(rvei).nodeName() == "ct:VariabilityReference"
                 || randomVariableElement.at(rvei).nodeName() == "VariabilityReference")
            {
                QDomNodeList variabilityReference = randomVariableElement.at(rvei).childNodes();
                for (int vri = 0;vri<variabilityReference.size();vri++)
                {
                    if (variabilityReference.at(vri).nodeName() == "ct:SymbRef")
                    {
                        QPair<QString,QString> vr;
                        vr.first = parseQDomNodeAttribute(variabilityReference.at(vri),"blkIdRef");
                        vr.second = parseQDomNodeAttribute(variabilityReference.at(vri),"symbIdRef");
                        variabilityReferences.push_back(vr);
                    }
                }
            }
            else if (randomVariableElement.at(rvei).nodeName().contains("Distribution")
                     && randomVariableElement.at(rvei).firstChild().nodeName().contains("ProbOnto"))
            {
                QDomNodeList normalDistributionElement = randomVariableElement.at(rvei).firstChild().childNodes();
                for (int ndi = 0;ndi<normalDistributionElement.size();ndi++)
                {
                    QString nameOfParam = parseQDomNodeAttribute(normalDistributionElement.at(ndi),"name").toLower();
                    if (nameOfParam == "mean")
                    {
                        QDomNodeList meanValues = normalDistributionElement.at(ndi).childNodes();
                        if (meanValues.at(0).nodeName().contains("Assign")
                            && ( meanValues.at(0).firstChild().nodeName().contains("Real")
                                 || meanValues.at(0).firstChild().nodeName().contains("Int")
                               )
                        )
                        {
                            rvd.mean = meanValues.at(0).firstChild().firstChild().nodeValue();
                        }
                        else if (meanValues.at(0).nodeName().contains("Assign")
                                && meanValues.at(0).firstChild().nodeName().contains("SymbIdRef")) {
                            rvd.mean = meanValues.at(0).firstChild().firstChild().nodeValue();
                            rvd.dependentVariables.insert(rvd.mean);
                        }
                        else
                        {
                            if (meanValues.size())
                            {

                                addMessage(LixoftObject::WARNING,QString("'" )+ meanValues.at(0).nodeName() + QString("'")
                                                                 + QString(" is unknown ( line")
                                                                 + QString::number(meanValues.at(0).lineNumber())
                                                                 + QString(")")
                                          );
                            }
                            else
                            {
                                addMessage(LixoftObject::WARNING,"Undeterminate tags sequence"
                                                                 + QString(" is unknown ( line ")
                                                                 + QString::number(normalDistributionElement.at(ndi).lineNumber())
                                                                 + QString(")")
                                          );
                            }
                        }

                    }
                    else if (nameOfParam == "stdev" || nameOfParam == "stddev")
                    {
                        QDomNodeList sdValue = normalDistributionElement.at(ndi).childNodes();
                        if (sdValue.size() == 1 && (sdValue.at(0).nodeName().contains("Assign")
                                                   && ( sdValue.at(0).firstChild().nodeName().contains("Real")
                                                        || sdValue.at(0).firstChild().nodeName().contains("Int")
                                                      )
                                               )
                                )
                        {                            
                            rvd.stddev = sdValue.at(0).firstChild().firstChild().nodeValue();

                        }
                        else if (sdValue.size() == 1 && (sdValue.at(0).nodeName().contains("Assign")
                                 )) {
                            rvd.isVariance=false;
                            QString tsd = parseQDomNodeAttribute(sdValue.at(0).firstChild(),"symbIdRef");
                            rvd.stddev = tsd;
                            rvd.omegaName = rvd.stddev;
                            rvd.dependentVariables.insert(rvd.stddev);
                        }
                        else
                        {
                            if (sdValue.size())
                            {

                                addMessage(LixoftObject::WARNING,QString("'" )+ sdValue.at(0).nodeName() + QString("'")
                                                                 + QString(" is unknown ( line ")
                                                                 + QString::number(sdValue.at(0).lineNumber())
                                                                 + QString(")")
                                          );
                            }
                            else
                            {
                                addMessage(LixoftObject::WARNING,"Undeterminate tags sequence"
                                                                 + QString(" is unknown ( line ")
                                                                 + QString::number(normalDistributionElement.at(ndi).lineNumber())
                                                                 + QString(")")
                                          );
                            }
                        }
                    }
                    else if (nameOfParam == "variance" || nameOfParam=="var")
                    {
                        QDomNodeList sdValue = normalDistributionElement.at(ndi).childNodes();
                        if (sdValue.size() == 1 && sdValue.at(0).nodeName().contains("Assign"))
                        {
                            QString tsd = parseQDomNodeAttribute(sdValue.at(0).firstChild(),"symbIdRef");
                            rvd.isVariance = true;
                            rvd.dependentVariables.insert(tsd);
                            rvd.omegaName = tsd;
                            rvd.stddev = tsd;
                        }
                        else if (sdValue.size() == 1 && (sdValue.at(0).nodeName().contains("Assign")
                                                         && ( sdValue.at(0).firstChild().nodeName().contains("Real")
                                                              || sdValue.at(0).firstChild().nodeName().contains("Int")
                                                            )
                                                     ))
                        {
                            QString stddev = sdValue.at(0).firstChild().nodeValue();
                            rvd.stddev= stddev;
                        }
                        else
                        {
                            if (sdValue.size())
                            {

                                addMessage(LixoftObject::WARNING,QString("'" )+ sdValue.at(0).nodeName() + QString("'")
                                                                 + QString(" is unknown ( line ")
                                                                 + QString::number(sdValue.at(0).lineNumber())
                                                                 + QString(")")
                                          );
                            }
                            else
                            {
                                addMessage(LixoftObject::WARNING,"Undeterminate tags sequence"
                                                                 + QString(" is unknown ( line ")
                                                                 + QString::number(normalDistributionElement.at(ndi).lineNumber())
                                                                 + QString(")")
                                          );
                            }
                        }

                    }
                }
            }
            else if (randomVariableElement.at(rvei).nodeName().contains("Distribution")
                     && randomVariableElement.at(rvei).firstChild().nodeName().contains("UncertML")
                     && randomVariableElement.at(rvei).firstChild().firstChild().nodeName().contains("NormalDistribution"))
            {
                QDomNodeList normalDistributionElement = randomVariableElement.at(rvei).firstChild().firstChild().childNodes();
                for (int ndi = 0;ndi<normalDistributionElement.size();ndi++) {
                    if (normalDistributionElement.at(ndi).nodeName() == "mean")
                    {
                        QDomNodeList meanValues = normalDistributionElement.at(ndi).childNodes();
                        if (meanValues.size() == 1 && meanValues.at(0).nodeName() == "var")
                        {
                            rvd.mean = parseQDomNodeAttribute(meanValues.at(0),"varId");
                            rvd.dependentVariables.insert(rvd.mean);
                        }
                        else if (meanValues.size() == 1 && meanValues.at(0).nodeName() == "rVal")
                            rvd.mean = meanValues.at(0).firstChild().nodeValue();
                        else
                        {
                            if (meanValues.size())
                            {

                                addMessage(LixoftObject::WARNING,QString("'" )+ meanValues.at(0).nodeName() + QString("'")
                                                                 + QString(" is unknown ( line")
                                                                 + QString::number(meanValues.at(0).lineNumber())
                                                                 + QString(")")
                                          );
                            }
                            else
                            {
                                addMessage(LixoftObject::WARNING,"Undeterminate tags sequence"
                                                                 + QString(" is unknown ( line ")
                                                                 + QString::number(normalDistributionElement.at(ndi).lineNumber())
                                                                 + QString(")")
                                          );
                            }
                        }

                    }
                    else if (normalDistributionElement.at(ndi).nodeName() == "stddev")
                    {
                        QDomNodeList sdValue = normalDistributionElement.at(ndi).childNodes();
                        if (sdValue.size() == 1 && sdValue.at(0).nodeName() == "var")
                        {
                            rvd.isVariance=false;
                            rvd.stddev = parseQDomNodeAttribute(sdValue.at(0),"varId");
                            rvd.omegaName = rvd.stddev;
                            rvd.dependentVariables.insert(rvd.stddev);
                        }
                        else if (sdValue.size() == 1 && sdValue.at(0).nodeName() == "rVal")
                            rvd.stddev = sdValue.at(0).firstChild().nodeValue();
                        else
                        {
                            if (sdValue.size())
                            {

                                addMessage(LixoftObject::WARNING,QString("'" )+ sdValue.at(0).nodeName() + QString("'")
                                                                 + QString(" is unknown ( line ")
                                                                 + QString::number(sdValue.at(0).lineNumber())
                                                                 + QString(")")
                                          );
                            }
                            else
                            {
                                addMessage(LixoftObject::WARNING,"Undeterminate tags sequence"
                                                                 + QString(" is unknown ( line ")
                                                                 + QString::number(normalDistributionElement.at(ndi).lineNumber())
                                                                 + QString(")")
                                          );
                            }
                        }
                    }
                    else if (normalDistributionElement.at(ndi).nodeName() == "variance")
                    {
                        QDomNodeList sdValue = normalDistributionElement.at(ndi).childNodes();
                        if (sdValue.size() == 1 && sdValue.at(0).nodeName() == "var")
                        {
                            QString stddev = parseQDomNodeAttribute(sdValue.at(0),"varId");
                            rvd.isVariance = true;
                            rvd.dependentVariables.insert(stddev);
                            rvd.omegaName = stddev;
                            rvd.stddev = stddev;
                        }
                        else if (sdValue.size() == 1 && (sdValue.at(0).nodeName() == "rVal" || sdValue.at(0).nodeName() == "prVal"))
                        {
                            QString stddev = sdValue.at(0).firstChild().nodeValue();
                            rvd.stddev= stddev;
                        }
                        else
                        {
                            if (sdValue.size())
                            {

                                addMessage(LixoftObject::WARNING,QString("'" )+ sdValue.at(0).nodeName() + QString("'")
                                                                 + QString(" is unknown ( line ")
                                                                 + QString::number(sdValue.at(0).lineNumber())
                                                                 + QString(")")
                                          );
                            }
                            else
                            {
                                addMessage(LixoftObject::WARNING,"Undeterminate tags sequence"
                                                                 + QString(" is unknown ( line ")
                                                                 + QString::number(normalDistributionElement.at(ndi).lineNumber())
                                                                 + QString(")")
                                          );
                            }
                        }

                    }
                }
            }
            else throwLater(lixoft::exception::Exception("PharmMLLoader08::getRandomVariable","Unsupported tag",THROW_LINE_FILE));
        }
    }
}


void PharmMLLoader08::equationToVariableDefinition(
                                                 int blockNameId,
                                                 PharmMLLoader08::VariableDefinition& variable,
                                                 const QString& variableNameAssigned,
                                                 QSet<QString>& assigned,
                                                 const QString& variableName,
                                                 const QStringList& blkIds,
                                                 const PharmMLLoader08::LNodeSibling& equationNode
                                                 )
{
    QMap<QString,QVector<QString> > blockRef;
    QSet<QString> dependentVariables;
    QMap<QString,QString> aliases;
    QVector<QString> eq = pharmlEquationToMlxtran(blockNameId,
                            equationNode.siblingNode,
                            "",
                            blockRef,
                            dependentVariables,
                            "",
                            aliases);
    if (eq.size())
    {
        QRegExp rx("centeredBy#([a-zA-Z0-9]+)");
        if (eq[0].contains(rx)){
            int idx = rx.indexIn(eq[0]);
            QString centeredBy = rx.cap();
            centeredBy.remove("centeredBy#");
            eq[0].remove(QRegExp("\\-\\(centeredBy#[a-zA-Z0-9]+\\)"));
            variable.centeredBy = centeredBy;
        }
        QString blkref;
        for (int blk_i = 0;blk_i<blkIds.size();blk_i++)
        {
            if (blk_i < (blkIds.size()-1))
                blkref+=(blkIds.at(blk_i)+QString("_"));
            else
                blkref+=blkIds.at(blk_i);
        }
        blkref.replace(QRegExp("[^a-zA-Z0-9_]"),"");

        if (variableNameAssigned.length()) {
            _symbolIds[variableName].transformed = variableNameAssigned;
            variable.equation=(variableNameAssigned+QString(" = ") + eq[0]);
            assigned.insert(variableNameAssigned);
        }
        else if (variableName.trimmed() != eq[0].trimmed() && dependentVariables.contains(variableName))
        {
            _symbolIds[variableName].transformed = QString("t")+variableName;
            variable.equation=(/*blkref+*/QString("t")+variableName+QString(" = ") + eq[0]);
            assigned.insert(QString("t")+variableName);
        }
        else {
            assigned.insert(/*blkref+QString("_")+*/variableName);
            if (eq.size() && !eq[0].contains("=")) {
              variable.equation=(variableName+QString(" = ") + eq[0]);
            }
        }
        variable.dependentVariables = dependentVariables;
    }
}

QVector<QString> PharmMLLoader08::pharmlEquationToMlxtran(int blockNameId,
                                                        const QDomNode& node,
                                                        const QString& leftPart,
                                                        QMap<QString,QVector<QString> >& blockRef,
                                                        QSet<QString>& dependentVariables,
                                                        const QString& blkIdRef,
                                                        const QMap<QString,QString>& aliases)
{
    QDomNodeList ops = node.childNodes();
    QVector<QString> operationList;
    for (int i = 0;i<ops.size();i++)
    {
        if (ops.at(i).nodeName().contains("Binop"))
        {
            QDomNodeList binopList = ops.at(i).childNodes();
            int countBinop = 0;
            for (int ni2 = 0;ni2<binopList.size();ni2++)
            {
                if (binopList.at(ni2).nodeName()!="#comment")
                    countBinop++;
            }

            if (countBinop != 2)
            {
                throwLater(lixoft::exception::Exception("lixoft::translator::PharmMLLoader08",
                                                         (QString("[Bad Equation]: bad equation definition at line #")
                                                          + QString::number(ops.at(i).lineNumber())
                                                          + QString(" binary operation takes two elements.")).toUtf8().data(),
                                                         THROW_LINE_FILE
                                                        ));
            }
            else
            {
                QString bop = parseQDomNodeAttribute(ops.at(i),"op");
                if (bop == "plus") bop = "+";
                else if (bop == "minus") bop = "-";
                else if (bop == "times") bop = "*";
                else if (bop == "lt")    bop = "<";
                else if (bop == "leq")   bop = "<=";
                else if (bop == "gt")    bop = ">";
                else if (bop == "geq")   bop = ">=";
                else if (bop == "eq")    bop = "==";
                else if (bop == "neq")   bop = "~=";
                else if (bop == "and")   bop = "&";
                else if (bop == "or")    bop = "|";
                else if (bop == "divide") bop = "/";
                else if (bop == "power") bop = "^";
                else throwLater(lixoft::exception::Exception("PharmMLLoader08",std::string("Operator '")
                                                                             + std::string(bop.toUtf8().data())
                                                                             + std::string("' is no recognize.")));

                QVector<QString> operandes = pharmlEquationToMlxtran(blockNameId,ops.at(i),"",blockRef,dependentVariables,blkIdRef,aliases);
                if (operandes.size() == 2)
                {
                    QString partOperation="";
                    if (bop == "xor")
                    {
                        partOperation = QString("(( !(") +operandes[0] + QString(") & ") + operandes[1] + QString(")")
                                + QString(" | ((") + operandes[0] +  QString(") & !(") + operandes[1] + QString(")))");

                    }
                    else
                    {
                        partOperation = QString("(") + operandes[0]
                                + QString(")") + bop + QString("(")
                                + operandes[1] + QString(")");
                    }
                    if (leftPart.isEmpty())
                        operationList.push_back(partOperation);
                    else
                        operationList.push_back(leftPart + " = "+ partOperation);
                }
                else
                {
                    throwLater(lixoft::exception::Exception("lixoft::translator::PharmMLLoader08",
                                (QString("[Bad Equation/serialization]: bad equation definition at line #")
                                 + QString::number(ops.at(i).lineNumber())
                                 + QString(" binary operation takes two elements. '")
                                 + QString::number(operandes.size())+QString("' element(s) given")).toUtf8().data(),
                                THROW_LINE_FILE));
                }
            }
        }
        else if (ops.at(i).nodeName().contains("Uniop"))
        {
            QDomNodeList uniopList = ops.at(i).childNodes();
            if (uniopList.size() != 1)
            {
                throwLater(lixoft::exception::Exception("lixoft::translator::PharmMLLoader08",
                           (QString("[Bad Equation]: bad equation definition at line #")
                            + QString::number(ops.at(i).lineNumber())
                            + QString(" unary operation takes one element.")).toUtf8().data(),
                            THROW_LINE_FILE));
                return QVector<QString>();

            }
            else
            {
                QString uop = parseQDomNodeAttribute(ops.at(i),"op");
                if (uop == "minus") uop="-";
                else if (uop == "plus") uop="+";
                QVector<QString> operande = pharmlEquationToMlxtran(blockNameId,ops.at(i),"",blockRef,dependentVariables,blkIdRef,aliases);
                if (operande.size() == 1)
                {
                    if (operande[0].contains("##Stat##")) {
                        operande[0].remove("##Stat##");
                        operationList.push_back("centeredBy#" + operande[0]);
                    }
                    else {
                        QString partOperation = uop + QString("(") + operande[0] + QString(")");
                        if (leftPart.isEmpty())
                            operationList.push_back(partOperation);
                        else
                            operationList.push_back(leftPart + "=" + partOperation);
                    }
                }
                else
                {
                    throwLater(lixoft::exception::Exception("lixoft::translator::PharmMLLoader08",
                                                            (QString("[Bad Equation/serialization]: bad equation definition at line #")
                                                             + QString::number(ops.at(i).lineNumber())
                                                             + QString(" unary operation takes one element.")).toUtf8().data(),
                                                             THROW_LINE_FILE));
                    return QVector<QString>();
                }
            }
        }
        else if (ops.at(i).nodeName().contains("Piecewise"))
        {
            if (blockNameId == PharmMLLoader08::COVARIATE_SECTION) {

                addMessage(LixoftObject::LERROR,(QString("[Bad Equation]: bad covariate definition. Transformation of continuous covariate should be a 'one line' (PieceWise is not recognized) equation #")
                                                 + QString::number(ops.at(i).lineNumber())
                                                 + QString(" 'ct:SymbRef' should have 'symbIdRef' attribute.")));
                break;
            }
            QDomNodeList piecewiseElts = ops.at(i).childNodes();
            bool ifSet = false;
            for (int piece_i = 0;piece_i<piecewiseElts.size();piece_i++)
            {
                if (piecewiseElts.at(piece_i).nodeName() == "Piece" || piecewiseElts.at(piece_i).nodeName() == "math:Piece")
                {
                    QVector<QString> conditionEq;
                    QVector<QString> inCondition;
                    QDomNodeList pieceElts = piecewiseElts.at(piece_i).childNodes();
                    for (int pei = 0;pei<pieceElts.size();pei++)
                    {
                        if (pieceElts.at(pei).nodeName() == "Condition" || pieceElts.at(pei).nodeName() == "math:Condition")
                        {
                            conditionEq = pharmlEquationToMlxtran(blockNameId,pieceElts.at(pei),"",blockRef,dependentVariables,blkIdRef,aliases);
                        }
                    }
                    inCondition = pharmlEquationToMlxtran(blockNameId,piecewiseElts.at(piece_i),"",blockRef,dependentVariables,blkIdRef,aliases);
                    if (conditionEq.size() && !ifSet)
                    {
                        operationList.push_back("if " + QString(conditionEq[0]) +"\n");
                        ifSet=true;
                    }
                    else if (conditionEq.size() && ifSet)
                    {
                        operationList.push_back("elseif " + QString(conditionEq[0]) +"\n");
                    }
                    else
                    {
                        operationList.push_back("else\n");
                    }
                    if (!leftPart.isEmpty())
                        operationList.push_back(leftPart + "=" + inCondition[0]+ "\n");
                }
            }
            operationList.push_back("end\n");
        }
        else if (ops.at(i).nodeName().contains("Statsop")) {
             QString uop = "##Stat##" + parseQDomNodeAttribute(ops.at(i),"op");
             QVector<QString> resultsOp;
             resultsOp.push_back(uop);
             return resultsOp;
        }
        else if (ops.at(i).nodeName() == "ct:SymbRef")
        {
            QString symbIdRef = parseQDomNodeAttribute(ops.at(i),"symbIdRef");
            QString nblkIdRef = parseQDomNodeAttribute(ops.at(i),"blkIdRef");
            dependentVariables.insert(symbIdRef);
            if (nblkIdRef.length())
            {
                nblkIdRef.replace(QRegExp("[^a-zA-Z0-9_]"),"");
                dependentVariables.insert(/*nblkIdRef+"_"+*/symbIdRef);
            }
            else
                dependentVariables.insert(symbIdRef);

            if (blockNameId == PharmMLLoader08::INDIVIDUAL_SECTION)
            {
                _individualParametersSymbols.insert(symbIdRef);
            }
            if (nblkIdRef.isEmpty()) nblkIdRef = blkIdRef;
            if (symbIdRef.isEmpty())
            {
                throwLater(lixoft::exception::Exception("lixoft::translator::PharmMLLoader08",
                                                        (QString("[Bad Equation]: bad equation symbol element at line #")
                                                         + QString::number(ops.at(i).lineNumber())
                                                         + QString(" 'ct:SymbRef' should have 'symbIdRef' attribute.")).toUtf8().data(),
                                                        THROW_LINE_FILE));
                return QVector<QString>();
            }
            else
            {
                if (!nblkIdRef.isEmpty())
                    blockRef[symbIdRef].push_back(nblkIdRef);
                QMap<QString,QString>::const_iterator findSymb = aliases.find(symbIdRef);
                if (findSymb != aliases.end()) symbIdRef = findSymb.value();
                /*if (nblkIdRef.length())
                {
                    operationList.push_back(nblkIdRef+"_"+symbIdRef);
                }
                else*/
                if (blockNameId == PharmMLLoader08::COVARIATE_SECTION)
                {
                   for (QMap<QString,QString>::iterator cit = columnMapping.begin();
                        cit != columnMapping.end();
                        cit++)
                   {
                       if (cit.value() == symbIdRef)
                       {
                           symbIdRef = cit.key();
                           break;
                       }
                   }
                }
                else if (blockNameId == PharmMLLoader08::LONGITUDINAL_SECTION)
                {
                    for (QMap<int,QPair<QString,QString> >::iterator dataIt =  _dataSet.columns.begin();
                         dataIt!=_dataSet.columns.end();
                         dataIt++)
                    {
                        if (dataIt.value().first == "TIME")
                        {
                            QMap<QString,QString>::const_iterator findTime = columnMapping.find(dataIt.value().second);
                            if (findTime !=  columnMapping.end() && symbIdRef == findTime.value())
                            {
                                dependentVariables.remove(symbIdRef);
                                symbIdRef="t";
                            }
                        }
                    }
                }
                operationList.push_back(symbIdRef);
            }
        }
        else if (ops.at(i).nodeName() == "ct:Real" || ops.at(i).nodeName() == "ct:Int")
        {
            QDomNodeList realNode = ops.at(i).childNodes();
            if (realNode.size())
            {
                operationList.push_back(realNode.at(0).nodeValue());
            }
        }
    }
    for (int opi = 0;opi<operationList.size();opi++) {
       operationList[opi] = operationList[opi].trimmed();
    }
    return operationList;
}


void PharmMLLoader08::pharmMLCovariateModelToMlxtran(const QDomNode& node)
{
    QDomNodeList covariatesElements = node.childNodes();
    QString covariateBlockRef = parseQDomNodeAttribute(node,"blkId");
    QSet<QString> covariateBlockIds;
    if (!covariateBlockRef.isEmpty()) covariateBlockIds.insert(covariateBlockRef);
    // parse Covariate Model attributes
    for (int covelt_i = 0;covelt_i < covariatesElements.size();covelt_i++)
    {
        if (covariatesElements.at(covelt_i).nodeName() == "Covariate")
        {
            QString covariateName = parseQDomNodeAttribute(covariatesElements.at(covelt_i),"symbId");
            QDomNodeList typedCovariates = covariatesElements.at(covelt_i).childNodes();
            for (int typedCovariate_i = 0;typedCovariate_i<typedCovariates.size();typedCovariate_i++)
            {
                if (typedCovariates.at(typedCovariate_i).nodeName() == "Continuous")
                {
                    QDomNodeList continuousCovariateElements = typedCovariates.at(typedCovariate_i).childNodes();
                    for (int ccov_i = 0;ccov_i < continuousCovariateElements.size();ccov_i++)
                    {
                        if (continuousCovariateElements.at(ccov_i).nodeName() != "Transformation")
                        {
                            QString lineNumber = QString::number(continuousCovariateElements.at(ccov_i).lineNumber());
                            addMessage(LixoftObject::WARNING,QString("[Covariate bad definition]: does not support '")
                                                              + continuousCovariateElements.at(ccov_i).nodeName()
                                                              + QString("' section at line #")+lineNumber
                                                              + QString(" this section will be ignored and the MLXTRAN model expected")
                                                              + QString(" could be bad."));
                        }
                        else
                        {
                            QDomNodeList tranformationEquations = continuousCovariateElements.at(ccov_i).childNodes();
                            for (int eq_i = 0;eq_i<tranformationEquations.size();eq_i++)
                            {
                                if (tranformationEquations.at(eq_i).nodeName() == "Equation"
                                        || tranformationEquations.at(eq_i).nodeName() == "math:Equation")
                                {
                                    QMap<QString,QVector<QString> > covariateRefs;
                                    QMap<QString,QString> aliases_TEMP;
                                    QSet<QString> dependentVariables;
                                    QVector<QString> equation = pharmlEquationToMlxtran(PharmMLLoader08::COVARIATE_SECTION,tranformationEquations.at(eq_i),"",covariateRefs,dependentVariables,covariateBlockRef, aliases_TEMP);

                                    _continuousCovariates[covariateBlockRef+QString("::")+covariateName] = equation[0];
                                }
                            }
                        }
                    }
                }
                else if (typedCovariates.at(typedCovariate_i).nodeName() == "Categorical")
                {
                    QDomNodeList categorialNodes = typedCovariates.at(typedCovariate_i).childNodes();
                    for (int ci = 0;ci<categorialNodes.size();ci++)
                    {
                        if (categorialNodes.at(ci).nodeName() == "Category")
                        {
                            QMap<QString,QString>& covariateModalities = _categoricalCovariates[covariateBlockRef+QString("_")+covariateName];
                            QString catId = parseQDomNodeAttribute(categorialNodes.at(ci),"catId");
                            QString catName = "";
                            if (categorialNodes.at(ci).childNodes().size()
                                    && categorialNodes.at(ci).firstChild().nodeName() == "ct:Name")
                            {
                                catName = categorialNodes.at(ci).firstChild().firstChild().nodeValue();
                            }
                            covariateModalities[catId] = catName;
                        }
                    }
                }
            }
        }
    }
}

void PharmMLLoader08::pharmMLParameterModelToMlxtran(const QDomNode& node)
{
    QDomNodeList parameterElements = node.childNodes();
    QString parameterBlockRef = parseQDomNodeAttribute(node,"blkId");
    /*
    QSet<QString>& depV = _individualParameters[readIndividualParameter].dependentVariables;
    for (QMap<QString,QString>::const_iterator readCovIt = readCovariates.begin();
         readCovIt !=  readCovariates.end();
         readCovIt++)
    {
        std::cerr<<readCovIt.key().toUtf8().data()<<" "<<readCovIt.value().toUtf8().data()<<std::endl;
        depV.insert(readCovIt.key());
        depV.insert(readCovIt.value());
    }*/
    for (int pi = 0;pi<parameterElements.size();pi++)
    {
        if (parameterElements.at(pi).nodeName() == "IndividualParameter")
        {
            QDomNode individualParameter = parameterElements.at(pi);
            QString readIndividualParameter = parseQDomNodeAttribute(individualParameter,"symbId");
            QString readTransformation = "";
            QString readPopulationParameter = "";
            QMap<QString,QString> readCovariates;
            QVector<QString> readEtas;
            QDomNodeList individualParameterElement = individualParameter.childNodes();
            for (int ipei = 0;ipei<individualParameterElement.size();ipei++)
            {
                if (individualParameterElement.at(ipei).nodeName() == "GaussianModel")
                {
                    QDomNodeList gaussianModelElements = individualParameterElement.at(ipei).childNodes();
                    for (int gmei = 0;gmei<gaussianModelElements.size();gmei++)
                    {
                        if (gaussianModelElements.at(gmei).nodeName() == "Transformation")
                        {
                            QDomNodeList transformationNodes = gaussianModelElements.at(gmei).childNodes();
                            if (transformationNodes.size() == 1)
                            {
                                QString transformation = transformationNodes.at(0).nodeValue();
                                if (transformation != "log" && transformation != "logit" && transformation != "probit")
                                {
                                    throwLater(lixoft::exception::Exception("lixoft::translator::PharmMLLoader08",
                                              (QString("[Individual Parameter bad model]: bad transformation '")
                                               + transformation + QString("' at line #")
                                               + QString::number(gaussianModelElements.at(gmei).lineNumber())
                                               + QString(". The managed transformations are log, logit and probit")).toUtf8().data(),
                                              THROW_LINE_FILE));
                                }
                                else readTransformation = transformation;
                            }
                            else
                            {
                                throwLater(lixoft::exception::Exception("lixoft::translator::PharmMLLoader08",
                                                             (QString("[Individual Parameter bad model]: bad transformation '")
                                                              + gaussianModelElements.at(gmei).nodeName() + QString("' at line #")
                                                              + QString::number(individualParameterElement.at(ipei).lineNumber())).toUtf8().data(),
                                                             THROW_LINE_FILE));

                            }
                        }
                        else if (gaussianModelElements.at(gmei).nodeName() == "LinearCovariate")
                        {
                            QDomNodeList linearCovariates = gaussianModelElements.at(gmei).childNodes();
                            for (int lci = 0;lci<linearCovariates.size();lci++)
                            {
                                if (linearCovariates.at(lci).nodeName() == "PopulationParameter")
                                {
                                    QDomNodeList populationParameter = linearCovariates.at(lci).childNodes();
                                    for (int popi = 0;popi < populationParameter.size();popi++)
                                    {
                                        if (populationParameter.at(popi).nodeName() == "ct:Assign")
                                        {
                                            QDomNodeList populationParameterNode = populationParameter.at(popi).childNodes();
                                            if (populationParameterNode.size() == 1)
                                            {
                                                readPopulationParameter = parseQDomNodeAttribute(populationParameterNode.at(0),"symbIdRef");
                                            }
                                            else
                                            {
                                                addMessage(LixoftObject::WARNING,
                                                           QString("[Individual Parameter bad model]: to many population parameters, section '")
                                                           + linearCovariates.at(lci).nodeName() + QString("' at line #")
                                                           + QString::number(individualParameterElement.at(ipei).lineNumber()));
                                            }
                                        }
                                    }
                                }
                                else if (linearCovariates.at(lci).nodeName() == "Covariate")
                                {
                                    QDomNodeList covariateEltments =  linearCovariates.at(lci).childNodes();
                                    QString rcov;
                                    QVector<QPair<QString,QString> > rbeta;

                                    for (int ci = 0;ci<covariateEltments.size();ci++)
                                    {
                                        if (covariateEltments.at(ci).nodeName() == "ct:SymbRef")
                                        {
                                            rcov = parseQDomNodeAttribute(covariateEltments.at(ci),"blkIdRef").toLower()+parseQDomNodeAttribute(covariateEltments.at(ci),"symbIdRef").toLower();
                                        }
                                        else if (covariateEltments.at(ci).nodeName() == "FixedEffect")
                                        {
                                            QDomNodeList fixedEffectNodes = covariateEltments.at(ci).childNodes();
                                            QPair<QString,QString> catBeta;
                                            for (int fi = 0;fi<fixedEffectNodes.size();fi++)
                                            {
                                                if (fixedEffectNodes.at(fi).nodeName() == "ct:SymbRef")
                                                {
                                                    catBeta.second = parseQDomNodeAttribute(fixedEffectNodes.at(fi),"symbIdRef");
                                                }
                                                /*else if (fixedEffectNodes.at(fi).nodeName() == "Category")
                                             {
                                                 catBeta.first = parseQDomNodeAttribute(fixedEffectNodes.at(fi),"catId");
                                             }*/
                                            }
                                            rbeta.push_back(catBeta);
                                        }
                                    }
                                    for (int rbetai = 0;rbetai<rbeta.size();rbetai++)
                                    {
                                        //   readCovariates[rcov+QString("::")+rbeta[rbetai].first ] = rbeta[rbetai].second;
                                        readCovariates[ rcov ] = rbeta[rbetai].second;
                                    }
                                }
                                else
                                {
                                    addMessage(LixoftObject::WARNING,
                                                QString("[Individual Parameter bad model]: unmanaged section '")
                                                + linearCovariates.at(lci).nodeName() + QString("' at line #")
                                                + QString::number(individualParameterElement.at(ipei).lineNumber()));

                                }
                            }
                        }
                        else if (gaussianModelElements.at(gmei).nodeName() == "RandomEffects")
                        {
                            QDomNodeList randomEffects = gaussianModelElements.at(gmei).childNodes();
                            for (int rei = 0;rei<randomEffects.size();rei++)
                            {
                                if (randomEffects.at(rei).nodeName() == "ct:SymbRef")
                                {
                                    QString etaName = parseQDomNodeAttribute(randomEffects.at(rei),"symbIdRef");
                                    readEtas.push_back(etaName);
                                }
                            }
                        }
                        else if (gaussianModelElements.at(gmei).nodeName() != "#comment")
                        {
                            throwLater(lixoft::exception::Exception("lixoft::translator::PharmMLLoader08",
                                                           (QString("[Individual Parameter bad model]: not managed '")
                                                             + gaussianModelElements.at(gmei).nodeName() + QString("' at line #")
                                                             + QString::number(individualParameterElement.at(ipei).lineNumber())
                                                             + QString(".")).toUtf8().data(),
                                                            THROW_LINE_FILE));
                        }
                    }
                }
                else if (individualParameterElement.at(ipei).nodeName().contains("Assign")
                         && individualParameterElement.at(ipei).firstChild().childNodes().size()
                         && (individualParameterElement.at(ipei).firstChild().firstChild().nodeName()=="Equation"
                             || individualParameterElement.at(ipei).firstChild().firstChild().nodeName()=="math:Equation"))
                {
                    isMonolixCompliant = false;
                    QMap<QString,QVector<QString> > blockRef;
                    QMap<QString,QString> aliases;
                    QVector<QString> eq = pharmlEquationToMlxtran(PharmMLLoader08::INDIVIDUAL_SECTION,individualParameterElement.at(ipei).firstChild(),
                                                          readIndividualParameter,
                                                          blockRef,
                                                          _individualParameters[readIndividualParameter].dependentVariables,
                                                          "",
                                                          aliases);
                    for (int eqi = 0;eqi<eq.size();eqi++)
                    {
                        if (!eq[eqi].contains(QRegExp(readIndividualParameter+QString("\\s*="))))
                        {
                            eq[eqi] =QString(";" + eq[eqi]);
                        }
                        _individualParameters[readIndividualParameter].equation += (eq[eqi] + QString("\n"));
                    }
                    _individualParameters[readIndividualParameter].equation = _individualParameters[readIndividualParameter].equation.trimmed();
                }
            }
            if (readPopulationParameter.length())
            {
                _individualParameters[readIndividualParameter].population = readPopulationParameter;
                _individualParameters[readIndividualParameter].transformation = readTransformation;
                _individualParameters[readIndividualParameter].covariates = readCovariates;
                if (readEtas.size())
                {
                    _individualParameters[readIndividualParameter].omegas = readEtas;
                }                
            }
            _individualParameters[readIndividualParameter].isParameterModel = false;

        }
        else if (parameterElements.at(pi).nodeName() == "SimpleParameter" || parameterElements.at(pi).nodeName() == "Parameter")
        {
            QString symbName = parseQDomNodeAttribute(parameterElements.at(pi),"symbId");
            _individualParametersSymbols.insert(symbName);
            QDomNodeList simpleParameterEquation = parameterElements.at(pi).childNodes();
            for (int spei = 0;spei < simpleParameterEquation.size();spei++)
            {
                if (simpleParameterEquation.at(spei).nodeName().contains("Assign")
                    && simpleParameterEquation.at(spei).firstChild().childNodes().size()
                    && (simpleParameterEquation.at(spei).firstChild().firstChild().nodeName()=="Equation"
                        || simpleParameterEquation.at(spei).firstChild().firstChild().nodeName()=="math:Equation"))
                {

                    QMap<QString,QVector<QString> > blockRef;
                    QMap<QString,QString> aliases;
                    QVector<QString> eq = pharmlEquationToMlxtran(PharmMLLoader08::INDIVIDUAL_SECTION,simpleParameterEquation.at(spei).firstChild(),
                                                          symbName,
                                                          blockRef,
                                                          _individualParameters[symbName].dependentVariables,
                                                          "",
                                                          aliases);
                    for (int eqi = 0;eqi<eq.size();eqi++)
                    {
                        if (!eq[eqi].contains(QRegExp(symbName+QString("\\s*="))))
                        {
                            eq[eqi] = QString(symbName + " = " + eq[eqi]);
                        }
                        _individualParameters[symbName].equation += (eq[eqi] + QString("\n"));
                        _individualParameters[symbName].isParameterModel = true;
                    }
                    _individualParameters[symbName].equation = _individualParameters[symbName].equation.trimmed();
                }
            }
        }
        else if (parameterElements.at(pi).nodeName() == "RandomVariable")
        {
            QDomNode randomVariable = parameterElements.at(pi);
            QDomNodeList randomVariableElement = randomVariable.childNodes();
            QString randomVariableName = parseQDomNodeAttribute(randomVariable,"symbId");
            QString meanValue;
            QString stddev;
            bool isVariance = false;
            QString omegaName;
            _individualParametersSymbols.insert(randomVariableName);
            QVector<QPair<QString,QString> > variabilityReferences;
            for (int rvei = 0;rvei<randomVariableElement.size();rvei++)
            {
                if (randomVariableElement.at(rvei).nodeName() == "ct:VariabilityReference")
                {
                    QDomNodeList variabilityReference = randomVariableElement.at(rvei).childNodes();
                    for (int vri = 0;vri<variabilityReference.size();vri++)
                    {
                        if (variabilityReference.at(vri).nodeName() == "ct:SymbRef")
                        {
                            QPair<QString,QString> vr;
                            vr.first = parseQDomNodeAttribute(variabilityReference.at(vri),"blkIdRef");
                            vr.second = parseQDomNodeAttribute(variabilityReference.at(vri),"symbIdRef");
                            variabilityReferences.push_back(vr);
                        }
                    }
                }
                else if (randomVariableElement.at(rvei).nodeName() == "NormalDistribution")
                {
                    QDomNodeList normalDistributionElement = randomVariableElement.at(rvei).childNodes();
                    for (int ndi = 0;ndi<normalDistributionElement.size();ndi++)
                    {
                        if (normalDistributionElement.at(ndi).nodeName() == "mean")
                        {
                            QDomNodeList meanValues = normalDistributionElement.at(ndi).childNodes();
                            if (meanValues.size() == 1 && meanValues.at(0).nodeName() == "var")
                            {
                                meanValue = parseQDomNodeAttribute(meanValues.at(0),"varId");
                                _individualParametersSymbols.insert(meanValue);
                            }
                            else if (meanValues.size() == 1 && meanValues.at(0).nodeName() == "rVal")
                                meanValue = meanValues.at(0).firstChild().nodeValue();

                        }
                        else if (normalDistributionElement.at(ndi).nodeName() == "stddev")
                        {
                            QDomNodeList sdValue = normalDistributionElement.at(ndi).childNodes();
                            if (sdValue.size() == 1 && sdValue.at(0).nodeName() == "var")
                            {
                                stddev = parseQDomNodeAttribute(sdValue.at(0),"varId");
                                omegaName = stddev;
                                _individualParametersSymbols.insert(stddev);
                            }
                            else if (sdValue.size() == 1 && sdValue.at(0).nodeName() == "rVal")
                                stddev = sdValue.at(0).firstChild().nodeValue();
                        }
                        else if (normalDistributionElement.at(ndi).nodeName() == "variance")
                        {
                            QDomNodeList sdValue = normalDistributionElement.at(ndi).childNodes();
                            if (sdValue.size() == 1 && sdValue.at(0).nodeName() == "var")
                            {
                                stddev = parseQDomNodeAttribute(sdValue.at(0),"varId");
                                _individualParametersSymbols.insert(stddev);
                                omegaName = stddev;
                                isVariance = true;
                            }
                            else if (sdValue.size() == 1 && sdValue.at(0).nodeName() == "rVal")
                            {
                                stddev = sdValue.at(0).firstChild().nodeValue();
                                isVariance = true;
                            }

                        }

                    }
                }
                else
                {
                    throwLater(lixoft::exception::Exception("lixoft::translator::PharmMLLoader08",
                                                            (QString("[Individual Parameter bad model]: Random Variable element '")
                                                             + randomVariableElement.at(rvei).nodeName() + QString("' at line #")
                                                             + QString::number(randomVariableElement.at(rvei).lineNumber())
                                                             + QString(", is not managed by Monolix.")).toUtf8().data(),
                                                            THROW_LINE_FILE));
                }
            }
            _randomVariables[randomVariableName].mean = meanValue;
            _randomVariables[randomVariableName].isVariance = isVariance;
            _randomVariables[randomVariableName].omegaName = omegaName;
            _randomVariables[randomVariableName].stddev = stddev;
            _randomVariables[randomVariableName].variability = variabilityReferences;

        }
        else if (parameterElements.at(pi).nodeName() == "Correlation")
        {
            QString eta1;
            QString eta2;
            QString varBlkIdRef;
            QString varSymIdRef;
            QDomNodeList correlationElement = parameterElements.at(pi).childNodes();
            for (int cei = 0;cei<correlationElement.size();cei++)
            {
                if (correlationElement.at(cei).nodeName()=="ct:VariabilityReference")
                {
                    if (correlationElement.at(cei).childNodes().size()==1
                            && correlationElement.at(cei).firstChild().nodeName() == "ct:SymbRef")
                    {
                        QDomNode etaNode = correlationElement.at(cei).firstChild();
                        varBlkIdRef = parseQDomNodeAttribute(etaNode,"blkIdRef");
                        varSymIdRef = parseQDomNodeAttribute(etaNode,"symbIdRef");

                    }
                }
                else if (correlationElement.at(cei).nodeName()=="RandomVariable1")
                {
                    if (correlationElement.at(cei).childNodes().size()==1
                            && correlationElement.at(cei).firstChild().nodeName() == "ct:SymbRef")
                    {
                        QDomNode etaNode = correlationElement.at(cei).firstChild();
                        eta1 = parseQDomNodeAttribute(etaNode,"symbIdRef");

                    }
                }
                else if (correlationElement.at(cei).nodeName()=="RandomVariable2")
                {
                    if (correlationElement.at(cei).childNodes().size()==1
                            && correlationElement.at(cei).firstChild().nodeName() == "ct:SymbRef")
                    {
                        QDomNode etaNode = correlationElement.at(cei).firstChild();
                        eta2 = parseQDomNodeAttribute(etaNode,"symbIdRef");
                    }
                }
                else if (correlationElement.at(cei).nodeName()=="CorrelationCoefficient")
                {
                    addMessage(LixoftObject::WARNING,
                                QString("[Individual Parameter : Correlation]: not supported correlation coefficient settings through a project. ")
                                + QString("Section '")+ correlationElement.at(cei).nodeName() + QString("' at line #")
                                + QString::number(correlationElement.at(cei).lineNumber()));
                    if (correlationElement.at(cei).childNodes().size()==1
                            && correlationElement.at(cei).firstChild().nodeName().contains("Real"))
                    {

                    }
                }
            }
            PharmMLLoader08::ParameterCorrelation pc;
            pc.randomVariable1 = eta1;
            pc.randomVariable2 = eta2;
            pc.blkRef = varBlkIdRef;
            pc.blkSymb = varSymIdRef;
            _parameterCorrelation.push_back(pc);
        }
        else if (parameterElements.at(pi).nodeName() == "SimpleParameter" || parameterElements.at(pi).nodeName() == "Parameter")
        { /* Skip */ }
        else if (parameterElements.at(pi).nodeName() == "#comment")
        { /* Skip */ }
        else
        {
            addMessage( LixoftObject::WARNING,
                        QString("[Parameter Model]: Unsupported section '")
                        + parameterElements.at(pi).nodeName()
                        + QString("' section at line #")+QString::number(parameterElements.at(pi).lineNumber())
                        + QString(" this section will be ignored and the model expected")
                        + QString(" could be bad."));

        }
    }
}

void PharmMLLoader08::pharmMLStructuralModelToMlxtran(const QDomNode& node)
{
    QMap<QString,QString> dosingVariables;
    QString blkIdRefTmp;
    QMap<QString,QString> aliasesTmp;


    for (QMap<QString,PharmMLLoader08::Dosing>::const_iterator dit = _dosingVariables.begin();dit!=_dosingVariables.end();dit++)
    {
        if (dit.value().dosingTypes == "doseAmount" && (dit.value().inputTarget=="dose" || dit.value().inputTarget=="parameter"))
        {
            aliasesTmp[dit.key()] = "amtDose";
        }
        else if (dit.value().dosingTypes == "dosingTimes")
        {
            aliasesTmp[dit.key()] = "tDose";
        }
    }

    // first parse variables
    QDomNodeList structuralEquations = node.childNodes();
    for (int seqi = 0;seqi<structuralEquations.size();seqi++)
    {
        if(structuralEquations.at(seqi).nodeName() == "ct:Variable"
                || structuralEquations.at(seqi).nodeName() == "SimpleParameter"
                || structuralEquations.at(seqi).nodeName() == "Parameter")

        {
            QString variableAssigned = parseQDomNodeAttribute(structuralEquations.at(seqi),"symbId");            
            QDomNodeList variableElements = structuralEquations.at(seqi).childNodes();
            if (variableElements.size() == 0) // inputs or dosing
            {
                if (_dosingVariables.contains(variableAssigned))
                {
                    QMap<QString,PharmMLLoader08::Dosing>::const_iterator findIt = _dosingVariables.find(variableAssigned);
                    if (findIt.value().dosingTypes == "dosingTimes") dosingVariables[variableAssigned] = "";
                    else if (findIt.value().dosingTypes == "doseAmount") dosingVariables[variableAssigned] = "";
                }
            }
        }
    }
    int countSM = 0;
    for (int seqi = 0;seqi<structuralEquations.size();seqi++)
    {
        if (structuralEquations.at(seqi).nodeName() == "ct:DerivativeVariable")
        {
            QDomNodeList derivativeElements = structuralEquations.at(seqi).childNodes();
            QString odeComponent = parseQDomNodeAttribute(structuralEquations.at(seqi),"symbId");
            for (int dei = 0;dei < derivativeElements.size();dei++)
            {
                if (derivativeElements.at(dei).nodeName() == "ct:Description")
                {
                    QDomNodeList descNodes = derivativeElements.at(dei).childNodes();
                    if (descNodes.size())
                    {
                        _structuralModelEquations[QString("ddt_")+odeComponent].description = descNodes.at(0).nodeValue();
                    }
                }
                else if (derivativeElements.at(dei).nodeName() == "ct:IndependentVariable")
                {

                }
                else if (derivativeElements.at(dei).nodeName() == "ct:InitialCondition")
                {
                    QDomNodeList initialConditionElements = derivativeElements.at(dei).childNodes();
                    for (int ici = 0;ici < initialConditionElements.size();ici++)
                    {
                        if (initialConditionElements.at(ici).nodeName() == "ct:Assign")
                        {
                            QMap<QString,QVector<QString> > blockRef;
                            QSet<QString> dependentVariables;
                            QVector<QString> equation = pharmlEquationToMlxtran(PharmMLLoader08::LONGITUDINAL_SECTION,initialConditionElements.at(ici),"",blockRef, dependentVariables, blkIdRefTmp, aliasesTmp);
                            if (equation.size()) {
                                QString i1 = (odeComponent+QString("_0")).trimmed();
                                QString i2 = equation[0].trimmed();
                                if (i1 != i2) {
                                    _structuralModelEquations[odeComponent+QString("_0")].equation = equation[0];
                                }

                            }

                            /*
                            QDomNodeList initialValue = initialConditionElements.at(ici).childNodes();
                            for (int ivi = 0;ivi < initialValue.size();ivi++)
                            {
                                if (initialValue.at(ivi).nodeName().contains("Real"))
                                {
                                    QDomNodeList realNode = initialValue.at(ivi).childNodes();
                                    if (realNode.size())
                                        _structuralModelEquations[odeComponent+QString("_0")].equation = realNode.at(0).nodeValue();

                                }
                                else if (initialValue.at(ivi).nodeName() == "ct:SymbRef")
                                {
                                    //QString derivativeInitialCondBlkId = parseQDomNodeAttribute(initialValue.at(ivi),"blkIdRef");
                                    QString derivativeInitialCondSymb = parseQDomNodeAttribute(initialValue.at(ivi),"symbIdRef");
                                    _structuralModelEquations[odeComponent+QString("_0")].equation = derivativeInitialCondSymb;
                                  _structuralModelEquations[odeComponent+QString("_0")].initialODEParameter = derivativeInitialCondSymb;

                                }
                                else if (initialValue.at(ivi).nodeName() == "Equation" || initialValue.at(ivi).nodeName() == "math:Equation")
                                {
                                    QMap<QString,QVector<QString> > blockRef;
                                    QSet<QString> dependentVariables;
                                    QVector<QString> equation = pharmlEquationToMlxtran(PharmMLLoader08::LONGITUDINAL_SECTION,initialValue.at(ivi),"",blockRef, dependentVariables, blkIdRefTmp, aliasesTmp);
                                    if (equation.size())
                                        _structuralModelEquations[odeComponent+QString("_0")].equation = equation[0];
                                }
                            }
                            */
                        }
                        if (initialConditionElements.at(ici).nodeName() == "ct:InitialValue")
                        {
                            QDomNodeList initialValue= initialConditionElements.at(ici).childNodes();
                            for (int ivi = 0;ivi<initialValue.size();ivi++)
                            {                                                         
                                if (initialValue.at(ivi).nodeName() == "ct:Assign")
                                {
                                    QMap<QString,QVector<QString> > blockRef;
                                    QSet<QString> dependentVariables;
                                    QVector<QString> equation = pharmlEquationToMlxtran(PharmMLLoader08::LONGITUDINAL_SECTION,initialValue.at(ivi),"",blockRef,dependentVariables, blkIdRefTmp, aliasesTmp);
                                    if (equation.size()) {
                                        QString i1 = (odeComponent+QString("_0")).trimmed();
                                        QString i2 = equation[0].trimmed();
                                        if ( i1 != i2 ) {
                                            _structuralModelEquations[odeComponent+QString("_0")].equation = equation[0];
                                        }
                                    }
                                    /*
                                    QDomNodeList theValue = initialValue.at(ivi).childNodes();
                                    for (int tvi = 0;tvi < theValue.size();tvi++)
                                    {
                                        if (theValue.at(tvi).nodeName() == "ct:Real")
                                        {
                                            QDomNodeList realNode = theValue.at(tvi).childNodes();
                                            if (realNode.size())
                                                _structuralModelEquations[odeComponent+QString("_0")].equation = realNode.at(0).nodeValue();

                                        }
                                        else if (theValue.at(tvi).nodeName() == "ct:SymbRef")
                                        {
                                            //QString derivativeInitialCondBlkId = parseQDomNodeAttribute(initialValue.at(ivi),"blkIdRef");
                                            QString derivativeInitialCondSymb = parseQDomNodeAttribute(theValue.at(tvi),"symbIdRef");
                                            _structuralModelEquations[odeComponent+QString("_0")].equation = derivativeInitialCondSymb;
                                        _structuralModelEquations[odeComponent+QString("_0")].initialODEParameter = derivativeInitialCondSymb;

                                        }
                                        else if (theValue.at(tvi).nodeName() == "Equation" || theValue.at(tvi).nodeName() == "math:Equation")
                                        {
                                            QMap<QString,QVector<QString> > blockRef;
                                            QSet<QString> dependentVariables;
                                            QVector<QString> equation = pharmlEquationToMlxtran(PharmMLLoader08::LONGITUDINAL_SECTION,theValue.at(tvi),"",blockRef,dependentVariables, blkIdRefTmp, aliasesTmp);
                                            if (equation.size())
                                                _structuralModelEquations[odeComponent+QString("_0")].equation = equation[0];
                                        }

                                    }*/
                                }
                            }
                        }
                    }
                }
                else if (derivativeElements.at(dei).nodeName() == "ct:Assign")
                {
                    QSet<QString> dependentVariables;
                    QVector<QString> equation = pharmlEquationToMlxtran(PharmMLLoader08::LONGITUDINAL_SECTION,derivativeElements.at(dei),"",_structuralModelParameters,dependentVariables, blkIdRefTmp, aliasesTmp);
                    for (int ei = 0;ei<equation.size();ei++)
                    {
                        QString addBolus = "";
                        if (_dosingVariables.contains(odeComponent))
                        {
                            QMap<QString,PharmMLLoader08::Dosing>::iterator findIt = _dosingVariables.find(odeComponent);
                            if (findIt.value().dosingTypes == "doseAmount" && (findIt.value().inputTarget == "target" || findIt.value().inputTarget == "derivativeVariable")) // add PK
                            {
                                dosingVariables[odeComponent] = "done";
                                _pkBlock+=(QString("compartment(amount=") + odeComponent + QString(")\niv()\n"));
                            }
                        }
                        QString assignedVariable = QString("ddt_")+odeComponent;
                        _structuralModelEquations[assignedVariable].equation = equation[ei] + addBolus;
                    }

                   /* for (int aei = 0;aei<assignedElements.size();aei++)
                    {

                        if (assignedElements.at(aei).nodeName() == "Equation" || assignedElements.at(aei).nodeName() == "math:Equation")
                        {
                            QSet<QString> dependentVariables;
                            QVector<QString> equation = pharmlEquationToMlxtran(PharmMLLoader08::LONGITUDINAL_SECTION,assignedElements.at(aei),"",_structuralModelParameters,dependentVariables, blkIdRefTmp, aliasesTmp);
                            for (int ei = 0;ei<equation.size();ei++)
                            {
                                QString addBolus = "";
                                if (_dosingVariables.contains(odeComponent))
                                {
                                    QMap<QString,PharmMLLoader08::Dosing>::iterator findIt = _dosingVariables.find(odeComponent);
                                    if (findIt.value().dosingTypes == "doseAmount" && (findIt.value().inputTarget == "target" || findIt.value().inputTarget == "derivativeVariable")) // add PK
                                    {
                                        dosingVariables[odeComponent] = "done";
                                        _pkBlock+=(QString("compartment(amount=") + odeComponent + QString(")\niv()\n"));
                                    }
                                }
                                QString assignedVariable = QString("ddt_")+odeComponent;
                                _structuralModelEquations[assignedVariable].equation = equation[ei] + addBolus;
                            }
                        }
                        else if (assignedElements.at(aei).nodeName() == "ct:Real")
                        {
                            QString readRealValue;
                            QString assignedVariable = QString("ddt_")+odeComponent;
                            readRealValue = assignedElements.at(aei).firstChild().firstChild().nodeValue();
                            QString addBolus = "";
                            if (_dosingVariables.contains(odeComponent))
                            {
                                QMap<QString,PharmMLLoader08::Dosing>::const_iterator findIt = _dosingVariables.find(odeComponent);
                                if (findIt.value().dosingTypes == "doseAmount" && (findIt.value().inputTarget == "target" || findIt.value().inputTarget == "derivativeVariable"))
                                {
                                    dosingVariables[odeComponent] = "done";
                                    _pkBlock+=(QString("depot(target=") + odeComponent + QString(")\n"));
                                }

                            }
                            _structuralModelEquations[assignedVariable].equation = readRealValue + addBolus;
                        }
                    }*/
                }
            }
        }
        else if(structuralEquations.at(seqi).nodeName() == "ct:Variable"
                || structuralEquations.at(seqi).nodeName() == "SimpleParameter"
                || structuralEquations.at(seqi).nodeName() == "Parameter" )
        {
            QDomNodeList variableElements = structuralEquations.at(seqi).childNodes();
            QString variableAssigned = parseQDomNodeAttribute(structuralEquations.at(seqi),"symbId");            
            for (int vei = 0;vei < variableElements.size();vei++)
            {
                if (variableElements.at(vei).nodeName() == "ct:Assign")
                {
                    QDomNodeList assignedElements = variableElements.at(vei).childNodes();
                    for (int aei = 0;aei<assignedElements.size();aei++)
                    {
                        if (assignedElements.at(aei).nodeName() == "Equation"
                                || assignedElements.at(aei).nodeName() == "math:Equation")
                        {
                            QSet<QString> dependentVariables;
                            QVector<QString> equation = pharmlEquationToMlxtran(PharmMLLoader08::LONGITUDINAL_SECTION,assignedElements.at(aei),variableAssigned,_structuralModelParameters,dependentVariables,blkIdRefTmp, aliasesTmp);
                            for (int ei = 0;ei<equation.size();ei++)
                            {
                                _structuralModelEquations["#ISASSIGNED"+QString::number(countSM)+variableAssigned].equation = equation[ei];
                                countSM++;
                            }
                        }
                        else if (assignedElements.at(aei).nodeName() == "ct:Real")
                        {
                            QString readRealValue;
                            readRealValue = assignedElements.at(aei).firstChild().nodeValue();
                            _structuralModelEquations[variableAssigned].equation = readRealValue;
                        }
                    }
                }
            }
        }
    }
    //
    for (QMap<QString,PharmMLLoader08::Dosing>::const_iterator dit = _dosingVariables.begin();
         dit != _dosingVariables.end();dit++)
    {
        QString dv = dosingVariables[dit.key()];
        if (dv.isEmpty() && (dit.value().inputTarget == "target" || dit.value().inputTarget == "derivativeVariable"))
        {
            _structuralModelEquations[QString("ddt_")+dit.key()].equation = "0";
            _pkBlock+=(QString("depot(target=") + dit.key() + QString(")\n"));
        }

    }
}



void PharmMLLoader08::pharmMLObservationModelToMlxtran(const QDomNode& node)
{
    QDomNodeList observationModelNodes = node.childNodes();
    QString observationModelRef = parseQDomNodeAttribute(node,"blkId");
    QString blkIdRefTmp;
    for (int oi = 0;oi<observationModelNodes.size();oi++)
    {
        if (observationModelNodes.at(oi).nodeName() == "RandomVariable")
        {
            QDomNode randomVariable = observationModelNodes.at(oi);
            QDomNodeList randomVariableElement = randomVariable.childNodes();
            QString randomVariableName = parseQDomNodeAttribute(randomVariable,"symbId");
            QString meanValue;
            QString stddev;
            bool isMeanVariable = false;
            bool isStdDevVariable = false;
            QVector<QPair<QString,QString> > variabilityReferences;
            for (int rvei = 0;rvei<randomVariableElement.size();rvei++)
            {
                if (randomVariableElement.at(rvei).nodeName() == "ct:VariabilityReference")
                {
                    QDomNodeList variabilityReference = randomVariableElement.at(rvei).childNodes();
                    for (int vri = 0;vri<variabilityReference.size();vri++)
                    {
                        if (variabilityReference.at(vri).nodeName() == "ct:SymbRef")
                        {
                            QPair<QString,QString> vr;
                            vr.first = parseQDomNodeAttribute(variabilityReference.at(vri),"blkIdRef");
                            vr.second = parseQDomNodeAttribute(variabilityReference.at(vri),"symbIdRef");
                            variabilityReferences.push_back(vr);
                        }
                    }
                }
                else if (randomVariableElement.at(rvei).nodeName() == "NormalDistribution")
                {
                    QDomNodeList normalDistributionElement = randomVariableElement.at(rvei).childNodes();
                    for (int ndi = 0;ndi<normalDistributionElement.size();ndi++)
                    {
                        if (normalDistributionElement.at(ndi).nodeName() == "mean")
                        {
                            QDomNodeList meanValues = normalDistributionElement.at(ndi).childNodes();
                            if (meanValues.size() == 1 && meanValues.at(0).nodeName() == "var")
                            {
                                meanValue = parseQDomNodeAttribute(meanValues.at(0),"varId");
                                isMeanVariable = true;
                            }
                            else if (meanValues.size() == 1 && meanValues.at(0).nodeName() == "rVal")
                                meanValue = meanValues.at(0).firstChild().nodeValue();
                        }
                        else if (normalDistributionElement.at(ndi).nodeName() == "stddev")
                        {
                            QDomNodeList sdValue = normalDistributionElement.at(ndi).childNodes();
                            if (sdValue.size() == 1 && sdValue.at(0).nodeName() == "var")
                            {
                                stddev = parseQDomNodeAttribute(sdValue.at(0),"varId");
                                isStdDevVariable = true;
                            }
                            else if (sdValue.size() == 1 && sdValue.at(0).nodeName() == "rVal")
                                stddev = sdValue.at(0).firstChild().nodeValue();

                        }
                        else if (normalDistributionElement.at(ndi).nodeName() == "varance")
                        {
                            QDomNodeList sdValue = normalDistributionElement.at(ndi).childNodes();
                            if (sdValue.size() == 1 && sdValue.at(0).nodeName() == "var")
                            {
                                stddev = parseQDomNodeAttribute(sdValue.at(0),"varId");
                                isStdDevVariable = true;
                            }
                            else if (sdValue.size() == 1 && sdValue.at(0).nodeName() == "rVal")
                                stddev = sdValue.at(0).firstChild().nodeValue();
                        }
                    }
                }
                else if (randomVariableElement.at(rvei).nodeName() == "#comment")
                {
                    /* skip */
                }
                else
                {
                    throwLater(lixoft::exception::Exception("lixoft::translator::PharmMLLoader08",
                                                            (QString("[Observation model]: Random Variable element '")
                                                              + randomVariableElement.at(rvei).nodeName() + QString("' at line #")
                                                              + QString::number(randomVariableElement.at(rvei).lineNumber())
                                                              + QString(", is not managed by Monolix.")).toUtf8().data(),
                                                            THROW_LINE_FILE));
                }
            }
            if (isMeanVariable)
            {
                addMessage(LixoftObject::WARNING,
                            QString("[Observation model]: Random Variable element mean should be 0.0 in section '")
                            + observationModelNodes.at(oi).nodeName() + QString("' at line #")
                            + QString::number(observationModelNodes.at(oi).lineNumber())
                            + QString(". Mean value is forced to 0.0, the model expected could be bad."));
                meanValue = "0.0";

            }
            else
            {
                double mv = meanValue.toDouble();
                if (fabs(mv) > std::numeric_limits<double>::epsilon())
                {
                    addMessage(LixoftObject::WARNING,
                                QString("[Observation model]: Random Variable element mean should be 0.0, the current value is '")
                                + meanValue
                                + QString (" in section '")
                                + observationModelNodes.at(oi).nodeName() + QString("' at line #")
                                + QString::number(observationModelNodes.at(oi).lineNumber())
                                + QString(". Mean value is forced to 0.0, the model expected could be bad."));
                    meanValue = "0.0";

                }
            }
            if (isStdDevVariable)
            {
                addMessage( LixoftObject::WARNING,
                            QString("[Observation model]: Random Variable element stddev should be 1.0 in section '")
                             + observationModelNodes.at(oi).nodeName() + QString("' at line #")
                             + QString::number(observationModelNodes.at(oi).lineNumber())
                             + QString(". StdDev value is forced to 1.0, the model expected could be bad."));
                stddev = "1.0";

            }
            else
            {
                double stdv = stddev.toDouble();
                if (fabs(stdv-1.0) > std::numeric_limits<double>::epsilon())
                {
                    addMessage( LixoftObject::WARNING,
                                QString("[Observation model]: Random Variable element stddev should be 1.0, the current value is '")
                                + stddev
                                + QString (" in section '")
                                + observationModelNodes.at(oi).nodeName() + QString("' at line #")
                                + QString::number(observationModelNodes.at(oi).lineNumber())
                                + QString(". Stdev value is forced to 1.0, the model expected could be bad."));
                    stddev = "1.0";
                }
            }
            _observationRandomEffects.push_back(QPair<QString,QString>(
                                    QString("__X_")+randomVariableName + QString("= distribution=normal, mean=0, sd=1.0 }\n"),
                                    randomVariableName + QString(" = ") + meanValue
                                    + QString("+ __X_")+randomVariableName + QString("*sqrt(") + stddev + QString(")")
                                   ));
        }
        else if (observationModelNodes.at(oi).nodeName() == "General")
        {
            isMonolixCompliant = false;
            addMessage(LixoftObject::WARNING,
                        QString("[Observation model]: General error model is not managed by Monolix. Section '")
                        + observationModelNodes.at(oi).nodeName() + QString("' at line #")
                        + QString::number(observationModelNodes.at(oi).lineNumber())
                       );
            QString obsModelName = parseQDomNodeAttribute(observationModelNodes.at(oi),"symbId");
            QMap<QString,QString> aliasesTmp;
            int countSM = 0;
            QDomNodeList assignList = observationModelNodes.at(oi).childNodes();
            for (int assi = 0;assi<assignList.size();assi++)
            {
                if (assignList.at(assi).nodeName() == "ct:Assign")
                {
                    QDomNodeList assignedElements = assignList.at(assi).childNodes();
                    for (int aei = 0;aei<assignedElements.size();aei++)
                    {
                        if (assignedElements.at(aei).nodeName() == "Equation"
                                || assignedElements.at(aei).nodeName() == "math:Equation")
                        {
                            QSet<QString> dependentVariables;
                            QVector<QString> equation = pharmlEquationToMlxtran(PharmMLLoader08::LONGITUDINAL_SECTION,
                                                                                assignedElements.at(aei),
                                                                                obsModelName,
                                                                                _structuralModelParameters,
                                                                                dependentVariables,blkIdRefTmp, aliasesTmp);
                            for (int ei = 0;ei<equation.size();ei++)
                            {
                                _structuralModelEquations["#ISASSIGNED"+QString::number(countSM)+obsModelName].equation = equation[ei];
                                countSM++;
                            }
                        }
                        else if (assignedElements.at(aei).nodeName() == "ct:Real")
                        {
                            QString readRealValue;
                            readRealValue = assignedElements.at(aei).firstChild().nodeValue();
                            _structuralModelEquations[obsModelName].equation = readRealValue;
                        }
                    }
                }
            }
        }
        else if (observationModelNodes.at(oi).nodeName() == "Standard")
        {
            QDomNodeList standadObsNodes = observationModelNodes.at(oi).childNodes();
            QVector<QString> errorModelParameters;
            QString pharmMLErrorModel;
            QString outputVariable;
            for (int soi = 0;soi<standadObsNodes.size();soi++)
            {
                if (standadObsNodes.at(soi).nodeName() == "Output")
                {
                    QDomNodeList outputNodes = standadObsNodes.at(soi).childNodes();
                    for (int outi = 0;outi<outputNodes.size();outi++)
                    {
                        if (outputNodes.at(outi).nodeName() == "ct:SymbRef")
                        {
                            outputVariable = parseQDomNodeAttribute(outputNodes.at(outi),"symbIdRef");
                            _structuralModelOutputs.push_back(outputVariable);
                        }
                    }
                }
                else if (standadObsNodes.at(soi).nodeName() == "ErrorModel")
                {
                    QDomNodeList errorModel = standadObsNodes.at(soi).childNodes();
                    for (int emi = 0;emi<errorModel.size();emi++)
                    {
                        if (errorModel.at(emi).nodeName() == "ct:Assign")
                        {
                            QDomNodeList errorModelAssign = errorModel.at(emi).childNodes();
                            for (int emai = 0;emai<errorModelAssign.size();emai++)
                            {
                                if (errorModelAssign.at(emai).nodeName() == "Equation"
                                        || errorModelAssign.at(emai).nodeName() == "math:Equation")
                                {
                                    QDomNodeList errorModelEq = errorModelAssign.at(emai).childNodes();
                                    for (int emei = 0;emei<errorModelEq.size();emei++)
                                    {
                                        if (errorModelEq.at(emei).nodeName() == "FunctionCall"
                                                || errorModelEq.at(emei).nodeName() == "math:FunctionCall")
                                        {
                                            QDomNodeList errorModelFunCall = errorModelEq.at(emei).childNodes();
                                            for (int emfci = 0;emfci<errorModelFunCall.size();emfci++)
                                            {
                                                if (errorModelFunCall.at(emfci).nodeName()=="ct:SymbRef")
                                                {
                                                    pharmMLErrorModel = parseQDomNodeAttribute(errorModelFunCall.at(emfci),"symbIdRef");
                                                }
                                                else if (errorModelFunCall.at(emfci).nodeName() == "math:FunctionArgument"
                                                         || errorModelFunCall.at(emfci).nodeName() == "FunctionArgument")
                                                {
                                                    QString outputArgument = parseQDomNodeAttribute(errorModelFunCall.at(emfci),"symbId");
                                                    QDomNodeList functionArguments = errorModelFunCall.at(emfci).childNodes();
                                                    QString blockRef;
                                                    for (int fai = 0;fai<functionArguments.size();fai++)
                                                    {
                                                        blockRef = parseQDomNodeAttribute(functionArguments.at(fai),"blkIdRef");
                                                    }
                                                    if (blockRef.isEmpty())
                                                        errorModelParameters.push_back(outputArgument);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if (standadObsNodes.at(soi).nodeName() == "ResidualError")
                {

                }
                _errorModel[observationModelRef+"::"+outputVariable] = pharmMLErrorModel;
            }
            if (errorModelParameters.size())
            {
                _errorModelParameters[observationModelRef] = errorModelParameters;
            }
        }
        else if (observationModelNodes.at(oi).nodeName() == "Discrete")
        {
            Categorical& categoricalModel = _discreteModels[observationModelRef];
            QDomNodeList discreteModelsNodes = observationModelNodes.at(oi).childNodes();
            for (int dmi = 0;dmi<discreteModelsNodes.size();dmi++)
            {
                if (discreteModelsNodes.at(dmi).nodeName() == "CategoricalData")
                {
                    QDomNodeList categoricalDataList = discreteModelsNodes.at(dmi).childNodes();
                    for (int cdi = 0;cdi<categoricalDataList.size();cdi++)
                    {
                        if (categoricalDataList.at(cdi).nodeName() == "ListOfCategories")
                        {
                            QDomNodeList listOfCategories = categoricalDataList.at(cdi).childNodes();
                            for (int loci = 0;loci<listOfCategories.size();loci++)
                            {
                                if (listOfCategories.at(loci).nodeName()=="Category")
                                {
                                    QString category = parseQDomNodeAttribute(listOfCategories.at(loci),"symbId");
                                    if (!categoricalModel.modalitiesAlias.contains(category))
                                    {
                                        categoricalModel.modalitiesAlias[category] = category;
                                    }
                                }
                            }
                        }
                        else if (categoricalDataList.at(cdi).nodeName() == "SimpleParameter" || categoricalDataList.at(cdi).nodeName() == "Parameter")
                        {
                            QString simpleParameter = parseQDomNodeAttribute(categoricalDataList.at(cdi),"symbId");
                            categoricalModel.simpleParameters.insert(simpleParameter);
                        }
                        else if (categoricalDataList.at(cdi).nodeName() == "CategoryVariable")
                        {
                            QString categoryVariable = parseQDomNodeAttribute(categoricalDataList.at(cdi),"symbId");
                            categoricalModel.categoryVariable = categoryVariable;
                        }
                        else if (categoricalDataList.at(cdi).nodeName() == "ProbabilityAssignment")
                        {

                        }
                        else if (categoricalDataList.at(cdi).nodeName() == "PMF")
                        {
                            if (categoricalDataList.at(cdi).firstChild().nodeName().contains("Distribution")){/*
                                && categoricalDataList.at(cdi).firstChild().firstChild().nodeName().contains("ProbOnto")
                                && categoricalDataList.at(cdi).firstChild().firstChild().firstChild().nodeName().contains("Parameter")
                                && categoricalDataList.at(cdi).firstChild().firstChild().firstChild().firstChild().nodeName().contains("Assign")
                                && categoricalDataList.at(cdi).firstChild().firstChild().firstChild().firstChild().firstChild().nodeName().contains("Vector")
                                    && categoricalDataList.at(cdi).firstChild().firstChild().firstChild().firstChild().firstChild().nodeName().contains("VectorElement")
                                    ) {*/
                            }
                        }
                    }
                }
                else if (discreteModelsNodes.at(dmi).nodeName() == "CountData")
                {
                    std::cerr<<"CountDATA";
                    QDomNodeList countDataList = observationModelNodes.at(oi).childNodes();
                    for (int cdl = 0;cdl < countDataList.size();cdl++) {

                    }
                }
                else if (discreteModelsNodes.at(dmi).nodeName() == "TimeToEventData")
                {
                    std::cerr<<"TTE"<<std::endl;
                }
            }
            // Binomial

            // Discrete Markov
            // Categorical (count)
            // Continuous Markov

            // TTE
        }
    }
}

void PharmMLLoader08::pharmMLEstimationTaskToMlxtran(const QDomNode& node)
{
    QDomNodeList modelingStep = node.childNodes();
    for (int mi = 0;mi<modelingStep.size();mi++)
    {
        if (modelingStep.at(mi).nodeName() == "EstimationStep")
        {
            QDomNodeList estimationStep = modelingStep.at(mi).childNodes();
            for (int ei = 0;ei<estimationStep.size();ei++)
            {
                if (estimationStep.at(ei).nodeName().contains("ParametersToEstimate"))
                {
                    QDomNodeList parameterToEstimate = estimationStep.at(ei).childNodes();
                    for (int pi = 0;pi<parameterToEstimate.size();pi++)
                    {
                        if (parameterToEstimate.at(pi).nodeName().contains("ParameterEstimation"))
                        {
                            QString readParameter;
                            QString blockRef;
                            QString fixed;
                            QString readValue = "0.0";
                            QDomNodeList parameterEstimation = parameterToEstimate.at(pi).childNodes();
                            for (int pei = 0;pei<parameterEstimation.size();pei++)
                            {
                                if (parameterEstimation.at(pei).nodeName()=="ct:SymbRef")
                                {
                                    readParameter = parseQDomNodeAttribute(parameterEstimation.at(pei),"symbIdRef");
                                    blockRef = parseQDomNodeAttribute(parameterEstimation.at(pei),"blkIdRef");
                                }
                                else if (parameterEstimation.at(pei).nodeName().contains("InitialEstimate"))
                                {

                                    fixed = parseQDomNodeAttribute(parameterEstimation.at(pei),"fixed");
                                    if (parameterEstimation.at(pei).firstChild().nodeName().contains("Real")
                                        || parameterEstimation.at(pei).firstChild().nodeName().contains("Int"))
                                    {
                                        readValue = parameterEstimation.at(pei).firstChild().firstChild().nodeValue();
                                    }
                                    else if (parameterEstimation.at(pei).firstChild().nodeName().contains("Equation") &&
                                             (parameterEstimation.at(pei).firstChild().firstChild().nodeName().contains("Real")
                                              || parameterEstimation.at(pei).firstChild().firstChild().nodeName().contains("Int")))
                                    {
                                        readValue = parameterEstimation.at(pei).firstChild().firstChild().firstChild().nodeValue();
                                    }
                                }
                            }
                            for (QMap<QString,PharmMLLoader08::StatisticalDefinition>::const_iterator iit = _individualParameters.begin();
                                 iit!=_individualParameters.end();
                                 iit++)
                            {
                                if (iit.value().population == readParameter)
                                {
                                    if (!fixed.isEmpty() && fixed=="true") readValue += " [method=FIXED]";
                                    _initialValues["pop_{"+iit.key()+"}"] = readValue;
                                }
                                if (iit.value().omegas.size())
                                {
                                    for (int omega_i = 0;omega_i<iit.value().omegas.size();omega_i++)
                                    {
                                        QString readOmega = "";
                                        QMap<QString,PharmMLLoader08::RandomVariableData>::const_iterator findrvIt = _randomVariables.find(iit.value().omegas[omega_i]);
                                        if (findrvIt != _randomVariables.end())
                                        {
                                            readOmega  = findrvIt.value().stddev;
                                        }
                                        if (readOmega == readParameter)
                                        {
                                            if (!fixed.isEmpty() && fixed=="true") readValue += " [method=FIXED]";
                                            _initialValues["omega2_{"+iit.key()+"}"] = readValue;
                                        }
                                    }
                                }
                            }
                            QMap<QString,QVector<QString> >::const_iterator findObsModel = _errorModelParameters.find(blockRef);
                            if (findObsModel != _errorModelParameters.end())
                            {
                                for (int poi = 0;poi<findObsModel.value().size();poi++)
                                {
                                    _initialValues[findObsModel.value()[poi]+"_{"+blockRef+"}"] = readValue;
                                }
                            }
                        }
                    }
                }
            }
        }
        else if (modelingStep.at(mi).nodeName() == "mstep:NONMEMdataSet"
                 || modelingStep.at(mi).nodeName() == "NONMEMdataSet")
        {
            QDomNodeList nonmemMapping =  modelingStep.at(mi).childNodes();
            QMap<QString,QString> categoryMapping;
            QString blkId;
            for (int nmi = 0;nmi<nonmemMapping.size();nmi++)
            {
                if (nonmemMapping.at(nmi).nodeName() == "mstep:ColumnMapping"
                    || nonmemMapping.at(nmi).nodeName() == "ColumnMapping")
                {
                    QDomNodeList columnMapping = nonmemMapping.at(nmi).childNodes();
                    for (int cmi = 0;cmi<columnMapping.size();cmi++)
                    {
                        if (columnMapping.at(cmi).nodeName() == "ds:CategoryMapping"
                            || columnMapping.at(cmi).nodeName() == "CategoryMapping")
                        {
                            QDomNodeList modalitiesList = columnMapping.at(cmi).childNodes();
                            for (int modi = 0;modi<modalitiesList.size();modi++)
                            {
                                if (modalitiesList.at(modi).nodeName() == "ds:Map"
                                    || modalitiesList.at(modi).nodeName() == "Map")
                                {
                                    QString dataCateg = parseQDomNodeAttribute(modalitiesList.at(modi),"dataSymbol");
                                    QString symbCateg = parseQDomNodeAttribute(modalitiesList.at(modi),"modelSymbol");
                                    categoryMapping[symbCateg] = dataCateg;
                                }
                            }
                        }
                        if (columnMapping.at(cmi).nodeName() == "ct:SymbRef"
                            || columnMapping.at(cmi).nodeName() == "SymbRef")
                        {
                            blkId = parseQDomNodeAttribute(columnMapping.at(cmi),"blkIdRef");
                        }
                    }
                }
            }
        }
    }
}

void PharmMLLoader08::pharmMLDataToMlxtran(const QDomNode& node)
{
    QDomNodeList trialDesign = node.childNodes();
    for (int ti = 0;ti<trialDesign.size();ti++)
    {
        if (trialDesign.at(ti).nodeName() == "Structure")
        {
            QDomNodeList structureNodes = trialDesign.at(ti).childNodes();
            for (int sni = 0;sni<structureNodes.size();sni++)
            {
              if (structureNodes.at(sni).nodeName() == "Epoch")
                {
                    QDomNodeList epochNodes = structureNodes.at(sni).childNodes();
                    for (int eni = 0;eni<epochNodes.size();eni++)
                    {
                        if (epochNodes.at(eni).nodeName() == "Start")
                        {

                        }
                        else if (epochNodes.at(eni).nodeName() == "End")
                        {

                        }
                    }
                }
              else if (structureNodes.at(sni).nodeName() == "Arm")
                {

                }
              else if (structureNodes.at(sni).nodeName() == "Cell")
                {
                    QDomNodeList cellNodes = structureNodes.at(sni).childNodes();
                    for (int cell_i = 0;cell_i<cellNodes.size();cell_i++)
                    {
                        if (cellNodes.at(cell_i).nodeName() == "EpochRef")
                        {

                        }
                        else if (cellNodes.at(cell_i).nodeName() == "EpochRef")
                        {
                        }
                        else if (cellNodes.at(cell_i).nodeName() == "ArmRef")
                        {

                        }
                        else if (cellNodes.at(cell_i).nodeName() == "SegmentRef")
                        {

                        }
                    }

                }
              else if (structureNodes.at(sni).nodeName() == "Segment")
              {
                    QDomNodeList segmentNodes = structureNodes.at(sni).childNodes();
                    for (int segi = 0;segi<segmentNodes.size();segi++)
                    {
                        if (structureNodes.at(segi).nodeName() == "ActivityRef")
                        {

                       }
                    }
              }
              else if (structureNodes.at(sni).nodeName() == "Activity")
              {
                  QDomNodeList activityNodes = structureNodes.at(sni).childNodes();
                  for (int ani = 0;ani<activityNodes.size();ani++)
                  {
                      if (activityNodes.at(ani).nodeName() == "Bolus" || activityNodes.at(ani).nodeName() == "Oral")
                      {
                          QDomNodeList bolusNodes = activityNodes.at(ani).childNodes();
                          for (int bni = 0; bni<bolusNodes.size();bni++)
                          {
                              if (bolusNodes.at(bni).nodeName() == "DoseAmount")
                              {
                                  QString targetType = parseQDomNodeAttribute(bolusNodes.at(bni),"inputType");
                                  if (targetType.isEmpty())
                                      targetType = parseQDomNodeAttribute(bolusNodes.at(bni),"inputTarget");
                                  QDomNodeList doseAmountNodes = bolusNodes.at(bni).childNodes();
                                  for (int dani = 0;dani<doseAmountNodes.size();dani++)
                                  {
                                      if (doseAmountNodes.at(dani).nodeName() == "ct:SymbRef")
                                      {
                                          QString doseAmtSym = parseQDomNodeAttribute(doseAmountNodes.at(dani),"symbIdRef");
                                          //QString doseAmtSym = parseQDomNodeAttribute(doseAmountNodes.at(dani),"");
                                          PharmMLLoader08::Dosing d = _dosingVariables[doseAmtSym];
                                          d.dosingTypes = "doseAmount";
                                          d.inputTarget = targetType;
                                          _dosingVariables[doseAmtSym] = d;
                                      }
                                  }
                              }
                              else if (bolusNodes.at(bni).nodeName() == "DosingTimes")
                              {
                                  QDomNodeList doseTimeNodes = bolusNodes.at(bni).childNodes();
                                  for (int dtni = 0;dtni<doseTimeNodes.size();dtni++)
                                  {
                                      if (doseTimeNodes.at(dtni).nodeName() == "ct:SymbRef")
                                      {
                                          QString doseTimeSym = parseQDomNodeAttribute(doseTimeNodes.at(dtni),"symbIdRef");
                                          PharmMLLoader08::Dosing d = _dosingVariables[doseTimeSym];
                                          d.dosingTypes = "dosingTimes";
                                          d.dosingTime = doseTimeSym;
                                          _dosingVariables[doseTimeSym] = d;
                                      }
                                  }

                                }
                            }
                        }
                    }
                }
            }
        }
        else if (trialDesign.at(ti).nodeName() == "Population")
        {
            QDomNodeList populationNodes = trialDesign.at(ti).childNodes();
            for (int pni = 0;pni<populationNodes.size();pni++)
            {
                if (populationNodes.at(pni).nodeName() == "Demographic")
                {

                }
                else if (populationNodes.at(pni).nodeName() == "IndividualTemplate")
                {

                }
                else if (populationNodes.at(pni).nodeName() == "ds:DataSet" || populationNodes.at(pni).nodeName() == "DataSet")
                {
                    QDomNodeList dsDataSet = populationNodes.at(pni).childNodes();
                    for (int dsi = 0;dsi<dsDataSet.size();dsi++)
                    {
                        if (dsDataSet.at(dsi).nodeName() == "ds:Definition" || dsDataSet.at(dsi).nodeName() == "Definition")
                        {
                            QDomNodeList dsColumn = dsDataSet.at(dsi).childNodes();
                            for (int dsci=0;dsci<dsColumn.size();dsci++)
                            {
                                if (dsColumn.at(dsci).nodeName() == "ds:Column" || dsColumn.at(dsci).nodeName() == "Column")
                                {
                                    QString columnId   = parseQDomNodeAttribute(dsColumn.at(dsci),"columnId");
                                    QString columnType = parseQDomNodeAttribute(dsColumn.at(dsci),"columnType");
                                    QString columnNum  = parseQDomNodeAttribute(dsColumn.at(dsci),"columnNum");
                                    QString valueType  = parseQDomNodeAttribute(dsColumn.at(dsci),"valueType");
                                    if (columnType == "id" )             columnType="ID";
                                    else if (columnType == "time" )      columnType = "TIME";
                                    else if (columnType == "dose" )      columnType = "DOSE";
                                    else if (columnType == "reg" )       columnType = "REG";
                                    else if (columnType == "covariate" ) columnType = "COV";
                                    else if (columnType == "rate" )      columnType ="RATE";
                                    else if (columnType == "ii" )        columnType = "INF";
                                    else if (columnType == "ss" )        columnType = "SS";
                                    else if (columnType == "addl" )        columnType = "ADDL";
                                    else if (columnType == "dv" )        columnType = "Y";
                                    else if (columnType == "dvid" )        columnType = "YTYPE";
                                    else if (columnType == "occasion" )  columnType = "OCC";
                                    else if (columnType == "mdv" )  columnType = "MDV";
                                    else
                                    {
                                        QString errorString = QString("Column type '")+columnType + QString("' is not managed.")
                                                              + QString( "Line in XML file:")
                                                              + QString::number(dsColumn.at(dsci).lineNumber());
                                        /*throwLater(lixoft::exception::Exception("lixoft::translator::PharmMLLoader08",
                                                                      errorString.toUtf8().data(),
                                                                      THROW_LINE_FILE));
                                                                      */
                                        addMessage(LixoftObject::WARNING,errorString);
                                    }
                                    _dataSet.columns[columnNum.toInt()] = QPair<QString,QString>(columnType,columnId);
                                }
                            }
                            for (QMap<QString,QString>::iterator cit =_continuousCovariates.begin();
                                 cit != _continuousCovariates.end();
                                 cit++)
                            {
                                QStringList transformedCovariate=cit.key().split("::");
                                int columNum = _dataSet.columns.size();
                                if (transformedCovariate.size() == 2)
                                {
                                    _dataSet.columns[columNum] = QPair<QString,QString>("COV","covariate");
                                }
                            }
                        }
                        else if (dsDataSet.at(dsi).nodeName() == "ds:ImportData" || dsDataSet.at(dsi).nodeName()=="ImportData")
                        {
                            QDomNodeList dsData = dsDataSet.at(dsi).childNodes();
                            for (int dsdi = 0;dsdi<dsData.size();dsdi++)
                            {
                                if (dsData.at(dsdi).nodeName() == "ds:name" || dsData.at(dsdi).nodeName() == "name")
                                {
                                    _dataSet.name = dsData.at(dsdi).firstChild().nodeValue();
                                }
                                else if (dsData.at(dsdi).nodeName() == "ds:url" || dsData.at(dsdi).nodeName() == "url")
                                {
                                    _dataSet.dataUrl= dsData.at(dsdi).firstChild().nodeValue();
                                }
                                else if (dsData.at(dsdi).nodeName() == "ds:format" || dsData.at(dsdi).nodeName() == "format")
                                {
                                    _dataSet.dataFormat = dsData.at(dsdi).firstChild().nodeValue();
                                }
                                else if (dsData.at(dsdi).nodeName() == "ds:delimiter" || dsData.at(dsdi).nodeName() == "delimiter")
                                {
                                    _dataSet.columnDelimiter = dsData.at(dsdi).firstChild().nodeValue();
                                }
                            }
                        }
                    }
                }
            }
        }
        else if (trialDesign.at(ti).nodeName() == "IndividualDosing")
        {
            QDomNodeList individualDosingNodes = trialDesign.at(ti).childNodes();
            for (int idni = 0;idni<individualDosingNodes.size();idni++)
            {

            }
        }
    }
}
QString PharmMLLoader08::toString() const
{
 return LanguageTranslator::toString();
}

}}
