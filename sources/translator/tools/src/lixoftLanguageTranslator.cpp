#include <QDebug>
#include <QString>
#include <QFile>
#include <QDir>
#include <QFileInfo>
#include <QTextStream>
#include <QRegExp>
#include <QCoreApplication>
#include <QTextStream>
#include <QSharedPointer>


#include <iostream>

#include "lixoft/translator/LanguageTranslator.h"
#include "lixoft/translator/LixoftTranslatorFactory.h"

void usage()
{
    std::cout<<"---------- language translator (version 1.0.8) ----------"<<std::endl;
    std::cout<<"                                   "<<std::endl;
    std::cout<<"                                   "<<std::endl;
    std::cout<<"--from=[mlxproject | pharmml | mlxtran ]"<<std::endl;
    std::cout<<"--to=[mlxproject | pharmml | mlxtran ]"<<std::endl;
    std::cout<<"--option=only-observation-model|with-observation-model"<<std::endl;
    std::cout<<"--input-file=<filename>"<<std::endl;
    std::cout<<"--output-dir=<directory>"<<std::endl;
    std::cout<<"--output-file=<model file>"<<std::endl;
    std::cout<<"--pharmml-version=0.6|0.8"<<std::endl;
    std::cout<<"--display-error=full|partial|none"<<std::endl;
    std::cout<<"-h, --help        : help for lixoftTranslator"<<std::endl;

}


int main(int argc, char **argv)
{
    QCoreApplication app(argc,argv);
    QString pharmmlversion="0.8";
    QString fromL;
    QString toL;
    QString inputFile;
    QString output;
    QVector<QPair<QString,QString> > options;
    QString displayInfo = "full";
    int outputType = lixoft::translator::LanguageTranslator::OUTPUT_IS_A_DIRECTORY;
        if (argc>1)
        {

          for (int i=1; i < argc; ++i)
          {
            QString arg(argv[i]);
            if (arg.contains("--from="))
            {
              arg.remove("--from=");

              fromL = arg;
            }
            else if (arg.contains("--to="))
            {
              arg.remove("--to=");
              toL = arg;
            }
            else if (arg.contains("--input-file="))
            {
                arg.remove("--input-file=");
                inputFile = arg;
            }
            else if (arg.contains("--output-dir="))
            {
                if (!output.isEmpty()) throw lixoft::exception::Exception("lixoftLanguagetranslator::main","cannot define 'output-dir', output already defined");
                arg.remove("--output-dir=");
                output = arg;
				output += QDir::separator();
                outputType = lixoft::translator::LanguageTranslator::OUTPUT_IS_A_DIRECTORY;

            }
            else if (arg.contains("--output-file="))
            {
                if (!output.isEmpty()) throw lixoft::exception::Exception("lixoftLanguagetranslator::main","cannot define 'output-file', output already defined");
                arg.remove("--output-file=");
                output = arg;
                outputType = lixoft::translator::LanguageTranslator::OUTPUT_IS_A_FILE;
            }
            else if (arg.contains("--display-error"))
            {
                arg.remove("--display-error=");
                displayInfo = arg;
            }
            else if (arg.contains("--pharmml-version"))
            {
                arg.remove("--pharmml-version=");
                pharmmlversion = arg;
            }
            else if (arg.contains("--option="))
            {
                arg.remove("--option=");
                QStringList optionList = arg.split(",");
                for (int oli = 0;oli<optionList.size();oli++)
                {
                    if (optionList.at(oli) == "only-observation-model")
                    {
                        QPair<QString,QString> option;
                        option.first = "mlxtran";
                        option.second = "only-observation-model";
                        options.push_back(option);
                    }
                    else if (optionList.at(oli) == "with-observation-model")
                    {
                        QPair<QString,QString> option;
                        option.first = "mlxtran";
                        option.second = "with-observation-model";
                        options.push_back(option);
                    }
                }
            }
          }

          // create translator
          QSharedPointer<lixoft::translator::LanguageTranslator> translator = lixoft::translator::LixoftTranslatorFactory::getTranslator(fromL,toL,
                                                                                                    inputFile,
                                                                                                    output,
                                                                                                    options,
                                                                                                    outputType,
                                                                                                    pharmmlversion
                                                                                                    );
          // run translator
          try {
              translator->checkOptions();
              translator->translate();
              translator->serialize();
              if (displayInfo=="full")
              {
                  translator->displayMessage();
              }
              translator->throwNow();
          }
          catch(const lixoft::exception::Exception &ex) {
              std::cerr<<"[Exception raised]:"<<ex.displayAll().c_str()<<std::endl;
          }
          catch(...) {
              std::cerr<<"[Exception raised]:unknown"<<std::endl;
          }

        }
        else usage();
        std::cerr<<"Conversion ended"<<std::endl;
    return 0;

}
