
include_directories("include"
                    "${LIXOFTLANGUAGETRANSLATOR_SRC_DIR}/include"
                    "${MLXPLORE_SRC_DIR}/include"
                    "${MLXTRAN_SRC_DIR}/include"
                    "${MLXTRAN_SRC_DIR}/manager/include"
                    "${MLXTRAN_SRC_DIR}/v2/parsers/include"
                    "${MLXTRAN_SRC_DIR}/v2/parsers/grammars/include"
                    "${MLXTRAN_SRC_DIR}/v2/parsers/src"
                    "${MLXBASE_SRC_DIR}/include"
                    "${MLXUSEFUL_SRC_DIR}/include"
                    "${CORE_SRC_DIR}/include"
                    "${DATA_SRC_DIR}/lixoftData/include"
                    "${DATA_SRC_DIR}/lixoftUseful/include"
                    "${SPIRIT_SRC_DIR}/include"
                    #directories for licence
                    "${MLXSESSIONMANAGER_SRC_DIR}/include"
                    "${RLM_SRC_DIR}/include"
                    # mlxUseful necessary services
                    "${MLXUSEFUL_SRC_DIR}" 
                    # Boost
                    "${BOOST_SRC_DIR}/include"
                    # POCO
                    "${POCO_SRC_DIR}/Foundation/include"
                    "${POCO_SRC_DIR}/Util/include"
                    # CPPUnit
                    "${CPPUNIT_SRC_DIR}/include"
                    #Logger (used by CPPunit)
                    "${MLXLOGGER_SRC_DIR}/include"
                    "${POCO_SRC_DIR}/XML/include"
                    "${POCO_SRC_DIR}/Net/include"
                    "${CPPUNIT_SRC_DIR}/include"
                    "${MLXLICENSE_SRC_DIR}/include"
                    "${RLM_SRC_DIR}/include"
                    "${MLX_QT_INCLUDE_HEADERS}"
                    "${CVODE_INSTALLED_SRC_DIR}/include"
                    )
                   

if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE ${CMAKE_DEFAULT_BUILD_TYPE})
endif(NOT CMAKE_BUILD_TYPE)

add_executable(lixoftLanguageTranslator
              src/lixoftLanguageTranslator.cpp
)


target_link_libraries("lixoftLanguageTranslator" "lixoftCore" "mlxtran" "lixoftLanguageTranslators")

install(TARGETS lixoftLanguageTranslator
        LIBRARY DESTINATION "lib"
        COMPONENT "Core" 
        RUNTIME DESTINATION "lib"
        COMPONENT "Core")


