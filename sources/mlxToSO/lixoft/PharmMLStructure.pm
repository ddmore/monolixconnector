package lixoft::PharmMLStructure;

use strict;
use warnings;
use Data::Dumper;

sub new 
{
  my $package = shift;
  my $class = ref($package) || $package;
  my $self = {
               DATA=>{}
             };
  return bless $self, $class;
}

sub getHeader
{
  my $self = shift;
  my $pharmmlProjectName = shift;
  my $message = shift;
  my $messErr = "";
  my $messWarn = "";
  my $elapsedTime = "";
  my $terminaison = "";
  my $mlxProjectName = $pharmmlProjectName;
  my $mlxProjectDir = $pharmmlProjectName;
  $mlxProjectName=~s/\.xml//;
  $mlxProjectDir=~s/\.xml/\_project/;
  for (my $mi = 0;$mi<scalar(@$message);$mi++)
  {
    if ($message->[$mi]=~/^ERROR/)
    {
      $message->[$mi]=~s/^ERROR\://;
      $messErr.=$message->[$mi];
      $messErr.=" / ";
    }
    elsif($message->[$mi]=~/^WARN/)
    {
      $message->[$mi]=~s/^WARN\://;
      $messWarn.=$message->[$mi];
      $messWarn.=" / ";
    }
    elsif($message->[$mi]=~/^ET/)
    {
      $terminaison="0PTIMIZATION SUCCESSFUL";
      $elapsedTime = $message->[$mi];
      $elapsedTime =~s/^ET\://g;
    }
  }
  my $taskInformation="\
          <TaskInformation> \
  ";
  $messWarn=~s/[^a-zA-z0-9 ]//g;
  if ($messWarn) {
  $taskInformation.= " \
              <Message type=\"WARNING\"> \
                  <Toolname> \
                      <ct:String>MONOLIX</ct:String> \
                  </Toolname> \
                  <Name> \
                      <ct:String>monolix warning</ct:String> \
                  </Name> \
                  <Content> \
                      <ct:String>".$messWarn."</ct:String> \
                  </Content> \
                  <Severity> \
                      <ct:Int>1</ct:Int> \
                  </Severity> \
              </Message> \
  ";
  }
  if ($messErr) {
  $taskInformation.= " \
              <Message type=\"ERROR\"> \
                  <Toolname> \
                      <ct:String>MONOLIX</ct:String> \
                  </Toolname> \
                  <Name> \
                      <ct:String>monolix error</ct:String> \
                  </Name> \
                  <Content> \
                      <ct:String>".$messErr."</ct:String> \
                  </Content> \
                  <Severity> \
                      <ct:Int>2</ct:Int> \
                  </Severity> \
              </Message> \
  ";
  }
  $taskInformation.=" \
              <OutputFilePath oid=\"o1\"> \
                  <ds:path>pop_parameter.txt</ds:path> \ 
              </OutputFilePath> \
              <RunTime> $elapsedTime </RunTime> \
              <NumberChains> 0 </NumberChains> \
              <NumberIterations> 0 </NumberIterations> \
          </TaskInformation> \
  ";
  
  my $soHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
  <SO xmlns=\"http://www.pharmml.org/so/0.3/StandardisedOutput\" \
      xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \
      xmlns:ds=\"http://www.pharmml.org/pharmml/0.8/Dataset\" \
      xmlns:ct=\"http://www.pharmml.org/pharmml/0.8/CommonTypes\" \
      xsi:schemaLocation=\"http://www.pharmml.org/so/0.3/StandardisedOutput \
                           http://www.pharmml.org/so/0.3/StandardisedOutput\" \
      implementedBy=\"MJS\" writtenVersion=\"0.3\" metadataFile=\"\" > \
      \
    <PharmMLRef name=\"$pharmmlProjectName\"/> \
    \
      <SOBlock blkId=\"SO1\"> \
        \
        <ToolSettings> \
            <File oid=\"f1\"><ds:path>tables.xmlx</ds:path></File> \
        </ToolSettings> \
        <RawResults> \
        <DataFile> \
           <ds:ExternalFile oid=\"d1\"> \
              <ds:path>$mlxProjectDir/covariatesSummary.txt</ds:path> \
              <ds:format>CSV</ds:format> \
              <ds:delimiter>COMMA</ds:delimiter> \
           </ds:ExternalFile> \
        </DataFile> \
        <DataFile> \
           <ds:ExternalFile oid=\"d2\"> \
              <ds:path>$mlxProjectDir/estimates.txt</ds:path> \
              <ds:format>CSV</ds:format> \
              <ds:delimiter>COMMA</ds:delimiter> \
           </ds:ExternalFile> \
        </DataFile> \
        <DataFile> \
           <ds:ExternalFile oid=\"d3\"> \
              <ds:path>$mlxProjectDir/fim_lin.txt</ds:path> \
              <ds:format>CSV</ds:format> \
              <ds:delimiter>COMMA</ds:delimiter> \
           </ds:ExternalFile> \
        </DataFile> \
        <DataFile> \
           <ds:ExternalFile oid=\"d4\"> \
              <ds:path>$mlxProjectDir/finegrid.txt</ds:path> \
              <ds:format>CSV</ds:format> \
              <ds:delimiter>COMMA</ds:delimiter> \
           </ds:ExternalFile> \
        </DataFile> \
        <DataFile> \
           <ds:ExternalFile oid=\"d5\"> \
              <ds:path>$mlxProjectDir/fulltimes.txt</ds:path> \
              <ds:format>CSV</ds:format> \
              <ds:delimiter>COMMA</ds:delimiter> \
           </ds:ExternalFile> \
        </DataFile> \
        <DataFile> \
           <ds:ExternalFile oid=\"d6\"> \
              <ds:path>$mlxProjectDir/indiv_eta.txt</ds:path> \
              <ds:format>CSV</ds:format> \
              <ds:delimiter>COMMA</ds:delimiter> \
           </ds:ExternalFile> \
        </DataFile> \
        <DataFile> \
           <ds:ExternalFile oid=\"d7\"> \
              <ds:path>$mlxProjectDir/individualContributionToLL.txt</ds:path> \
              <ds:format>CSV</ds:format> \
              <ds:delimiter>COMMA</ds:delimiter> \
           </ds:ExternalFile> \
        </DataFile> \
        <DataFile> \
           <ds:ExternalFile oid=\"d8\"> \
              <ds:path>$mlxProjectDir/indiv_parameters.txt</ds:path> \
              <ds:format>CSV</ds:format> \
              <ds:delimiter>COMMA</ds:delimiter> \
           </ds:ExternalFile> \
        </DataFile> \
        <DataFile> \
           <ds:ExternalFile oid=\"d9\"> \
              <ds:path>$mlxProjectDir/pop_parameters.txt</ds:path> \
              <ds:format>CSV</ds:format> \
              <ds:delimiter>COMMA</ds:delimiter> \
           </ds:ExternalFile> \
        </DataFile> \ 
        <DataFile> \
           <ds:ExternalFile oid=\"d10\"> \
              <ds:path>$mlxProjectDir/predictions.txt</ds:path> \
              <ds:format>CSV</ds:format> \
              <ds:delimiter>COMMA</ds:delimiter> \
           </ds:ExternalFile> \
        </DataFile> \
        <DataFile> \
           <ds:ExternalFile oid=\"d11\"> \
              <ds:path>$mlxProjectDir/project.mat</ds:path> \
              <ds:format>CSV</ds:format> \
              <ds:delimiter>COMMA</ds:delimiter> \
           </ds:ExternalFile> \
        </DataFile> \
        <DataFile> \
           <ds:ExternalFile oid=\"d12\"> \
              <ds:path>$mlxProjectDir/project.xmlx</ds:path> \
              <ds:format>CSV</ds:format> \
              <ds:delimiter>COMMA</ds:delimiter> \
           </ds:ExternalFile> \
        </DataFile> \
        <DataFile> \
           <ds:ExternalFile oid=\"d12\"> \
              <ds:path>$mlxProjectDir/results.mat</ds:path> \
              <ds:format>CSV</ds:format> \
              <ds:delimiter>COMMA</ds:delimiter> \
           </ds:ExternalFile> \
        </DataFile> \
        <DataFile> \
           <ds:ExternalFile oid=\"d12\"> \
              <ds:path>$mlxProjectDir/$mlxProjectName\_model.txt</ds:path> \
              <ds:format>CSV</ds:format> \
              <ds:delimiter>COMMA</ds:delimiter> \
           </ds:ExternalFile> \
        </DataFile> \
        <GraphicsFile oid=\"g1\"> \
          <ds:path>$mlxProjectDir/saem.png</ds:path> \
        </GraphicsFile> \
        </RawResults> \
        $taskInformation \
        <Estimation> \n";
        
  return $soHeader;
}

sub TaskInformation {
  
}

sub getFooter
{
 my $self = shift;
 my $message = shift;
 my $messWarn = "";
 my $messErr  = "";
 my $elapsedTime = "";
 my $terminaison = "";
 
 for (my $mi = 0;$mi<scalar(@$message);$mi++)
 {
   if ($message->[$mi]=~/^ERROR/)
   {
     $message->[$mi]=~s/^ERROR\://;
     $messErr.=$message->[$mi];
     $messErr.=" / ";
   }
   elsif($message->[$mi]=~/^WARN/)
   {
     $message->[$mi]=~s/^WARN\://;
     $messWarn.=$message->[$mi];
     $messWarn.=" / ";
   }
   elsif($message->[$mi]=~/^ET/)
   {
     $terminaison="0PTIMIZATION SUCCESSFUL";
     $elapsedTime = $message->[$mi];
     $elapsedTime =~s/^ET\://g;
   }
 }


 my $soFooter = "\
        </Estimation> \
      </SOBlock> \
     </SO>\n";
 return $soFooter;
}


sub getPopulationEstimates
{
  my $self = shift;
  my $parameterNames         = shift; # reference on population names;
  my $populationParameterMLE = shift; 
  my $outputDirectory        = shift;
  my $config                 = shift;
  my $fn                     = shift;
  my $ret="";
  my $retXML = "";
  my %filterN = %$fn;
  if (scalar(scalar(@$populationParameterMLE))) {
    open(DDMORE_POP,">$outputDirectory/ddmore_pop.csv") or die "Cannot open file '$outputDirectory/ddmore_pop.csv'";
    $ret="\
              <PopulationEstimates> \
                <MLE id=\"i1\">";
    $ret.="\
                  <ds:Definition xmlns=\"http://www.pharmml.org/pharmml/0.8/Dataset\">";
    my %filterId = ();
    my $countPNi = 1;
    for (my $cname_i = 0;$cname_i<scalar(@$parameterNames);$cname_i++)
    {
          if ($cname_i == 0)
          { 
            print DDMORE_POP $parameterNames->[$cname_i];
          }
          else
          {
            print DDMORE_POP ",".$parameterNames->[$cname_i];
          }
          if (exists($filterN{$parameterNames->[$cname_i]})) {
            $ret.="\
                       <ds:Column columnId=\"".$parameterNames->[$cname_i]."\" columnType=\"undefined\" valueType=\"real\" columnNum=\"".($countPNi)."\"/>";
            $countPNi++;
            $filterId{$cname_i} = 1;
          }
    }
    print DDMORE_POP "\r\n";
    $ret.="\
                 </ds:Definition>";
    $retXML.="\
                 <ds:Table xmlns=\"http://www.pharmml.org/pharmml/0.8/Dataset\"> \
                   <ds:Row>";
    for (my $cvalue_i=0;$cvalue_i<scalar(@$populationParameterMLE);$cvalue_i++)
    {
       if ($cvalue_i == 0)
       { 
         print DDMORE_POP $populationParameterMLE->[$cvalue_i];
       }
       else
       {
         print DDMORE_POP ",".$populationParameterMLE->[$cvalue_i];
       }
       if (exists($filterId{$cvalue_i})) {
         $retXML.="\
                            <ct:Real>".$populationParameterMLE->[$cvalue_i]."</ct:Real>";
       }
    }
    $retXML.="\
                   </ds:Row>\
                 </ds:Table>";
    if ($config->getValue("output.MLEAsCSV")=~/true/)
    {
      $ret.="\
                 <ds:ImportData oid=\"MLE\">\
                    <ds:path>ddmore_pop.csv</ds:path> \
                    <ds:format>CSV</ds:format> \
                    <ds:delimiter>COMMA</ds:delimiter>\
                 </ds:ImportData>";
    }
    else 
    {
     $ret.=$retXML;
    }
    $ret.="\
                </MLE> \
              </PopulationEstimates>";
    close (DDMORE_POP); 
  }
  return $ret
}
sub getPrecisionPopulationEstimates
{
  my $self = shift;
  my $parameterNames         = shift; # reference on population names;
  my $standardError          = shift; # standard error
  my $relStandardError       = shift; # relative standard error
  my $correlation            = shift;
  my $fim                    = shift;
  my $outputDirectory        = shift;
  my $config                 = shift;
  my $fn                 = shift;
  # my $correlationMatrix     = shift;
  # my $fim                   = shift; # fisher information matrix
  my %filterN = %$fn;
 
  open (DDMORE_FIM, ">$outputDirectory/ddmore_fim.csv") or die "Cannot open file '$outputDirectory/ddmore_fim.csv' : $!";
  open (DDMORE_CORR, ">$outputDirectory/ddmore_corr.csv") or die "Cannot open file '$outputDirectory/ddmore_corr.csv': $!";
  open (DDMORE_SE, ">$outputDirectory/ddmore_se.csv") or die "Cannot open file '$outputDirectory/ddmore_se.csv': $!";
  open (DDMORE_RSE, ">$outputDirectory/ddmore_rse.csv") or die "Cannot open file '$outputDirectory/ddmore_rse.csv': $!";
  my %filterIdR = (); 
  my %filterIdC = (); 
  my $retXML = "";
  my $ret="\
            <PrecisionPopulationEstimates> \
              <MLE>";
  if (scalar(@$fim)) {
    $ret.="\
                  <FIM>";
    $retXML.="\
                    <ct:Matrix matrixType=\"Any\">
                      <ct:RowNames>";
   for (my $cname_i = 0;$cname_i <scalar(@$fim);$cname_i++)
   {
     if ($cname_i == 0)
     {
       print DDMORE_FIM $fim->[$cname_i]->[0];
     }
     else
     {
       print DDMORE_FIM ",".$fim->[$cname_i]->[0];
     }
     if (exists($filterN{$fim->[$cname_i]->[0]})) {
       $retXML.="\
                           <ct:String>".$fim->[$cname_i]->[0]."</ct:String>";
       $filterIdR{$cname_i} = 1;
       $filterIdC{$cname_i} = 1;
     }
   }
  $retXML.="\
                      </ct:RowNames>";
  $retXML.="\
                      <ct:ColumnNames>";
   for (my $cname_i = 0;$cname_i <scalar(@$fim);$cname_i++)
   {
     if (exists($filterN{$fim->[$cname_i]->[0]})) {
       $retXML.="\
                           <ct:String>".$fim->[$cname_i]->[0]."</ct:String>";
     }
   }
  $retXML.="\
                      </ct:ColumnNames>";
   print DDMORE_FIM "\r\n";
   for (my $cname_i = 0;$cname_i<scalar(@$fim);$cname_i++)
   {
     if (exists($filterIdR{$cname_i})) {
       $retXML.="\
                      <ct:MatrixRow>";
       my $coli = 0;
       for ($coli = 1;$coli<scalar(@{$fim->[$cname_i]});$coli++)
       {
         if ($coli == 1)
         {
           print DDMORE_FIM $fim->[$cname_i]->[$coli];
         }
         else
         {
           print DDMORE_FIM ",".$fim->[$cname_i]->[$coli];
         }
         if(exists($filterIdC{$coli-1})) {
           $retXML.="\
                             <ct:Real>".$fim->[$cname_i]->[$coli]."</ct:Real>";
         }
       }
       print DDMORE_FIM "\r\n";
       $retXML.="\
                        </ct:MatrixRow>";   
     }
   }
   $retXML.="\
                     </ct:Matrix>";
   if ($config->getValue("output.FIMAsCSV")=~/true/)
   {
    $ret.="\
                 <ds:ImportData oid=\"FIM\">\
                    <ds:path>ddmore_fim.csv</ds:path> \
                    <ds:format>CSV</ds:format> \
                    <ds:delimiter>COMMA</ds:delimiter>\
                 </ds:ImportData>";
   }
   else
   {
    $ret.=$retXML;
   }
   $ret.="\
                  </FIM>";
 }

my $withCorrelation = 0;
for (my $corr_i = 0;$corr_i<scalar(@$correlation) && !$withCorrelation;$corr_i++)
{
 $withCorrelation = scalar(@{$correlation->[$corr_i]});
}
if ($withCorrelation)
{
  $ret.="\
                  <CorrelationMatrix>";
  $retXML="\
                    <ct:Matrix matrixType=\"Any\">
                      <ct:RowNames>";
   for (my $cname_i = 0;$cname_i <scalar(@$correlation);$cname_i++)
   {
     if ($cname_i == 0)
     {
       print DDMORE_CORR $correlation->[$cname_i]->[0];
     }
     else
     {
       print DDMORE_CORR ",".$correlation->[$cname_i]->[0];
     }
     $retXML.="\
                         <ct:String>".$correlation->[$cname_i]->[0]."</ct:String>";
   }
  $retXML.="\
                      </ct:RowNames>";
  $retXML.="\
                      <ct:ColumnNames>";
   for (my $cname_i = 0;$cname_i <scalar(@$correlation);$cname_i++)
   {
     $retXML.="\
                         <ct:String>".$correlation->[$cname_i]->[0]."</ct:String>";
   }
  $retXML.="\
                      </ct:ColumnNames>";
   print DDMORE_CORR "\r\n";
   my $numOfParams = scalar(@$correlation)+1;
   for (my $cname_i = 0;$cname_i <scalar(@$correlation);$cname_i++)
   {
     $retXML.="\
                      <ct:MatrixRow>";
     my $coli = 0;
     for ($coli = 1;$coli<scalar(@{$correlation->[$cname_i]});$coli++)
     {
       if ($coli == 1)
       {
         print DDMORE_CORR $correlation->[$cname_i]->[$coli];
       }
       else
       {
         print DDMORE_CORR ",".$correlation->[$cname_i]->[$coli];
       }
       $retXML.="\
                         <ct:Real>".$correlation->[$cname_i]->[$coli]."</ct:Real>";
     }
     for (;$coli<$numOfParams;$coli++)
     {
       print DDMORE_CORR ",0.0";
       $retXML.="\
                         <ct:Real>0.0</ct:Real>";
     }
     print DDMORE_CORR "\r\n";
     $retXML.="\
                      </ct:MatrixRow>";   
   }
   $retXML.="\
                     </ct:Matrix>";

   if ($config->getValue("output.CorrelationAsCSV")=~/true/)
   {
    $ret.="\
                 <ds:ImportData oid=\"Correlation\">\
                    <ds:path>ddmore_corr.csv</ds:path> \
                    <ds:format>CSV</ds:format> \
                    <ds:delimiter>COMMA</ds:delimiter>\
                 </ds:ImportData>";
   }
   else
   {
    $ret.=$retXML;
   }
   $ret.="\
                  </CorrelationMatrix> ";
}
# end correlation

 $ret.="\
                <StandardError> \
                   <ds:Definition xmlns=\"http://www.pharmml.org/pharmml/0.8/Dataset\"> \
                      <ds:Column columnId=\"Parameter\" columnType=\"undefined\" valueType=\"string\" columnNum=\"1\"/> \
                      <ds:Column columnId=\"SE\" columnType=\"undefined\" valueType=\"real\" columnNum=\"2\"/> \
                   </ds:Definition>";
  $retXML="\
                   <ds:Table>";
  print DDMORE_SE "parameter,SE\r\n";                       
  
  for (my $cname_i = 0;$cname_i<scalar(@$parameterNames);$cname_i++)
  {
        print DDMORE_SE $parameterNames->[$cname_i].",".$standardError->[$cname_i]."\r\n";
        if (exists($filterN{$parameterNames->[$cname_i]})) {
          $retXML.="\
                       <ds:Row><ct:String>".$parameterNames->[$cname_i]."</ct:String><ct:Real>".$standardError->[$cname_i]."</ct:Real></ds:Row>";
        }
  }
  $retXML.="\
                   </ds:Table>";
  if ($config->getValue("output.StandardErrorAsCSV")=~/true/)
  {
   $ret.="\
               <ds:ImportData oid=\"StandardError\">\
                  <ds:path>ddmore_se.csv</ds:path> \
                  <ds:format>CSV</ds:format> \
                  <ds:delimiter>COMMA</ds:delimiter>\
               </ds:ImportData>";  
  }
  else
  {
    $ret.=$retXML;
  }
  $ret.="\
                 </StandardError>
                 <RelativeStandardError> \
                   <ds:Definition xmlns=\"http://www.pharmml.org/pharmml/0.8/Dataset\"> \
                      <ds:Column columnId=\"Parameter\" columnType=\"undefined\" valueType=\"string\" columnNum=\"1\"/> \
                      <ds:Column columnId=\"RSE\" columnType=\"undefined\" valueType=\"real\" columnNum=\"2\"/> \
                   </ds:Definition>";
  $retXML="\
                   <ds:Table>";
  print DDMORE_RSE "parameter,RSE\r\n";
  for (my $cname_i = 0;$cname_i<scalar(@$parameterNames);$cname_i++)
  {
        print DDMORE_RSE $parameterNames->[$cname_i].",".$relStandardError->[$cname_i]."\r\n";
        if (exists($filterN{$parameterNames->[$cname_i]})) {
           $retXML.="\
                     <ds:Row><ct:String>".$parameterNames->[$cname_i]."</ct:String><ct:Real>".$relStandardError->[$cname_i]."</ct:Real></ds:Row>";
        }
  }
  $retXML.="\
                   </ds:Table>";
  if ($config->getValue("output.RelativeStandardErrorAsCSV")=~/true/)
  {
   $ret.="\
               <ds:ImportData oid=\"RelativeStandardError\">\
                  <ds:path>ddmore_rse.csv</ds:path> \
                  <ds:format>CSV</ds:format> \
                  <ds:delimiter>COMMA</ds:delimiter>\
               </ds:ImportData>";  
  }
  else
  {
    $ret .= $retXML;
  }
  $ret.="\
                 </RelativeStandardError>";          
       

  $ret.="\
               </MLE> \
             </PrecisionPopulationEstimates>\n";
  close(DDMORE_SE);
  close(DDMORE_RSE);
  close(DDMORE_FIM);
  close(DDMORE_RSE);         
  return $ret;
}

sub getIndividualEstimates
{
  my $self            = shift;
  my $indivParam      = shift;
  my $indivEta        = shift;
  my $outputDirectory = shift;
  my $config          = shift;
  my $pNames          = shift;
  my $fn              = shift;
  my $retXML = "";
  my %pharmmlNames = %$pNames;
  my %filterN = %$fn;
  my @occnames = ();
  my $occTagType = "Real";


  foreach my $pn (keys %pharmmlNames) {
    if ($pn=~/occasion/) {
      my @ptoks = split(/:/,$pn);
      my $vt = $pharmmlNames{$pn};
      push @occnames,$ptoks[1]."#".$vt;
    }
  }
  open (DDMORE_INDIV, ">$outputDirectory/ddmore_indivmean.csv") or die "Cannot open file '$outputDirectory/ddmore_indivmean.csv'";
  open (DDMORE_INDIVETA,">$outputDirectory/ddmore_indiveta.csv") or die "Cannot open file '$outputDirectory/ddmore_indiveta.csv'";
  my $ret = "\
             <IndividualEstimates> \
               <Estimates> \
                  <Mean> \
                    <ds:Definition xmlns=\"http://www.pharmml.org/pharmml/0.8/Dataset\">";
  my $cnum = 1;
  for (my $coli = 0;$coli<scalar(@{$indivParam->[0]});$coli++)
  {
    if (($indivParam->[0]->[$coli] !~/_sd$/) && ($indivParam->[0]->[$coli] !~/^DISCARD_/))
    {
      if ($coli == 0)
      {
        print DDMORE_INDIV $indivParam->[0]->[$coli];
        my @iov_ids = split(/#/,$indivEta->[1]->[$coli]); 
        $ret.="\
                       <ds:Column columnId=\"".$indivParam->[0]->[$coli]."\" columnType=\"id\" valueType=\"string\" columnNum=\"$cnum\"/>";
        for (my $iovid = 1;$iovid<scalar(@iov_ids);$iovid++) {
          $cnum++;
          if (scalar(@occnames) == (scalar(@iov_ids)-1)) {
            my ($on,$ot) = split(/#/,$occnames[$iovid-1]);
            $ret.="\
                       <ds:Column columnId=\"$on\" columnType=\"occasion\" valueType=\"$ot\" columnNum=\"$cnum\"/>";
            if ($ot eq "int") { $occTagType = "Int"}
            if ($ot eq "real") { $occTagType = "Real"}
            if ($ot eq "string") { $occTagType = "String"}
          }
          else { 
            $ret.="\
                       <ds:Column columnId=\"OCC\" columnType=\"occasion\" valueType=\"real\" columnNum=\"$cnum\"/>";
          }
        }
      }
      else
      {
        print DDMORE_INDIV ",".$indivParam->[0]->[$coli];
        $ret.="\
                       <ds:Column columnId=\"".$indivParam->[0]->[$coli]."\" columnType=\"undefined\" valueType=\"real\" columnNum=\"".$cnum."\"/>";
      }
    }
    $cnum++;
  }
  print DDMORE_INDIV "\r\n";
  $ret.="\
                    </ds:Definition>";
  $retXML="\
                    <ds:Table xmlns=\"http://www.pharmml.org/pharmml/0.8/Dataset\">";
  for (my $rowi = 1;$rowi<scalar(@$indivParam);$rowi++)
  {
    $retXML.="\
                       <ds:Row>";
                         
    for (my $coli = 0;$coli<scalar(@{$indivParam->[$rowi]});$coli++)
    {
      if (($indivParam->[0]->[$coli] !~/_sd$/) && ($indivParam->[0]->[$coli] !~/^DISCARD_/))
      {
        if ($coli == 0)
        {
          print DDMORE_INDIV $indivParam->[$rowi]->[$coli];
          my @iov_ids = split(/#/,$indivParam->[$rowi]->[$coli]);
          $retXML.="\
                       <ct:String>".$iov_ids[0]."</ct:String>";
          for (my $iovi = 1;$iovi<scalar(@iov_ids);$iovi++) {
          $retXML.="\
                       <ct:$occTagType>".$iov_ids[$iovi]."</ct:$occTagType>";
          }
        
        }
        else
        {
          print DDMORE_INDIV ",".$indivParam->[$rowi]->[$coli];
          $retXML.="\
                       <ct:Real>".$indivParam->[$rowi]->[$coli]."</ct:Real>";
        }
      }
    }
    print DDMORE_INDIV "\r\n";
    $retXML.="\
                       </ds:Row>";
  }
  $retXML.="\
                    </ds:Table>";
  if ($config->getValue("output.individualEstimateMeanAsCSV")=~/true/)
  {
   $ret.="\
               <ds:ImportData oid=\"individualEstimateMeanAsCSV\">\
                  <ds:path>ddmore_indivmean.csv</ds:path> \
                  <ds:format>CSV</ds:format> \
                  <ds:delimiter>COMMA</ds:delimiter>\
               </ds:ImportData>"; 
  }
  else
  {
    $ret.=$retXML
  }
  $ret.="\
                  </Mean>
                </Estimates>";
  $ret.="\
                <RandomEffects> \
                    <EffectMean> \
                       <ds:Definition xmlns=\"http://www.pharmml.org/pharmml/0.8/Dataset\">";
  $cnum = 1;
  for (my $coli = 0;$coli<scalar(@{$indivEta->[0]});$coli++)
  {
    if (($indivEta->[0]->[$coli] !~/_sd$/) && ($indivEta->[0]->[$coli] !~/^DISCARD_/))
    {
      if ($coli == 0)
      {
        print DDMORE_INDIVETA $indivEta->[0]->[$coli];
        my @iov_ids = split(/#/,$indivEta->[1]->[$coli]);
        $ret.="\
                       <ds:Column columnId=\"".$indivEta->[0]->[$coli]."\" columnType=\"id\" valueType=\"string\" columnNum=\"$cnum\"/>";
        for (my $iovid = 1;$iovid<scalar(@iov_ids);$iovid++) {
          $cnum++;
          if (scalar(@occnames) == (scalar(@iov_ids)-1)) {
            my ($on,$ot) = split(/#/,$occnames[$iovid-1]);
            $ret.="\
                       <ds:Column columnId=\"$on\" columnType=\"occasion\" valueType=\"$ot\" columnNum=\"$cnum\"/>";
            if ($ot eq "int") { $occTagType = "Int"}
            if ($ot eq "real") { $occTagType = "Real"}
            if ($ot eq "string") { $occTagType = "String"}
          }
          else {
            $ret.="\
                           <ds:Column columnId=\"OCC\" columnType=\"occasion\" valueType=\"real\" columnNum=\"$cnum\"/>";
            $occTagType="Real";
          }
        }
      }
      else
      {
        print DDMORE_INDIVETA ",".$indivEta->[0]->[$coli];
        $ret.="\
                       <ds:Column columnId=\"".$indivEta->[0]->[$coli]."\" columnType=\"undefined\" valueType=\"real\" columnNum=\"".$cnum."\"/>";
      }
    }
    $cnum++;
  }
  
  $ret.="\
                      </ds:Definition>";
  $retXML="
                      <ds:Table xmlns=\"http://www.pharmml.org/pharmml/0.8/Dataset\">";
  print DDMORE_INDIVETA "\r\n";
  for (my $rowi = 1;$rowi<scalar(@$indivParam);$rowi++)
  {
    $retXML.="\
                       <ds:Row>";
                         
    for (my $coli = 0;$coli<scalar(@{$indivEta->[$rowi]});$coli++)
    {
      if (($indivEta->[0]->[$coli] !~/_sd$/) && ($indivEta->[0]->[$coli] !~/^DISCARD_/))
      {
        if ($coli == 0)
        {
          my @iov_ids = split(/#/,$indivEta->[$rowi]->[$coli]);
          print DDMORE_INDIVETA $indivEta->[$rowi]->[$coli];
          $retXML.="\
                       <ct:String>".$iov_ids[0]."</ct:String>";
          for (my $iovi = 1;$iovi<scalar(@iov_ids);$iovi++) {
          $retXML.="\
                       <ct:$occTagType>".$iov_ids[$iovi]."</ct:$occTagType>";
          }
        }
        else
        {
          print DDMORE_INDIVETA ",".$indivEta->[$rowi]->[$coli];
          $retXML.="\
                       <ct:Real>".$indivEta->[$rowi]->[$coli]."</ct:Real>";
        }
      }
    }
     print DDMORE_INDIVETA "\r\n";
    $retXML.="\
                       </ds:Row>";
  }
  $retXML.="\      
                      </ds:Table>";
  if ($config->getValue("output.randomEffectMeanAsCSV")=~/true/)
  {
   $ret.="\
               <ds:ImportData oid=\"RandomEffectMean\">\
                  <ds:path>ddmore_indiveta.csv</ds:path> \
                  <ds:format>CSV</ds:format> \
                  <ds:delimiter>COMMA</ds:delimiter>\
               </ds:ImportData>"; 
  }
  else
  {
    $ret.=$retXML;
  }
  $ret.="\                    
                     </EffectMean> \  
                </RandomEffects>";
                
  $ret.="\
             </IndividualEstimates>";
  close(DDMORE_INDIV);
  close(DDMORE_INDIVETA);
  return $ret;
}

sub getLL
{
  my $self            = shift;
  my $ll              = shift;
  my $bic             = shift;
  my $aic             = shift;
  my $contribToLL     = shift;
  my $outputDirectory = shift;
  my $config          = shift;

  my $retXML="";
  $ll =~s/\([^)]+\)//;
  $aic =~s/\([^)]+\)//;
  $bic =~s/\([^)]+\)//;
  $ll = -1 * $ll / 2;

  open (DDMORE_CRIT,">$outputDirectory/ddmore_criterion.csv") or die "Cannot create file '$outputDirectory/ddmore_criterion.csv'";
  open (DDMORE_CTOLL,">$outputDirectory/ddmore_ctoll.csv") or die "Cannot create file '$outputDirectory/ddmore_ctoll.csv'";
  my $idName = "FID";
  $idName = $contribToLL->[0]->[0];
  print DDMORE_CRIT "LL,AIC,BIC\r\n";
  print DDMORE_CRIT "$ll,$aic,$bic\r\n";
  print DDMORE_CTOLL "$idName,ICtoLL\r\n";
  my $ret="\
              <OFMeasures>
                <LogLikelihood>".$ll."</LogLikelihood>
                <IndividualContribToLL>
                    <ds:Definition xmlns=\"http://www.pharmml.org/pharmml/0.8/Dataset\">
                        <ds:Column columnId=\"".$idName."\" columnType=\"id\" valueType=\"string\" columnNum=\"1\"/>
                        <ds:Column columnId=\"ICtoLL\" columnType=\"undefined\" valueType=\"real\" columnNum=\"2\"/>
                    </ds:Definition>";
  $retXML="\
                    <ds:Table xmlns=\"http://www.pharmml.org/pharmml/0.8/Dataset\">";
  for (my $rowi = 1;$rowi<scalar(@$contribToLL);$rowi++)
  { 
    my $theId    = $contribToLL->[$rowi]->[0];
    my $theValue = $contribToLL->[$rowi]->[1];
    print DDMORE_CTOLL "$theId,$theValue\r\n";
    $retXML.="\
                        <ds:Row><ct:String>".$theId."</ct:String><ct:Real>".$theValue."</ct:Real></ds:Row>";
  }
  $retXML.="\
                    </ds:Table>";
  if ($config->getValue("output.individualContributionToLL")=~/true/)
  {
   $ret.="\
               <ds:ImportData oid=\"RandomEffectMean\">\
                  <ds:path>ddmore_ctoll.csv</ds:path> \
                  <ds:format>CSV</ds:format> \
                  <ds:delimiter>COMMA</ds:delimiter>\
               </ds:ImportData>"; 
  }
  else
  {
    $ret.=$retXML;
  }
  $ret.="\
                </IndividualContribToLL>
                <InformationCriteria>
                    <AIC>".$aic."</AIC>
                    <BIC>".$bic."</BIC>
                </InformationCriteria>
            </OFMeasures>";
  close(DDMORE_CRIT);
  close (DDMORE_CTOLL);
  return $ret;
}

sub getPredictionAndResidual
{
  my $self            = shift;
  my $pfiles = shift;
  my $outputDirectory = shift;
  my $config          = shift;
  my $pNames    = shift;
  my $retXML="";
  my $ret="";

  my @predictionsFiles = @$pfiles;
  my %pharmmlNames = %$pNames;
  my @occnames = ();
  my $occTagType="Real";

  foreach my $pn (keys %pharmmlNames) {
    if ($pn=~/occasion/) {
      my @ptoks = split(/:/,$pn);
      my $vt = $pharmmlNames{$pn};
      push @occnames,$ptoks[1]."#".$vt;
    }
  }


  my $countPredictions = scalar(@predictionsFiles);

  if ($countPredictions && scalar( @{$predictionsFiles[0]->{predictions}})) {

    open (DDMORE_PRED,">$outputDirectory/ddmore_pred.csv") or die "Cannot create file '$outputDirectory/ddmore_pred.csv'";
    open (DDMORE_POPWRES,">$outputDirectory/ddmore_popwres.csv") or die "Cannot create file '$outputDirectory/ddmore_popwres.csv'";
    open (DDMORE_INDWRES,">$outputDirectory/ddmore_indwres.csv") or die "Cannot create file '$outputDirectory/ddmore_indwres.csv'";

    if (scalar(@predictionsFiles)>0  
        && scalar(@{$predictionsFiles[0]->{predictions}->[0]}) > 8)
    {
    print DDMORE_POPWRES "ID,TIME,RES\r\n";
    my @predictions = @{$predictionsFiles[0]->{predictions}};
    my $cnum = 2;
    $ret.="\
              <Residuals><ResidualTable>
                  <ds:Definition xmlns=\"http://www.pharmml.org/pharmml/0.8/Dataset\">
                      <ds:Column columnId=\"ID\" columnType=\"id\" valueType=\"string\" columnNum=\"1\"/>";
    if (scalar(@predictions) > 1) {
       my @iov_ids    = split(/#/,$predictions[1]->[0]);
       for (my $iovi = 1;$iovi<scalar(@iov_ids);$iovi++) {
          if (scalar(@occnames) == (scalar(@iov_ids)-1)) {
            my ($on,$ot) = split(/#/,$occnames[$iovi-1]);
            $ret.="<ds:Column columnId=\"$on\" columnType=\"occasion\" valueType=\"$ot\" columnNum=\"$cnum\"/>";
            if ($ot eq "int") { $occTagType = "Int"}
            if ($ot eq "real") { $occTagType = "Real"}
            if ($ot eq "string") { $occTagType = "String"}
          }
          else {          
            $ret.="<ds:Column columnId=\"OCC\" columnType=\"occasion\" valueType=\"real\" columnNum=\"$cnum\"/>";
          }
          $cnum++;
       }
    }

    $ret.="\
                      <ds:Column columnId=\"TIME\" columnType=\"idv\" valueType=\"real\" columnNum=\"$cnum\"/>";
    $cnum++;
    $ret.="\
                      <ds:Column columnId=\"WRES\" columnType=\"undefined\" valueType=\"real\" columnNum=\"$cnum\"/>";
    $cnum++;
    $ret.="\
                      <ds:Column columnId=\"IWRES\" columnType=\"undefined\" valueType=\"real\" columnNum=\"$cnum\"/>\n";
      if (scalar(@predictionsFiles) > 1) {
    $cnum++;
        $ret.="       <ds:Column columnId=\"DVID\" columnType=\"undefined\" valueType=\"real\" columnNum=\"$cnum\"/>\n";
      }
      $ret.="</ds:Definition>";
      $retXML="
                  <ds:Table xmlns=\"http://www.pharmml.org/pharmml/0.8/Dataset\">";

  for (my $fi = 0;$fi < scalar(@predictionsFiles);$fi++)
  { 
    my @predictions = @{$predictionsFiles[$fi]->{predictions}};
      my $WRES_IDX  = 6;
      my $IWRES_IDX = 8;
      for (my $ki = 0;$ki<scalar(@{$predictions[0]});$ki++) {
        my $hn = $predictions[0]->[$ki]; 
        $hn=~s/^\s+//;
        $hn=~s/\s+$//;
        if ($hn eq "popWRes") {
          $WRES_IDX = $ki;
        }
        if ($hn eq "indWRes_mean") {
          $IWRES_IDX = $ki;
        }
      }
    for (my $rowi = 1;$rowi<scalar(@predictions);$rowi++)
    {
       my @iov_ids    = split(/#/,$predictions[$rowi]->[0]);
       my $time  = $predictions[$rowi]->[1];
       my $res   = $predictions[$rowi]->[$WRES_IDX];
       my $iwres = $predictions[$rowi]->[$IWRES_IDX];
       print DDMORE_POPWRES "$iov_ids[0],$time,$res,$iwres\r\n";
       $retXML.="\
                      <ds:Row><ct:String>".$iov_ids[0]."</ct:String>";
       for (my $iovi=1;$iovi<scalar(@iov_ids);$iovi++) {
        $retXML.="<ct:$occTagType>".$iov_ids[$iovi]."</ct:$occTagType>";
       }
       $retXML.="<ct:Real>".$time."</ct:Real><ct:Real>".$res."</ct:Real><ct:Real>".$iwres."</ct:Real>";
       if (scalar(@predictionsFiles) > 1) {
          $retXML.="<ct:Real>".($fi+1)."</ct:Real>";
       }
       $retXML.="</ds:Row>\n";
    } 
  }

    $retXML.="\ 
                  </ds:Table> \
               </ResidualTable></Residuals>";
    $ret.=$retXML;
  }

    print DDMORE_PRED "ID,TIME,PRED,IPRED";
    if (scalar(@predictionsFiles) > 1) {
      print DDMORE_PRED ",DVID";
    }
    print DDMORE_PRED "\r\n";
    $ret.= "\
              <Predictions>
                <ds:Definition xmlns=\"http://www.pharmml.org/pharmml/0.8/Dataset\">
                    <ds:Column columnId=\"ID\" columnType=\"id\" valueType=\"string\" columnNum=\"1\"/>\n";
    my $cnum = 2; 
    if (scalar(@predictionsFiles) > 0)
    {
      my @predictions = @{$predictionsFiles[0]->{predictions}};
      if (scalar(@predictions)>1) {
          my @iov_ids = split(/#/,$predictions[1]->[0]);
          for (my $iovi = 1;$iovi<scalar(@iov_ids);$iovi++) {
            if (scalar(@occnames) == (scalar(@iov_ids) - 1)) {
              my ($on,$ot) = split(/#/,$occnames[$iovi-1]);
              $ret.= "\
                          <ds:Column columnId=\"$on\" columnType=\"occasion\" valueType=\"$ot\" columnNum=\"$cnum\"/>\n";
              if ($ot eq "int") { $occTagType = "Int"}
              if ($ot eq "real") { $occTagType = "Real"}
              if ($ot eq "string") { $occTagType = "String"}
            }
            else {
              $ret.= "\
                          <ds:Column columnId=\"OCC\" columnType=\"occasion\" valueType=\"real\" columnNum=\"$cnum\"/>\n";
              $occTagType="Real";
            }
            $cnum++;
          }

      }
    }
    
    $ret.= "\
                    <ds:Column columnId=\"TIME\" columnType=\"idv\" valueType=\"real\" columnNum=\"$cnum\"/>\n";
    $cnum++;
    $ret.= "\
                    <ds:Column columnId=\"PRED\" columnType=\"undefined\" valueType=\"real\" columnNum=\"$cnum\"/>\n";
    $cnum++;
      if (scalar(@predictionsFiles) && scalar(@{$predictionsFiles[0]->{predictions}->[0]}) > 4) {
                 $ret.="   <ds:Column columnId=\"IPRED\" columnType=\"undefined\" valueType=\"real\" columnNum=\"$cnum\"/>\n";
      }
    $cnum++;
      if (scalar(@predictionsFiles) > 1) {
        $ret.="       <ds:Column columnId=\"DVID\" columnType=\"undefined\" valueType=\"real\" columnNum=\"$cnum\"/>\n";
      }
   
    $ret.= "\
                </ds:Definition>";
    $retXML="\
                <ds:Table xmlns=\"http://www.pharmml.org/pharmml/0.8/Dataset\">";
    for (my $fi = 0;$fi < scalar(@predictionsFiles);$fi++)
    { 
      my @predictions = @{$predictionsFiles[$fi]->{predictions}};
      my $PRED_IDX  = 3;
      my $IPRED_IDX = 4;
      for (my $ki = 0;$ki<scalar(@{$predictions[0]});$ki++) {
        my $hn = $predictions[0]->[$ki]; 
        $hn=~s/^\s+//;
        $hn=~s/\s+$//;
        if ($hn eq "popPred") {
          $PRED_IDX = $ki;
        }
        if ($hn eq "indPred_mean") {
          $IPRED_IDX = $ki;
        }
      }
      # check headers 
      for (my $rowi = 1;$rowi<scalar(@predictions);$rowi++)
      {
        my $id    = $predictions[$rowi]->[0];
        my $time  = $predictions[$rowi]->[1];
        my $pred  = $predictions[$rowi]->[$PRED_IDX];
        my $withIPred = scalar(@{$predictions[$rowi]});
        if ($withIPred > 4) {
          my $ipred = $predictions[$rowi]->[$IPRED_IDX];
          my @iov_ids=split(/#/,$id);
          print DDMORE_PRED "$id,$time,$pred,$ipred\r\n";
          $retXML.="\
                      <ds:Row><ct:String>".$iov_ids[0]."</ct:String>";
          for (my $iovi = 1;$iovi<scalar(@iov_ids);$iovi++) {
            $retXML.=("<ct:$occTagType>".$iov_ids[$iovi]."</ct:$occTagType>");
          }
          $retXML.="<ct:Real>".$time."</ct:Real><ct:Real>".$pred."</ct:Real><ct:Real>".$ipred."</ct:Real>";
          if (scalar(@predictionsFiles) > 1) {
            $retXML.="<ct:Real>".($fi+1)."</ct:Real>";
          }
          $retXML.="</ds:Row>\n";
        }
        else {
          $id=~s/#\d+$//;
          print DDMORE_PRED "$id,$time,$pred\r\n";
          $retXML.="\
                      <ds:Row><ct:String>".$id."</ct:String><ct:Real>".$time."</ct:Real><ct:Real>".$pred."</ct:Real>";
          if (scalar(@predictionsFiles) > 1) {
            $retXML.="<ct:Real>".($fi+1)."</ct:Real>";
          }
          $retXML.="</ds:Row>\n";
        }
      }
    }
    $retXML.="\
                </ds:Table>";
    if ($config->getValue("output.predictionsAsCSV")=~/true/)
    {
     $ret.="\
                 <ds:ImportData oid=\"Predictions\">\
                    <ds:path>ddmore_pred.csv</ds:path> \
                    <ds:format>CSV</ds:format> \
                    <ds:delimiter>COMMA</ds:delimiter>\
                 </ds:ImportData>"; 
    }
    else
    {
      $ret.=$retXML;
    }
    $ret.="\
              </Predictions>";

    close (DDMORE_PRED);
    close (DDMORE_POPWRES);
    close (DDMORE_INDWRES);
  }
  return $ret;
}


1;
__END__
