package lixoft::OutputContainer;

use strict;
use warnings;

sub new 
{
  my $package = shift;
  my $class = ref($package) || $package;
  my $self = {
               TABLES=>{}
             };
  return bless $self, $class;
}

sub setValue 
{
    my $self = shift;
    my $name = shift;
    my $headerRef = shift;
    my $dataRef = shift;
    @{$self->{TABLES}->{"$name"}->{"header"}} = @{$headerRef};
    @{$self->{TABLES}->{"$name"}->{"data"}} = @{$dataRef};

}

sub getHeader
{
  my $self = shift;
  my $name = shift;
  return $self->{TABLES}->{"$name"}->{"header"};
}

sub getData
{
  my $self = shift;
  my $name = shift;
  return $self->{TABLES}->{"$name"}->{"data"};
}


1;
__END__
