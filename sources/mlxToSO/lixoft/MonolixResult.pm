package lixoft::MonolixResult;

use strict;
use warnings;

use Data::Dumper;

sub new 
{
  my $package = shift;
  my $class = ref($package) || $package;
  my $self = {
               DATA=>{}
             };
  return bless $self, $class;
}

sub loadPharmMLName
{
  my $self = shift;
  my $pharmMLNamesFile = shift;
  open(PHARMMLNAMES, "<$pharmMLNamesFile") or die "Cannot open '$pharmMLNamesFile'\n";
  # create mapping hash table
  my %mapPharmMLNames = ();
  while(<PHARMMLNAMES>)
  {
    chomp;
    s/\s+//g;
    if (/\:beta\:/)
    {
      my @toks = split(/[\:=#]/);
      my $betaName = "beta_$toks[0]_$toks[2]";
      if ($toks[5]!~/__UNDEF__/) {
        $betaName.=("_".$toks[5]);
      }
      $mapPharmMLNames{$betaName} = $toks[4];
    }
    elsif (/\:omega/)
    {
      my @toks = split(/[\:=]/);
      my $omegaName = "omega_$toks[0]";
      $mapPharmMLNames{$omegaName} = $toks[2];
      $omegaName = "omega2_$toks[0]";
      $mapPharmMLNames{$omegaName} = $toks[2];
    }
    elsif (/\:gamma/)
    {
      my @toks = split(/[\:=]/);
      my $omegaName = "gamma_$toks[0]";
      $mapPharmMLNames{$omegaName} = $toks[2];
      $omegaName = "gamma2_$toks[0]";
      $mapPharmMLNames{$omegaName} = $toks[2];
    }
    elsif (/\:pop/)
    {
      my @toks = split(/[\:=]/);
      my $popName = "$toks[0]_pop";
      $mapPharmMLNames{$popName} = $toks[2];
    }
    elsif (/\:errorParameter\:/)
    {
      my @toks = split(/[\:=]/);
      my $errParamName = "errorPx$toks[0]";
      if (!exists($mapPharmMLNames{$errParamName})) {
        $mapPharmMLNames{$errParamName} = "$toks[3]";
      }
      else {
        $mapPharmMLNames{$errParamName} .= "#$toks[3]";
      }
    }
    elsif (/\:eta/)
    {
      my @toks = split(/[\:=]/);
      $mapPharmMLNames{"eta_$toks[0]"} = $toks[2];
    }
    elsif(/\:correlation_as_cov/) {
      my @toks = split(/[\:=#]/);
      $mapPharmMLNames{"ETA_".$toks[0]."_ETA_".$toks[1]} = $toks[scalar(@toks)-1];
      $mapPharmMLNames{"ETA_".$toks[1]."_ETA_".$toks[0]} = $toks[scalar(@toks)-1];
      $mapPharmMLNames{"ETACOV_".$toks[0]."_ETACOV_".$toks[1]} = $toks[scalar(@toks)-1];
      $mapPharmMLNames{"ETACOV_".$toks[1]."_ETACOV_".$toks[0]} = $toks[scalar(@toks)-1];
    }
    elsif(/\:correlation/) {
      my @toks = split(/[\:=#]/);
      $mapPharmMLNames{"ETA_".$toks[0]."_ETA_".$toks[1]} = $toks[scalar(@toks)-1];
      $mapPharmMLNames{"ETA_".$toks[1]."_ETA_".$toks[0]} = $toks[scalar(@toks)-1];
    }
    elsif(/\:occasion/) {
      my @toks = split(/=/);
      $mapPharmMLNames{$toks[0]} = $toks[1];
    }
  }
  my $cmodel = 0;
  foreach my $entry (keys %mapPharmMLNames) {
    if ($entry =~/errorPx/) {
     $cmodel++ 
    }
  }
  my $id = 1; 
  foreach my $entry (sort { $a cmp $b } keys %mapPharmMLNames) {
    if ($entry =~/errorPx/) { 
      if ($cmodel > 1) {
        my @errp = split(/#/,$mapPharmMLNames{$entry}); 
        if (scalar(@errp) == 1) {
          $mapPharmMLNames{"a_$id"} = $errp[0];
        }   
        elsif (scalar(@errp) == 2) {
          $mapPharmMLNames{"a_$id"} = $errp[0];
          $mapPharmMLNames{"b_$id"} = $errp[1];
        }   
        elsif (scalar(@errp) == 3) {
          $mapPharmMLNames{"a_$id"} = $errp[0];
          $mapPharmMLNames{"b_$id"} = $errp[1];
          $mapPharmMLNames{"c_$id"} = $errp[2];
        }
        $id++;
      }
      else {
        my @errp = split(/#/,$mapPharmMLNames{$entry}); 
        if (scalar(@errp) == 1) {
          $mapPharmMLNames{"a"} = $errp[0];
        }   
        elsif (scalar(@errp) == 2) {
          $mapPharmMLNames{"a"} = $errp[0];
          $mapPharmMLNames{"b"} = $errp[1];
        }   
        elsif (scalar(@errp) == 3) {
          $mapPharmMLNames{"a"} = $errp[0];
          $mapPharmMLNames{"b"} = $errp[1];
          $mapPharmMLNames{"c"} = $errp[2];
        }
      }
    }
  }
  close(PHARMMLNAMES);
  return %mapPharmMLNames;
}

sub loadCriterion
{
  my $self = shift;
  my $criterionFile = shift;
  my $minus2LL  = "";
  my $bic       = "";
  my $aic       = "";
  open(CRITERION, "<$criterionFile")  or die "Cannot open '$criterionFile'\n";
  while(<CRITERION>)
  {
    if (/log-likelihood/)
    {
      my @toks = split(/:/);
      $toks[1]=~s/\s+//g;
      $minus2LL = $toks[1];
    }
    elsif(/Akaike Information Criteria/)
    {
      my @toks = split(/:/);
      $toks[1]=~s/\s+//g;
      $aic=$toks[1];
    }
    elsif(/Bayesian Information Criteria/)
    {
      my @toks = split(/:/);
      $toks[1]=~s/\s+//g;
      $bic=$toks[1];
    }
  }
  close(CRITERION);
  return ($minus2LL,$bic,$aic);
}

sub loadCorrelation
{
  my $self = shift;
  my $criterionFile = shift;
  my $pname = shift;
  my $inCorrelation  = 0;
  open(CRITERION, "<$criterionFile")  or die "Cannot open '$criterionFile'\n";
  my @correlation = ();
  my %pharmmlNames = %$pname;
  my %covOrCor = ();
  my %cov = ();
  open(CRITERION, "<$criterionFile")  or die "Cannot open '$criterionFile'\n";
  while(<CRITERION>) {
    if (/corr\(([^,]+),([^\)]+)\)\s*\:\s*/) {
      my $p1 = $1;
      my $p2 = $2;
      my @toks=split(/:/);
      my $l1 = $toks[1];
      $l1=~s/^\s+//; 
      $l1=~s/\s+$//; 
      @toks = split(/\s+/,$l1); 
      $covOrCor{$p1."#".$p2} = $toks[0];
    }
    if (/omega2_([^\s]+)\s*\:\s*([^\s]+)/) {
      $cov{$1} = sqrt($2);
    }
    if (/omega_([^\s]+)\s*\:\s*([^\s]+)/) {
      $cov{$1} = $2;
    }
  }
  foreach my $c1 (keys %cov) { 
    my @corrline = ();
    push @corrline,$c1;
    foreach my $c2 (keys %cov) {
      my $covcor1 = "ETACOV_".$c1."_ETACOV_".$c2;
      my $covcor2 = "ETACOV_".$c2."_ETACOV_".$c1;
      my $coef = 0;
      if ($c1 eq $c2) {
        $coef = 1;
      }
      elsif (exists($pharmmlNames{$covcor1})) {
        if (exists($covOrCor{$c1."#".$c2})) {
          #$coef = $cov{$c1} * $cov{$c2} * $covOrCor{$c1."#".$c2};
          $coef = $covOrCor{$c1."#".$c2};
        }
        elsif (exists($covOrCor{$c2."#".$c1})) {
          #$coef = $cov{$c1} * $cov{$c2} * $covOrCor{$c2."#".$c1};
          $coef = $covOrCor{$c2."#".$c1};
        }
      }
      elsif (exists($pharmmlNames{$covcor2})) {
        if (exists($covOrCor{$c1."#".$c2})) {
          #$coef = $cov{$c1} * $cov{$c2} * $covOrCor{$c1."#".$c2};
          $coef = $covOrCor{$c1."#".$c2};
        }
        elsif (exists($covOrCor{$c2."#".$c1})) {
          #$coef = $cov{$c1} * $cov{$c2} * $covOrCor{$c2."#".$c1};
          $coef = $covOrCor{$c2."#".$c1};
        }
      }
      elsif (exists($covOrCor{$c1."#".$c2})) {
          $coef = $covOrCor{$c1."#".$c2};
      }
      elsif (exists($covOrCor{$c2."#".$c1})) {
          $coef = $covOrCor{$c2."#".$c1};
      }
      push @corrline,$coef;
    }
    push @correlation,\@corrline;  
  }
#  print STDERR Dumper @correlation;
  close(CRITERION);
  
  if (!scalar(@correlation)) { 
    open(CRITERION, "<$criterionFile")  or die "Cannot open '$criterionFile'\n";
    while(<CRITERION>)
    {
       chomp;
       if ($inCorrelation)
       {
          $_=~s/^\s*//;
          $_=~s/\s*$//;
          if (/______/ || /Eigenvalue/ || (length($_)==0 && scalar(@correlation) > 0))
          {
            $inCorrelation = 0;
          }
          else
          {
              s/^\s*//g;
              s/\s*$//g;
              my @toks = split(/\s+/);
              if (scalar(@toks) > 1)
              {
                push @correlation,\@toks;
              }
          }
       }
       if (/correlation matrix of the estimates/)
       {
         $inCorrelation = 1;
       }
       elsif (/correlation matrix/)
       {
       }
    }
    close(CRITERION);
  }
  return @correlation;
}

sub loadParamNames
{
  my $self = shift;
  my $estimatesFile = shift;
  my @paramNames;
  open(ESTIMATES,"<$estimatesFile") or die "Cannot open '$estimatesFile'\n";
  my $skipFirstLine = <ESTIMATES>;
  while(<ESTIMATES>)
  {
    chomp;
    my @splitLine = split(/\s*;\s*/);
    $splitLine[0]=~s/\s*//g;
    push @paramNames,$splitLine[0];
  }
  close(ESTIMATES);
  return @paramNames;
}

sub loadSE
{
  my $self = shift;
  my $estimatesFile = shift;
  my @se;
  open(ESTIMATES,"<$estimatesFile") or die "Cannot open '$estimatesFile'\n";
  my $skipFirstLine = <ESTIMATES>;
  while(<ESTIMATES>)
  {
    chomp;
    my @splitLine = split(/\s*;\s*/);
    $splitLine[2]=~s/\s*//g;
    push @se,$splitLine[2];
  }
  close(ESTIMATES);
  return @se;
}

sub loadRSE
{
  my $self = shift;
  my $estimatesFile = shift;
  my @rse;
  open(ESTIMATES,"<$estimatesFile") or die "Cannot open '$estimatesFile'\n";
  my $skipFirstLine = <ESTIMATES>;
  while(<ESTIMATES>)
  {
    chomp;
    my @splitLine = split(/\s*;\s*/);
    $splitLine[3]=~s/\s*//g;
    push @rse,$splitLine[3];
  }

  close(ESTIMATES);
  return @rse;
}

sub loadPValue
{
  my $self = shift;
  my $estimatesFile = shift;
  my @pvalue;
  open(ESTIMATES,"<$estimatesFile") or die "Cannot open '$estimatesFile'\n";
  my $skipFirstLine = <ESTIMATES>;
  while(<ESTIMATES>)
  {
    chomp;
    my @splitLine = split(/\s*;\s*/);
    $splitLine[4]=~s/\s*//g;
    push @pvalue,$splitLine[4];
  }
  close(ESTIMATES);
  return @pvalue;
}

sub loadPopulationParameters
{
  my $self = shift;
  my $estimatesFile = shift;
  my @params;
  my $columnCount = 0;
  open(ESTIMATES,"<$estimatesFile") or die "Cannot open '$estimatesFile'\n";
  my $skipFirstLine = <ESTIMATES>;
  while(<ESTIMATES>)
  {
    chomp;
    my @splitLine = split(/\s*;\s*/);
    $splitLine[1]=~s/\s*//g;
    push @params,$splitLine[1];
  }
  close(ESTIMATES);
  return @params;
}

sub loadFim
{
  my $self = shift;
  my $fimFile = shift;
  my @fim;
  my $fim_exist = 1;
  my $columnCount = 0;
  open(FIM,"<$fimFile") or $fim_exist=0;
  if (!$fim_exist){ 
    my $tmpmess = "Cannot open '$fimFile' "; 
    $fimFile=~s/_lin/_sa/;
    warn "$tmpmess try '$fimFile'\n";
    $fim_exist = 1;
  }
  open(FIM,"<$fimFile") or $fim_exist=0;
  if (!$fim_exist){ 
    warn "Cannot open '$fimFile'\n"; 
  }
  while(<FIM>)
  {
    chomp;
    my @splitLine = split(/\s*[;:]\s*/);
    for(my $si = 0;$si<scalar(@splitLine);$si++)
    {
        $splitLine[$si]=~s/\s*//g;
    }
    push @fim,\@splitLine;
  }
  close(FIM);
  return @fim;
}

sub loadIndividualParameters
{
  my $self = shift;
  my $indivParamFile = shift;
  my @indivParam;
  open(INDIVPARAM,"<$indivParamFile") or die "Cannot open '$indivParamFile'\n";
  while(<INDIVPARAM>)
  {
    chomp;
    s/^\s*//g;
    s/\s*$//g;
    my @splitLine = split(/\s+/);
    for(my $si = 0;$si<scalar(@splitLine);$si++)
    {
        $splitLine[$si]=~s/\s*//g;
    }
    push @indivParam,\@splitLine;
  }
  close(INDIVPARAM);
  return @indivParam;
}

sub loadIndividualEta
{
  my $self = shift;
  my $indivEtaFile = shift;
  my @indivEta;
  open(INDIVETA,"<$indivEtaFile") or die "Cannot open '$indivEtaFile'\n";
  while(<INDIVETA>)
  {
    chomp;
    s/^\s*//g;
    s/\s*$//g;
    my @splitLine = split(/\s+/);
    for(my $si = 0;$si<scalar(@splitLine);$si++)
    {
        $splitLine[$si]=~s/\s*//g;
    }
    push @indivEta,\@splitLine;
  }
  close(INDIVETA);
  return @indivEta;
}

sub loadContribToLL
{
  my $self = shift;
  my $cToLLFile = shift;
  my @cToLL;
  open(CTOLL,"<$cToLLFile") or die "Cannot open '$cToLLFile'\n";
  while(<CTOLL>)
  {
    chomp;
    s/^\s*//g;
    s/\s*$//g;
    my @splitLine = split(/\s+/);
    for(my $si = 0;$si<scalar(@splitLine);$si++)
    {
        $splitLine[$si]=~s/\s*//g;
    }
    push @cToLL,\@splitLine;
  }
  close(CTOLL);
  return @cToLL;
}

sub loadPredictions
{
  my $self = shift;
  my $predictionsFile = shift;
  my @predictions;
  open(PRED,"<$predictionsFile") or die "Cannot open '$predictionsFile'\n";
  while(<PRED>)
  {
    chomp;
    s/^\s*//g;
    s/\s*$//g;
    my @splitLine = split(/\s+/);
    for(my $si = 0;$si<scalar(@splitLine);$si++)
    {
        $splitLine[$si]=~s/\s*//g;
    }
    push @predictions,\@splitLine;
  }
  close(PRED);
  return @predictions;
}

sub loadMessage
{
    my $self = shift;
    my $outFile = shift;
    my $convFile = shift;
    my @message;
    
    open(MESSAGE, "<$outFile") or die "Cannot open '$outFile'\n";
    my @correlation = ();
    while(<MESSAGE>)
    {
      chomp;
      if (/Elapsed time is /)
      {
        s/Elapsed time is //g;
        s/ seconds.+$//g;
        push @message, "ET:$_";
      }
    }
    close(MESSAGE);
    open(CONVOUT,"<$convFile") or die "Cannot open '$convFile'\n";
    while(<CONVOUT>)
    {
      chomp;
      if (/warning/)
      {
        push @message,"WARN:$_";
      }
      elsif (/error/)
      {
        push @message,"ERROR:$_";
      }
    }
    close(CONVOUT);
    return @message;
}

1;
__END__
