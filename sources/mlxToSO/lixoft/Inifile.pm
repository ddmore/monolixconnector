package lixoft::Inifile;

use strict;
use warnings;

sub new 
{
  my $package = shift;
  my $class = ref($package) || $package;
  my $self = {
               DATA=>{}
             };
  return bless $self, $class;
}

sub load 
{
    my $self = shift;
    my $inifile = shift;
    # open file read only
    open(IN, $inifile) || return -1;
    my %data;

    my $k;
    my $v;
    my $count = 0;
    my $section_name = '';
    while (<IN>)
    {
        chomp;
        # skip blank lines and comments
        next if /^\s*$/o; next if /^;/o;
        # it's either a section heading or a key=value pair
        if (/^\[\s*([^\]]+)\]\s*/o)
        {
            $section_name = $1;
            $self->{DATA}->{$section_name} = {};
        }
        else
        {
            ($k, $v) = split(/\s*=/o, $_, 2);
            # trim trailing white-space
            $v =~ s/\s+$//o;
            $self->{DATA}->{$section_name}->{$k} = $v;
        }
    }
    close(IN);
    return 0;
}

sub getValue 
{
  my $self = shift;
  my $keypath = shift;
  my ($section,$key) = split(/\./,$keypath);
  if (!exists($self->{DATA}->{"$section"})) 
  { return ""; }
  if (!exists($self->{DATA}->{"$section"}->{"$key"})) 
  { return ""; }
  return $self->{DATA}->{$section}->{$key};
}

sub setValue 
{
  my $self = shift;
  my $keypath = shift;
  my $keyvalue = shift;
  my ($section,$key) = split(/\./,$keypath);
  $self->{DATA}->{"$section"}->{"$key"} = $keyvalue;
  return $self;
}

1;
__END__
