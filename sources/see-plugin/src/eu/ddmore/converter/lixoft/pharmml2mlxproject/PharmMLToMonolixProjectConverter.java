package eu.ddmore.converter.lixoft.pharmml2mlxproject;
import eu.ddmore.converter.lixoft.utils.LixoftToolsInitializer;
import eu.ddmore.convertertoolbox.api.domain.LanguageVersion;
import eu.ddmore.convertertoolbox.api.domain.Version;
import eu.ddmore.convertertoolbox.domain.LanguageVersionImpl;
import eu.ddmore.convertertoolbox.domain.VersionImpl;
import eu.ddmore.convertertoolbox.api.response.ConversionReport;
import eu.ddmore.converter.lixoft.commons.LixoftConversionReport;
import java.io.File;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;



/**
 *
 * @author blaudez
 */
public class PharmMLToMonolixProjectConverter implements eu.ddmore.convertertoolbox.api.spi.ConverterProvider {

    LanguageVersion target = null;
    LanguageVersion source = null;
    Version converterVersion = null;
    
    public static String VERSION="0.0.35b";
    
    public PharmMLToMonolixProjectConverter()
    {
        Version targetVersion = new VersionImpl(4,3,2);
        target = new LanguageVersionImpl("MonolixProject",targetVersion);
        
        Version sourceVersion = new VersionImpl(0,8,1);
        source = new LanguageVersionImpl("PharmML",sourceVersion); 
        
        LixoftToolsInitializer.installTools();
    }
    
    @Override
    @SuppressWarnings("empty-statement")
    public ConversionReport performConvert(File pharmml, File mlxproject) throws IOException
    {
        if (!mlxproject.isDirectory())
            throw new IOException("Destination '" + mlxproject.getAbsolutePath() +"' should be a directory.");
        if (!pharmml.isFile())
            throw new IOException("Source '"+ pharmml.getCanonicalPath() +"' should be a file.");
        
        String programPath = LixoftToolsInitializer.getToolsPath();
        if (programPath == null)
        {
            throw new IOException("Program path is not correctly set : the extraction of tools has certainly failed.");
        }
        else if (programPath.isEmpty())
        {
            throw new IOException("Empty program path : the extraction of tools has certainly failed. ");
        }
        String externalCmd=programPath + File.separator + "lixoftLanguageTranslator.exe";
        externalCmd = externalCmd.replace('/', '\\');
        File testExternal = new File(externalCmd);
        if (!testExternal.exists())
        {
            // alternative test (maybe linux)
            String linuxExec =programPath + File.separator + "lixoftLanguageTranslator";
            
            File testlinuxExternal = new File(linuxExec);
            if (!testlinuxExternal.exists())
            {
              throw new IOException("Cannot found '"+externalCmd+"[.exe]' tool, please check your installation.");
            }
            
            externalCmd = linuxExec;
        }
        
        
        externalCmd=externalCmd + " --pharmml-version=0.8 --from=pharmml --to=mlxproject --input-file="
                                + pharmml.getCanonicalFile() + " --output-dir="
                                + mlxproject.getAbsolutePath();
        System.err.println("Exec:"+externalCmd);
        Process p = Runtime.getRuntime().exec(externalCmd);
        BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
        LixoftConversionReport cr = new LixoftConversionReport();
        cr.addBufferedDetail(stdError);
        
        try {
                 p.waitFor();  // wait for process to complete
        } catch (InterruptedException e) {
                System.err.println(e);  // "Can't Happen"
        };
        Writer writer = null;
        try {
              writer = new BufferedWriter(new OutputStreamWriter(
              new FileOutputStream(mlxproject.getAbsolutePath()+File.separator+"converter.out"), "utf-8"));
              writer.write(cr.toString());
            } catch (IOException ex) {
        } finally {
             try {writer.close();} catch (Exception ex) {}
        }
        return cr;        
    }

    @Override
    public LanguageVersion getSource() {                
        return source;
    }

    @Override
    public LanguageVersion getTarget() {
        return target;
    }

    @Override
    public Version getConverterVersion() {
        return converterVersion;
    }
    
    @Override
    public String toString()
    {
        String converterString;
        converterString = "[PharmMLToMonolixProjectConverter]";
        if (converterVersion != null)
        {
            converterString += ("version:["+converterVersion.toString()+"]");
        }
        else 
        {
            converterString += "version: no version";
        }
        if (source != null)
        {
            converterString += ("source:["+source.toString() + "]");
        }
        else 
        {
            converterString += "source:no source";
        }
        if (target != null)
        {
            converterString += (", target:["+target.toString() + "]");
        }
        else 
        {
            converterString += ", target:no target";
        }
        return converterString;
    }
}
