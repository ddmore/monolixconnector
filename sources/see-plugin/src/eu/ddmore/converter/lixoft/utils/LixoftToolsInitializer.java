package eu.ddmore.converter.lixoft.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class LixoftToolsInitializer 
{
    private static String toolsPath = null;
    
    public static void installTools()
    {
        String destinationPath = System.getProperty("java.io.tmpdir")
                                + File.separator + "lixoft" 
                                + File.separator + "ddmore";
        if (destinationPath.length() == 0)
        {
            destinationPath = System.getProperty("user.home") 
                                 + File.separator + "lixoft" 
                                 + File.separator + "ddmore";
        }
        toolsPath=destinationPath + File.separator + "lib";                
        if (new File(destinationPath).exists())
        {
            // test property file
            String versionFile = destinationPath + File.separator + "version.properties";
            // property file exists
            if (new File(versionFile).exists())
            {
                Properties prop = new Properties();
                InputStream input = null;

                try {

                       input = new FileInputStream(destinationPath + File.separator + "version.properties");
                        // load a properties file
                       prop.load(input);
                       String converterVersion = prop.getProperty("version");
                       if (converterVersion.equals(eu.ddmore.converter.lixoft.pharmml2mlxproject.PharmMLToMonolixProjectConverter.VERSION))
                       {
                          return;
                       }


                } catch (IOException ex) {
                        ex.printStackTrace();
                } finally {
                        if (input != null) {
                                try {
                                        input.close();
                                } catch (IOException e) {
                                        e.printStackTrace();
                                }
                        }
                }
            }
            else // create propertie file
            {
 
            }
            
        }
        System.out.println("Extract lixoft tools.");
        byte[] buffer = new byte[1024]; 
        try{
           String jarFile = LixoftToolsInitializer.class.getProtectionDomain().getCodeSource().getLocation().getPath();
           jarFile = jarFile.replace("file:/","");
           jarFile = jarFile.replace("!/","");
           // check OS
           String toolsOS = "win64";
           boolean is64bit = (System.getProperty("os.arch").indexOf("64") != -1);
           if (System.getProperty("os.name").contains("Linux"))
           {
               toolsOS = "linux64";
           }
           if (!is64bit) {
               toolsOS="win32";
           }
         
           //get the zip file content
           ZipInputStream zis;
           try {
             zis = new ZipInputStream(new FileInputStream(jarFile));
           }
           catch(java.io.FileNotFoundException e) {
               throw new java.io.FileNotFoundException("LIXOFT File not Found ["+jarFile+"]:"+e.getMessage());
           }
           //get the zipped file list entry
           ZipEntry ze = zis.getNextEntry();

           while(ze!=null){
              String fileName = ze.getName();
              // filter
              boolean extractDirectory = fileName.contains("lixofttools") && fileName.contains(toolsOS);
              int idx = fileName.indexOf(toolsOS);
              if (idx > 0 && toolsOS.equals("linux64")) fileName = fileName.substring(idx+7);
              else if (idx > 0) fileName = fileName.substring(idx+5);
              File newFile = new File(destinationPath
                                      + File.separator + fileName);

              if (extractDirectory)
              {
                   if (ze.isDirectory())
                   {
                       new File(fileName).mkdirs();
                   }
                   else
                   {
                         new File(newFile.getParent()).mkdirs();

                         FileOutputStream fos = new FileOutputStream(newFile); 
                         if (fileName.contains("lib"))
                         {
                             newFile.setExecutable(true);
                         }

                         int len;
                         while ((len = zis.read(buffer)) > 0) {
                             fos.write(buffer, 0, len);
                         }
                         fos.close();
                   }
              }            
              ze = zis.getNextEntry();
           }

           zis.closeEntry();
           zis.close();
           
           // create property file for versioning
           Properties prop = new Properties();
           OutputStream output = null;
           String versionFile = destinationPath + File.separator + "version.properties";
           try {
                    output = new FileOutputStream(versionFile);
                    // set the properties value
                    prop.setProperty("version", eu.ddmore.converter.lixoft.pharmml2mlxproject.PharmMLToMonolixProjectConverter.VERSION);
                    prop.store(output, null);

           } catch (IOException io) {
                    io.printStackTrace();
           } finally {
                       if (output != null) {
                         try {
                                 output.close();
                             } catch (IOException e) {
                                  e.printStackTrace();
                            }
                        }
           }   
         
       }catch(IOException ex){
          ex.printStackTrace(); 
       }
    }
    
    public static String getToolsPath()
    { return toolsPath; }
}
