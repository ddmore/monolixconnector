/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.ddmore.converter.lixoft.commons;

import eu.ddmore.convertertoolbox.api.response.ConversionDetail;
import eu.ddmore.convertertoolbox.api.response.ConversionReport;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author blaudez
 */
public class LixoftConversionReport implements ConversionReport{
    
    ConversionReport.ConversionCode _conversionCode = null;
    List<ConversionDetail> _conversionDetails = null;
    
    public LixoftConversionReport()
    {
        _conversionCode = ConversionReport.ConversionCode.SUCCESS;
        _conversionDetails = new LinkedList<ConversionDetail>();
    }

    @Override
    public ConversionCode getReturnCode() {
        return _conversionCode;
    }

    @Override
    public void setReturnCode(ConversionCode cc) {
        _conversionCode = cc;
    }

    @Override
    public List<ConversionDetail> getDetails(ConversionDetail.Severity svrt) {
        List<ConversionDetail> selectedBySeverityConvertionDetail;
        selectedBySeverityConvertionDetail = new LinkedList<ConversionDetail>();
        if (svrt != ConversionDetail.Severity.ALL)
        {
            for (int di = 0;di<_conversionDetails.size();di++)
            {
                if (_conversionDetails.get(di).getServerity() == svrt)
                    selectedBySeverityConvertionDetail.add(_conversionDetails.get(di));
            }
            return selectedBySeverityConvertionDetail;
        }
        return _conversionDetails;
    }

    @Override
    public void addDetail(ConversionDetail cd) {
        _conversionDetails.add(cd);
    }
    
    public void addBufferedDetail(BufferedReader bufferReader)
    {
        String s;
        try {
            while((s = bufferReader.readLine()) != null)
            {
                String patternInfo = "\\:info.*";
                String patternWarning = "\\:warning.*";
                String patternError = "\\:error.*";
                LixoftConversionDetail cd = new LixoftConversionDetail();               
                if (s.toLowerCase().matches(patternInfo))
                {                    
                    //s = s.replaceAll("\\[INFO\\]","");
                    cd.setSeverity(ConversionDetail.Severity.INFO);
                }
                else if (s.toLowerCase().matches(patternWarning))
                {
                    //s = s.replaceAll("\\[WARNING\\]","");
                    cd.setSeverity(ConversionDetail.Severity.WARNING);
                }
                else if (s.toLowerCase().matches(patternError))
                {
                    //s = s.replaceAll("\\[ERROR\\]","");
                    cd.setSeverity(ConversionDetail.Severity.ERROR);
                    _conversionCode = ConversionReport.ConversionCode.FAILURE;
                }
                cd.setMessage(s);
                this.addDetail(cd);
            }
        } catch (IOException ex) {
            Logger.getLogger(LixoftConversionReport.class.getName()).log(Level.WARNING,"Empty buffer" , ex);
        }
    }
    
    @Override
    public String toString()
    {
        String message;
        message = "LixoftConversionReport:Messages:\n";
        for (ConversionDetail cd : _conversionDetails)
        {
            message+="* " + cd.getMessage() + "\n";
        }
        return message;
    }
    
}
