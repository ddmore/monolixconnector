/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.ddmore.converter.lixoft.commons;
import eu.ddmore.convertertoolbox.api.response.ConversionDetail;
import java.util.Map;
import java.util.HashMap;

/**
 *
 * @author blaudez
 */
public class LixoftConversionDetail implements ConversionDetail{

    private Map<String,String> _info;
    private ConversionDetail.Severity _severity;
    private String _message;
    
    
    public LixoftConversionDetail()
    {
        _severity = ConversionDetail.Severity.INFO;
        _message = new String();
        _info = new HashMap<String,String>();
    }
    
    @Override
    public Severity getServerity() {
        return _severity;
    }

    @Override
    public void setSeverity(Severity svrt) {
        _severity = svrt;
    }

    @Override
    public Map<String, String> getInfo() {
        return _info;
    }

    @Override
    public void setInfo(Map<String, String> info) {
        _info = info;
    }

    @Override
    public void addInfo(String k, String v) {
        _info.put(k, v);
    }

    @Override
    public String getMessage() {
        return _message;
    }

    @Override
    public void setMessage(String message) {
        _message = message;
    }
    
}
