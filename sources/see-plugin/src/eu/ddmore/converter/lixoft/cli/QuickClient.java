/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.ddmore.converter.lixoft.cli;

import java.io.File;
import eu.ddmore.converter.lixoft.pharmml2mlxproject.PharmMLToMonolixProjectConverter;
import eu.ddmore.convertertoolbox.api.domain.LanguageVersion;
import eu.ddmore.convertertoolbox.api.domain.Version;
import eu.ddmore.convertertoolbox.api.response.ConversionDetail;
import eu.ddmore.convertertoolbox.api.response.ConversionReport;
import eu.ddmore.convertertoolbox.domain.LanguageVersionImpl;
import eu.ddmore.convertertoolbox.domain.VersionImpl;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author blaudez
 */
public final class QuickClient 
{
    public static void main(String args[])
    {
                
        PharmMLToMonolixProjectConverter myConverter = new PharmMLToMonolixProjectConverter();
        System.err.println("Lixoft Quick Client");
        System.err.println("*******************");
        System.err.println("Source :" + myConverter.getSource().getLanguage() + " " + myConverter.getSource().getVersion().toString());
        System.err.println("Target :" + myConverter.getTarget().getLanguage() + " " + myConverter.getTarget().getVersion().toString());
        System.err.println("Client arguments:");        
        for (int i = 0;i<args.length;i++)
        {
            System.err.println("\t* " + args[i]);
        }
        if (args.length == 6)
        {
            String pharmlFile = args[0];
            String mlxProjectOutDir = args[1];
            String sourceLanguage = args[2];
            String sourceVersion = args[3];
            String targetLanguage = args[4];
            String targetVersion = args[5];
            
            System.err.println("Test language");
            System.err.println("*************");
            System.err.print("\t Source:");
            if (sourceLanguage.equals(myConverter.getSource().getLanguage()))
            {
                System.err.println("Success");
            }
            else
            {
                System.err.println("Failed");
            }
            System.err.print("\t Target:");
            if (targetLanguage.equals(myConverter.getTarget().getLanguage())) 
            {
                System.err.println("Success");
            }
            else
            {
                System.err.println("Failed");
            }
            System.err.println("Test version");
            System.err.println("*************");
            Version l1v = new VersionImpl(4,3,2);
            LanguageVersion l1 = new LanguageVersionImpl("MonolixProject",l1v);
            
            Version l2v = new VersionImpl(0,3,1);
            LanguageVersion l2 = new LanguageVersionImpl("PharmML",l2v);
                                
            
            System.err.println("\tWith class source:"+l2.equals(myConverter.getSource()) + " # " +l2 + "#" + myConverter.getSource());
            System.err.println("\tWith class target:"+l1.equals(myConverter.getTarget()) + " # " +l1 + "#" + myConverter.getTarget());
            
            
            
            System.err.println("Test Run");
            System.err.println("********");
            File sourceFile = new File(pharmlFile);
            File destinationDir = new File(mlxProjectOutDir);
            try {
                ConversionReport myReport = myConverter.performConvert(sourceFile, destinationDir);
                System.err.println("Status:"+myReport.getReturnCode());
                List<ConversionDetail> details = myReport.getDetails(ConversionDetail.Severity.ALL);
                for (int di = 0;di<details.size();di++)
                {
                    ConversionDetail cd = details.get(di);
                    System.err.println("["+cd.getServerity()+"] "+ cd.getMessage());
                }
            } catch (IOException ex) {
                Logger.getLogger(QuickClient.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        else
        {
            System.err.println("Bad number of arguments");
        }
    }
}
